# Syntaxe

Le Web / une page web
Internet
techno-nihilisme
*data center*
Âmes-en-peine
*post mortem*

# Figures de style

- Anaphore (substantif féminin) : Une anaphore consiste à répéter un ou des mot(s) identique(s) au début ou à la fin de vers ou de phrase. Figure de l'insistance.