On croisera dans ce roman un ordinateur quantique, des champs de cannabis, la cathédrale d'Amiens, un authentique commerce triangulaire, des kibboutz dans les plaines de l'Aisne, une interview *live* de Mediapart menée par une intelligence artificielle, des morceaux des Doors et de Renaud, un chapitre sous le contrôle d'Amazon & Alphabet, un bon vieux PC sous GNU/Linux avec un vrai clavier, un réseau libre nommé unsecure!!, un consultant en vie après la mort, une crypto-monnaie locale, des thanatoprogrammeurs et deux fendoirs picards.

Ce futur proche ne diverge de notre monde que par quelques détails : la découverte d'une théorie matérialiste de la vie après la mort, l'éviction par son propre pays d'une région impopulaire, un restaurant théâtre de massacres quotidiens...



Face à la technique qui les possède, à laquelle ils ont livré leur vie, face aux entreprises-léviathans auxquelles ils ont vendu leur âme, littéralement, les personnages essaient de vivre de leur mieux, surtout préoccupés par l'empreinte qu'ils laisseront à leur mort, trace incertaine de leurs derniers instants.