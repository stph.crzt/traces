<?php
$comment = $_GET['comment'];
include "connect.php";
header("Access-Control-Allow-Origin: *");

/** Connexion **/
try {
  $connexion = new PDO("mysql:host=".$host.";dbname=".$dbname, $user, $mdp);
}
catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}
/** Préparation et exécution de la requête **/
$sql = "SELECT max(id) AS max FROM comment;";
$resultset = $connexion->prepare($sql);
$resultset->execute();
$max = 1;
/** Traitement du résultat **/
if ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
	$max = $row['max'] + 1;
}

/** Préparation et exécution de la requête **/
$sql = "INSERT INTO comment (id, comment) VALUES (?, ?);";
$resultset = $connexion->prepare($sql);
$result = $resultset->execute(array($max, "$comment"));

if ($result) {
  echo $max;
}
else {
  echo 0;
}

/** Déconnexion **/
$connexion=null;
?>
