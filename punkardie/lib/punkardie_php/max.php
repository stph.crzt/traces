<?php
include "connect.php";
header("Access-Control-Allow-Origin: *");

/** Connexion **/
try {
  $connexion = new PDO("mysql:host=".$host.";dbname=".$dbname, $user, $mdp);
}
catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}
/** Préparation et exécution de la requête **/
$sql = "SELECT max(id) AS max FROM comment;";
$resultset = $connexion->prepare($sql);
$resultset->execute();

/** Traitement du résultat **/
if ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
	echo $row['max'];
}

/** Déconnexion **/
$connexion=null;

?>
