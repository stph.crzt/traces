function getXmlFile (f) {
  var vXhr = new XMLHttpRequest();
  vXhr.open("GET", f, false);
  vXhr.overrideMimeType("application/xhtml+xml");
  vXhr.send(null);
  return  vXhr.responseXML;
}

function getTextFile (f) {
  var vXhr = new XMLHttpRequest();
  vXhr.open("GET", f, false);
  vXhr.send(null);
  return  vXhr.responseText;
}

function alertXml (dom) {
  var xmlString = new XMLSerializer().serializeToString(dom);
  alert(xmlString);
}

function changeDiv(id, max, done) {
  var fileNumber = getNumber(max,done);
  var fileName = id + "/" + fileNumber + ".xml"
  //Retrieve XML root node from file
  var root = getXmlFile(fileName).documentElement;
  var div = document.getElementById(id);
  div.textContent = null;
  div.appendChild(document.adoptNode(root));
}

function changeAudio(id, url, name, max, done, autoplay) {
  var div = document.getElementById(id);
  var fileNumber = getNumber(max,done);
  var fileName = url + "/" + name + "_" + zeroFormat(fileNumber,3) + ".mp3"
  //Retrieve XML root node from file
  var audio = document.createElement("audio");
  audio.setAttribute("src",fileName);
  audio.setAttribute("controls","");
  if (autoplay) { audio.setAttribute("autoplay","") };
  div.textContent = null;
  div.appendChild(document.adoptNode(audio));
}

function zeroFormat(number,digit) {
  /* add Zero digits before ,number if necessary */
  /* temp version works only if digit=3 */
  if (digit==3) {
    if (number<10) return "00" + number;
    if (number<100) return "0" + number;
    return number;
  }
  return number;
}

function dice(max) {
  return Math.floor(Math.random() * max + 1);
}

function contains(array, value) {
  i = 0;
  result = false;
  while (result==false && i<array.length) {
    result = array[i]==value;
    i++;
  }
  return result;
}

function getNumber(max, done) {
  if (done.length == max) {
    last = done.pop();
    done.length = 0;
    done.push(last);
  }
  var number = dice(max);
  while (contains(done,number)) {
    number++;
    if (number > max) number = 1;
  };
  done.push(number);
  return number;
}

function changeDivPhp(id, done, force) {
  /**
    id : id of the div which displays comment
    done : liste of already displayed comments
    force : display a specific comment if set (random if not)
  **/
  var max = getTextFile("https://crzt.fr/lib/punkardie_php/max.php");
  // Function is called with a specific comment to display (force is set and valid)
  if (force > 0 && force <= max) {
    number = force;
  }
  // Function is called for a random comment to display
  else {
    var number = getNumber(max,done);
  }
  var comment = getXmlFile("https://crzt.fr/lib/punkardie_php/comment.php?id=" + number).documentElement;
  var div = document.getElementById(id);
  div.textContent = null;
  div.appendChild(document.adoptNode(comment));
}

function showInsertComment(id) {
  var div = document.getElementById(id);
  var addLabel = document.getElementById('addCommentLabel');
  var add = document.getElementById('addComment');
  div.style.visibility = 'hidden';
  addLabel.style.visibility = 'hidden';
  add.style.visibility = 'visible';
}

function hideInsertComment(id) {
  var div = document.getElementById(id);
  var addLabel = document.getElementById('addCommentLabel');
  var add = document.getElementById('addComment');
  div.style.visibility = 'visible';
  addLabel.style.visibility = 'visible';
  add.style.visibility = 'hidden';
}

function insertComment(idNewComment, idComment, done) {
  var content = document.getElementById(idNewComment).value
  if (content) {
    content = content.replace(new RegExp('\r\n|\r|\n','g'), '</p><p>');
    content = "<div xmlns='http://www.w3.org/1999/xhtml'>" + "<p>" + content + "</p>" + "</div>";
    var result = getTextFile("https://crzt.fr/lib/punkardie_php/insert.php?comment=" + encodeURI(content));
    changeDivPhp(idComment, done, result);
  }
  hideInsertComment(idComment);
}
