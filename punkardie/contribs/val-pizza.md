# Pizza !

— Allô, Val ?
— Oui.
— T’es où ?
— Sur le chemin du retour, on arrive.
— Vous êtes là dans combien de temps ? je vous attends. J’ai faim.
— Je sais pas exactement, la Teslamazone a programmé un stop au Pizza-Up.
— Encore ? Mais il y a déjà dix-huit pizzas dans le frigo ! C'est débile.
— Je sais, mais on a pas encore eu de correctif. J'ai même l'impression que ça empire, non seulement elle surinterprète tout ce qu'elle entend, mais en plus ses fonctions d'annulation semblent HS. Et les gamins ont pigé le truc... 
À l'arrière de la voiture les enfants chantent en chœur « Pizza ! Pizza ! Pizza ! »
— Chut, les enfants, je n’entends pas ce que dit papa ! Tu n’as pas eu de problème de ton côté en rentrant du boulot ?
— Non, j’ai pris le Kangoobuntu aujourd’hui.
— T'es pas sérieux ! Si tu te fais à nouveau choper avec une voiture libre tu vas prendre cher.
— Je sais, mais j'avais déjà du mal à accepter la loi sur l'Autonomie quand les voitures proprios fonctionnaient à peu près correctement, alors là, franchement, je peux pas.
— Entre les pizzas et les contraventions c’est notre paye qui va y passer ce mois-ci.
— Ouais, on va y laisser un bras.
— Parle moins fort, si la voiture t'entend elle va te programmer une vente d'organe.
— C'est sensible à ce point ? Bon, on se parlera quand elle sera au garage.
— D'ac. Attends, tu peux me lancer une lessive ? j’ai rien à me mettre pour demain.
— Ça marche.
— Et merde !
— Quoi ?
— La Teslamazone vient de programmer un stop chez Rabillemoi… J'en peux plus.
— Détends-toi, demain c'est moi qui gère les mômes. Tu ne prononces pas un seul mot pendant ton trajet, tu n'appelles personnes, tu ne chantes pas, tout ira bien.
— Mais toi, tu me promets de ne pas prendre le Kangoobuntu ? Si tu te fais arrêter avec des enfants à bord d'une voiture libre, c'est la tôle direct.
— Tu m’apporteras des oranges ?
— Ou des pizzas…

 #commonsensefailure #alicebob #bonustrack #traces
 
*par Val et Steph*