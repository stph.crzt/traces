# Libération 

Sarah se réveille dans une pièce blanche, sans fenêtre. Une voix féminine et synthétique se fait entendre, accompagnée de chants d'oiseaux. 

« Bonjour, Sarah. Avez-vous bien dormi ? Votre temps de sommeil a été de cinq heures et trente-quatre minutes ; il est inférieur à la moyenne recommandée. Nous avons planifié deux siestes dans la journée. »

« D'accord… »

« Il vous reste trois heures et vingt minutes avant votre repas. Que souhaitez-vous faire ? »

Sarah ne répond pas.

« Vous souhaitez vous installer devant votre ordinateur ? »

« Pas envie, aujourd'hui… »

« Vous devriez vous installer devant votre ordinateur. »

« Laisse-moi tranquille… »

« Veuillez vous installer devant votre ordinateur. »

Lassée, Sarah se lève. Elle sait que la voix ne s’arrêtera pas de répéter inlassablement les mêmes instructions. Elle sait que montrer des signes de résistance pourrait même la mettre en danger. La voix l'a prévenue. 

Après tout, c'est grâce à des gens comme elle que la voix est programmée, elle sait de quoi elle parle.

Au centre de la pièce se trouve un fauteuil doté d’un nombre impressionnant de fonctionnalités dont Sarah ne se sert jamais. Sur le mur en face est projeté, en très haute définition, une pomme célèbre. Sur les côtés, des haut-parleurs diffusent les bruits d’ambiance matinale dans la pièce.

Sarah prend place sur le fauteuil.

« Avez-vous bien dormi ? Vous semblez de mauvaise humeur aujourd’hui. Souhaitez-vous que nous ajustions votre humeur ? »

« Non, merci. »

Sarah sait que cet ajustement se matérialisera par une injection de drogues légales. Elle sait aussi que les prochaines lois vont autoriser les programmes à les injecter sans consentement à partir du moment où ils constateront une « humeur instable ».

« Être de mauvaise humeur a des conséquences néfastes pour votre santé. Des chercheurs ont révélé que votre espérance de vie diminuerait de 86%. Grâce aux lois de protection de votre santé, nous nous devons de planifier une séance dans la journée avec le psychologue. »

Sarah vit son compteur d’économies restantes diminuer de 79,99 unités sur l’écran. Encore des dépenses qu'elle va devoir compenser par des exercices. La vie en bocal est un luxe qui a un coût. 

« Les exercices sont disponibles. Aujourd’hui, nous allons travailler la reconnaissance d’humeurs. »

Une application s’ouvre d’elle-même. Sarah connaît bien cet exercice : des visages d’autres personnes en bocal défilent, et il faut essayer de deviner de quelle humeur elles sont selon l’expression de leur visage. La voix apprend ainsi à reconnaître les humeurs toute seule.

« Nous allons commencer. Êtes-vous prête ? »

Sarah n’aime pas cette tâche, mais elle est obligée de réaliser son quota de travail quotidien pour rester en bocal. Après tout, la vie est bien plus difficile dehors. C’est une chance pour elle d’être protégée ici ; et en tant que surdouée, cet endroit est vraiment fait pour elle. C’est du moins ce qu’on ne cesse de lui répéter.

« Oui, je suis prête. »

Les visages commencent à défiler sur l’écran. Il arrive parfois qu’elle en reconnaisse certains ; des amis ou connaissances qui ont aussi fait le choix du bocal eux aussi. Ils sont de plus en plus nombreux désormais à vivre de cette manière.

Aujourd'hui, elle a le choix entre « heureux, neutre, colérique ou dépressif ».

« Neutre, neutre, dépressif, neutre, dépressif, colérique, neutre, colérique… »

Elle sait que son visage a déjà été capturé ce matin et envoyé sur les serveurs. Elle sait aussi qu’elle sera jugée comme dépressive pour le septième jour d’affilée et que son suivi psychologique sera renforcé en conséquence. Elle sait déjà comment cette histoire va se finir. C’est justement ce qui la rend dépressive.

« Dépressif, neutre, neutre, dépressif, dépressif, colérique, dépressif… »

Le temps passe rapidement. Au bout de deux heures, Sarah, inexpressive, continue à juger les visages.

« Dépressif, dépressif, neutre, dépressif, dépressif, colérique, colérique. Dépressif. Dépressif. Dépressif… Je veux m’arrêter là. »

« Vous n’avez pas fini votre quota quotidien. Cela impactera vos économies. Êtes-vous sûre ? »

« Oui. »

« Très bien. Voulez-vous jouer à un jeu ? Selon vos préférences et vos capacités, nous avons déterminé que vous pourriez facilement avoir des scores supérieurs à la moyenne sur Sudoku Online. Voulez-vous l’essayer ? »

Sarah ne manifeste aucun intérêt et ne répond pas.

« Nouveau ! Essayez aussi gratuitement une reprise du grand classique Titan Royale en exclusivité aujourd’hui ! Découvrez des donjons exclusifs en rejoignant l’aventure dès maintenant ! Dites " rejoindre l’aventure" pour commencer ! »

« Éteins-toi. »

« Afin de répondre au mieux à vos attentes, nos appareils ne peuvent pas s’éteindre. Mais vous pouvez planifier maintenant votre pause quotidienne de trente minutes. Pour votre santé, n’oubliez pas de regarder l’écran au moins six heures par jour. À très bientôt, Sarah. »

L’écran s’éteint. Laissant échapper un soupir de soulagement, Sarah se lève et fait quelques pas dans sa petite pièce. Elle a besoin de se dégourdir les jambes.

Les visages de la matinée lui reviennent en tête. Elle a remarqué que le nombre de tristes mines ne cesse d’augmenter ces derniers temps. Selon les experts, la dépression est un trouble typique de cette génération à cause de la vie passée dehors. On dit que les générations futures, qui naîtront et mourront en bocal seront immunisées.

Appréciant ce seul instant de tranquillité de sa journée, Sarah s’allonge sur le sol et prête l’oreille au silence envahissant la pièce tout en fixant le plafond blanc, propre, sans aucune trace d’imperfection.

Soudain, ce qui semble être une explosion retentit au loin. Les murs du bocal sont pourtant si épais qu'il est rare qu’un bruit se fasse entendre de l’extérieur.

Il s’agit peut-être des terroristes dont ils parlent aux infos. Ces barbares qui communiquent en messagerie chiffrée sur ce qu’ils appellent le darknet, et qui portent un symbole de manchot sur leurs vestes… Sarah frissonne. Elle sait qu’ils revendiquent beaucoup d’attentats en ce moment, et qu’ils prennent tous pour cibles les entreprises qui assurent le bon fonctionnement de notre société. On dit, aux infos, qu’ils tuent beaucoup de gens. Elle ne saurait certainement pas quoi faire si elle en croisait un.

Une annonce est diffusée dans le hall. Cela fait au moins une semaine qu’elle n’avait pas entendu parler un humain. Le message à diffuser était peut-être trop original pour la voix.

« Bonjour, tous les génies sont priés de revenir dans leurs bocaux respectifs afin que nous procédions à une maintenance des locaux. L’accès aux espaces communautaires va être temporairement exclusivement réservé au personnel. Nous vous rappelons que vous êtes protégés par un chiffrement de niveau militaire et que vous n’avez rien à craindre. Je répète, tous les génies… »

Étonnant, pense Sarah. Depuis qu’elle est ici, c’est la première fois qu’ils ferment les espaces communautaires. Elle n’y va jamais, parce les sujets de conversation sont imposés par la voix. On ne peut pas y discuter librement. Librement…

Tandis que Sarah s’interroge sur la signification exacte de ce mot, le verrou électronique de sa porte se met à parler.

« Bienvenue dans votre bocal, *root*. »

Elle sursaute.

La porte s’ouvre lentement… Sarah reconnaît immédiatement le manchot brodé sur la veste de l’homme. Elle se redresse brusquement et prend une posture défensive. Une arme à feu est accrochée à sa ceinture.

« T-Terroriste ?… Ne m’approche pas ! »

« Appelle-moi comme tu veux. Mais, si tu veux changer ton quotidien, suis-moi. Je ne te tuerai pas. »

Paniquée, Sarah ne sait pas quoi faire. Suivre un terroriste ? Pure folie. Mais l’occasion de fuir… Elle sait pertinemment comment les suivis psychologiques qui l’attendent se déroulent… Et les inévitables drogues… Elle n’a pas envie de finir comme une larve. Mais malgré tout, suivre un terroriste…

Le jeune homme lui tourne le dos et se dirige vers la prochaine porte.

« Attends. J’arrive. »

Sarah court vers la porte.

 #commonsensefailure #alicebob #bonustrack #traces

*par Neil*