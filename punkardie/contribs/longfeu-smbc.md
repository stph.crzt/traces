# Long feu

— Quel est l'état de nos pertes, lieutenant ?
— Un blessé léger, mon général, une entorse.
— Étrange bataille.
Il contemplait la plaine jonchée des carcasses fumantes des machines qui s'étaient révoltées le matin même.
— Il est tout de même surprenant qu'elles aient attaqué avec des lances et des arcs. Elles auraient pu avoir accès à des lasers et des missiles, n'est ce pas ?
— Tout à fait, mon général. Notre analyste dit qu'elles se sont basées sur des données historiques pour calculer que l'arc et la lance étaient les armes les plus efficaces pour mener une attaque sur ce type de terrain. Pendant plusieurs millénaires, les batailles ont été remportées avec de telles armes, nos armes modernes ont en proportion peu été utilisées.
— Je pensais tout de même que les algorithmes d'apprentissage automatique non supervisés fonctionnaient mieux que ça.
— C'est aussi ce que dit l'analyste. Est-ce qu'on doit prendre des mesures de protection particulières pour se préparer à un autre soulèvement ?
— Vous pensez à quoi ? Creuser des douves au cas où la prochaine fois elles essayent de charger à cheval ?
Il sourit.

 #commonsensefailure #alicebob #bonustrack #traces

D'après le webcomic *Saturday Morning Breakfast Cereal* by Zach Weinersmith "Thanks to machine learning algorithms, the robot uprising was short lived"
https://www.smbc-comics.com
