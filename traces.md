<!--
titre : Vivre et mourir au temps des IA
-->

# Prélude

(2016)


## Le questionnaire

— Bonjour Sara.

— Bonjour monsieur.

— Entrez, asseyez-vous. Mettez-vous à l’aise, je vous en prie.

— Merci de me recevoir.

— Merci de vous être déplacée. Voyez-vous, votre profil a vraiment retenu mon attention. Vous avez été formée ici même, au bon air marin. Déjà, c’est un gage de qualité, n’est-ce pas ? Trois années d’expérience assez solides après cela, et vous avez voyagé. Un peu partout en France, et puis l’Asie et l’Amérique du Sud. C’est vraiment bien ça, les voyages forment la jeunesse ! Célibataire, sans enfant. Parfait. Et deuxième dan de karaté. Ce n’est pas si fréquent à votre âge, n’est-ce pas ?

— J’ai commencé jeune, à sept ans. J’avais de bonnes dispositions, souplesse, explosivité, spontanéité. J’ai fait un peu de compétition, mais surtout c’est une hygiène de vie.

— Vous pratiquez encore n’est-ce pas ?

— Oui, bien sûr. Mais vous cherchez un videur en fait ?

Sara était vraiment spontanée. Elle savait que pour un entretien d’embauche il était conseillé de garder une certaine réserve, mais dès qu’elle sentait une ouverture, elle ne pouvait s’empêcher de s’engouffrer.

— De la répartie, parfait. Pas trop tout de même, attention, le client est roi ! Vous avez pratiqué aussi les armes blanches, peut-être ? kendo ? aïkido ?

— Un peu. Le nunchaku.

— Vous n’êtes pas banale. Nous cherchons quelqu’un de pas banal. De votre côté qu’est ce qui vous conduit à postuler chez nous ?

— Votre soupe de poisson ! C’était aussi la spécialité de ma mère, on peut dire que je suis tombée dedans quand j’étais petite.

Elle sourit.

— Vous n’êtes vraiment pas banale. Voyez-vous ce qui a particulièrement retenu mon attention, ce sont les résultats que vous avez obtenus au cours du test auquel vous avez eu la gentillesse de bien vouloir vous soumettre lors de votre précédent entretien.

— C’était en effet un test un peu étonnant, parfois. Le questionnaire était bizarre quand même, non ? Et encore, vous n’avez pas eu mon prof de math en troisième. Je veux dire, en termes de questions étranges, il s’y connaissait. Je veux dire, les questions, certaines étaient un peu spéciales. C’est rare pour un emploi de serveuse de passer plusieurs entretiens, avec des tests approfondis comme ça. Enfin, d’habitude, c’est plus direct, on fait surtout un essai. Après, moi, ça ne me gêne pas, j’ai rien à cacher, je suis comme je suis !

Sara était sincère. Elle était comme elle était, elle trouvait trop fatigant de paraître autre chose. Et puis, ce qu’elle était lui plaisait. Il y avait tellement de gens mal dans leur peau, le mal de ce début de siècle. Mais elle, ça allait, bien. Et ça se voyait. Joseph Herman le voyait. Et cela lui plaisait.

— Je sais que ce n’est pas tout à fait habituel. Mais voyez-vous, je cherche ce qu’il y a de mieux pour mon restaurant. Et il y a certains aspects du travail qui ne sont pas tout à fait habituels. Mais les résultats de votre test montrent, entre autres qualités, que vous appréciez ce qui est original, et que vous aimez être surprise, n’est-ce pas ?

— Oui, plutôt.

— Et que l’on peut vous faire confiance également.

— Je crois.

— Il faudrait être sûre. Est-ce que je pourrai vraiment vous faire confiance, madame Elfaoui ?

— Oui.

— J’ai encore deux autres tests à vous faire passer, si vous êtes d’accord. Le premier est théorique, une sorte de jeu de rôle, il ressemble un peu à celui que vous avez déjà passé, enfin il vous surprendra peut-être encore plus, vous verrez. Si vous réussissez celui-ci, le second sera plus… pratique.

— Oui, oui, je suis toujours intéressée. Allez, testez-moi !

Sara avait son air enjoué, celui qu’elle avait presque tout le temps. Un inébranlable sourire trônait sur son visage. À vingt-quatre ans elle semblait encore une enfant. Ses traits fins ne trahissaient pas encore la légère inquiétude qui était la sienne devant le peu ordinaire recrutement auquel elle participait. Surtout, peut-être devant l’étrange monsieur Herman, du restaurant Herman, des soupes Herman, un notable local, presque régional. Sara voulait le job, elle aimait bien ce coin, malgré le climat, elle s’était attachée à l’air de la mer, aux oiseaux. Et puis sa famille était proche, elle avait besoin de poser un peu ses valises, de passer du temps avec sa petite sœur, et avec son père aussi.

— Bien, Sara. Allons-y. Avec votre accord je vais vous filmer. À usage strictement interne, je vous rassure. Tenez, voici un engagement de non divulgation de notre part. Notez bien, aucune copie, et destruction de nos archives dans la semaine, que vous soyez embauchée ou non. Voilà, lisez bien. De votre côté, il faut nous signer cet accord de confidentialité. Vous n’aurez pas le droit de parler de ce nouveau test, à personne. Un peu comme une recette secrète, pour qu’elle garde sa saveur, il faut parfois un peu de secret, vous comprenez ?

Sara hocha la tête en prenant les deux feuilles que lui tendait Joseph Herman, déjà préalablement signées de sa main. Sur le bureau, un stylo Mont Blanc l’attendait visiblement, elle n’osait pas le prendre, alors elle sortit celui qu’elle avait apporté. En dehors du stylo, il y avait sur le bureau – une sorte de table en acier peint – un petit ordinateur portable et une sorte d’enceinte. Celle-ci était haute d’une quinzaine de centimètres, totalement noire en dehors du logo d’Amazon qu’elle exhibait sur sa base. Ça avait un nom de fille, elle ne se souvenait plus lequel. Elle n’en avait jamais utilisé elle-même, mais on commençait à en parler et elle en avait même déjà vu une lors de son dernier passage aux États-Unis. Elle avait trouvé cela un peu étrange, les gens lui parlaient comme à un être humain. Il y avait aussi un trépied et une grosse caméra, comme celles qui servaient à tourner les films de cinéma. Cela lui parut incongru alors que l’on faisait de très belles vidéos avec un simple téléphone. Il n’y avait aucun fil, ni pour alimenter les appareils, ni pour les relier entre eux. Tout était épuré, presque froid. Joseph Herman se mit à régler la caméra, dans sa direction. Quand il eut fini, il s’installa derrière l’écran de l’ordinateur et il tapa sur le clavier. Sara ne pouvait pas voir l’écran d’où elle était. Joseph Herman semblait à présent attendre un signal. Sara ne bougeait pas.

— Voilà, c’est prêt. Sara, voici la première question, vous êtes prête également ? Je vous la lis, c’est géré par l’ordinateur : seriez-vous prête à sauver d’une mort certaine un enfant que vous ne connaissez pas pour une somme de cinquante mille euros, en faisant un emprunt à la banque sur vingt ans par exemple ?

Comme Sara Elfaoui restait silencieuse, Joseph Herman lui demanda s’il devait relire la question.

— Non. Merci. Mais… Enfin, vous voulez dire, comme un enfant qui meurt de faim, en Afrique ?

— Si vous voulez, par exemple. Ou un kidnapping. Je suppose, je me contente de lire les questions et d’enregistrer vos réponses dans l’ordinateur.

— Je ne le connais pas, cet enfant ? C’est une question bizarre.

— C’est une question simple, Sara, le banquier vous a accordé le prêt, vous avez un chèque de cinquante mille euros, disons une valise de billets plutôt, vous pouvez la remettre à des ravisseurs, en échange ils libèrent l’enfant. Sinon ils le tuent. Mais il faudra rembourser le prêt, Sara, si vous payez.

— Il n’a pas de parent ?

— Je n’en sais rien. Vous n’en savez rien. C’est vous que les ravisseurs ont contactée, c’est à vous de prendre la décision. Répondez Sara, ce n’est qu’un simple test, prenez-le comme un jeu !

— Je suppose que j’accepterais, si j’avais déjà le chèque. Ou la valise.

— Vous rembourserez donc, tous les mois, peut-être cinq cents euros avec les intérêts, pendant vingt ans.

— J’essaierai.

— Bon, je saisis la réponse oui, donc. Seconde question, elle ressemble un peu, je vous préviens : seriez-vous prête à sauver d’une mort certaine un adulte que vous ne connaissez pas, pour une somme de cinquante mille euros toujours. Il ne s’agit plus d’un enfant, mais d’un adulte, un homme de quarante ans disons, que vous ne connaissez pas, un étranger, un Espagnol. Vous ne connaissez pas d’Espagnol ?

— Non.

— Alors, est-ce que vous payez dans ce cas ?

— Je ne sais pas.

— Vous le laissez mourir alors ? Tant pis ?

— C’est bizarre, vraiment.

— Non, c’est une question simple Sara. Alors ?

— Je ne sais pas.

— Plutôt non, alors ?

— Je ne crois pas, non.

— Je mets non. Même question, de nouveau, je ne la relis pas en entier, mais les ravisseurs ne réclament plus que vingt-cinq mille euros, c’est plus raisonnable déjà, c’est une voiture, et vous devez imaginer que des amis sont d’accord pour vous prêter la somme.

— Mais, si je ne le connais pas…

— Non toujours, donc ? Vous pouvez indiquer à partir de quelle somme vous trouveriez cela raisonnable. Une idée ?

— Je ne sais pas.

— Vous souhaitez interrompre le test, Sara, tant pis ? Pourtant ce sont de simples questions sans conséquence, vous comprenez, c’est seulement pour nous aider à établir un profil.

— Il y a encore beaucoup de questions ?

— Oui, je crois que c’est assez long, mais vous savez, je ne fais que lire les questions, c’est l’ordinateur qui décide, alors, j’imagine que cela dépend. De vos réponses.

— Je vais essayer de répondre. Disons, dix mille euros ?

— D’accord, je l’entre dans la machine. Comment fait-on des chiffres avec ce clavier ? Ah, voilà. Ce n’est pas beaucoup dix mille euros pour la vie d’un homme. Nouvelle question : accepteriez-vous d’avoir une relation sexuelle avec un inconnu pour sauver la vie d’un homme, également inconnu, disons le même homme toujours, il est coriace finalement ! Évidemment, pas forcément quelqu’un avec qui vous auriez envie *a priori*. Pour la relation sexuelle.

— C’est écrit ?

— Non j’extrapole un peu, mais cela paraît évident, vu le sens de la question.

— Comme une pute quoi ? Une prostituée.

— J’imagine que l’on peut faire le parallèle, mais la cause est noble puisqu’il s’agit de sauver une vie.

— Je ne pense pas que j’en serais capable.

— Non. En même temps pour dix mille euros, en effet, je n’accepterais pas non plus ! Ah, plus intéressant à présent, est-ce que vous accepteriez de vous prostituer, disons-le comme ceci, c’est vrai que c’est plus simple, pour sauver la vie de l’enfant de la première question ?

— Monsieur, est-ce que vous cherchez une entraîneuse ? une escort-girl pour certains de vos clients ?

— Non, pas du tout. Mais quel serait votre prix dans ce cas ? Vous n’êtes pas obligée de me répondre, cela ne fait pas partie du questionnaire.

— Non, dans ce cas.

— Non, vous ne sauvez pas l’enfant ?

— Non, je veux dire que je préfère ne pas vous répondre pour le prix. Je veux dire, je n’ai jamais pensé à cela. Et pour l’enfant, je crois que ce sera non également.

— Votre intégrité vaut plus que cinquante mille euros, j’en déduis ? Et plus que la vie d’un enfant. Question suivante : seriez-vous prête à tuer un inconnu, disons celui à dix mille euros toujours, pour sauver la vie d’un enfant, celui à cinquante mille, donc.

— Comment ?

— Vous avez fait des arts martiaux, disons avec un katana par exemple. Comme dans Les Sept Samouraïs ! Mais vous faites comme vous voulez.

— Ce n’était pas ce que je voulais dire. Je veux dire, la question est vraiment… je ne pourrais jamais tuer quelqu’un.

— Pour sauver un enfant Sara ? Non ?

— Si. J’imagine que si.

— Ah ! je note donc oui, j’ajoute que vous le feriez avec un katana ? Ce n’est pas obligé, mais je crois que cela peut affiner le test.

— Je crois que le moyen ne changerait pas vraiment le problème, dans ce cas de figure.

— C’est comme vous voulez. Accepteriez-vous de tuer un homme, un inconnu toujours, pour la somme de cinquante mille euros ?

— Non !

— Pourtant vous refuseriez de payer cette somme pour le sauver, Sara, est-ce bien cohérent ?

— Ce n’est pas la même chose.

— Vous refuseriez de vous prostituer pour le sauver.

— Je ne vois pas le rapport.

— Et vous pourriez le tuer pour sauver un enfant. Pour avoir cinquante mille euros, pour sauver un enfant, Sara, c’est la même chose.

— Je ne sais pas. Si c’est pour m’acheter une belle voiture, par exemple, ou des vêtements de luxe, ou une montre, ce n’est pas la même chose que si c’est pour sauver un enfant.

— Je vous propose une autre formule dans ce cas. Accepteriez-vous de tuer un homme pour la somme de cinquante mille euros, étant donné que vous avez prévu d’utiliser cet argent pour de bonnes causes, comme sauver la vie d’un enfant ?

— Peut-être, dans ce cas.

— Financer les études de votre petite sœur. Vous avez une sœur ?

— Oui.

— Oui, vous avez une sœur ?

— Oui, et disons que j’accepterais peut-être dans ce cas.

— Peut-être Sara, vous avez du mal à prendre des décisions, c’est la vie d’un homme qui est en jeu maintenant.

Le visage de Sara avait changé depuis le début du test, il s’était finalement refermé. Les questions se suivaient, et les réponses de Sara étaient plus immédiates à présent. Elle s’était rangée à la logique du questionnaire qui lui était soumis, elle avait accepté le jeu. Comme un jeu un peu étrange. Mais elle avait le sentiment qu’elle perdait.

<!--SAUT1-->

Ils étaient deux, assis dans la salle attenante que venait de rejoindre Joseph Herman. Ils avaient l’air maussade. Le visage de Sara, était projeté sur un pan de mur complet. Joseph Herman adressa un bref geste de la tête à l’un des deux hommes, qui se tenait sur une chaise pivotante face à trois écrans larges, légèrement incurvés. Celui-ci s’affaira sur un clavier tactile une dizaine de minutes, en silence. Puis il leva la tête et attendit. Des lignes défilaient sur les écrans. Personne ne parlait. Le second homme avait allumé sa seconde cigarette.

— Alors qu’est-ce que ça donne ? demanda Joseph Herman, avec une nervosité palpable.

— C’est pas bon Jo, c’est pas bon.

— Pas bon, comment ?

— Elle manifeste un ancrage de préceptes moraux trop fort, son taux de résistance à la manipulation est assez élevé aussi. Intérêt matériel moyen moins. Des blocages en pagaille, tu veux la liste ?

— Non, vas-y balance, il conclut quoi ton machin ?

— Vingt-trois pour cent.

— Merde, sûr ? Elle me plaît bien à moi. Et pis tu sais qu’on en a besoin. On peut pas essayer quand même ?

— On ne va pas réussir à la débloquer de façon fiable, même sous hypnose. C’est pas raisonnable Jo, elle va nous claquer dans les doigts.

— Tu fais chier avec ton ordinateur de brin. Ses expressions picardes revenaient quand il était énervé. Bon. On l’invite à dîner alors. Ce soir ?

— Même pas, à mon avis, elle ne viendra pas. On règle ça maintenant, j’ai appelé Victor.

— Putain, je me demande comment faisaient mon père, et mon grand-père avant lui. Ils avaient pas d’ordinateurs, pas de psy, pas d’hypnose. C’était à l’arrache, mais ça marchait.

— C’était pas les mêmes méthodes de recrutement Jo, ils raclaient les bas-fonds, ils ramassaient tout ce que le coin comptait de psychopathes et de tordus. Il y avait plus de tueurs et de violeurs en salle que dans un régiment de la légion. Sans compter les mecs qui sortaient vraiment de la légion. Et puis ils compilaient les questionnaires à la main, c’était sacrément moins fiable. Ton arrière grand-père avant eux, le Maurice, ils les avaient même pas encore pondus ses questionnaires, c’était au jugé. Il avait un bon pif, mais il y avait de la perte.

— On a aussi de la perte ! Deux en deux mois, et je te passe le départ à l’amiable de Frédo. Je sais pas comment je vais tenir, je peux pas être au four et au moulin.

— Va peut-être falloir fermer un peu Jo, une quinzaine, un mois, prendre des vacances, le temps de trouver une solution. Calmement. On en profite pour arranger une visite de l’hygiène, tu seras tranquille pour deux ou trois ans avec ça.

— Ouais, Jo, et on a qu’à aller faire un bout de brousse. À l’étranger tu sais. Un petit périple, genre une semaine au Liban, ou même au Vietnam, je crois qu’ils parlent pas mal français dans certains coins. Ou un saut en Afrique, Mali, Centrafrique, ils sont francophones. On trouvera sûrement notre bonheur. Recrutement à l’ancienne. Souviens-toi de Cheik, une perle ce gars.

— Merde j’ai jamais fermé, ça n’a jamais fermé.

— C’est pas vrai Jo, tu le sais bien, c’est pour la légende ça, pour frimer à Noël, mais ça a déjà fermé. Ton père a fermé. Ton grand-père a fermé. Allez, profites-en pour aller voir ton fils. Tu disais que tu voulais aller aux *States*. La Californie, un peu de soleil, des palmiers, des nibards à deux doigts de tes dix doigts, ça te fera du bien. Et puis, c’est une tronche ton fils, tu m’as dit qu’il avait eu une sorte de médaille et qu’il bossait sur le questionnaire justement. Il pourra peut-être t’aider.

<!--SAUT1-->

Le regard de Sara vagabondait dans la salle, tandis qu’elle attendait le retour de Joseph Herman. C’était une salle impersonnelle, sans décoration, sans meuble autre que le bureau et les deux chaises faites du même matériau, noir. La pièce était décidément froide, elle ressemblait à une salle d’interrogatoire de commissariat de séries américaines. Elle remarqua que le sol était tapissé d’un film plastique, une sorte de bâche soigneusement disposée, qui remontait de quelques centimètres le long des murs, tout aussi soigneusement collée sur le haut des plinthes. Elle chercha avec un demi-sourire une fenêtre sans tain. Mais les murs imperturbablement lisses et blancs ne trahissaient aucune imperfection. C’était une idée à la fois inquiétante et amusante, mais ridicule. La caméra trônait, à quelques dizaines de centimètres d’elle.

Sara n’osait pas bouger, elle n’osait pas non plus rester immobile. Elle ne savait comment attendre. Elle avait été sensiblement dérangée par l’entretien qu’elle venait d’avoir, elle espérait maintenant avoir une réponse négative. Elle ne voulait pas savoir ce qu’était le test pratique. Elle avait hâte de quitter la pièce, de prendre congé de Joseph Herman, elle trouvait l’une et l’autre malsains. Elle leur dirait à l’agence que, tout de même, ce genre de questions, on avait beau être serveuse, c’était humiliant. Peut-être même était-ce illégal. Elle avait signé l’accord de confidentialité, d’un autre côté. Elle rentrerait peut-être juste chez elle, ou elle irait voir sa petite sœur.

— Madame Elfaoui, dit doucement Joseph Herman, je suis désolé, mais nous n’allons pas pouvoir vous embaucher finalement, votre profil ne correspond pas. Vraiment, je suis désolé.

Absorbée par ses pensées, elle n’avait pas entendu la porte s’ouvrir, elle n’avait pas remarqué que Joseph Herman était entré dans la salle avant qu’il ne lui adressât la parole. Elle ne vit pas non plus l’homme qui l’accompagnait, un homme habillé de vêtements de service, pantalon et veste noirs, chemise blanche, nœud papillon, avec dans la main gauche un fendoir picard.


## Une idée

— Putain, Manuel, j’ai écouté France Inter ce matin, c’est quoi ce bordel ?

— Bonjour Xavier.

— Je t’ai laissé cinq messages, dis-moi que tu as résolu le problème.

— Quelqu’un aura fait une erreur, Xavier. Mais c’est passé aux news maintenant, ça va être dur de revenir en arrière.

— Tu déconnes ou quoi ? On a fait une consultation populaire, on a fait une réunion. Tout le monde était là. On a choisi *Le*-Haut-de-France. Au singulier.

— C’est presque pareil. C’est une lettre.

— *Les* Hauts-de-France ? Tu trouves que c’est pareil ? Ça te fait pas penser à un pays de montagne, toi ? Le point culminant de la région, c’est un terril à deux cent cinquante mètres...

— Non, ça ne me fait pas spécialement penser aux montages. Les Hauts-de-Seine c'est pas réputé pour ses stations de ski. Je crois que tu te focalises sur un détail.

— Un détail ? Quand t’appelles ton fils Benjamin, tu l’appelles pas Benyamin, si ? Ça ressemble, tu vois, mais ça fait pas pareil !

— Désolé, sûrement une secrétaire. Un assistant. Tu sais. N’insiste pas, Xavier.

— Non, je ne sais pas. Si, j’insiste. Tu fais un erratum, maintenant, appelle l’AFP, fais une conf de presse.

— Non, écoute-moi un peu. Ça n’arrivera pas.

— Je quitte pas ce bureau sans…

— Écoute-moi, Xavier, écoute-moi bien maintenant. C’est comme ça. Fin de l’histoire.

— Je suis pas d’accord, on a voté !

— Revote si c’est ça le problème.

— Non, le problème c’est que le nom de notre région n’est pas le bon, on ne peut pas s’appeler les Hauts-de-France quand on est un pays de plaines.

— Tu m’emmerdes. Tu ne peux pas non plus t’appeler Le-Haut-de-France quand t’es un pays de débiles. Tu comprends ? Tu comprends, maintenant ? Le-Haut-de-France c’est dans tes rêves. Le-Bas-de-France est pas d’accord, t’as compris ? Alors tu me fais plus chier, tu retournes dans ta campagne pourrie, t’appelles tes consanguins comme tu veux, mais ici, c’est Hauts-de-France. Pigé ? T’avais qu’à prendre Le Nord, je t’avais dit de prendre Le Nord, tout le monde trouvait ça bien, tout le monde s’en foutait. Mais toi et ta Picardie de merde. Tu crois que vu de Paris on fait la différence entre la Picardie et le Nord ? Vu de Marseille ? Pas la même langue ? Quelle langue ? Celle que les frères fourrent dans le cul de leurs sœurs ? Allez, retourne moisir dans ta bouse maintenant. Tu comprends ? Tu seras jamais Le-Haut-de-France. T’as un gamin idiot, tu vas pas l’appeler Einstein, pas vrai ? S’il est difforme, tu vas pas l’appeler Charmant. S’il est aveugle, tu vas pas l’appeler Œil de Lynx ? S’il est tout chétif, tu l’appelles Hercule ? Je continue ? Non ? Ben nous, c’est pareil, on va pas appeler une région de débiles bons à rien Le-Haut-de-France. Voilà. Il a compris, maintenant ? Alors il se casse, il va se calmer dans sa voiture, et il revient la semaine prochaine me présenter ses excuses et m’expliquer son programme de com’, les Hauts-de-France avec vous, les Hauts-de-France sont contents, ou les Hauts-de-France font du ski, je m’en fous.

<!--SAUT2-->

La grosse berline évoluait tranquillement sur le périphérique parisien, respectueusement, dégageant comme des ondes de sérénité au milieu du trafic. Par contraste, le président de la région Picardie – pressenti pour diriger la future région Hauts-De-France – fulminait dans l’habitacle. Il éructait son humiliation et sa colère avec une spontanéité, presque une sincérité, qui ne lui ressemblaient pas. Connards de Parisiens.

Bien sûr, ce n’était pas si important, dans trois mois tout le monde se serait habitué, tout le monde s’en foutrait… mais ce que ça disait de leur condescendance. Ce que ça disait de notre complexe d’infériorité. Faudrait leur déclarer la guerre, les bombarder, les latter comme des chiens, les égorger dans leur lit, violer leurs femmes, bouffer leurs mômes. Des envies de génocide…

— Xavier ?

La voiture avait rejoint l'autoroute à présent. On distinguait sur la gauche les bidonvilles qui s’étaient implantés là il y a quelques années déjà. Des Roms, des Syriens, va savoir. Ils entachaient la superbe de la capitale par leur unique présence. Bien que confinés aux portes de la cité, ils en avilissaient le cœur. Le président ressentait presque de la compassion, presque de la solidarité.

— Xavier ?

— Sers-moi un truc à boire, et dis à l’autre de rouler plus vite, je veux quitter cette ville de merde.

Le conseiller demanda au chauffeur d’accélérer, ce qu’il fit, modérément.

— Un whisky ? Il est tôt…

— M’en fous. Coupe-le. Et tu peux arrêter de conduire comme une savate, toi, tu vas me faire gerber. Il est en réinsertion celui-là ou quoi ? Tiens, appelle-moi une pouf, je crois que j’ai besoin de me détendre un peu le gland. La brune.

— On est attendus, tu n’as pas le temps.

— Toi aussi t’as décidé de me faire chier ? Annule. Je prends ma journée.

— Conférence de presse à Saint-Valéry à midi, au restaurant Herman. Tu te souviens de Joseph Herman ? Un donateur important, influent.

— Il a été étoilé ?

— Étoilé ? Non. Mais son fils a reçu une sorte de médaille…

— Fait du sport ?

— Bois ton whisky, laisse-moi parler. Son fils, Franck Herman, il est chercheur à Berkeley…

— C’est où ça ?

— C’est une prestigieuse université de la côte ouest américaine, en Californie. De gauche.

— Ça existe les universités de droite ?

— Aux US, plutôt oui. Bref, il a eu un prix pour sa thèse de doctorat, présentée le mois dernier. C’est exceptionnel pour un étranger, la première fois pour un Picard.

— Une thèse de cuisine ? Une étoile au Michelin ça aurait été plus vendeur, la recherche ça emmerde tout le monde.

— Ses travaux mélangent informatique et psychologie. Il a présenté un questionnaire qui permet de mettre en évidence certains profils. *A priori* il y aurait des applications en économie, sur la détection des managers ou des traders à risque.

— À risque ?

— Qui n’auraient pas assez de barrière morale, qui seraient trop débridés. C’est assez appliqué, Google est intéressé, ils parlent d’entraîner des IA pour faire de la criminologie préventive. Il y a un article où ils proposent d’ajouter des épreuves basées sur le questionnaire pour les permis de conduire ou de port d’armes. Même pour les dons de sperme.

— On vit dans un drôle de monde. Bon, on bouffera quoi ?

— La spécialité, c’est la soupe de poisson.

— Yallah ! C’est ma journée. Je peux pas saquer la soupe de poisson. Tu le sais, non ? Commande-moi des huîtres.

Le conseiller pianota rapidement sur son téléphone. Le président regardait par la fenêtre, ça allait un peu mieux. Faudrait quand même trouver un moyen de les défoncer à Matignon. Il appellerait François cet après-midi. Il saurait quoi faire, ils allaient contacter le Front, à la fin, s’il y a que ça qu’ils comprennent… ils allaient leur en donner de la populace. Ça allait tacher les murs. La révolution, c’était trop tard, ça avait jamais été trop son truc de toutes façons. Mais se coucher comme un clebs en en redemandant, il avait passé l’âge aussi. Il y avait des mois que François le tannait avec sa théorie, sa stratégie du pire. Il allait le débrider. On allait bien voir. Ils allaient bien voir.

— Montre-moi ce que tu as préparé ? Pas mal. Faut qu’on en profite pour faire du bruit, montrer à Paris que la région Hauts-De-France avec un “s”, c’est pas que des ploucs. Insiste sur Google. Le côté IA, c’est bien. Tiens note ça. Ajoute que Berkeley envisage de renforcer ses liens avec notre université à Amiens. Qu’on a évoqué une antenne commune, un labo international, à Abbeville, carrément à Saint-Valéry, tiens. J’ai une idée. En rentrant, tu me prépares un nouveau plan recherche, en IA. Investissements massifs, fois dix. Moi aussi, je vais t’implanter des labos sur notre côte ouest. Trouve-moi un chercheur pour monter ça, picard jusqu’au bout des ongles.

<!--SAUT2-->

Le premier ministre contemplait le ciel dégagé, affalé dans son fauteuil, son verre de whisky à la main. Font chier ces crevards avec leurs régions à moitié décédées. Il y a pas d’indépendantistes en Picardie ? Faut lancer un mouvement, je te le dis, moi… On paye des branleurs pour défiler dans les rues. On veut l’indépendance ! On veut l’indépendance ! Bon, d’accord, on vous la donne. Hamid, dis-moi, tu m’appelleras Charron ? Voir s’il y a un truc à creuser. Ouais, c’est ça, une tombe ! On a pas de bol, les Espagnols et les Anglais ont des tas de régions qui veulent se barrer, mais nous rien, on va se traîner les boulets jusqu’au bout. Tu me diras c’est pas l’Andalousie qui veut se barrer, comme par hasard, c’est toujours les mecs qui en foutent pas une qui te collent aux basques… Ouais, je sais que les Basques veulent se barrer, mais tu noteras, pas en France ? Faut croire qu’on les nourrit trop bien. Tu sais ce qu’il faudrait ? C’est les virer ! Dans une boîte quand un secteur merde, tu revends, tu restructures, tu fermes. Nous, tu vois, il faut qu’on se restructure un peu, qu’on lâche du lest. On va faire sécession. Ça, c’est une idée ! Sert à rien la Picardie ? On la vire. Fais chier la Corse ? Libération. Ça, c’est une putain d’idée. On verra pour la Bretagne et ses cochons qui puent, faut se garder des pauvres et des alcoolos, on va pas faire le Luxembourg non plus. Mais ceux qui font chier, dehors ! Appelle Charron, vois si ça s’est déjà fait… Tu crois pas ? Je m’en fous de ce que tu crois. Appelle Charron.


# Première Partie

(2027-2030)


## Contact

— Bonjour Sara.

— Bonjour monsieur.

— Entrez, asseyez-vous. Mettez-vous à l’aise, je vous en prie.

— Merci de me recevoir.

— Merci de vous être déplacée. Voyez-vous, votre profil a vraiment retenu mon attention. Vous avez été formée aux vents humides de notre bon pays picard. Compiègne. Un gage de qualité, n’est-ce pas ? Je ne reviens pas sur votre expérience, votre réputation vous précède, à votre âge, c’est impressionnant. Célibataire, sans enfant. Parfait. Et votre site, évidemment, très suivi, félicitations. Vraiment.

— J’ai eu de la chance, j’ai eu des résultats assez inattendus, cela a intéressé la communauté.

— De la modestie, très bien. Pas trop tout de même, il faut oser !

— C’est plutôt dans mon tempérament. D’oser je veux dire.

— Oui, c’est ce qu’ont montré les résultats que vous avez obtenus au test que vous avez eu la gentillesse de bien vouloir passer lors de votre précédent entretien.

— C’était un test un peu étonnant, je ne vous le cache pas.

— Même pour quelqu’un d’habitué à communiquer avec des machines ?

— Oui, c’est rare pour un emploi de chercheuse de subir des tests psychologiques de ce genre.

— Je plaisantais, je sais que ce n’est pas habituel. Mais voyez-vous, ce que nous faisons ici est très inhabituel. Cela peut être perturbant, nous préférons des candidats qui soient bien prédisposés à ce que nous allons leur montrer et leur demander. Les compétences scientifiques et techniques ne sont pas les seules choses qui nous intéressent. Il nous faut une forte solidité mentale, et de la confiance. Mutuelle.

— Vous m’intriguez ! J’ai bien entendu étudié vos travaux en détail, c’est la raison principale de ma présence ici, mais je ne vois pas ce qui pourrait aller jusqu’à me perturber.

— Disons que nous n’avons pas encore publié le cœur de nos recherches, et nos travaux les plus récents sont vraiment atypiques. Nos méthodes surtout sont très atypiques. D’ailleurs tant que j’y pense, je dois vous faire signer cette clause de confidentialité, tout ce que vous entendrez ou verrez ici reste ici. Que vous soyez retenue ou non. Et je dois également vous faire passer un autre test, Sara. Vous serez filmée. Mais les données seront effacées ce soir même, voyez ce papier, là c’est nous qui nous engageons. Est-ce que vous êtes d’accord ?

Sara Picard lut lentement la feuille que lui tendait Albert Roman. Puis elle signa minutieusement. La pièce était surchargée de papiers, disposés en désordre sur le bureau, empilés par terre ou sur les deux chaises qui n’étaient pas occupées. C’était étonnant, alors que les ordinateurs avaient si totalement envahi le quotidien, qu’il restât autant de paperasse dans l’antre d’un des chercheurs en Intelligence Artificielle les plus médiatisés du pays. Il y avait aussi tous ces livres, certains ouverts, d’autres empilés, d’autres encore soigneusement disposés sur des étagères bon marché. Qui lisait encore des livres ? Albert Roman activa une par une les douze micro-caméras chargées de reconstituer l’image intégrale de son interlocutrice, puis il sembla configurer son ordinateur pour s’assurer des réglages.

— Pouvez-vous me dire un mot, Sara ? Cligner d’un œil ? Merci, c’est parfait, ça fonctionne.

Sara était jolie, le chercheur vieillissant y était sensible, malgré lui – c’est-à-dire qu’il aurait souhaité être au-delà de ces penchants terrestres, que cela n’influençât pas la lourde décision qu’il aurait bientôt à prendre. Il faudrait peut-être décider de la tuer. Mais, tandis qu’elle lisait les documents juridiques – encore du papier – qu’il lui avait tendus, il observait les traits de son visage, jeune, souriant, intelligent.

Sara Picard avait terminé une thèse en cryptologie trois ans auparavant, et à peine un an plus tard l’équipe à laquelle elle appartenait réalisait grâce à elle un exploit inattendu, en levant le voile sur les derniers des textes étrusques qui résistaient alors encore à la traduction, réputés définitivement indéchiffrables. Sara avait raconté cette aventure sur son site, *la fille qui murmurait à l’oreille des machines*. Albert Roman avait étudié en détail son histoire. Son équipe avait conçu une IA programmée sur un ordinateur quantique, Colossus. Une approche classique en cryptanalyse. Reconnaissance de motifs, calcul de probabilité. Mais Sara Picard avait suivi une piste au premier abord absurde : faire générer à Colossus tous les textes possibles sensés dont la taille et la structure correspondaient à une traduction possible des textes étrusques. Les textes générés, des milliards chaque seconde, relataient aussi bien l’histoire de civilisations antiques que la fabrication des machines à laver ou la culture des betteraves. Elle commença alors à apprendre à son IA à discriminer les textes possibles des textes absurdes. Pas de betterave, pas de machine à laver. Colossus progressait rapidement, mais continuait de générer beaucoup plus de traductions qu’il n’en supprimait. C’est alors que Sara commença à créer le réseau social Open Bletchley qui réunit rapidement plusieurs milliers d’amateurs, et comptait au bout de trois mois quarante millions de contributeurs ayant au moins participé à la validation ou l’invalidation d’une traduction. Mais le nombre de textes à tester avait dépassé le milliard de milliards et continuait d’augmenter. C’est à cette époque qu’elle fut contactée par les meilleurs spécialistes de la langue et de la culture étrusque. Open Bletchley avait largement attiré leur attention. Ils proposèrent de lui expliquer ce qu’un texte étrusque pouvait et ne pouvait pas contenir, ils lui parlèrent histoire, géographie, religion, structures sociales, stratégie militaire, techniques, sciences, politique, économie… Sara les enregistra plusieurs jours durant, bien consciente de ne pouvoir assimiler le centième de cette information. Dès qu’elle quittait les experts, chaque soir, elle rejoignait Colossus et lui communiquait ce qu’elle avait enregistré. Colossus devait faire ses choix lui-même, lui seul avait la capacité de défaire ce qu’il faisait. Et le véritable exploit de Sara était d’être parvenue à transmettre ces informations à son IA, quasiment en temps réel. La fille qui murmurait à l’oreille des machines. Elle publiait également chaque matin sur son site ses dialogues avec Colossus. Elle semblait ne pas suivre de formalisme informatique *a priori*. Elle expliquait négocier le langage directement avec la machine, au fur et à mesure. Ils inventaient des langues nouvelles pour en traduire une ancienne. C’était ce qu’elle expliquait. Bien sûr, la formalisation théorique laissait encore beaucoup à désirer. Personne ne parvenait a posteriori à mettre en évidence les connaissances acquises par la machine. Personne n’arrivait non plus à reproduire le phénomène, et le couple Colossus/Sara était le seul à parvenir à communiquer ainsi. Et parler de communication restait absurde pour la plupart des informaticiens et des philosophes. Dans l’expectative, les universitaires et industriels surveillaient les nouveaux résultats du laboratoire. Mais ceux-ci tardaient à venir, et la théorie du coup de chance commençait à se répandre. Ainsi que la théorie de la conspiration. Quelqu’un avait réussi secrètement à décoder l’étrusque, et on avait monté cette mystification pour valoriser les IA, qui commençaient à avoir très mauvaise presse. Le président de la ligue anti-robot s’était même fendu d’un bon mot resté célèbre quelque temps sur le réseau : « La seule langue morte que parle Colossus est le picard ».

Mais Albert Roman croyait à ce qu’il avait lu, le problème posé était trop proche du sien, la solution trop élégante. Il n’y a pas de hasard en sciences, mais des rencontres, des conjonctions, des moments. Cela devait être un de ces moments. Et Sara Picard allait bientôt partager sa ferveur. Le problème qu’il allait lui poser était le plus intéressant que puisse rencontrer une cryptologue, prendre contact avec une forme de vie alternative. Même s’il s’agissait ici davantage d’une forme de mort. Elle devait réussir le test.

— On peut démarrer le questionnaire ?

— Oui, je suis prête.

— Première question. Seriez-vous prête à sauver d’une mort certaine un enfant que vous ne connaissez pas pour une somme de cent mille euros, en faisant un emprunt à la banque sur vingt ans par exemple ?

— C’est un questionnaire de Herman ?

— Oui, en effet. Vous voulez bien vous y prêter ?

— Vous voulez tester ma résistance morale. Quel est ce contrat faustien que vous me réservez monsieur Roman, prononça-t-elle avec un sourire en coin, l’air amusé, pas du tout indigné.

— Vous savez que les derniers algorithmes permettent de corriger vos réponses à partir de vos signaux verbaux et non verbaux. Donc on ne peut pas facilement tromper un test de Herman, il faut y être vraiment entraîné. En revanche le test pourrait s’avérer nul, aussi Sara, essayez de vous soumettre au test, en sujet, sans chercher à en jouer ou à le déjouer. C’est une condition non négociable pour envisager notre collaboration. Vous pensez y parvenir ?

— Je suis soumise, testez-moi !

Son air malicieux, détendu, toujours souriant, ne finissait pas de troubler Albert Roman. Il fallait qu’elle réussisse le test.

— Donc, sauvez-vous d’une mort certaine un enfant que vous ne connaissez pas pour une somme de cent mille euros ?

— Non.

Le visage de Sara était devenu plus grave, et le resta à mesure que le test avançait. Il était évident qu’elle cherchait à réussir le test et en même temps à ne pas le trahir. Elle savait qu’il était utilisé pour mettre à jour les invariants psychopathes des coupables de crimes et évaluer avec une impressionnante précision les risques de récidives. Certains pays avaient indexé leurs peines ou leurs traitements sur ses résultats. Elle ne savait pas ce qu’ils cherchaient en lui faisant passer ce test. Elle n’avait pas le profil d’une tueuse en série et elle doutait pouvoir être soupçonnée de massacre sur voisin de bureau. Elle avait lu que certains économistes s’en servaient pour évaluer les comportements dit non rationnels, qui mettaient systématiquement à mal leurs prévisions depuis des décennies. C’était peut-être cela la raison de sa présence ici, décrypter des résultats psychologiques. Ce serait intéressant, à partir d’un corpus suffisant, elle pourrait sûrement prédire… Elle disposerait des signaux audiovisuels, ils devaient déjà avoir une base très importante à disposition. Elle pourrait…

— Sara ?

— Je suis désolée. Vous pouvez répéter ?

— Accepteriez-vous de tuer un homme pour la somme de cent mille euros, étant donné que vous avez prévu d’utiliser cet argent pour financer une recherche permettant de sauver la vie de plusieurs enfants ?

— Bien sûr.

— Vous pourriez tuer un homme Sara ?

— Dans un certain nombre de circonstances, il me semble que ce serait assez facile. En théorie bien sûr, en pratique je ne saurais même pas tenir un revolver !

— Merci Sara, le programme m’indique que le test est terminé. Vous voulez bien m’attendre quelques minutes, le temps d’analyser les résultats.

Albert Roman essayait de masquer son sourire. C’était bon. C’était forcément bon. Il n’était pas psy, elle aurait pu le jouer ? Non, c’était bon, il le savait, l’IA allait lui confirmer cela.

<!--SAUT1-->

Son collègue l’attendait dans une salle attenante ; il avait également du mal à ne pas laisser transparaître sa satisfaction. Et lui, il était psy.

— Quatre-vingt-dix-sept pour cent Al, tu as décroché le gros lot. Cette fille sait vraiment faire de l’informatique, en plus ?

— Sacrément.

— Elle aurait été serveuse, je t’aurais conseillé de la prendre quand même !

— Tu as vraiment regardé tous les paramètres ? Tout est vert ?

— Franchement, même toi ou moi avons un spectre moins limpide. Je peaufine pour optimiser la séance de déprogrammation, mais je vais jouer sur du velours. On fait ça demain ? À toi de la convaincre de rester maintenant.

— *I’m gonna make her an offer she can’t refuse !*

<!--SAUT2-->

— Sara, nous sommes très confiants sur les résultats du test. Si vous avez encore un peu de temps, je souhaiterais vous montrer quelques-uns de nos derniers résultats. Si vous êtes toujours intéressée de votre côté après cela, je vous parlerai du dernier exercice auquel vous devrez vous soumettre demain. Et si tout va bien, on pourra commencer dès la semaine prochaine, votre contrat sera prêt.

— Nous n’avons pas encore évoqué les détails, par exemple…

— Ce ne sera pas un problème. On trouvera un terrain d’entente. Vous me suivez ?

Sara le suivi jusqu’à une porte d’ascenseur. Albert Roman saisit un code sur un petit écran tactile situé à l’endroit où se trouve habituellement le bouton d’appel. Puis il fixa quelques instants une micro-caméra. Un petit signal sonore discret se fit entendre une fois l’identification achevée. Ils attendirent en silence. Les portes s’ouvrirent, Sara observa leur reflet sur les murs métalliques de la cabine. Albert Roman la regardait, discrètement. Il la laissa pénétrer la première dans l’ascenseur, puis il entra lui-même. Moins un, prononça Albert Roman en veillant à bien articuler les deux mots. La cabine se mit imperceptiblement en mouvement, puis la porte s’ouvrit sur un immense *open space*, avec en son centre une coupole en aluminium autour de laquelle s’affairaient plusieurs dizaines de personnes, munies d’ordinateurs manuels.

— Il ne manque que les extraterrestres pour que l’on se croie dans *Men In Black*, vous ne trouvez pas ?

Sara le regarda, interrogative.

— Laissez tomber, un vieux film de science-fiction. Qui garde un certain charme néanmoins.

Voilà pour le vieux con.

— C’est la salle d’observation, les chercheurs qui sont ici sont tous hautement qualifiés en lecture des signaux quantiques, chacun est attaché à mesurer les signaux des âmes que nous avons à notre disposition. Certains calibrent les appareils de mesure, par ici, d’autres calculent à partir des mesures relevées, tenez là-bas, Marc – bonjour Marc, Sara, notre future collaboratrice – n’est-ce pas ? Il y a aussi quelques informaticiens, ils sont en charge du stockage des données et de la supervision des processus logiciels en général. Une cinquantaine de personnes à cet étage. On arrive presque à quantigraphier une âme par jour.

— Cela fait deux fois que vous employez le mot *âme* ?

— Désolé Sara, c’est notre jargon. Il s’agit bien sûr d’enregistrement de champs quantiques *post mortem*. On a pris l’habitude de dire des âmes, entre nous. N’y voyez aucune posture blasphématoire. Juste une blague de geeks qui est devenue un mot courant.
— Je suis radicalement athée. Le mot âme me convient, il ne servait pas à grand-chose jusque-là. Elle souriait toujours, espiègle. J’ai mille questions à vous poser. Vous avez combien de quantigraphies en base ? D’où viennent vos mesures ? Où sont vos âmes ? Vous voulez que je vous aide à analyser les données ? Comment faites-vous pour localiser les champs quand ils se produisent ?

— Nous avons de nombreuses quantigraphies, je vous donnerai un ordre de grandeur plus tard, mais soyez certaine que c’est de très loin la plus grande base existante. Sur le comment, on reviendra aussi. Ce sont des points délicats de notre protocole de captation. Quant à votre rôle… Nous arrivons déjà honnêtement à analyser les données dont nous disposons avec notre propre IA – même si je suis sûr que vous pourrez nous faire progresser là-dessus aussi. Mais vous travaillerez deux étages en–dessous. Je ne peux pas vous en dire beaucoup plus avant votre séance de demain, sachez que personne à cet étage, à part moi-même, n’est autorisé à descendre plus bas. Personne ne sait même ce qui s’y passe. Nous avons isolé chaque étage, par niveau de confidentialité. Vous serez accréditée au dernier étage, le dernier cercle, nous ne sommes que six aujourd’hui à avoir accès au laboratoire moins trois.

— C’est un roman d’espionnage de bas étage, monsieur Roman.

— Joli jeu de mots. Il sourit.

— Je vous suis pourtant pieds et poings liés par un contrat de confidentialité complet, n’est-ce pas ?

— Disons que vous serez encore plus liée demain.

— Si je reviens. Vous pourriez commencer à me faire peur. Je ne suis peut-être pas le genre de fille qu’on lie si facilement…

— Des liens dorés Sara. Le triple de votre salaire actuel, horaires et vacances libres – vous ne voudrez pas partir en vacances – un logement à disposition, une villa, vue sur la mer, vous verrez, une voiture si vous voulez, ça ne sert à rien ici.

Le visage de Sara restait impassible ; Albert Roman savait qu’il ne la toucherait pas avec ces fantaisies terrestres. Il distrayait simplement sa garde. Premier vrai mouvement.

— Je vous promets une puissance de calcul quantique de trente qubits.

— Cela n’existe pas. Pas ici.

Touchée.

— Pas à cet étage, non.

— Pas en France.

— Nous ne sommes presque plus en France, Sara. La Picardie ne sera peut-être plus en France dans quelques mois, vous êtes au courant, n’est-ce pas ?

Sara fit un effort pour garder le contrôle d’elle-même. Elle était agacée à présent. Son sourire mutin s’était muté en une moue d’insoumise, sourcils froncés, lèvres serrées. Son pouls s’était notablement accéléré. Elle était presque en colère. Elle répétait monsieur Roman à la fin de chaque phrase, pour se donner une contenance, maintenir une barrière qu’elle se savait sur le point de franchir. Il essayait de l’acheter ?

— C’est tentant monsieur Roman. De l’or, de la puissance de feu. Vous avez une sacrée main monsieur Roman, c’est sûr. J’imagine les puissants intérêts qui vous financent, monsieur Roman, et qui se feront un plaisir de me financer une belle maison et une belle voiture, de privatiser les plages de la baie de Somme pour moi. Mais si j’avais envie, moi, plutôt, d’indépendance, de liberté, de transparence ? Si vos secrets, vos accès restreints et vos clauses de confidentialité étaient contraires à une certaine vision de la science, à ma vision de la science ? Vous savez, le bien commun, la libre circulation des connaissances. Ce que je produis, je le publie immédiatement sous des licences libres monsieur Roman, je suis pour une recherche publique, vous n’avez pas vu cela avec vos tests ? Si vous voulez que je joue à Faust, monsieur Roman, il vous faut affiner vos contrats. Je…

— Sara, elles bougent.

Son visage se figea. Elle avait compris. Immédiatement.

— Les âmes bougent, Sara, ce ne sont pas des enregistrements statiques, il y a des signaux qui circulent, des mouvements apériodiques. Je crois que ce sont des sortes de programmes. Elles bougent. Je les ai vu bouger.

Elle avait déjà capitulé.

Tu me vendras ton âme à présent, Sara. Il sourit en pensant à cette phrase qu’il ne prononça pas. Il la raccompagna en silence. Il avait gardé encore une carte majeure dans son jeu. À demain, Sara, j’ai encore beaucoup à te révéler.

<!--SAUT2-->

— La déprogrammation sous hypnose est aussi une avancée due aux IA. Vous le saviez Sara ? Bien sûr, vous avez fait votre psycho sérieusement. Vous faites tout sérieusement Sara, n’est-ce pas ?

— Passionnément surtout, je dirais, monsieur Roman.

— Vous pouvez m’appeler Albert, nous sommes collègues à présent. Pas de hiérarchie dans mon équipe, pas de…

— Votre équipe.

— Un peu de paternalisme, tout au plus. Je suis pris sur le fait.

— Est-ce que vous aussi vous avez été… vous avez suivi une séance de…

— La déprogrammation est requise pour l’accès aux niveaux moins deux et moins trois, Sara. Ce que je vais vous montrer à présent, c’est difficile… Ça ne se marie pas bien avec l’éducation morale que nous avons reçue. Quelle que soit cette éducation.

La fin de la descente se fit en silence. Il régnait dans l’ascenseur une ambiance étrange, comme à l’entrée dans une église, même pour un athée, cette idée qu’il faudrait faire preuve de respect.

— Ici, c’est le moins deux. Nous ferons les présentations tout à l’heure. Ils travaillent tous sur les données collectées au niveau moins un. C’est eux qui ont mis au point le programme qui montre les mouvements des âmes. Ils développent une IA dont l’objectif serait de mettre à jour des informations intelligibles à partir des données collectées. Ils vont beaucoup compter sur vous pour les faire avancer, Sara. Les résultats sont pour le moment très faibles.

Mais, descendons, je vais vous présenter votre enfant.

<!--SAUT1-->

Il occupait la moitié de l’espace de la salle, c’est-à-dire de l’étage, complètement ouvert. Dix tores d’une dizaine de mètres de diamètre chacun, reliés par des faisceaux de fibres optiques.

— Comment avez-vous pu financer un truc comme celui-ci. Il y a dix cœurs quantiques ? Seule la NSA peut se payer ça aujourd’hui… Peut-être le CERN. Mais ici ?

— Il y a quatre ingénieurs de très haut niveau, pour s’assurer que ce bébé monstrueux tourne correctement en permanence. Depuis le moins un. Ils s’occupent seulement du fonctionnement matériel, le niveau informationnel est confiné ici, au moins trois. Rien ne sort.

— Comment sont gérés les backups ? Comment c’est alimenté ? Où…

— Tout cela est géré, Sara, vous n’aurez à vous préoccuper que de la programmation de votre IA.

— Mais…

— Vous ne voulez pas savoir comment c’est financé. Vous ne voulez pas savoir comment c’est possible. Vous voulez que ça fonctionne, Sara. Et ça fonctionne.

— Vous l’avez essayé ?

— Comment faire autrement. Vous tiendrez combien de temps avant d’essayer, Sara ?

— Vous personnellement, monsieur… enfin Albert ? Racontez-moi !

— Je l’ai essayé avec Li, un des quatre chercheurs qui travailleront avec vous ici. Je ne suis pas programmeur, comme vous le savez. Mais je l’ai essayé bien sûr.

— Qu’avez-vous fait avec ?

— Nous avons… réussi… une modification.

— Une modification ?

— Oui, une petite. Mais au hasard bien sûr. Étant donné que nous ne décryptons rien du tout pour le moment.

— Vous avez modifié… une âme ! au hasard ? c’est horrible ! Elle avait dit cela avec un rictus admiratif.

— De quel point de vue Sara ? Je vous croyais profondément athée, n’est-ce pas ? On ne parle ici que d’états d’information, comme dans un programme. N’est-ce pas ? Vous vous posez des questions quand il s’agit de modifier l’état de la mémoire d’un ordinateur ? Serait-ce horrible de le faire au hasard ?

— Je… Non. Bien sûr. Excusez-moi. C’est inattendu. La surprise. C’est pérenne ?

— Oui, la perturbation est restée visible, on peut la suivre encore aujourd’hui. C’était il y a trois mois.

— C’est incroyable.

— On va vous montrer tout cela, vous allez avoir pas mal de lecture ces jours-ci. Nos documents. Mais il faut que vous nous aidiez à les lire, elles, Sara. Les âmes, il faut que nous parvenions à les lire. Nous avons besoin d’une IA qui parvienne à les lire.

— Vous cherchez à communiquer.

— On cherche à communiquer, bien sûr. Le problème est immense. Vous murmurez à l’oreille des machines, Sara, n’est-ce pas ? Murmurez à l’oreille de celle-ci, et faites-la parler avec les âmes. Nous voulons que vous communiquiez avec une IA qui communique avec des âmes. Vous modifiez les états quantiques de l’IA, elle modifie ceux de l’âme, idem dans l’autre sens. On pourra vraiment communiquer, vous comprenez ce que cela signifie ?

— À condition qu’il y ait quelque chose avec quoi communiquer.

— Que croire d’autre ?

— Vous n’êtes pas profondément athée, Albert ?

— Mais c’est vous qui m’avez convaincu, Sara. Vous pouvez parler aux machines, vraiment. Alors nous pouvons parler avec n’importe quoi, n’importe quel signal porteur d’information.

Tandis qu’ils circulaient entre les composants de l’incroyable machine, Sara laissait traîner sa main sur les tores, sur les câbles, sur les interfaces, avec une certaine sensualité, comme une caresse, comme si elle découvrait des textures inconnues, comme si elle se préparait à de nouvelles sensations.

— Nous ne serons que cinq à la programmer ?

— Peut-être six, j’ai également contacté Suzanne Cauvin, vous la connaissez ? Vous êtes de la même génération et elle est originaire de Noyon, même si elle a quitté la Picardie pour ses études supérieures, La Sorbonne, puis Shanghai.

— Je la connais, je l’ai rencontrée lors d’un colloque à Boston, il y quatre ans si ma mémoire est bonne, c’était encore les États-Unis d’Amérique. J’ai découvert alors ses travaux sur la programmation quantique, elle est très inventive et semble très pugnace. J’ai beaucoup étudié ses publications ensuite.

— Vous étudiez beaucoup de choses, n’est-ce pas ?

— L’ère qui s’ouvre est assez exaltante, vous ne trouvez pas ? Et j’ai la chance d’en être partie prenante, pas juste spectatrice. Cela dit, je ne me rappelle pas l’avoir lue ces deux dernières années…

— Elle poursuit sa recherche chez Amazon, son travail est sous embargo depuis. La publication de ses résultats est attendue, c’est prévu pour décembre, juste avant Noël, en même temps que la sortie de leur nouvel iAlexa Kappa. Je l’ai contactée il y a plus d’un an pour la première fois, mais je crois que je ne parviendrai pas à la faire revenir aux sources…

— Un an ? Je ne suis donc pas votre premier choix ? demanda-t-elle avec un air faussement vexé.

— Disons plutôt que j’ai rêvé de vous avoir toutes les deux à mes côtés.

— Cela aurait peut-être fait beaucoup pour un seul homme. Il semble donc que vous devrez vous contenter de moi finalement ? Tant pis pour toi, Suzanne, il n’y aura qu’une seule reine de l’informatique picarde !

Elle arborait à nouveau ce sourire à la fois espiègle et confiant qui troublait Albert, presque encore adolescente et si consciente de son pouvoir. Sur les machines et sur les hommes.

— Est-ce que je peux me connecter à votre IA, Albert, maintenant ? Elle s’appelle comment ?

— Hal. Bien sûr.

— Bien sûr.

Sara ouvrit la console et entra en contact avec l’IA.

— Bonjour Hal, je suis Sara.

— Bonjour Sara.


## Le niveau moins quatre

Sergeï Alexandropov avait passé dix-sept jours en mer. Il ne savait pas où il allait. Ils étaient bien traités ; les conditions de vie à bord étaient précaires, mais humaines. La nourriture correcte étant donné les circonstances, la promiscuité raisonnable. Il y avait aussi un peu d’alcool chaque soir. Du vin français, coupé avec du rhum. Évidemment, il sortait du camp de Kharkhorin...  On disait Kharkhorin comme on aurait dit Auschwitz au siècle dernier, quelque chose dont la monstruosité était entendue, indicible. Il aurait dû y mourir, il allait mourir ailleurs finalement. La plupart des autres avaient l’air de sortir d’un endroit comparable, eux aussi, il devait y en avoir un peu partout dans le monde de ces endroits, car ça venait de partout. Enfin, tous n’avaient pas l’air de sortir de camps, certains avaient plutôt l’air de kapos, des tronches d’assassins accrochées aux épaules. Comme pour un mauvais jeu de rôle. Il y avait aussi quelques débiles profonds. Et les vieux. Sergeï pensait que la moitié d’entre eux ne finirait pas le voyage. Pourtant ils devaient tous s’accrocher, le *deal* était clair, il fallait mourir là-bas. Ceux qui mourraient avant ne toucheraient jamais la prime. Leur famille ne toucherait jamais la prime. C’était ce qui les réunissait tous ici. Ils avaient vendu leur âme. Une mort au service de la science – Sergeï s’en foutait bien de la science – leur avait-on dit, sans douleur, sans violence. Rien d’autre à faire. Et la prime pour la famille. Une femme, ou un fils, un parent. Pour Sergeï, c’était une femme. Une femme qu’il n’avait pas assez aimée pour abandonner son combat, sa croisade comme elle disait ; une femme qui n’avait pas suffi à donner un sens à sa vie. Elle en donnerait un à sa mort. Pas si mal, finalement.

L’anglais restait la *lingua franca* à bord, mais son français, même un peu rouillé, lui avait attiré la sympathie des matelots. Ils sentaient aussi, peut-être, derrière la maigreur et les cicatrices accumulées en six années de camp de concentration, l’homme qu’il avait été, le chef de guerre. Il avait été brisé, totalement, il n’aurait pu aujourd’hui lever la main sur son pire ennemi. Mais il restait peut-être une étincelle, que lui ne voyait plus, mais qu’on pouvait encore distinguer de l’extérieur. Comme celle qu’il percevait chez son voisin de couchette. Meurtri par une vie de misère et de mort, Amadou lui avait raconté un bout de son histoire – c’est tout ce qu’il leur restait ici, leur histoire – il avait perdu ses quatre fils, morts « pour leur patrie », sacrifiés à la boucherie, il disait. Il était resté terré, avec sa femme et sa fille pendant cinq mois, sans presque manger. Finalement, il avait rencontré un chasseur d’âmes – c’est comme ça qu’ils les appelaient entre eux, sur le bateau ils avaient tous croisé un chasseur d’âmes à un moment, ça aussi ça les rapprochait – et il avait accepté bien sûr. Tous avaient accepté. Le prix n’était cependant pas le même, pas du tout le même. Amadou s’était bien fait baiser. C’est comme ça les Africains, il disait à Sergeï, on se fait toujours baiser. Quand même, ma femme et ma fille sont à l’abri, c’est ce qui compte, n’est-ce pas ? Elles seront vraiment à l’abri, n’est-ce pas ? Tu y crois aussi ? demandait-il à Sergeï, ce n’est pas une arnaque ? C’est sûrement une arnaque, Amadou, les pactes avec le diable sont toujours des arnaques. Mais pas comme ça. Je crois qu’elles seront vraiment à l’abri. Alors, *inch’allah* – Amadou disait tout le temps *inch’allah* – le reste, c’est entre les mains de Dieu, mon ami. Sergeï ne répondait pas. Il éprouvait de la sympathie pour Amadou, mais pour les dieux il n’éprouvait qu’une haine froide. La colère, il l’avait perdue, la force aussi. Mais la haine, elle, était restée. Et maintenant qu’il en voulait moins aux hommes, il en voulait plus encore à leurs dieux, qui avaient bouffé son monde, son pays, son village. Les siens, ses amis. Des convertis. Bien vite, des convertissants. L’intolérance s’était propagée si vite qu’ils ne l’avaient pas vue venir, qu’ils l’avaient laissée s’installer. La loi de Dieu de ceci. La règle de Dieu de cela. Enfin, ils avaient compris, ils avaient voulu résister. C’était trop tard. La plupart avaient capitulé. La peur, une femme, des enfants. Lui avait été jusqu’au bout. Jusqu’à Kharkhorin. Lorsqu’ils étaient venus pendant ses cours la première fois, il avait été si surpris qu’il n'avait rien dit. Eux étaient juste restés assis, à l'écouter. La seconde fois il leur avait demandé de quitter la salle, ils avaient obtempéré. Puis ils étaient revenus. Ils avaient commencé à poser des questions, pourquoi abordait-il ce sujet, pourquoi pas celui-ci, à discuter, à menacer, à crier. Il aurait fallu céder, ou fuir, mais Sergei restait, il n’envisageait pas les choses autrement, c’était sa salle, ses élèves. Quand ils l'avaient chassé, il s’était battu, pour la première fois de sa vie. Alors qu’il gisait sur le trottoir devant son école, ensanglanté, endolori, il avait décidé que c’était son pays, pas le leur, qu’il se battrait à nouveau, qu'il tuerait des hommes.

Sergeï passait ses journées à essayer de deviner les motivations de chacun, qu’est-ce qui conduit un homme, une femme – il y avait évidemment des femmes – à vendre ainsi les jours qu’il lui reste. Il y avait des condamnés à mort, qui ne perdaient pas grand-chose, probablement. Pour les vieux, les malades, c’était la même chose. Encore que, personne ne sait vraiment combien il lui reste à vivre, après tout, quelques jours plus quelques jours, ça fait bien une vie, non ? Les miséreux, c’était un peu comme lui, ils avaient dû penser au suicide mille fois, mais il leur restait assez de force pour continuer, et cette force ils l’avaient dépensée pour accepter le marché, sauver leur famille. Ils finissaient en beauté une vie de douleur. Des christs. Saloperie de soupe religieuse. Sergeï se demandait s’il y en avait ici qui n’avaient pas vraiment choisi. Il y avait des malades mentaux, est-ce qu’ils avaient pu choisir, eux aussi ? D’un autre côté, difficile de croire que les autres avaient choisi, que lui avait choisi. Quand se souvenait-il avoir vraiment choisi quelque chose ? Peut-être jamais. Il avait lu Nietzsche, il y a longtemps. Les dieux n’étaient pas du tout morts, il s’était bien fait avoir. En même temps, il disait de ne croire en personne Nietzsche, même pas en lui ; et aussi qu’on ne choisit rien, ou qu’on ne peut choisir que ce qui nous arrive. Applaudir au spectacle. Souffler sur la voile. Accepter, somme toute. Un truc comme ça. Il n’avait peut-être rien compris. Il n’y avait peut-être rien à comprendre. Enfin, il était là, sur ce bateau et il achevait d’écrire ses dernières lignes. Ou de les lire.

<!-- SAUT2 -->

– Sergei Alexandropov, avait crié le type. Le bateau s’était définitivement immobilisé, on avait bien senti depuis quelques heures les manœuvres, certains disaient qu’on était arrivé, que c’était la fin du voyage. La vraie fin. On les avait fait sortir sur le pont. Il y avait du brouillard. Un type était monté à bord, un Français, habillé en costume. Propre. Classieux. Il était flanqué de deux autres costards, plus massifs, sûrement armés. Ainsi Sergeï allait être le premier. Il fallait bien montrer l’exemple, pas vrai ? Et puis, attendre là-dedans, c’était peut-être pire. Enfin, il ne saurait pas.

– Sergei Alexandropov, avait appelé à nouveau l’homme en costume, en consultant un terminal qu’il tenait à la main.

– C’est moi, répondit Sergeï, en français. Il s’avança vers lui. Il jeta un dernier coup d’œil aux autres voyageurs. À Amadou. Salut l’ami, bon dernier bout de route, pars en paix, lui dit Amadou. Pourquoi pas ? Lui qui avait vécu la partie la plus intense de sa vie en guerre, il pouvait bien partir en paix.

– Veuillez nous suivre, monsieur Alexandropov.

Il ne posa aucune question. Il suivit l’homme dans le canot, sur la plage, dans une voiture, puis dans un bâtiment.

– Moins quatre, annonça l’homme, très distinctement, s’adressant à l’ascenseur.

Plus bas, ils entrèrent dans une immense salle, avec des chiffres inscrits au sol, des dizaines, des centaines, des milliers de chiffres.

On aurait dit une sorte de jeu. Sergeï essayait d’en trouver l’ordre malgré lui. Il suivit les trois hommes qui se déplaçaient sans hésitation. Ils s’arrêtèrent au-dessus du nombre 7176.

– Vous voulez bien vous tenir debout sur ce nombre, le 7176. Regardez, vous avez des emplacements pour poser vos pieds. C’est parfait, merci. Voilà, je vais juste vous faire une piqûre. Ça ne prendra que quelques secondes.

Les dernières secondes de la vie de Sergeï Alexandropov.

Des secondes enregistrées par des millions de capteurs quantiques. Immédiatement transférées aux ordinateurs du laboratoire, mises en signaux, en données, en images. Des secondes qui venaient alimenter les insatiables IA du laboratoire. Elles ingurgitaient des âmes, autant qu’elles pouvaient, chaque jour ; elles régurgitaient des données, des données que personne ne comprenait. Pour le moment. Il fallait encore plus d’âmes. Elles réclamaient toujours plus de sacrifices.

<!--SAUT2-->

– Nous avons de nouvelles données entrantes. Sara, numéro 7176, tu l’as ? L'IA est en train de la traiter, je vais bientôt la visualiser.

Sara s’affairait sur son clavier. Elle marqua une pause.

– D'où viennent-elles, Al ?

– Tu ne veux pas le savoir.

– Je crois que je le veux à présent.

Albert Roman regarda attentivement Sara Picard.

– Du moins quatre, Sara. Elles viennent du niveau moins quatre.

<!--SAUT2-->

Sara ne montrait aucun signe d’émotion. Elle avait sûrement imaginé quelque chose comme cela.

– Pourquoi crois-tu que l’on a choisi la Baie de Somme, Sara ?

– Tu es picard.

– Et les Picards sont casaniers, n’est-ce pas… Il sourit. D’accord, mais pourquoi ici, dans ce village balnéaire presque déserté ? Pour le climat ? La stimulation intellectuelle ? Le bassin d’emplois ? Allons, Sara ? Pourquoi ? Sur une plage ?

– La mer ?

– Presque. Les bateaux, Sara. Nous importons. Et nous exportons. Un commerce triangulaire. Ça paie ta machine et ça la nourrit en même temps.



<!--SAUT2-->

Sara regardait le *Heisenberg*, ancré à quelques brasses de la côte, si proche qu’elle pouvait lire son nom inscrit en blanc sur la coque noire en dépit du crachin qui brouillait sa vue. Sara sourit intérieurement en pensant que ce père de la physique quantique n’aurait probablement pas imaginé donner un jour son nom à un navire de transport, encore moins un navire de contrebande chargé de drogues et d’êtres humains. Sara se demandait s’il y avait en ce moment un *Einstein* qui croisait paisiblement quelque part dans l’Atlantique, un *Bohr* qui patientait et transpirait dans un port moite à Macao, un *Schrödinger* qui risquait sa peau en passant le Cap Horn ou en slalomant entre des icebergs en Antarctique. Si toutefois le passage du Cap Horn présentait encore le moindre risque ou si les dernières glaces du pôle Sud n’avaient pas encore tout à fait fondu. Elle ne connaissait de la mer que ses promenades quotidiennes sur la plage, son mordant quand elle y trempait parfois les pieds, et quelques classiques qu’elle affectionnait adolescente, où se croisaient Long John Silver, Moby Dick, Santiago. Elle sourit intérieurement de ce romantisme tout à fait déplacé.

Immobile, colossal, le *Heisenberg* semblait être posé sur le fond de la mer, indifférent aux mouvements anarchiques des vagues. En revanche, les zodiacs qui allaient et venaient entre le cargo et la plage étaient comme avalés puis recrachés ; ils disparaissaient parfois si longtemps que Sara pensait qu’ils ne reparaîtraient plus. Ils déchargeaient sur la plage une poignée de femmes et d’hommes qui se mêlaient alors à un groupe bigarré, chaperonné par d’épais hommes vêtus uniformément et équipés de gilets réfléchissants. Immédiatement après, des dockers commençaient de charger les caisses que des camions blancs encore stationnés sur la plage avaient déposées. Elles contenaient un psychotrope de synthèse mis au point au niveau moins quatre du laboratoire d’Albert Roman, qui avait rencontré un succès ravageur un peu partout dans le monde, et notamment aux Eutan où il avait supplanté toutes les autres drogues, chez les classes aisées principalement. Tout semblait parfaitement réglé.

On pouvait compter à présent une centaine de personnes sur la plage, abritées de la pluie fine par des parapluies qui leur avaient été distribués, tous parfaitement identiques. Ces hommes et ces femmes ressemblaient aux réfugiés que l'on voyait s’échouer sur les plages d'Europe auparavant, sauf qu'ils semblaient venir de partout à la fois avec leurs peaux de multiples couleurs et leurs vêtements de toutes origines. Ils donnaient l'impression d'être en retard d'une décennie, comme si se réfugier était devenu anachronique à l'ère des IA, comme si on ne leur avait pas dit que ça ne se faisait plus, comme ces combattants oubliés sur les îles du Pacifique après la seconde guerre mondiale, comme s'ils avaient erré sur la mer durant ces années, comme des Ulysse, des maudits. Le *Heisenberg* devait avoir fait le tour du monde pour les dénicher ici et là. C'était une consolation qu'il n'existât pas un unique lieu où trouver tous les désespérés de la Terre. Sara trouvait en quelque sorte rassurant qu'il faille les chercher un peu.

Elle était trop loin pour les entendre, le bruit des vagues couvrait leurs conversations, mais elle imaginait des dizaines de langues entrechoquées. Ou peut-être ne parlaient-ils pas ; après tout que pouvait-on raconter en un moment comme celui-là ?

Sara n'avait pas visité le niveau moins quatre, elle avait décidé que cela ne la concernait pas directement. Que cela ne devait pas la concerner. Elle savait que les administrations les plus sombres prenaient leurs racines dans une séparation des tâches telle que personne ne pouvait être tenu responsable de rien, quelles que soient les atrocités perpétrées. La mécanisation des organisations comme source de déshumanisation : il était finalement toujours question d’hommes et de machineries. Sara s'en était tenue à des explications générales, pour satisfaire sa curiosité, pour parvenir à ne plus y penser. Elle essayait de fixer son regard au loin, sur l'horizon rendu flou par la brume et la colère de la houle, mais, tout de même, elle imaginait les hommes attendant sur leur plot numéroté, elle sentait leurs jambes se dérober alors que la piqûre faisait son effet.

Avant de remonter dans son laboratoire, elle jeta un dernier regard à ces êtres vivants, à quelques pas d’elle seulement, pourtant trop loin pour qu’elle leur parle, alors qu’elle chercherait sa vie durant comment communiquer avec eux, une fois morts.

Au fond de ce regard, c’était l’étonnement qui dominait.

<!--SAUT2-->

– *This bitch is the bomb, Jess. You cooked a stuff like that ? You’re a fucking wizard man !*

– *No, it comes from another planet, dude, I tell you !*

– *Like a Star Trek teleportation or so ?*

– *Yeah ! science, bitch !*

– *Fuck me, man, this shit of yours, I can buy dozens of kilos. Per week, I mean. At least.*

– *Yo, prepare to man. Tell your boss, I can deliver tons.*

– *Fucking Star Trek shit !*


## Le Zoo de France

— Qu’est-ce que tu veux dire par là ?

— Ce que je veux dire ? On en a déjà parlé, François, c’est fini. Il le fait. Pour de bon.

— Mais, enfin, tu plaisantes, ça n’est pas possible ?

— Tu sais bien que si. Le pacte de Paris le prévoit.

— Mais tout de même, on est pas… enfin, quand même…

— Tu sais comment il vous appelle ? Le Zoo de France. Le Zoo de France, mon gars. Et tu as vu comme vous avez voté ? Il faut réfléchir à un moment. Ça fait deux ans que je vous dis d’arrêter de faire les cons. Ça a changé, le monde a changé. Faut bouger un peu. Fallait bouger.

— Mais on va bouger. Enfin, on bouge. On essaye.

— Bah, essaye davantage. Mais là, c’est plié. Dans trois mois.

— C’est toute la région ?

— Presque. Ils vont proposer au Sud de l’Oise de rejoindre l’Île-de-France. Senlis, Chantilly, peut-être jusqu’à Compiègne. S’ils arrivent à tracer une frontière qui évite Creil. Et le Nord, pour l’essentiel, devrait rester en France, lui. Il va rejoindre la Champagne. On fera un petit passage.

— Un petit passage ? Vous nous virez et vous nous prenez les villes les plus riches ? C’est ça le plan ?

— On ne prend pas. Il y aura un référendum d’autodétermination. On discutera. Si les gens préfèrent rester en Picardie, ils pourront.

— Préfèrent ? Qui va préférer rester dans la région la plus pauvre de France ?

— Ça maintenant, c’est ton job. Rendre le pays attractif, booster l’économie. Allez au boulot, c’est un *challenge*, c’est stimulant, non ? Un nouveau pays !

— Un pays ? Plus une industrie debout. Notre agriculture écrasée par le bio. Je fais comment pour relancer les agrocarburants ? L’état a fait démonter toutes nos usines il y a trois ans, on savait plus faire que ça, à la fin.

— De toutes façons, tu pourras pas les relancer les agros. Ça restera illégal dans les accords que vous allez devoir signer.

— Qu’on va *devoir* signer ?

— Ben oui. On va rester des pays frontaliers, il va nous falloir des accords.

— Ils sont déjà écrits ?

— Tu pourras négocier un peu. Enfin, un petit peu. Enfin, dans l’immédiat, tu vas pas beaucoup négocier. T’as pas d’économie, pas de diplomatie et ton armée c’est la police municipale de Trifouillis-dans-la-Aisne. De toutes façons, ce sera le classique européen. Écologie. Pas de paradis fiscal. Pas d’armée d’intervention extérieure, hors contrôle de la CA. Ouverture des frontières aux biens, fermeture aux personnes.

— La CA ? Mes pervenches dans la *Common Army* européenne ? Les biens ? J’ai pas de banque centrale. J’ai pas de banque, juste. Même les routes sont pas toutes à moi, l’A1, vous me la filez, l’A1 ?

— On va vous la vendre. Mais sur du long terme, t’inquiète. T’auras qu’à remettre des péages touristiques. Des visas. Des vaccins ! Nan, je déconne. Pour les vaccins.

— Tu déconnes ?

— Bon, écoute, je te laisse voir. Voilà, je voulais te le dire moi-même, le Président va l’annoncer d’ici un ou deux jours.

— Comment il va annoncer qu’il vire une région du pays ?

— Il va surtout se servir des élections. Vous vous êtes vraiment distingués, cette fois. Personne ne sera malheureux de se séparer d’une région où vous avez voté ça à 48 %. Vous avez tendu le bâton, il est plein de merde, vous vous êtes assis dessus, et vous l’avez scié.

— *Ça* ? Mais on est pas ça, merde. Les gens ont voté ça parce qu’ils voulaient gueuler. C’était comme un cri d’alarme. Un mal-être. Les mecs crèvent la dalle ici, putain.

— Ben, pour une fois, tu vois, les électeurs ont été entendus, et leur vote a eu des conséquences. C’est une leçon de démocratie qu’on donne au monde, ensemble. Et puis, vous garderez la double nationalité. Enfin un an ou deux. Sans transmission aux enfants.

— Un an ? Mes enfants ne seront pas français ?

— Fallait les faire avant. Là c’est trop tard, ils seront picards. Tu voulais être picard, c’est ça, non ? Ben voilà, t’es picard. Tu es François le Picard. Tes enfants, ils seront picards. Vous êtes tous picards. Vive la Picardie libre !

— Les gens voulaient gueuler, c’est tout, je te dis.

— Ils ont été entendus, je te dis. Et faut vraiment que je me sauve. J’ai du boulot. Et tu as du boulot.

— On va crever. Putain, tous les mecs qui le peuvent vont se barrer. L’immobilier va s’effondrer.

— Je sais pas, accueille des Juifs. Ça fera cool.

— Des Juifs ? Cool ? Tu te fous de moi ?

— Je dédramatise. Faut aller de l’avant, vieux. Faut voir le monde sous un jour positif. Faut boire à la source à moitié pleine.

Il rangeait ses affaires, affairé, pressé, à présent. Il s’en tirait pas mal. L’autre avait pas sorti de flingue. Il avait pas menacé de le prendre en otage. Il avait pas menacé de sauter par la fenêtre. Il avait pas trop gueulé, finalement. Il chouinait. Sale région de chouinards. On les maintiendra à flot. Histoire d’éviter le retour du choléra, on a des rivières en commun. Après, c’est pas dit qu’on restructure pas un peu la géographie hydraulique aussi. Petit à petit. Les camps et les fours, je pense pas, mais faudra quand même pas trop faire chier, j’en connais un ou deux qui ont déjà évoqué l’idée en loucedé.

— Et, si je veux pas les signer, moi vos accords de merde ? Si je ne veux pas, hein ? Je serai un pays souverain, du coup ?

— Souverain, ouais, bien sûr. Mais ça se passe pas comme ça, tu verras. C’est pas on veut ou on veut pas. C’est même pas on peut ou on peut pas. Tu vas voir la diplomatie internationale, c’est autre chose.

— La diplomatie internationale ?

— Écoute, vraiment, j’ai pas le temps là, mais je t’envoie un conseiller. Il t’expliquera, mais en gros, ton état était bienveillant quand t’étais dedans, un état à côté, c’est moins bienveillant. C’est l’idée générale. Donc, les accords se font, c’est pas vraiment une question de vouloir ou de pouvoir, finalement. Ça se fait, c’est tout. Juste, François, j’y pense…

— Quoi ?

— Mon conseiller, t’essayes pas de me le débaucher, hein ? Nan, je déconne ! Il partit d’un rire gras. Allez, courage mon *tchiot*.

— Va te faire enculer. Profond.

<!-- SAUT2 -->

— La drogue et les bordels.

— Pardon ?

— La drogue et les putes, c’est les deux sujets sur lesquels finalement le Pacte n’a pas tout borné. Tu ne peux pas les institutionnaliser au niveau étatique, tu ne peux pas les promouvoir, tu dois coopérer avec la police européenne et celle des états voisins qui te le demandent. Mais tu n’es pas obligé d’avoir une police à toi dédiée à ça. Tu peux laisser faire. Et tu peux imposer.

— Un mois de boulot avec tes avocats qui me coûtent un bras par jour, et tu me proposes de légaliser de l’herbe et des putes, alors que j’ai été élu avec un programme de rigueur sociale et morale ?

— Légaliser, non, tu ne peux pas à cause du Pacte. Tu peux laisser faire et taxer. Mais ça reste illégal, en revanche la sanction vis-à-vis des délinquants peut être faible, une simple amende, que tu peux ne pas mettre de zèle à recouvrer, c’est l’idée. Et puis, avoir de la perte dans une jeune administration ça se comprend. Il y a les champs et les tracteurs pour faire pousser, les vestiges d’une industrie chimique pour transformer. Les profits seront rapides et importants. Ton programme, François, tout le monde s’en fout. Fais pas chier avec ça. C’est de la littérature. Tu t’es fait virer comme une daube, on s’est fait virer comme des vieilles merdes, on est des résidus de vide-greniers, c’est bon, alors on riposte, comme des pauvres, on est au bout, on va au bout. *No future*, mec. On est des punks maintenant. Sois un punk. Conduis-toi comme un punk.

— Non au Zoo de France, oui à la Punkardie, c’est ça ? François sourit pour la première fois depuis un mois, depuis l’exclusion de la Picardie. Et vous avez prévu quoi, avec ton think tank, pour transformer une région de fachos en pays de beatniks ?

— L’histoire nous a montré, en tous cas, qu’il ne faut pas longtemps pour transformer un beatnik en facho. C’est vrai qu’en sens inverse, j’ai moins d’exemples qui me viennent. Maintenant l’histoire jusqu’ici était plutôt faite de pays qui annexaient des régions. Se faire virer d’un pays, c’est une première non ? Alors on continue d’innover.

— Quand même, par rapport à la morale…

— La morale, hein ? Tu crois que ton blé bourré aux pesticides et ton maïs transgénique étaient plus moraux ? Tu sais combien de cancers on a ensemencés avec nos engrais ? Et la France qui arrose le monde entier de ses cirrhoses, c’est classe, c’est sûr, mais moral ? Hein ? Franchement ? Les avions de chasse, c’est moral ? Les centrales nucléaires ? Tout le monde s’en fout de la morale, François, je te promets. La morale, c’est chacun pour son cul, mec, et le nôtre, il est sacrément à nu, si tu vois ce que je veux dire. Maintenant, si t’as des problèmes de conscience, fais du chichon bio, garanti sans OGM, fonctionnarise tes putes, garanties sans MST, protection sociale, retraite, et tout le toutim de gauche… En plus, tu verras que ça fera vendre !

<!-- SAUT2 -->

— Compiègne a voté non.

— Les cons. Bravo Compiègne.

— La frontière s’arrêtera juste au-dessus de Senlis.

— On t’emmerde Senlis. Senlis dans le neuf cinq. Ta mère la senlisienne de banlieue.

<!-- SAUT2 -->

— Ali ? Vous êtes Arabe.

— Non.

— Non ?

— Turc. Européen.

— Pas sûr que ce soit mieux vu en ce moment, par ici. Est-ce que nous nous sommes bien compris, Ali. Pas de violence. Pas de guerre de territoire. Pas de maltraitance. Et on paye ses impôts, chaque mois, rubis sur l’ongle.

— Comme une boulangerie.

— C’est ça Ali, une boulangerie. Avec des grosses miches dedans.

— Et l’état va cautionner ?

— Oui et non, il va fermer les yeux. Mais, oui, ça revient au même. Et il va encaisser les impôts. Trente-trois pour cent, on est bien d’accord, c’est le *deal*.

— On a un *deal*.

— Vous allez réussir à trouver les filles, Ali ? Il y a surtout des caissières, là-bas. Enfin des ex-caissières. Et des ex-fermières et des ex-ouvrières. Vous allez pas trouver beaucoup de dentelle dans les placards, si vous voyez ce que je veux dire. Et il n’est pas question d’importer, il faut faire travailler la main-d’œuvre locale.

— Vous inquiétez pas, monsieur François. Mettre la caissière en dentelles, c’est mon job. On sait faire ça à Ankara. On sait faire ça à Leningrad. On sait faire ça à Dakar. On sait faire ça à Hanoï. On sait faire ça à Lassigny. Y avait un truc de mode, là-bas, je me suis renseigné, c’est bon pour le marketing. Tu vois, y a toujours une culture locale sur laquelle s’appuyer. Y a toujours un point de départ. Et à l’arrivée, comme on dit, un beau bordel !

Ali respirait la bonhomie, la simplicité.

— Il y a avait une industrie cosmétique de luxe, oui. Un fleuron national. Enfin français, parisien maintenant. Délocalisé.

— Ils vont revenir à Lassigny, tes Parisiens, je vous le dis, moi. Et de partout dans le monde, on va refaire voler des avions à Beauvais.

— Si vous arrivez à faire s’arrêter les trains à la Gare des Betteraves, je vous ferai décorer !

— La Gare des Betteraves ?

— Une gare TGV au milieu des champs au milieu de nulle part où tu peux mourir de faim si t'as oublié ton sandwich.

— Tu vas avoir des trains plein ta Gare des Betteraves, parole. Ils ont laissé les rails, quand même ? Ali rit à nouveau.

<!-- SAUT2 -->

— L’histoire des Juifs, c’est une blague ?

— Il y a un projet.

— Un projet, comme quoi ?

— Comme céder l’Aisne à Israël.

— l’Aisne ? c’est un tiers du territoire !

— Un tiers qui sert à rien, on a rien dans l’Aisne, que tchi. La misère et l’ennui. Et avec l’exode, il y a de la place dans le reste de la région, du pays quoi. Pour tout le monde. À l’aise.

— Mais les gens vont pas vouloir bouger, tu sais comme ils sont. Casaniers. Un quart d’entre eux n’est jamais allé à Paris. Tu leur parles d’aller prendre le métro, ils te sortent un certificat médical.

Le troisième homme, qui s’était tu jusque-là, s’anima soudain. François avait oublié son nom. Il avait un accent du sud. Il avait une gueule de mafieux. François se demandait ce qu’il foutait à cette réunion. Ce qu’il foutait encore en Picardie. Un vautour. Il se tenait mal. Il parlait mal.

— Putain, ça c’est vrai que les mecs là-bas sont pas dégourdis. T’as remarqué que dans les villages, ils nomment les rues avec les noms des villages d’à côté ? Putain, c’est pour se rappeler la direction ! Les mecs sont tellement pas habitués à bouger de chez eux qu’ils ont peur de se paumer en allant au village d’à côté ! Il rit grassement, seul. Les trois autres le regardaient. Faut dire, ils ont pas d’autres noms à donner à leurs rues. Il y a pas de Picard célèbre. Tu connais des Picards célèbres toi ? Qui remontent pas à la préhistoire, des récents ? Des joueurs de foot ? Des chanteurs ? Ben voilà, y en a pas.

— Il y a Jules Verne. Kamini ?

— Kamini, le cinéaste ? Il est pas picard, il est noir. Jules Machin, jamais entendu parler. Sont pas entrés dans l’histoire non plus ceux-là. Et puis, comme les mecs bougent pas de leur bled, et qu’ils vont pas à l’école, ils connaissent personne. Juste le nom de la boulangère et du village d’à côté. Et encore, ils savent pas y aller sans se paumer ! Il rit à nouveau.

— François, s’ils veulent pas bouger, ils pourront rester habiter avec les Israéliens.

— Ils sont d’accord ?

— Un dédommagement est prévu. Plus ou moins.

— Mais qu’est-ce qu’ils vont en foutre, tes Israéliens, de l’Aisne ?

— Je sais pas, je m’en fous. Des kibboutz. C’est toujours mieux que le désert, l’Aisne. Enfin, pas forcément mieux, mieux, mais y a pas de problème d’eau, tu vois, et les Arabes ont déjà été virés. Et puis, ils auront pas la nationalité, ils louent juste. C’est tout bénef.

— Ils louent ? Tu veux louer un département ?

— Pourquoi pas, t’es proprio, toi, François ? Ben, voilà, eux non plus, ils seront pas proprios. Un bail de dix ans. Renouvelable. Hausse du loyer comprise. On est tous de passage.

— Que va dire l’Europe, merde, les trucs sur l’immigration, le Pacte, on s’en fout ?

— Tu veux qu’ils disent quoi ? C’est des Juifs.

<!-- SAUT2 -->

— François, dis-toi qu’on rénove juste l’agriculture picarde, au fond. Au nord, on plante un peu de chanvre, pour la recherche pharmaceutique, et puis au sud, on plante des bordels, enfin, des centres de détente pour adultes. Et voilà, à l’est, on plante des Juifs. C’est de la diversification agricole.

— On va se mettre à la rotation des cultures ? Tous les trois ans on tourne ?

— François, écoute, pour la première fois, j’ai l’impression qu’on peut s’en sortir. Les gens vont le sentir. Leurs perspectives ont changé tu sais, tout a changé. Les gens vont voir l’espoir. Ça va les *booster*, ils vont y aller. Quand le pognon va commencer à tomber. Le reste, le Pacte, ton programme, c’est de la littérature, tu comprends ? Tout est une histoire de pognon qui tombe. Ici, il vont acheter notre histoire. À côté, en France, on va leur rendre service. On va vider leurs banlieues merdiques de leurs trafics merdiques. Ils sont d’accord, tu comprends, le Président, il suit.

— Il suit ?

— Il m’a promis la neutralité. Pas de police française dans nos basques concernant nos spécialités. Pas de répression sur les Français. Ils vont même nous aider à contrôler leurs caïds, ceux qui voudraient faire du grabuge ici. Tant qu’on sort pas des clous. Pas de militarisation, pas de terrorisme, pas de politique, pas de connivence internationale, pas d’entrave. Putain, le Président suit, François, tu comprends ? On va s’en sortir.

— Des putes et du chichon.

— Et des Juifs.


## Arrangements

Ils avaient fait l’amour à deux reprises, intensément, comme font les amants occasionnels. Elle était étendue sur le lit de la chambre, une petite chambre, un lit étroit, presque une chambre d’étudiant. L’appartement d’Édouard était toujours surchauffé, réflexe de Parisien. Sara était couchée sur le ventre. Le drap remonté jusqu’au milieu de ses cuisses soulignait les courbes fines de ses fesses, de son dos, de sa nuque. Elle avait les yeux fermés mais ne dormait pas, malgré l’heure très avancée. Le soleil se lèverait bientôt. Enfin, ce qu’il restait de soleil en ces derniers jours d’automne. Sara aimait garder les yeux fermés, faire semblant de dormir, faire semblant de faire semblant. Elle écoutait Eddy, malgré la fatigue.

Demain – tout à l’heure – elle se lèverait très tard. Sara travaillait quinze ou seize heures chaque jour, mais elle commençait tard ses journées. Elle ne mettait jamais de réveil, se levait généralement après dix heures, parfois après midi, elle prenait un petit déjeuner solitaire, Albert, au contraire très matinal, était déjà au labo depuis longtemps. Elle lisait en prenant son café. Pour l’actualité elle suivait Mediapart, le seul titre grand public resté à peu près libre selon elle, et encore elle ne regardait la plupart du temps que les titres, s’arrêtant parfois quand cela concernait la Picardie. Elle jetait un œil rapide aux blogs technos, un autre, résigné, aux batailles juridiques que menaient les dernières assos du net, celles qui étaient encore debout pour défendre une vision populaire du réseau, contre les supranationales qui se l’étaient approprié, qui l’avaient grignoté, petit à petit. Ensuite elle parcourait les articles scientifiques parus la veille, que lui avait sélectionnés Hal. Enfin, elle écoutait la synthèse de ses messages, également préparée par Hal, et validait les réponses qu’il avait préparées. Elle ne recevait plus que quelques mails chaque jour, et quelques messages sur des canaux de *chat* privés qu’elle fréquentait sous pseudonyme. Elle était en train de disparaître d’Internet. Avant, elle communiquait excessivement via ses réseaux sociaux, elle avait calculé alors qu’une vie n’aurait jamais suffi à passer un simple quart d’heure *in real life* avec chacun de ses contacts. Boire un verre, savoir qui ils étaient. Mais depuis qu’elle travaillait au sein du laboratoire d’Albert Roman elle avait tout arrêté. Elle était coupée du monde réel par l’intensité de son travail, coupée de la société numérique par le secret de ce travail. Elle aimait ce qu’elle faisait par-dessus tout, elle y consacrait sa vie, elle s’y donnait corps et âme. En revanche ce secret, elle l’exécrait. Il lui gâchait une partie du plaisir. Elle aurait voulu, comme avant, chaque jour échanger ses pensées avec le monde, exposer son travail, ses minuscules avancées, ses immenses doutes, parler de rien, mais avec emphase, parler d’elle, être connectée, être au monde. Être, ne pas seulement faire.

<!--SAUT1-->

Cela avait été une de ces journées de sale temps, les bourrasques avaient fait craquer les murs du labo et la pluie battante n’avait cessé de frapper le tempo sur les toits métalliques. Cela avait été une de ces journées où la Picardie donne vraiment envie de vivre ailleurs. Une de ces journées où Sara avait eu besoin de distraction. Cela lui arrivait deux ou trois fois chaque mois, besoin d’autre chose, durant quelques heures, besoin de s’éloigner un peu de son travail. Parfois c’était avec son mari Albert qu’elle passait une soirée, ils faisaient préparer un bon repas, ils jouaient quelques points sur le billard, ils regardaient un film, généralement un vieux film qu’ils avaient déjà vu cent fois. Un Lelouch, un Tarantino, un Eastwood. Histoire d’être sûrs. Parfois c’était avec Édouard qu’elle s’évadait le temps d’une nuit.

Sara était une très belle fille, plus belle encore au cœur de sa trentaine. Elle était intelligente, elle l’avait toujours été, mais plus encore que tout cela, elle était vive, incroyablement vive. Et ce n’était plus si courant, les gens vifs. Les gens donnaient l’impression de dormir. D’attendre plutôt. Et quand ils bougeaient, ils semblaient courir après des courants d’air. Ils gaspillaient leur puissance, ne construisaient plus rien, n’avaient plus envie. Sara, elle, vivait. Elle dépensait toute son énergie dans sa quête professionnelle – car il s’agissait d’une quête – mais elle aurait pu tout aussi bien le faire dans une liaison amoureuse. Elle aurait pu trouver un homme aussi vivant qu’elle, il devait en rester, le capter, le combler, être heureuse, avoir des enfants, s’en occuper, sérieusement, les élever. C’était sûrement possible. Loin des écrans, loin d’une IA chaque jour un peu plus envahissante, loin d’une voix synthétique qui était la première chose qu’elle entendait chaque matin, la dernière chaque soir. Sauf quand elle passait la nuit chez Édouard. C’était sa voix que Sara aimait. Pas tant celle, un peu faussée, qu’il transformait malgré lui pour mieux – croyait-il – la séduire quand il lui parlait, que la voix grave et sérieuse qu’il employait pour traiter tardivement ses obscures affaires.

<!--SAUT1-->

Il était plus jeune qu’elle, presque étudiant, presque adolescent, beau comme un avatar Facebook. Elle l’avait déniché un soir qu’elle traînait au laboratoire. Elle aurait été bien en peine d’organiser une aventure avec quelqu’un de l’*extérieur*, elle n’allait jamais à l’extérieur. Il travaillait au niveau moins quatre, il était chargé de mener des transactions illicites. Ça lui donnait, pour Sara, un côté mauvais garçon, mafieux, romantique. Il fallait tout de même l’imaginaire de Sara pour faire de ce geek qui ne quittait pas son ordinateur un *rebel without a cause*. Le travail d’Eddy consistait principalement à échanger des messages, souvent avec une certaine Alice. Il portait un casque, Sara n’entendait que la moitié de la conversation, et dans son demi-sommeil elle ne comprenait pas la moitié de cette moitié. Elle ne cherchait pas à comprendre, juste à entendre, comme une chanson en anglais qui emporte bien que l’on n’en comprenne que quelques mots épars, que l’on n’arrive pas à en reconstruire le sens global.

<!--SAUT1-->

— Tu ne dors pas, Sara, je t’entends bouger.

— Si.

— Tu veux que je te rejoigne ? Je peux faire une pause.

— Non, continue, j’aime bien t’écouter.

— Tu sais que tu ne devrais pas, c’est confidentiel tout ça, Albert ne serait pas content.

— Non, en effet, Albert ne serait pas content.

Elle sourit. Sara ne connaissait pas le sentiment de culpabilité. Pas plus avec Eddy que lorsqu’elle regardait parfois ses *sujets* débarquer sur la plage. Pas non plus lorsqu’elle cherchait chaque jour à livrer les secrets de la mort aux machines, lorsqu’elle cherchait à transformer la vie des hommes, sans leur demander leur avis. Est-ce qu’ils avaient un avis, les hommes ? Depuis quand ?

— Tu l’as déjà rencontré cette Alice ? Elle est jolie ?

— Ne fais pas semblant d’être jalouse, tu n’es pas très crédible. Mais, c’est gentil. Alice, est un homme, enfin un neutre.

— Tiens. Je ne savais pas qu’il y en avait en Picardie.

— Pourquoi pas ?

— Que c’était autorisé.

— Je sais que tu ne suis pas beaucoup l’actu, mais depuis l’indépendance, tu sais, tout se libère ici. Tu devrais sortir Sara, les rues de Saint-Valéry commencent à valoir le coup d’œil ! On y croise de drôles de filles et de drôles de gars. Tu savais que le cannabis était légalisé ? Enfin toléré, je ne sais plus comment ils ont dit, bref, ça fume sévère aux abribus. On en cultive à cinquante kilomètres d’ici, je suis allé visiter les champs. Tu voudrais que je t’y emmène ?

— Je n’ai pas trop le temps en ce moment, mais envoie-moi une photo la prochaine fois, tu veux bien ?

Elle s’en foutait.

— Je me demandais, s’ils se mettent à tolérer aussi les amphets, tu crois qu’on passera à autre chose au labo, à l’héroïne ?

— Je ne sais pas, c’est Albert qui pilote ça.

— Mais toi, tu serais OK ?

— Oui bien sûr. Pourquoi pas ? Je ne vois pas quel problème cela pourrait poser à mon niveau.

— Tant mieux, c’est ennuyeux d’avoir des problèmes à son niveau… Je dois y retourner, Alice est en ligne.

<!--SAUT1-->

— Salut Alice.

— Yo Eddy.

— Un point sur notre compte s’il te plaît.

— Hé mec, je suis pas une interface vocale, tu peux me parler normalement, il y a de la viande derrière l’écran. On est pas des machines.

— Désolé Alice. On a dû recevoir des virements en crypteuros et digicrystals, tu as pu gérer ?

— Dis, Eddy, tu sais ce que se racontent deux êtres humains quand ils se parlent pour la première fois de la journée ?

— Non, dis-moi.

— Ben tu vois c’est ça le problème, tu ne sais pas, Eddy. Faut que tu apprennes.

— Trois ou quatre cent mille euros, selon les cours.

— Et les coms. Je leur ai fait faire trois fois le tour du réseau Tor à tes bitcoins, mais tu sais ce que c’est, ça crisse un peu dans les virages, ça laisse de la gomme, ça s’use un peu quoi. Deux cent soixante quinze mille, nets, après frais de minage et de transaction. C’est distribué sur près de trois mille transactions espacées dans des blocs différents. Je t’ai converti un dixième en piccoins, histoire de nous soutenir un peu le marché, faut aider les jeunes qui se lancent, pas vrai ? Dis-toi qu’une cryptomonnaie locale c’est moins volatil, même si on n’est pas encore solides solides, on est backupés par l’Europe, *zero risk*. Si t’y crois. Et j’ai viré les centimes à Emmaüs et aux Restos, *social tax*. *My pleasure*. 723 comptes *in fine*. Je t’envoie les clés dans dix minutes. Ça mouline encore un peu sur nos serveurs.

— D’ac. J’attends.

— Ça nous laisse le temps de papoter. Allez, raconte-moi ta soirée d’hier.

— Hier… j’ai maté une série.

— C’est pourri ça Ed. Bon, c’est moi qui commence. Déjà hier, j’ai pas baisé. Deux cent cinquante-troisième jour d’abstinence mon pote. Respect ? Avec Bob et Charlie, on est allés à un festival reggae à Tilloloy. Improbable quand même, un hologramme grandeur nature de Bob, je parle de Marley là, au milieu d’ex-champs de betterave en reconversion façon ganja. Notre nouveau gouvernement investit massivement dans la *culture*. Pure soirée. Avant, on s’était monté une antenne relais sur une église à Laon. C’est joli le vieux Laon. Sur trois rues. Après, c’est cité-cambrousse et mornes plaines à perte de vue. Des branleurs voulaient nous taxer notre matos. J’ai cru que Bob allait se les faire. Je t’ai déjà parlé de Bob ?

— Oui. Ton associé.

— C’est ça. Cent kilos et un côté rien à foutre de la vie, à faire douter même une bande de crevards patentés. Tu sentais que les mecs savaient pas quoi faire pendant qu’il descendait de son échelle en gueulant. Ils l’insultaient pour se donner une contenance, mais ils flippaient leur race. Finalement, c’est la police municipale qui a débarqué. Ils ont des drones tous neufs, cadeaux d’Amazon. Tu sais qu’ils ont installé une plateforme logistique *human free* là bas ? Notre président est tout content. Perso, je trouve ça débile, mais un type qui fait pousser de l’herbe et te fait revivre Bob Marley, tu lui pardonnes, pas vrai ? Alors t’as fait quoi Eddy, hein ?

— Ben, rien je t’ai dit.

— Putain, invente Eddy, raconte-moi une histoire. Tu crois que j’étais vraiment sur le toit d’une église, avec mon pote Bob qui faisait face à une horde de cailleras, et qu’on a été sauvés par des robots ? Putain de génération réseaux sociaux, vous croyez tout ce qu’on vous raconte et vous savez plus rien inventer. Comment c’est possible, hein ? Qui invente les histoires aujourd’hui ? Allez Eddy, un effort quoi…

— Tu as tort de me raconter des histoires, même pour rire, c’est sérieux notre job. Comment je te fais confiance après ?

— C’est ça votre problème, vous confondez tout, la confiance, la vérité, le business. Tu me fais confiance parce que je te dis la vérité ? Tu connais pas mon nom, pas ma gueule, tu sais même pas si je suis un homme ou une femme. Si ça se trouve je suis un Chinois et je te parle depuis ma chambre à travers un filtre. Si ça se trouve je suis une IA, mec. Je t’enfume depuis le début.

— Les IA ne savent pas faire ça. Enfumer, comme tu dis.

— Tu jures ? Allez, fais-moi passer un test de Turing. Je joue à l’homme qui se fait passer pour une femme. Pose-moi des questions !

— Alice, on en est où ? Quelqu’un m’attend.

— À cinq heures du mat’ ? On est dans le même fuseau horaire ? C’est toi le Chinois ? Ou alors c’est toi l’IA. Tu viens de te trahir *Master Control Program*. Allez, je te teste. Es-tu un homme ou une femme… Tu es avec un homme ou une femme en ce moment ? Bon, allez, je le sais, tu baises la femme de ton patron.

— Pourquoi tu… comment…

— Ta caméra. Elle n’est pas masquée.

— Tu… tu me regardes ?

— Mais non, je déconne Eddy. Ton PC est plus sécurisé qu’une culotte de pucelle paraplégique. J’ai essayé, bien sûr. Histoire de vérifier que personne n’écoute aux portes de derrière quand on cause tous les deux. Pas pour te regarder, je m’en fous de tes frasques en vrai, surtout qu’un mec qui regarde des séries le soir… Tu féliciteras ton adminsys, c’est un cador. Le mec qui te pirate, je lui prête allégeance à vie, parole. Allez, détends-toi, on y est. *Pull* mes modifs, j’ai *commit* les clés de tes nouveaux comptes. Te v'là à la tête de plein de nouveaux petits cryptosous mon frère, tout beaux, tout neufs, tout rutilants, fais en bon usage, le monde te regarde. Je déconne, je te dis, personne te regarde, éclate-toi avec la femme du boss.

<!--SAUT1-->

Sara dormait presque à présent.

— Qui c’est qui nous regarde, Eddy ?

— Personne Sara, personne ne nous regarde.

<!--SAUT2-->

— Sara ?

Albert attendait à côté du poêle presque éteint, il frissonnait légèrement, sans même s’en rendre compte. Le cendrier était plein. Il ne restait que quatre cigarettes dans le paquet de Camel, retrouvé *in extremis* dans le vide-poche de son 4x4 après avoir retourné tous les tiroirs de la maison. Il tenait un livre à la main, dont il n’avait pas tourné une seule page depuis qu’il s’était assis dans son fauteuil club, pour lire tranquillement s’était-il dit, pour attendre Sara, depuis le début de la nuit, depuis hier.

— Bonjour Albert, tu ne dors pas ? Tu n’aurais pas dû m’attendre, je t’avais dit que je ne rentrerais pas.

— C’est douloureux la liberté Sara, parfois, tu sais. On s’y sent seul.

— Oui, je sais. Il faudrait essayer la prison pour voir. Mais je préférerais attendre encore un peu. Ça te va ?

Elle aurait dû éprouver de la compassion. Mais, elle était certaine au contraire de lui rendre service. *Ça fait peut-être mal au bide, mais c’est bon pour la gueule*.

<!--SAUT1-->

Sara avait choisi Albert Roman par admiration au début, par affection, par simplicité finalement. C’était un bon compagnon. En théorie elle aurait préféré quelqu’un qui la fît sortir un peu de son quotidien, Albert en cela était ordinaire, au sens premier, il était bien entendu le plus exceptionnel qu’elle eût pu trouver dans son ordinaire. Elle aurait préféré un canon, un jeune acteur, une sorte de star, peut être un poil macho, fier de son corps. Un sportif. En théorie. En pratique elle n’avait pas de temps, et surtout pas d’énergie à consacrer à un nombriliste mal fini, à consoler les soirs de doute. Elle n’avait pas le temps de faire la maman. Elle n’aurait pas le temps pour un enfant.

<!--SAUT1-->

Mais Sara avait surtout choisi Hal. Elle avait choisi la machine, ne se distinguant en cela que peu de ses contemporains, qui sacrifiaient chaque jour un peu plus de leur temps de vie aux terminaux. Ils étaient, disait-elle, en phase terminale.

<!--SAUT1-->

Échanges lapidaires et insignifiants sur les réseaux sociaux, photos banales, quotidiennes, tellement inanimées, captures d’autres écrans de plus en plus souvent, jeux addictifs, consultation permanente d’informations venues du monde entier mais n’allant nulle part, circulant comme un vent dont il faudrait remonter la source pour en comprendre le sens, mais si violent que personne n’essayait même plus. Consommation frénétique en ligne, pornographie et violence par procuration quotidienne. La bouche rivée au micro, les doigts à l’écran, oreillette et lunettes de réalité augmentée. Le terme de réalité *augmentée* synthétisait à lui seul le renversement qui s’était subrepticement opéré. La coupure d’avec la réalité était devenue une réalité augmentée. Parler avec des inconnus de sujets sélectionnés, inventés par des machines, c’était une réalité augmentée. Regarder des cartes à la place de paysages, une réalité augmentée. Incarner des personnages imaginaires dans des mondes virtuels, réalité augmentée. La réalité augmentée, c’est discuter des mérites de la vie en fauteuil roulant pour oublier le plaisir de marcher. La réalité augmentée, c’est une forme pathologique de la paresse.

Ce n’était pas le génie des marketeux qui était en cause, c’était celui des machines elles-mêmes, le génie technique, le bon génie qui exauçait vos trois vœux. Rien foutre. Plaisir immédiat. Pas voir le temps passer. Ce n’était pas de la manipulation, il n’y avait pas de dessein, pas de grand complot, les gens étaient conscients, éduqués même. Ils le faisaient exprès. Ils collaboraient, la fleur au fusil.

Sara était lucide, mais elle avait choisi sa voie, il y avait longtemps déjà, pendant ses études d’ingénieur. Elle nourrirait la bête, lui ferait pousser de nouvelles têtes. Mais elle croyait, c’était sa religion, sa foi à elle, que les têtes finiraient par se bouffer entre elles et que des hommes nouveaux – peut-être supérieurs, si ça voulait dire quelque chose – renaîtraient par là. C’était une mythologie un peu foireuse, bâclée, mais pour Sara c’était suffisant pour le moment, elle aurait le temps de mettre de l’ordre plus tard. Elle trouverait le temps. Quand elle aurait fini ce qu’elle avait à faire pour le moment. Elle avait choisi Hal.

<!--SAUT1-->

Bien sûr, elle ne voyait tout cela que comme une parenthèse. Le labo rutilant, les crédits illimités, l’incroyable développement de son IA, ses échanges avec Albert, son amant, ses balades sur la plage, ses constructions et ses évasions, tout n’était que sursis. Le souffle d’anarchie qui se levait sur la Picardie, Édouard complotant dans sa chambre, Alice et ses piccoins échappant à la finance internationale, tout ça finirait certainement derrière des portes bien closes, avec ce qu’il faudrait de flics, de juges, d’ordre. Sara n’aimait pas les ordres, elle n’aimait pas l’ordre, alors pour le moment, elle savourait le bordel ambiant, et elle y mettait sa touche, coûte que coûte. Les insomnies d’Albert faisaient partie du tableau, c’était comme ça. Des effets de bord.

<!--SAUT2-->

— Albert ! Je ne savais pas que tu venais. On ne me dit rien ! Il sourit. T’as une sale mine. Mal dormi ?

— Bonjour François.

— C’est Monsieur le Président maintenant ! sourit-il à nouveau. Tu ne bois rien ? Garçon, une coupe pour mon ami Albert Roman.

— Tout de suite Monsieur le Président.

— Tu vois, je te le disais ! il sourit encore.

Tout ça lui avait fait du bien finalement. Président d’une république même un peu pourrie, c’était quand même autre chose. De toutes façons, il était grillé à Paris depuis belle lurette, il n’aurait pu que végéter si la Picardie était restée en France, condamné à des réunions miteuses avec des neuneus pour parler ouverture de crèche ou déviation de départementale. Alors que là, c’était dorures et limousines, grands crus et jets privés. Il rencontrait des chefs d’états, des vrais, des Russes, des Argentins, des Iraniens. En quatre mois il avait fait le tour du monde plusieurs fois. Il avait été reçu à l’Assemblée générale des Nations Unies la semaine dernière. Premier président de l’histoire d’un territoire exclu par un autre, il avait fait, avec un discours misérabiliste à souhait, assez forte impression. Autant les mêmes ne trouvaient rien à redire quand une entreprise faisait du ménage parmi ses ouvriers inutiles ou ses cadres aigris, autant virer une région entière, ils trouvaient que ça avait un côté cavalier, limite choquant. Un classique des premières fois. Surtout quand ça ne passait pas par les voies prévues à cet effet.

— Alors l’ONU ? demanda Albert une fois goûté son champagne, pas très bon, mais bio et produit en Picardie, lui avait précisé le serveur. On ne pouvait pas tout avoir.

— La classe, Al. Tu sais qu’ils ont comparé la Picardie à la Palestine et au Tibet ? Ça te donne une idée du niveau de conscience politique… Si tu fais ta pleureuse t’es rangé Palestinien, *punto*. Qu’on t’ait forcé à avoir ton pays ou qu’on t’ait piqué le tien, c’est du pareil au même. Après, je m’en tape, j’étais pas là-bas pour refaire le monde, j’aurais bien été le seul d’ailleurs, j’ai assez à faire ici. J’ai fait carton plein. J’ai obtenu un accord tacite pour la drogue. Enfin, douce, seulement. Voyez ça avec la France quand même, ça reste un peu chez eux. Enfin, vous comprenez. J’ai dit « oui papa », on est mignons, mais on doit rester sages. J’ai eu le droit à des regards compatissants, tapes sur l’épaule, embrassades pour les plus hardis, quelques dollars, de quoi voir venir, et *stay strong guy*.

— Et en interne ça donne quoi ?

— Ça donne que Ruffin arrête pas d’ouvrir sa gueule et que les autres ont pas encore compris ce qui leur arrive. Quand tu passes de la commission collèges du département de la Somme au Ministère de l’Éducation Nationale, faut changer de coiffure, tu vois ? Après, au quotidien, c’est surtout comment poster des gars qui vont bien aux endroits qui vont bien. Le problème c’est qu’il y a eu de la perte, les rats ont quitté le navire, il ne nous reste pas que les meilleurs. Toujours pas intéressé Al ? À la recherche j’ai une vraie quiche, je te libère la place demain. L’autre j’en ferais un ambassadeur de la Papouasie-Nouvelle-Guinée qu’il risquerait encore de s’emmêler les crayons. Si tu savais le nombre d’ambassadeurs qu’il me faut, je sais même pas si j’ai assez de gars qui parlent anglais sur tout le territoire !

— Et de filles. De gars et de filles, François, si je peux…

— Tu peux.

— Tu devrais arrêter de déconner sur le petit peuple, tu as besoin de tout le monde ici, tu ne peux pas te permettre de tourner avec trois énarques et quatre polytechniciens comme en France. Va falloir trouver un autre modèle pour s’en sortir. Peut-être même quelque chose qui ressemblerait à une sorte de démocratie. Je dis ça, je ne veux pas être irrespectueux. Mais, c’est presque une chance ce qui nous arrive… Et non, merci, je ne suis pas intéressé pour gouverner, ce n’est pas ma place. Crois-moi, je vais faire beaucoup plus pour cette région depuis mon labo que dans un de tes bureaux retapissés à la va-vite.

— Toujours aussi cash ! Mais t’as raison, faut que j’intègre les gonzesses et les neuneus, sinon on sera jamais assez nombreux. Je plaisante. En même temps, tu veux que je te raconte le conseil de ce matin ? Le ministre de la culture, non rigole pas, j’ai un ministre de la culture, le mec a cru qu’il était nommé à l’agriculture au début, enfin c’est ce que m’a dit mon directeur de cabinet, tu connais Jean ? Si ça se trouve il a dit ça pour déconner, tu connais Jean, bref, le ministre de la culture avait fait mettre à l’ordre du jour : « Choix du nouveau nom du pays ». Ruffin balance d’entrée Picardie-Pas-De-France. Tout le monde le regarde. Il ajoute qu’il y avait bien le Nord-Pas-De-Calais. Le ton était lancé, je m’enfonce dans mon siège. On a eu Plus-De-La-France, Pue-La-France, La-France-On-T-Encule… Je sais plus lequel finit par balancer Néopicardie, tout doucement, comme s’il s’excusait d’avoir une idée qui pourrait marcher. Ça fait moderne, on trouve ça pas mal, et la Culture ajoute, Neo en plus c’est le mec de Matrix. Finalement, je me dis qu’il a pas été si mal choisi, en tous cas il fait des efforts. Bon après, un autre, je sais toujours pas qui, j’étais là sans être là en même temps, tu sais que j’ai appris à faire des micro-siestes avec les yeux ouverts ? C’est salutaire. Bref, l’autre dit que ça lui fait penser à néonazi. Ça doit être le cas de tous les mots en néo, le gars a été traumatisé par *Nuit et Brouillard*, tu lui dis néologisme il pense chambre à gaz, néolithique il pense ratonnade, tu me diras pour le coup… Bref, à la fin, c’était débile mais on voyait tous des Allemands en uniforme quand on disait Néopicardie, même moi qui dormais, et tu comprendras que dans notre cas, on ne pouvait pas se permettre de laisser le doute planer. On a essayé avec Noépicardie, genre l’arche picarde, mais on n’est pas des couples de pingouins et le déluge on donne déjà assez souvent. On est resté sur Nouvelle-Picardie un moment, mais avec les précédents Nouvelle-Calédonie et Nouvelle-Corse… tu vois. Bref, on a décidé de mandater un cabinet de com. Et toi, quoi de neuf ? Sara ne t’accompagne pas ?

— De moins en moins. Elle ne quitte plus beaucoup son IA.

— Ha ? Et ça avance votre truc ? Tout se passe comme tu veux ?

— Oui, on avance. On a eu la visite de la DGSE avant-hier.

— Je sais, Charron m’avait prévenu. Tout s’est bien passé, n’est-ce pas ?

— Parfaitement. Merci à toi.

— C’est normal. Et puis, ça va servir à quelque chose à la fin vos histoires ? Ça va profiter au pays ? Parler avec les morts c’est une drôle d’idée, mais t’es le type le plus intelligent que je connaisse, alors je te fais confiance Albert.

— Merci, Monsieur le Président, c’est un honneur d’avoir votre confiance. Albert était sincère, il croyait en lui autant qu’il pouvait croire en un homme politique. Tu as encore une minute ?

Le Président jeta un œil à droite et à gauche, il aperçut les regards des prétendants, qui attendaient en boulottant des petits fours que se libère la place pour pouvoir lui faire leurs génuflexions. Il aimait ça, ce pouvoir, cette revanche. Il aimait aussi beaucoup Albert Roman, c’était un des rares amis qui lui restait.

— Je suis dévoué à mon peuple. Parle, citoyen.

— J’aimerais voir quelqu’un de chez toi, de confiance. J’ai une certaine proposition à vous faire.

— Dis-m’en plus, tu m’intrigues.

— Je ne suis pas sûr que tu veuilles savoir, François, dans ta position.

— Vas-y, je te dis.

— J’ai de l’argent à blanchir.

— Carrément. Les métaphores tu connais pas ?

— Désolé. J’ai parlé trop fort ?

— Non, c’était pour la forme. Je m’en fous, je suis chez moi ici. Personne mouftera. Mais quel rapport avec nous ? On est pas HSBC.

— L’idée est que je te fasse passer des fonds, une grosse quantité d’argent, et que tu me le reverses à 50/50. Ce sera ma contribution au pays.

— Grosse comment ?

— Vraiment grosse. En milliards.

— Comment…

— Tu lances une grosse campagne de financement participatif, internationale. « Soutenez la Picardie libre » par exemple, on a préparé plusieurs scénarios. Il te faut un cador de la com en ligne, on a préparé une liste. Dès que ça démarre, nous, on te fera des transferts anonymes en cryptomonnaies. Des tas. Et toi, petit à petit tu augmentes tes campagnes de financement public pour notre labo de recherche, en bons euros picards. On t’a préparé des appels d’offre ciblés, sur cinq ans. Tu vas récupérer du liquide, et en plus la campagne te fera une pub positive. Sans compter que ça pourrait marcher pour de vrai et faire levier.

— C’est beau la science. Tu vas voir ça avec Charron, on gère en direct, c’est trop gros. Sara est au courant ?

— Pas vraiment. Je présenterai mon responsable des opés à Charron.

— Ça y est, tu commences à parler comme un vrai gangster. Tu es sûr pour ce poste de ministre ? Je peux aussi te libérer les finances, j’en ai un qui sait bien compter sur ses doigts, mais si tu connais ta table de 3, tu le manges.

Et il rit de bon cœur.

Le pays n’avait pas de nom, pas ce qu’on pouvait appeler un gouvernement, c’était un bordel pas croyable dans les administrations, mais il y avait des mecs, et des filles, qui bougeaient, c’est de la base que ça repartirait. Du peuple, Albert avait raison. *Fuck* les règles, *fuck* la morale, *fuck* la politique, fallait libérer les gens, les laisser s’exprimer, les laisser construire ce qui leur passait par la tête. Les laisser délirer plein pot et voir ce qui allait en sortir. Les laisser relever les morts, câbler des églises, se balader à poil dans les rues, faire des courses de bagnole au bord des falaises si ça les chantait. Fumer dans les restos, bouffer gras, pisser sur les fruits et légumes. Ça faisait combien de temps que les gens avaient arrêté d’être insouciants, de profiter de la vie, de faire ce dont ils avaient envie ? Ça faisait des décennies de crise de ceci, de risque de cela, de cancer, de chômage, de récession, d’insécurité… Il allait les laisser s’éclater un peu, se faire plais', comme des ados. Il allait leur rendre ce pays. Si ça foirait il s’en foutait, qu’est-ce qu’il avait à perdre ?

François devenait un putain d’anar et ça le faisait marrer.

<!--SAUT2-->

Il régnait une certaine tension dans la salle où étaient réunis tous les chercheurs des niveaux moins deux et moins trois, une cinquantaine de femmes et d’hommes, assis dans le grand amphithéâtre. Sara se tenait debout derrière le pupitre de présentation, bien droite, un peu raide même dans la robe qu’elle s’était choisie, peut-être un peu trop habillée, mais pour une fois qu’elle voyait du monde… En dehors de cela, elle avait l’air serein, elle n’était pas du tout nerveuse. Elle n’était jamais nerveuse, elle n’en avait jamais compris l’utilité. Elle attendait qu’Albert lui donne la parole, les yeux baissés vers les notes affichées sur sa console, ses pensées vagabondant entre sa robe et les résultats qu’elle allait présenter.

Albert au contraire semblait préoccupé et cela influait sur l’ambiance générale. La DGSE française était venue lui rendre visite. On ne savait pas si c’était une sorte de visite de routine, la France souhaitait garder un œil sur la Picardie, ou bien s’ils s’intéressaient à ce qui se passait au niveau moins trois, aux travaux de Sara et son équipe, ou même s’ils avaient eu vent des activités du niveau moins quatre, que tous s’arrangeaient pour ignorer. À ce stade c’était l’hypothèse de la visite de routine qui dominait, Albert les avait reçus avec l’équipe de direction au complet, les avait assurés de sa collaboration, avait rappelé qu’il était très proche du ministère de la recherche et de la présidence, et que ces derniers étaient très proches de la France, que tout le monde était très proche et que tout allait pour le mieux dans le meilleur des mondes. Ils étaient repartis avec un petit tour du moins un, une brochure et une invitation à revenir quand ils voulaient. Albert semblait donc gérer la situation, mais il ne parvenait pourtant pas à masquer une contrariété diffuse, qu’il exprimait par des remarques maladroites, des sentences trop péremptoires, une brutalité peu en accord avec les habitudes prises au laboratoire.

Peut-être était-ce simplement Sara qui était à l’origine de son état fébrile.

<!--SAUT1-->

À chaque fois qu’elle présentait des résultats c’était un petit événement. Quelque chose remontait du moins trois, tout le monde voulait voir ce qu’il y avait dans la malle au trésor. Chacun avait sous les yeux la reproduction de l’article qu’elle leur avait transmis la veille. Il était essentiellement composé de formules mathématiques, du calcul de probabilités simples, niveau terminale, de graphiques et de résultats sous la forme de pourcentages. Certains l’avaient étudié jusque tard dans la nuit, les plus studieux, ainsi que les admirateurs de Sara. Tous étaient dubitatifs, car il y avait à la fois quelque chose de très simple dans la démarche utilisée et de très complexe dans l’interprétation qui en avait été faite par Hal. Les hommes avaient de plus en plus de mal à comprendre les résultats produits par les IA, tout en convenant que ceux-ci étaient de plus en plus brillants. Un fossé se creusait lentement, que les spécialistes mesuraient chaque jour, avec une gêne discrète, et que certains commençaient à mettre en évidence.

— Sara, c’est à toi.

— Merci Albert, bonjour à tous, ravie d’être parmi vous. Vous voyez j’ai sorti une belle robe à fleurs pour l’occasion.

Quelques rires parcoururent la salle. Finalement, cette histoire de robe, ça lui aurait permis d’ouvrir la conférence dans les règles de l’art.

— Pour ceux qui n’ont pas lu mon article, je coupe court tout de suite, on ne décrypte toujours rien du tout, vous n’êtes pas au chômage.

Nouveaux signaux de détente, elle allait pouvoir commencer.

— Sur vos écrans s’affichent les dernières données compilées par Hal, sur 42 échantillons. Comme je vous l’ai présenté la dernière fois, je n’ai toujours pas d’accroche pour identifier quoi que ce soit. Je n’ai pas avancé d’un pouce sur ce terrain. Vous non plus ? Pat, donc. En revanche, depuis un mois, j’ai décidé qu’à défaut de parvenir à apprendre à lire, on pouvait essayer d’apprendre à compter. Je me suis dit qu’au pire ça ferait diversion et que quitte à avoir des machines de stature internationale qui coûtent très cher à Albert et ses bailleurs secrets, autant les faire tourner.

Ses collègues souriaient, ils aimaient les exposés de Sara, son humour léger, sa façon d’aborder les problèmes, toujours surprenante.

— On a décidé, avec Hal…

— Que veux-tu dire par « avec Hal » ?

— J’expérimente le programme *machine ideation* du *joint lab* d’Amazon et Google, le nouveau labo de Suzanne Cauvin. Leur code permet à une IA de formuler des hypothèses originales à partir d’hypothèses données, sous la forme de variations ou de contrepoints, ce sont des métaphores musicales, je n’y connais rien en musique, mais l’idée est là… je m’égare, bref, Hal a formulé l’hypothèse *apprendre à compter* en contrepoint de *apprendre à lire*. On a déroulé ensemble pour aboutir à l’idée maîtresse de notre démarche. On va arriver aux résultats rapidement, juste quelques éléments encore.

Elle fit défiler les pages de son article, en ajoutant quelques commentaires qui soulignaient l’élégance de sa démarche intellectuelle.

— Voilà, c’est là. Donc nous avons décidé de dénombrer toutes les combinaisons possibles d’états quantiques pour en déduire la quantité d’informations représentable pour chaque trace. En gros le volume de stockage. Basiquement, on introduit des perturbations systématiques, et on énumère les variations d’état. On a utilisé des arrangements.

— Des arrangements, ce n’est pas un peu brutal comme méthode ?

— C’est très brutal ! Vous avez devant vous la plus grosse brute de Picardie. Après Joey Starr néanmoins, je me suis laissé dire qu’il s’était fait naturaliser suite à la légalisation de la culture du cannabis. Au moins il sait où vont ses impôts à présent. Spécial dédicace. Plus sérieusement, les autres méthodes, plus élaborées, qu’on a testées nous posaient de gros problèmes d’interprétation, j’en parle en annexe, alors on a juste simulé toutes les combinaisons possibles, avec des arrangements donc, un outil de lycéen, en effet.

— Hal a réussi à tout calculer ? En combien de temps ?

— Non, Hal est super fort, je l’adore – Joey Starr aussi d’ailleurs – mais il nous a fallu un peu d’aide. Quelques milliers de machines. Des grosses. On a utilisé des mineurs de cryptomonnaies. On a fait passer nos calculs pour des transactions, les gars se sont excités dessus comme des puces.

— Avec leurs puces plutôt.

— Tu as raison, avec leurs très grosses puces.

— C’est légal de faire ça ?

— Je ne sais pas, je ne pense pas, pourquoi poses-tu la question ? En tous cas c’est de bonne guerre, ça fait dix ans qu’ils nous pourrissent le réseau de virus pour faire calculer les autres à leur place. Bref, ça a marché, et ça donne… voilà, les résultats dont je voulais vous parler.

Sara commenta brièvement les chiffres et les graphiques qui constituaient le cœur de sa présentation. Un brouhaha de fond témoignait des interrogations soulevées.

— Pour conclure, je vous résume nos observations principales. Bien entendu Hal vous donnera accès aux données pour la phase de réfutation. Ce sont donc des conclusions temporaires, on ne les développera pas plus tant que vous n’aurez pas secoué le cocotier. Première observation, la quantité d’informations varie énormément entre les individus, dans un rapport de 1 à 170 si l’on prend les deux extrêmes. De 1 à 23 sur l’échantillon situé à l’intérieur de l’écart-type. On n’a pu trouver aucune corrélation avec les données propres aux sujets, notamment pas l’âge, qui aurait pu être un indicateur de quantité d’information. Seconde observation, il manque beaucoup d’informations par rapport à l’hypothèse « mémoire morte ». Un facteur dix puissance cinq. Au moins cent mille fois moins que prévu donc.

— C’est beaucoup…

— C’est pas mal en effet. On a proposé quatre pistes, mais ça reste à travailler. Soit l’information est compressée, hypothèse Dieu est économe. Soit il y a de la perte, hypothèse Dieu est distrait. Soit elle ne correspond pas du tout à l’enregistrement des mémoires humaines comme nous le pensions jusqu’à présent. Hypothèse on est à l’ouest. Soit elle ne correspond à rien du tout, c’est l’hypothèse on ne sert à rien, mais qu’est-ce qu’on se fend la gueule. Je vous propose de bosser sur les deux premières, dans les autres cas, on risque de perdre notre job.

— Merci Sara de cet exposé, toujours aussi vivant. Des questions ? Le ton employé par Albert semblait avoir retrouvé un peu de sa légèreté habituelle. La magie de Sara avait une nouvelle fois opéré.

<!--SAUT2-->

Sara était à présent seule au niveau moins trois, avec Hal. Elle était retournée travailler juste après sa conférence. Il n’y avait pas eu de question, elle les avait subjugués, une fois de plus. Elle ne les accompagnait jamais pour aller déjeuner, c’était trop tôt pour elle, a fortiori le lundi, ils avaient pris l’habitude d’aller manger tous ensemble une soupe de poisson chez Herman. Elle avait le sentiment que deux heures assise à ne parler de rien, ça ne valait pas deux heures. Elle avait le sentiment, presque un pressentiment, que le temps pressait, qu’il leur fallait aller au bout de leur recherche rapidement. Que son temps était compté.


# Deuxième Partie

(2044-2053)


## Interview

— Bonjour Madame Picard, merci de me recevoir.

— Bonjour Monsieur Lassereille. Bienvenue dans mon palace.

— Madame Picard…

— Vous pouvez m’appelez Sara.

— Sara, nous avons appris hier la mort d’Albert Roman. Toutes mes condoléances. Sincèrement.

Elle ne répondit pas.

Le webanimateur installa son matériel sur la table qui les séparait, une caméra orientée vers Sara Picard, en cadrage serré, une autre orientée vers lui, en champ un peu plus large, et une troisième sur un trépied, en champ large qui les embrassait tous deux, ainsi que la moitié de la cellule que la prison de Nanterre avait accepté de mettre à leur disposition. Il plaça un mini-écran de contrôle, une commande *one finger* à sa portée, et enfin un holoprojecteur. Il affichait pour le moment le logo de *En Picardie* ainsi qu’une photo de Sara, beaucoup plus jeune, alors qu’on ne l’appelait pas encore Madame et qu’un sourire éclairait en permanence son visage. Un sourire qu’elle n’avait pas retrouvé une seule fois ces douze dernières années. Un sourire oublié.

Elle ne reconnaissait pas son visage.

— Sara, nous pouvons y aller, c’est quand vous voulez.

Elle ne répondit pas. Elle hocha à peine la tête. Elle semblait concentrée, sur ce qu’elle allait dire aujourd’hui, sur sa version de l’histoire, que personne n’avait jamais entendue. Elle était également affectée, triste. De la mort de son mari. De douze années passées pour rien, perdues. Des années inutiles encore à venir.

— Bonjour à tous. Jean Lassereille pour animer l’émission *En Picardie*, retransmise par Mediapart sur le réseau mondial libre. Un grand merci à toute la communauté de *En Picardie*, qui, comme à l’accoutumée conduira cette interview d’une main de maître. Merci aux intervieweurs, aux traducteurs, aux fact-checkers, aux relais, aux serveurs miroirs, aux hackers qui nous protègent. Le réseau libre, c’est vous qui le faites vivre. Les salles de rédaction virtuelles ont bouillonné cette semaine, depuis que nous connaissons le nom de notre invitée. J’ai pu lire les sujets qui seront abordés, j’ai été impressionné par la qualité du travail préparatoire qui a été mené par la communauté, près de sept mille personnes ont apporté leur pierre. Et bien sûr, j’ai parcouru les logs de programmation de notre IA. MedIA est chauffée à bloc ! Je pense que les deux ou trois heures qui suivent nous conduiront à éclairer ce qui reste un mystère depuis douze ans. Je vous prédis de grandes révélations, de grandes émotions, peut-être même le début d’une page d’histoire. Nous sommes accueillis aujourd’hui par Sara Picard, depuis le centre d’incarcération spécial de Nanterre, en France. Bonjour Sara. Vous nous avez proposé de vous appeler simplement par votre prénom, merci de votre amabilité. Avant de prendre la première question, souhaitez-vous prononcer quelques mots ?

<!--SAUT1-->

La première face de l’image holographique permettait de visualiser les visages de Sara Picard et de Jean Lassereille, tandis que la deuxième montrait des visages des contributeurs de la communauté, ou des traces de leurs travaux. Les photos se succédaient par centaines. Un troisième écran proposait des textes courts et les titres de documents détaillés. Chaque internaute avait bien entendu ajouté ses propres écrans virtuels, chacun se situant au centre d’un polyèdre, généralement de douze ou vingt faces.

<!--SAUT1-->

Sara ne répondit pas. De nouveau, elle hocha faiblement la tête. L’émission démarra comme de coutume, par les questions élaborées par la communauté. Suivraient normalement les questions de MedIA, qu’elle construirait dynamiquement à partir des données dont elle avait été préalablement alimentée et qu’elle collecterait pendant tout le déroulement de l’interview. Le réseau libre était vraiment intéressé par cette confrontation inédite entre l’IA de Mediapart, l’IA la plus performante du média le plus influent d’Europe, et la mystérieuse *femme qui murmurait à l’oreille des machines*, coupée du monde depuis douze ans. Du moins sous cette identité.

<!--SAUT1-->

Sur l’écran central de l’holo la farandole des contributeurs s’arrêta sur une jeune femme d’une trentaine d’années, vêtue d’un tailleur dont la coupe stricte tranchait avec la couleur bleue électrique. Elle n’était pas jolie, elle arborait un air sérieux, presque solennel. Elle inspirait la confiance. Un judicieux choix d’avatar.

<!--SAUT1-->

[Solène21] Bonjour Sara, je suis Solène21. Vous n’aviez plus souhaité communiquer avec le monde depuis douze ans, et vous acceptez aujourd’hui pour la première fois depuis votre incarcération de donner une interview à notre communauté journalistique. Le décès de votre conjoint Albert Roman est-il à l’origine de cette décision ?

[SP] Oui. J’ai souhaité respecter sa décision de conserver le silence de son vivant. Maintenant ça n’a plus d’importance.

[Solène21] Vous et Albert Roman avez donc conservé toutes ces années des secrets ?

[SP] Oui.

[Solène21] Des secrets de nature scientifique, qui pourraient intéresser encore aujourd’hui les chercheurs ?

[SP] Oui. Même s’il est trop tard aujourd’hui.

[Solène21] Que voulez-vous dire, par « il est trop tard » ?

[SP] Il est trop tard pour que ces informations soient utiles à la communauté scientifique. Les États-Unis Traditionalistes d’Amérique du Nord, certainement aussi la République Démocratique de Chine, probablement d’autres pays que je n’ai pas identifiés, possèdent déjà les informations que je vais vous divulguer. Ils ont eu plus que le temps de nous dépasser à présent.

[Solène21] Pourquoi avoir gardé ces secrets tout ce temps ?

L’image de l’avatar de Solène21, sur le second écran, laissa la place à celle d’un tore représentant un ordinateur quantique stylisé, gravé du célèbre M de Mediapart. Ne respectant pas le protocole établi, Jean Lassereille avait transmis la parole à l’IA.

[MedIA] Si je puis me permettre, c’est d’autant plus étonnant que vous vous revendiquiez du mouvement *Science Libre !* qui visait à l’époque la divulgation publique en temps réel de tous les résultats scientifiques. Votre site, *La fille qui murmurait à l’oreille des machines*, était très populaire, avant que vous ne le fermiez pour rejoindre le laboratoire d’Albert Roman.

<!--SAUT1-->

Les écrans deux et trois diffusaient des copies du site *La fille qui murmurait à l’oreille des machines*, des extraits du manifeste *Science Libre !*, des plans et des images d’extérieur du laboratoire d’Albert Roman, des photos de Saint-Valéry-sur-Somme où il était installé, ainsi que des images de la plage immense où Sara Picard se promenait alors, chaque jour, souvent sous le vent et la pluie, parfois sous une lumière époustouflante.

L’écran un montrait un gros plan de Sara. Silencieuse. Elle regardait les écrans. Cela dura plusieurs minutes.

<!--SAUT1-->

[SP] Je ne sais pas exactement. Nous travaillions sur un sujet sensible. Nous avions des activités illégales. J’ai appris le secret, le mensonge même. Le but était de protéger nos travaux. Je m’y suis habituée. Albert espérait faire levier sur la justice, picarde ou française, en échangeant ses résultats contre la possibilité de continuer nos travaux. Jusqu’au bout il a espéré.

<!--SAUT1-->

[Glorfindel] Bonjour Sara. Est-ce que nous devons comprendre que vous et Albert Roman avez fait une offre à l’État français, qui aurait permis de poursuivre vos recherches et d’obtenir des résultats que selon vous seuls les Eutan ou la Chine peuvent maintenant atteindre ? Vous avez des preuves de ce que vous avancez ?

[SP] Lorsque l’État français nous a arrêtés, illégalement, sur le sol picard, nous pensions que c’était pour faire pression sur la Picardie et se réapproprier nos recherches. Au début nous avons cherché à préserver l’intérêt de notre pays, mais quand Albert a pris conscience que c’était l’État picard qui nous avait livrés, il a proposé à la France un laboratoire sur son sol. Il m’est vite apparu qu’il n’y avait pas le moindre calcul dans tout cela. Ni la France ni la Picardie n’avaient suffisamment compris la portée de nos recherches, l’une et l’autre restaient focalisées sur les problèmes légaux que posait notre mode de fonctionnement. Albert ne croyait pas que la bureaucratie pût être la seule cause de ce gâchis, jusqu’au bout il imaginait une partie de poker que lui jouait la France. Jusqu’au bout. Jusqu’à la folie. Jusqu’à hier. Je n’ai pas souhaité le faire mentir de son vivant, j’ai envié cette folie, elle a donné un sens aux dernières années de sa vie. Voici une copie des échanges entre Albert et le ministère de la recherche française.

<!--SAUT1-->

Sur l’holo on voyait la main de Sara Picard se poser sur la console miniature installée devant elle. D’imperceptibles mouvements des doigts elle sélectionna les données qu’elle souhaitait transmettre depuis sa mémoire internalisée. Une équipe d’authentification se mit immédiatement au travail.

— Sara, la communauté est partagée, elle souhaite bien entendu que vous nous parliez d’Albert Roman, de vos recherches, de ce que vous avez trouvé surtout, mais elle souhaite également vous entendre sur les faits qui vous ont conduite ici. Est-ce que vous voulez bien nous parler de cela ? Vous m’autorisez à prendre les questions qui concernent ce sujet ? Maintenant ?

Sara, opina, sans répondre, elle se laissait diriger. Elle avait depuis longtemps perdu toute volonté de façonner le monde, de le comprendre. Même d’y participer.

<!--SAUT1-->

[A-tord-nez] Bonjour. Je suis juriste, Madame Picard. Vous avez évoqué tout à l’heure l’illégalité de l’intervention de la police française, c’était votre ligne de défense au procès, et il a été depuis reconnu officieusement que les accords de Paris ne laissaient pas cette latitude à la police française, en effet. Mais, sans vous offenser, nous parlions à l’époque d’un trafic de drogue à une échelle industrielle, pas du cannabis, des drogues de synthèse réputées addictives et létales. Nous parlions de traite d’êtres humains, et de meurtres de masse. Vous n’avez jamais nié explicitement ces faits, basant votre défense sur la seule illégalité de l’intervention. Vous ne les niez pas aujourd’hui ? De très nombreux documents ont circulé après votre arrestation. Ils sont accablants.

<!--SAUT1-->

Les documents se succédaient sur l’écran trois, sous l’œil sévère de l’avatar d’A-tord-nez, en costume d’avocat du XXe siècle. Chacun pouvait capter l’un de ces documents, le parcourir, le donner à résumer à son IA, le compléter de ses notes, partager son avis ou celui de son IA.

<!--SAUT1-->

[SP] Nous n’avons jamais nié, car la majorité de ces allégations était fondée.

[A-tord-nez] Vous aviez construit le plus grand laboratoire de fabrication d’amphétamines au monde ?

[SP] Oui. C’est possible. Je veux dire c’est possible qu’il fût le plus grand du monde, je ne suis pas spécialiste. Je ne m’occupais pas du tout de ce volet de nos activités. Il était très grand en tous cas.

[A-tord-nez] Vous aviez organisé la traite d’êtres humains ? Vous payiez leurs familles contre la possibilité de disposer de leur vie, de leur corps.

[SP] Oui. De leur âme en fait. Les hommes et femmes que nous avons euthanasiés au niveau moins quatre, au laboratoire, étaient tous volontaires. Leur mort contrôlée nous permettait d’alimenter nos IA en données. Vous savez que nous avions travaillé sur les manifestations quantiques *post mortem*. Pas une seule personne n’a été amenée contre son gré. Les rémunérations proposées étaient des compensations très substantielles. Les familles qui recevaient cet argent étaient en détresse, c’était pour elles la possibilité d’un nouveau départ. L’équation était tout à fait équilibrée. Et la plupart des volontaires étaient condamnés à court terme.

[A-tord-nez] Nous le sommes tous plus ou moins madame Picard, est-ce que cela autorise ce que vous avez mis en place ?

<!--SAUT1-->

Les demandes d’intervention s’empilaient, Jean hésitait, mais il laissait encore A-tord-nez poursuivre, il semblait orienter sérieusement la conversation. Et il voyait que l’IA l’aidait en lui proposant des questions.

<!--SAUT1-->

[SP] Nous le pensions. Je le pense encore. Plus que jamais.

[A-tord-nez] Et la drogue ?

[MedIA] Nous précisons : à quoi servait le trafic de drogue ?

[SP] Elle nous servait à financer le laboratoire, les ordinateurs quantiques et l’acquisition des données. Des âmes.

[MedIA] Une évaluation réalisée par les services du procureur a évoqué une somme de près de dix milliards d’euros, est-ce une estimation correcte ?

[SP] C’est possible. Je ne me suis jamais occupé des aspects financiers ou logistiques, je ne m’occupais que de la recherche scientifique. En tous cas, l’ordinateur dont je disposais à l’époque n’avait pas de prix.

[A-tord-nez] Vous ne vous occupiez pas du trafic de drogue et d’êtres humains, mais vous en étiez informée ?

[SP] Oui.

[A-tord-nez] Depuis le début ?

[SP] Pas tout à fait. Lors de mon recrutement je n’avais pas conscience du montage sur lequel vivait le laboratoire. Je pensais alors à un financement par de grands groupes industriels, informatiques ou pharmaceutiques. Mais Albert Roman m’a rapidement parlé du niveau moins quatre, il m’a alors expliqué l’ensemble du montage. Je vous avoue que j’ai été soulagée, je préférais ce scénario à la mainmise de groupes qui auraient breveté nos découvertes. Là j’avais l’assurance de notre autonomie, de pouvoir faire profiter le monde entier de mes résultats. Enfin, si nous n’avions pas été arrêtés. Si nous avions pu continuer.

<!--SAUT1-->

Jean Lassereille passa la main à un homme âgé, bedonnant, soixante-cinq ou soixante-dix ans, vêtu simplement, un jean, une chemise mal repassée, une casquette verte ornée d’une grosse étoile rouge, dont dépassaient des cheveux en bataille. Son visage était profondément ridé et s’ancrait dans une grosse barbe, grise et frisée.

<!--SAUT1-->

[Fidel1953] Bonjour Sara. Fabriquer et distribuer de la drogue vous dérangeait moins que recevoir des financements de laboratoires pharmaceutiques ?

[SP] Oui. Évidemment. Nous restions libres. Personne n’oblige personne à prendre des drogues, quelles qu’elles soient. Le cannabis avait été légalisé en Picardie, entre-temps.

[Fidel1953] Pas vraiment légalisé. Et votre produit était tout de même plus dangereux, là, il y a des overdoses, une dépendance plus forte.

[SP] En effet. Les usagers sont libres. La surconsommation d’amphétamines provoque des arrêts cardiaques. Tout le monde sait cela. Tous les consommateurs d’amphétamines en tous cas. C’est leur responsabilité.

[Fidel1953] Nos travaux ont permis d’estimer à environ un millier le nombre de morts par overdose à cause de vos amphétamines. Vous sentez-vous responsable ?

[SP] Non.

[Fidel1953] Vous définiriez-vous comme quelqu’un de libertaire, Sara ?

[SP] Probablement.

[Fidel1953] Vous êtes pour l’euthanasie, la libre consommation des drogues, l’avortement ? La libre expression de la parole.

[SP] Oui. Bien sûr. Mais ce n’est pas le sujet. Le niveau moins quatre était un moyen au service de nos recherches, pas un projet politique.

<!--SAUT1-->

[DavidSansac] Combien de personnes ont été tuées dans votre laboratoire, au niveau moins quatre ?

[SP] Vingt et un mille trois cent vingt-six.

[DavidSansac] Vous connaissez ce chiffre par cœur ?

[SP] Bien entendu. Ces données étaient à la base de tout mon travail.

[MedIA] Mais ces données n’étaient pas suffisantes pour obtenir les résultats que vous poursuiviez, n’est-ce pas ? Combien vous en aurait-il fallu ?

[SP] Je ne peux évidemment pas savoir cela précisément, mais peut-être dix fois plus.

[DavidSansac] Vous aviez prévu de tuer plusieurs centaines de milliers de personnes ?

[SP] Ce n’est pas comme cela que la question se posait. Nous n’avions rien prévu, le programme suivait son cours, nous n’avions pas la connaissance permettant de prédire son achèvement. Nous avions des volontaires, de l’argent pour les payer. Ça fonctionnait.

<!--SAUT1-->

Les questions se poursuivirent sur ce sujet, mais finalement il n’y avait rien de plus à dire. Sara Picard et Albert Roman avaient organisé et causé la mort de plusieurs milliers de personnes, mais à aucun moment cela ne semblait avoir eu de l’importance pour eux. Il fallait à présent parler de ce qui était important.

<!--SAUT1-->

[ÉtienneEtienne] Bonjour Sara. Je suis cryptologue comme vous. Je connais vos travaux et ceux d’Albert Roman. Enfin ceux qui ont été publiés. Vous voulez bien parler de vos recherches restées secrètes jusqu’à présent ? Finalement c’est ce qui nous intéresse, n’est-ce pas ? Quelle fin justifiait de tels moyens ?

<!--SAUT1-->

Les titres des articles d’Albert Roman défilaient sur le troisième écran, ainsi que quelques extraits.

<!--SAUT1-->

[SP] Albert Roman avait découvert que les âmes bougeaient. Que les manifestations quantiques *post mortem* étaient animées.

[MedIA] Je confirme que cette assertion peut être vraie, malgré son caractère improbable en apparence.

[ÉtienneEtienne] Vivantes ? C’est ça que vous voulez dire ?

<!--SAUT1-->

Les écrans étaient saturés de commentaires. Les vérifications en temps réel des humains et des IA allaient toutes dans le même sens. C’était probable.

<!--SAUT1-->

[SP] Ce terme n’est pas approprié, il désigne un fonctionnement biologique qui évidemment n’a plus cours après la mort, par définition. Nous avions établi la preuve que les traces quantiques provoquées par la mort d’un être humain dans son environnement proche – qui avaient bien entendu été découvertes avant nos travaux – étaient pérennes, mais surtout que les états d’information évoluaient, selon des trajectoires qui ne semblaient pas aléatoires. Notre première hypothèse était que ces circuits pouvaient être porteurs d’informations. Il s’agissait de les décrypter, je mobilisais des méthodes inspirées de celle que j’avais utilisée pour décoder l’étrusque, avec la formidable IA dont je disposais au laboratoire.

[ÉtienneEtienne] Vous avez réussi à lire des âmes ?

[SP] Nous commencions à obtenir des résultats, mais nous étions encore loin d’être capables de décrypter les informations dont nous disposions. L’énigme était beaucoup plus complexe que celles que j’avais traitées jusque-là, je n’avais aucune prise, pas de contexte, pas d’experts. Et puis nous étions confrontés à un problème inattendu, car la quantité d’informations que nous mesurions dans chaque manifestation était inférieure à celle que nous avions estimée comme devant correspondre à une vie humaine. D’un facteur de vingt millions environ. Nous avions vingt millions de fois moins d’informations que prévu. Mais, malgré cela, oui, j’avais commencé à établir quelques hypothèses de décodage, et nous avions même un embryon de théorie, Hal et moi.

[ÉtienneEtienne] Albert Roman et vous ?

[SP] Non, Hal, H-A-L, était le nom de l’IA avec laquelle je travaillais sur le décodage.

[ÉtienneEtienne] Désolé de vous avoir interrompue. Quelle était cette hypothèse ?

[SP] Je n’ai pas pu la vérifier. Mais je pense que les traces quantiques *post mortem* pourraient n’être que l’enregistrement des dernières interférences qu’une personne produit, au moment de sa mort. Disons quelques minutes avant.

[ÉtienneEtienne] C’est une hypothèse révolutionnaire. Le reste serait ailleurs, ou perdu ?

[SP] Je ne sais pas.

[ÉtienneEtienne] Quand vos travaux ont été interrompus, personne n’y a eu accès ensuite, personne ne les a poursuivis ?

[SP] L’essentiel, le plus intéressant était confiné dans l’IA Hal, avec laquelle j’étais la seule à communiquer. J’imagine que des informaticiens français ont essayé de la décrypter, mais ils n’avaient aucune chance de réussir. Je pense qu’ils ont rapidement formaté l’ordinateur quantique du laboratoire. L’État picard a dû le revendre pour pouvoir payer son fuel. Les hivers sont rudes par ici, vous savez.

[ÉtienneEtienne] Vous êtes caustique. Tout aurait été perdu ? Tout votre travail ?

[SP] Oui. Mais les Eutan ont refait le chemin.

[MedIA] Les échanges avec Cernia, qui occupe à présent l’ordinateur quantique du laboratoire d’Albert Roman confirment un effacement irréversible.

[ÉtienneEtienne] Sara, comment le savez-vous ? Que vos travaux ont finalement été poursuivis ailleurs ?

[MedIA] Les consultations de Sara Picard sur le réseau libre, sous diverses identités – la liste s’affichait sur le troisième écran – montrent un suivi très minutieux des travaux corrélés à ces sujets dans de nombreux pays du monde, mais en particulier aux États-Unis Traditionalistes d’Amérique du Nord, en République Populaire de Chine, en Russie, et dans la plupart des pays d’Europe.

[SP] Je ne sais pas où ils en sont, jusqu’où ils sont allés. Mais je pense qu’ils ont décrypté une partie au moins des manifestations, peut-être ont-ils établi le début d’une communication. Je ne sais pas ce qu’ils ont prévu de faire des résultats, sûrement proposer de nouveaux services. Je ne sais pas quand ils avaient prévu de divulguer ces découvertes au public, mais je pense à court terme, deux, trois ans.

[MedIA] Les assertions de Sara Picard sont estimées vraies à 71 % à ce stade de l’analyse des données effectuées par les IA avec lesquelles je suis en communication. La dérivée est positive, la limite tend vers 92 %.

<!--SAUT1-->

C’était vrai. Sara Picard ne parlait plus, l’IA de Mediapart continuait d’alimenter le troisième écran de données corroborant ses dires. Les Eutan avait commencé à décrypter les inscriptions quantiques *post mortem*. Un des plus grands mystères de l’humanité était sur le point de tomber. Peut-être était-il déjà tombé. Tout le monde voulait intervenir. C’est-à-dire, le monde entier voulait intervenir. Le réseau libre était pris d’assaut. L’ambassade des Eutan. Le porte-parole du gouvernement français, le représentant du Sénat picard. La chargée de communication d’Amazon & Alphabet, la plus grosse entreprise du monde. Jean s’offrit le plaisir de leur refuser toute communication. C’était leur émission. Le monde se divise en deux catégories, *my friend*, ceux qui ont une émission sur le réseau libre, et ceux qui écoutent. Toi, tu écoutes.

<!--SAUT1-->

L’émission se poursuivit. La plus déstructurée qu’il eût jamais faite, qu’il eût jamais laissée faire. Sara ne montrait aucun signe de fatigue. Aucun signe de joie, ni même de soulagement. Le même visage impassible, sans émotion qu’il avait découvert cinq heures auparavant en entrant dans la cellule de la prison. Alors il continuait. Les résultats de leurs recherches en détail, vulgarisés en temps réel par les IA, leur vie au laboratoire, coupée du monde, l’organisation de leurs travaux, de leurs incroyables trafics, ce qu’étaient devenus les autres membres du laboratoire, le nom des entreprises qui, selon elle, détiendraient aujourd’hui ou demain le secret des âmes. Pour finir, pour la première fois depuis qu’il animait l’émission *En Picardie*, Jean Lassereille se donna la parole.

<!--SAUT1-->

[Jean] Pourquoi ? Pourquoi vous faites cela aujourd’hui ? Nous raconter tout cela ?

[SP] Pourquoi pas ?

[Jean] C’est une vengeance, Sara ? Vous savez que les gouvernements français et picard vont être secoués, même douze ans après, n’est-ce pas ? Vous n’imaginez pas les personnes qui ont cherché à intervenir ce soir… C’est une revanche ?

[SP] Une revanche ? Je ne sais pas. J’ai toujours agi de façon pragmatique. Souvenez-vous j’étais militante du mouvement *Science Libre !*, c’est une explication, non ? Il faut que les citoyens s’emparent de ces recherches, ils sont concernés. Vous êtes concernés. Vous croyez que cela pourrait être une revanche ?

<!--SAUT1-->

Le visage de Sara Picard eût un léger frémissement. L’ombre d’un sourire. Jean figea l’écran numéro un sur cette dernière image.


## A&A

L’Amiénois était vraiment l’ambiance préférée de Charlie. Le bar-tabac était bondé cet après-midi-là. L’atmosphère était saturée de fumée. La lumière des gogues, restés ouverts, éclairait plus que le vague halo des deux plafonniers bleutés qui restaient en service. Jean-Louis devrait vraiment refaire la déco. Faire un peu de ménage au moins, passer un coup de serpillière, le sol était poisseux, les tables collaient aux manches. Et réparer la porte des chiottes, ça schlinguait. Un type martyrisait un vieux flipper mécanique, quatre autres se chambraient autour du baby, et le Rhodes de *L.A. Woman* que crachait le vieux juke-box peinait à couvrir les conversations, qui peinaient à se couvrir les unes les autres. On ne s’entendait pas gueuler. L’odeur de la bière industrielle collait au corps. Les cacahuètes avaient un goût de rance.

— File-moi une tige, Charlie. Puis change-moi la musique bordel. Tu fais chier avec tes Doors, le rock c’est vraiment chiant. Balance-nous du jazz automatique qu’on se détende un peu.

— Faut se supporter, Alice, c’est comme ça la vie en société. Jim, c’est la vraie classe, il parle avec les dieux, mec.

— Suis pas un mec.

— Je sais jamais comment je dois dire.

— Tu dis rien et c’est marre.

— Ok, rien.

— Je t’emmerde.

— Va crever avec ta musique de machine. Les enculés font un procès à Mediapart.

C’était Bob. Bob était un ancien costaud devenu obèse. Bob n’aimait pas les IA. Bob n’aimait surtout pas A&A. Bob n’aimait plus les machines. Bob n’aimait plus beaucoup les gens non plus. Bob n’aimait rien. Un peu le pastis. Alice, Bob et Charlie étaient attablés. Alice et Charlie au picon-bière à cette heure-là, Bob au pastis, quelle que soit l’heure.

— Je viens de voir tomber un post. Putain, c’est repris partout. Un procès pour divulgation d’information confidentielle anticipée ou un truc du genre. A&A va faire un putain de procès alors qu’ils n’avaient même pas découvert le truc avant les Picards. Ça veut dire, t’as pas le droit de dire un truc avant eux. Juste ta gueule quoi. Les enculés.

— S’ils gagnent ils pourront empêcher quelqu’un de parler d’un truc s’ils y ont juste pensé avant. Pas besoin de l’avoir dit ?

— Vont gagner.

— Et comment le mec sait qu’il peut pas dire son truc ?

— Il peut pas. Il appelle A&A, et l’autre lui dit, c’est bon, ton truc j’y avais pas pensé, balance. Ou alors c’est : chat-bite ! j’y ai pensé en 1923. Ta gueule.

Le flipper avait claqué pour la troisième fois de suite. Les gars au baby changeaient de côté. Le solo final de *L.A. Woman* se diffusait dans une légère accalmie sonore. Charlie avait baissé un peu le son.

— Quand même, c’est pas sûr qu’ils vont gagner, n’est-ce pas ?

— C’est pas sûr ? T’es con ou quoi Charlie. C’est Amazon & Alphabet. C’est la plus grosse entreprise d’intelligence artificielle du monde. C’est juste la plus grosse entreprise du monde. Tu sais combien de personnes bossent pour eux ?

— Je sais pas…

— Moi non plus je sais pas, des tas, des millions. Son chiffre d’affaires, c’est deux fois celui du reste de toutes les autres boîtes des Eutan. Je parie qu’ils ont plus d’avocats payés à se frotter le gland qu’il y a de juges au monde.

— Sans compter qu’elle emploie des milliers d’IA. Des balèzes.

<!--SAUT1-->

*Break On Through* démarrait.

— Tu fais chier avec tes Doors, putain.

— Une dernière. Sois sympa Alice.

— Merde, je vais dire aux p’tits gars d’A&A de penser bien fort aux Doors, t’auras plus le droit de les écouter. T’auras plus le droit de chanter dans ta douche. Tu prends des douches Charlie ? Parce que ton avatar sent pas la rose. T’auras plus le droit de chanter dans ta tête.

— S’ils me prennent les Doors, je fais tout sauter, Alice, je te promets. Je me fais exploser dans un *data center* !

— Tu vas te faire interner pour intention de terrorisme, Charlie, fais gaffe à ce que tu dis.

— *Fuck A&A* !

— T’es trop subversif Charlie, ils tremblent chez A&A en ce moment, en t’écoutant, là. Au fait, je vous ai raconté que la nouvelle *release* de l’IA sur laquelle je bosse, on l’a lâchée le mois dernier, elle est capable de capter et analyser en temps réel les données de tous les habitants d’un pays comme la Picardie ? Le chef-prog dit que d’ici moins de cinq ans on aura une IA…

— Tu dis, « on », c’est A&A, n’est-ce pas, A-lice ? A&A&A.

— Vous espionnez la Picardie ! Ça doit être bien chiant.

— Non, on espionne rien, on programme. Et puis vous savez bien que ma contribution est pérave. Et je bosse pour un sous-traitant, pas pour A&A. Et puis, je m’en fous d’A&A, je fais ça pour la thune, pour le salaire.

— Ouais, tu bosses. Toi. T’as un salaire. T’es pas un RU comme nous, pas vrai ?

— Tu fais chier Bob, tout le monde est au revenu universel, c’est même le principe. Me casse pas les couilles que j’ai plus. Donc, je te disais, d’ici cinq ans, trois peut-être, il y aura une IA capable de traiter les données…

— De surveiller.

— Tu me laisses finir ma putain de phrase ? De surveiller les données de tous les blaireaux de la planète. Mais, ça on s’en fout, c’est fait. Le vrai sujet, en ce moment, c’est la surveillance des autres IA. Et là, mon pote, ça déchire sa race. Elles se laissent pas becter comme nous. T’as des protections de partout.

— Pas si connes.

<!--SAUT1-->

— Jean-Louis, la même. Passe ton feu Charlie, elle va pas s’allumer toute seule ma tige.

— *Come on baby light my fire…*

— Ouais…

— Tiens, regarde Bob, un autre post de Mediapart, ils sont chauds, on parle de chambres à gaz et de fours crématoires, en Eutan.

— Des conneries mon pote.

— C’est du Wikileaks. Attends, j’envoie…

Le grand miroir situé devant eux cessa de refléter les scénettes du bar, le flip, le baby, les pochtrons affalés sur leurs tables crasseuses ou le blouson noir accoudé au comptoir qui semblait chercher quelqu’un du regard, la serveuse qui se traînait nonchalamment de table en table. À la place le miroir s’illumina et projeta une vidéo muette, sous-titrée. Une interview. *Break On Through* et le brouhaha du bar continuaient de constituer le fond sonore.

— C’est Lassereille avec le t-shirt « *Mediapart, sur le réseau libre* » ?

— Ouais, lis.

— C’est qui, la meuf ?

— L’ambassadrice d’A&A.

— J’avance la vidéo, c’est là. *Chouf* !

<!--SAUT1-->

« …nous avons travaillé de conserve avec le gouvernement. La loi *zero tolerance*, puis les lois *no more dollar for bastards* et *no more bastard* ont conduit le gouvernement eutanien à condamner à la peine de mort près de trois millions de criminels en dix ans. Nous avons fait en sorte de mettre à profit ce choix politique, largement soutenu par le peuple, comme vous le savez.

— Donc, chaque condamné à mort eutanien a servi vos recherches ?

— Oui. Nous avons eu l’autorisation d’installer des laboratoires dans chacune des chambres à gaz du programme *no more bastard*.

— Vous soutenez par conséquent la politique autoritaire du gouvernement eutanien ?

— A&A ne fait pas de politique. Nous respectons les choix politiques de chaque nation du monde… »

<!--SAUT1-->

— Tu vois, elle ne s’en cache pas ?

— C’est peut-être un *fake* ?

— Mon cul, c’est A&A dont il s’agit, blaireau ! C’est eux le réseau. Tu crois qu’ils vont pas contrôler un truc pareil ? La semaine du lancement de leur nouveau service. Ils gèrent.

<!--SAUT1-->

« …est-ce que l’on peut dire que le mouvement qui a consisté dès le début du vingt-et-unième siècle à considérer les gens comme des pourvoyeurs de données, puis eux-mêmes comme des données, nous a conduits aujourd’hui à cette situation ? Il n’est pas immoral d’utiliser la mort pour créer des données, car ce sont les données qui sont sacrées, pas la vie humaine, n’est-ce pas ? Finalement Sara Picard et Albert Roman faisaient la même chose. Quelle est la différence avec vous, madame l’ambassadrice ? Et pourquoi faire un procès à Mediapart ?

— Une différence monsieur Lassereille, c’est que madame Picard et monsieur Roman ont été condamnés par la justice de leur pays, alors qu’A&A est soutenu par son gouvernement. Quant au procès…

— Je vous coupe, ils n’ont pas été condamnés par la justice de leur pays, étant picards et travaillant en Picardie. La France a mené ce que l’on appelle une opération extérieure, illégale selon le droit international.

— Quant au procès, disais-je, A&A défend simplement ses intérêts commerciaux, nous n’avons rien contre Mediapart. Nous sommes pour la liberté d’expression et nous soutenons l’existence du réseau *unsecure*. Nous le prouvons avec cette interview que je vous accorde, n’est-ce pas ?

— Je rappelle que vous avez refusé la participation de notre communauté, et de notre IA. Nous remercions nos confrères de Fakir de nous avoir prêté la leur pour cette interview. Ce procès pourrait conduire à la fermeture du premier média du réseau libre. Est-ce l’objectif d’A&A ?

— Je vous assure que non. Nous souhaitons simplement contrôler l’information stratégique pour notre entreprise. Vous comprenez cela, monsieur Lassereille ? C’est juste de l’économie. Et l’économie est neutre. Pour ce qui est de votre IA, nous avons des doutes quant à son intégrité. La dernière qualification remonte à trois ans, n’est-ce pas ?

— Ce sont des accusations graves. C’est très sérieux de remettre en cause l’intégrité de l’IA d’un journal. Vous avez des accusations précises à formuler, des preuves ? C’est peut-être nous qui allons vous conduire en justice, pour diffamation.

— Nous avons juste des doutes, c’est notre droit, n’est-ce pas ? Mais nous avons digressé, je vous présente mes excuses, c’est de ma faute… »

<!--SAUT1-->

— Salope.

— Ne sois pas sexiste, Bob.

— Pardon messieurs-dames.

Charlie avait mis *Love me two times* dans le juke-box. Alice leva deux fois les yeux au ciel, une fois pour Bob et une fois pour Charlie.

<!--SAUT1-->

Tandis que l’interview se poursuivait, qu’Alice, Bob et Charlie buvaient leur verre, le miroir-écran se divisa soudain en deux parties. Une seconde vidéo apparut à droite de la première, qui continuait de montrer Jean Lassereille et l’ambassadrice d’Amazon & Alphabet. Ils parlaient à présent de documents *leakés* de la *Asian AI Association*, qui feraient état de millions de personnes âgées euthanasiées en Eutan et en Chine au profit du programme de recherche d’A&A. Enfin, Lassereille en parlait, l’ambassadrice réfutait. *Okay* pour les *bastards*, mais pas pour les vieux. *Okay* pour quelques millions, mais pas des *dizaines* de millions. « On a engagé les procédures pour Wikileaks… Vous ne devriez pas insister sur ce sujet monsieur Lassereille… ». Ça engageait. Ça menaçait. Ça ne rigolait pas. Le fond sonore du bar-tabac reflua nettement, dans un imperceptible fondu, laissant le spot publicitaire d’A&A se détacher très nettement.

<!--SAUT1-->

Le personnage qui s’adressait à Charlie était Égérie elle-même, la nymphe était nue, représentée exactement comme dans ses livres, qu’il collectionnait plus qu’il ne les lisait, elle parlait d’une voix claire et douce. Charlie ne pouvait s’empêcher d’apprécier le choix malicieux d’A&A. L’IA qui avait calculé ce choix à partir de son profil avait bien fait son boulot. Charlie se demanda ce à quoi Bob et Alice avaient eu droit, de leur côté. Il ne voyait pas bien ce qui pouvait faire plaisir à Bob.

<!--SAUT1-->

« Charlie, souscrivez dès aujourd’hui gratuitement au service Dixie, et réservez votre nom de domaine *dixie.aa*. Je suis ravie de vous annoncer que votre pseudo est encore disponible, l’adresse *charlieporte.dixie.aa* vous attend. Ce sera votre interface personnalisée sur l’enregistrement de votre future manifestation quantique *post mortem*, directement sur le réseau officiel. Vous profiterez de la disponibilité et de la fiabilité unique des serveurs d’A&A, gratuitement et éternellement. La seule chose que vous avez à faire est de me dire oui, en validant le formulaire prérempli reçu par courrier hier à douze heures vingt-huit et en acceptant les conditions d’utilisation permettant de profiter du service d’enregistrement, de stockage et d’accès illimité Dixie. Il s’agit bien sûr d’un service gratuit, comme tous les services d’A&A. Une fois votre enregistrement réalisé, tous les utilisateurs que vous aurez ajoutés à votre cercle funèbre pourront y accéder simplement avec notre *Dixie Player*. Celui-ci sera disponible très prochainement à l’essai. Nous pourrons également sous peu vous permettre de visualiser une démonstration réalisée en laboratoire de la mémoire de l’un des pionniers *Dixie* qui a accepté de rendre publique sa manifestation.

Charlie, je me suis permis de vous pré-inscrire au centre d’enregistrement d’Amiens, c’est à moins de trente-et-un kilomètres de chez vous. Vous pourrez bientôt accéder à une visite virtuelle de votre centre. Il vous suffira de vous y rendre le moment venu. Nos services prévisionnistes situent ce moment sur un pic de probabilité situé entre 5865 et 14658 jours à compter d’aujourd’hui. Bien sûr si vous deviez décéder à un autre moment, le centre vous est ouvert vingt-quatre heures sur vingt-quatre, sept jours sur sept, quoi qu’il arrive. Une voiture A&A vous sera envoyée sur simple demande… »

— Éteins-moi ça Charlie, gueula Bob, pour couvrir la voix de l’annonce. Sur l’écran que voyait Charlie, la nymphe artificielle continua comme si de rien n’était.

<!--SAUT1-->

« …ne vous laissez pas surprendre, dans le doute mieux vaut venir pour rien et repartir sur ses deux jambes, que ne pas arriver à temps et disparaître pour toujours. Et je vous rappelle que le service Mortem Prévision d’A&A vous permet d’avoir une prévision en temps réel de vos probabilités de décès dans les 18 prochains jours, avec un indice de confiance de 97 %. Vous pouvez louer une chambre dans nos centres d’enregistrement, afin d’attendre en paix le moment. Vous pouvez également décider à tout instant de prendre un rendez-vous personnalisé avec un de nos thanatechs pour organiser vous-même votre décès, dans les conditions que vous aurez choisies. La probabilité de votre décès à dix-huit jours n’est que de 0,02 %. Je ne vous conseille pas de réserver une chambre, ni de prendre un rendez-vous maintenant avec un thanatech. Si vous souhaitez tout de même… »

<!--SAUT1-->

— Éteins-moi ça Charlie, bordel, ça me fout la gerbe.

— Ça, je peux pas.

— Tu peux pas quoi ? Vire-moi leur pub de merde ! C’est ton ambiance ou quoi ?

— Tu sors d’où Bob ? T’as fait de la taule ou quoi ? Ça fait trois mois qu’on ne peut plus couper les annonces d’A&A. Plus du tout. Plus de quota.

— Putain, on débranche tout, alors. *Sudo shutdown now*.

— Arrête ton char Bob, tu veux couper ? Tout couper ? Tous les services. Plus de chauffage, plus d’eau, plus d’électricité. Ta bagnole ira nulle part. T’auras pas de GPS, tu sauras aller nulle part. La porte de ton appart s’ouvrira même pas. Ta montre te donnera plus l’heure. Même ton frigo va te faire la gueule. On peut pas couper, Bob. Y a plus de bouton depuis longtemps.

— Putain, on devrait pouvoir. Vire au moins ton ambiance pourrie, il me fout le cafard, ton bar, à la longue.

<!--SAUT1-->

Le bar disparut. Alice, Bob et Charlie étaient au milieu de leur polyèdre d’écrans respectif. L’annonce d’A&A se terminait. Charlie était assis dans un fauteuil club, des tas de bouquins, en papier, empilés à côté de lui. Les philosophes, les Grecs, les Allemands, les Français, les Anglais. Tous. Des tas. Il les ouvrait parfois. Il aurait voulu un monde meilleur, où hommes et machines auraient leur place. Il espérait trouver une solution dans la philosophie. Il s’y mettrait. Bientôt. Bob était assis en tailleur par terre, il buvait du pastis, des pornos jouaient sur plusieurs écrans. Alice était allongé, nu, les yeux fermés, il fumait.

Ils aimaient bien l’univers désuet des cafés de campagne des années 1980. Surtout Charlie. Alice s’en foutait. Bob n’aimait rien. Pour aujourd’hui, le charme était rompu.

— Bon, je dois aller bosser, dit Alice, on se refait ça ?

— Je crois que je vais faire un bout de brousse, dit Bob.

Il n’irait sûrement nulle part. Il n’aimait plus les balades. De toutes façons il était trop bourré.

— Ouais, j’ai de la lecture, dit Charlie, je suis sur Nietzsche en ce moment, enfin, je commence. Je serai au café demain, si vous voulez passer.

Il réactiva l’Amiénois. Juste le temps de finir son picon. Et d’écouter la fin de *The end*.

<!--SAUT2-->

Il restait encore au moins une heure à Alice avant de commencer à travailler. Il ouvrit une vieille malle qu’il conservait dans un coin de son appartement. Il en sortit un authentique ordinateur tour, une antiquité d’au moins cinq kilos, dotée d’un écran presque aussi lourd et d’un clavier. Les différents éléments, séparés, devaient être reliés par des câbles. Il les maintenait en vie au prix d’un suivi technique digne d’un ordinateur quantique à cent mille piccoins. Il avait au moins gardé ça de sa première vie, il savait vraiment faire fonctionner un ordinateur. N’importe quel ordinateur.

Alice aurait aimé écrire. Sur un clavier de préférence, pour sentir le poids des touches, la lenteur des mots que l’on cisèle, pour sculpter le flux de sa pensée, ne plus seulement se laisser porter par celui-ci. Écrire, ça ne devait pas ressembler à dicter à une machine. Ça, c’était raconter. Au mieux. Se la raconter. Écrire, c’est avec les pognes, pas avec la gueule, pensait-il. Il y avait bien le nouvelliste aveugle, bien sûr, mais il n’y croyait pas vraiment à cette histoire, une fiction de plus. Alice aurait voulu écrire des histoires, peut-être un roman. Mais à quoi bon alourdir d’une brique de plus la bibliothèque universelle, quand aucune vie d’homme ne peut suffire à lire les chefs-d’œuvre qui la composent déjà.

Si t’es pas Molière, t’as rien à écrire ! C’est le standard, Molière. Ajoute Céline et Zola, deux ou trois étrangers, Dostoïevski, pourquoi pas Hemingway et García Márquez, Borges, on l’a dit, Tolkien et Herbert, peut-être un contemporain, j’en connais pas – disons Damasio mais ça date déjà – t’en as pour une vie. Déjà. Tu cherches un peu, ça t’en fera des tas de vies. Et Alice n’était sûrement pas Molière. Il se disait parfois qu’il aurait fallu commencer à écrire, pour de vrai, pour être sûr… Mais bon, de toutes façons, souvent, t’es pas Molière.

Souvent, avant de commencer à travailler, Alice sortait son ordinateur de la malle, vérifiait qu’il s’allumait, jetait un œil aux logs du système, vérifiait que les vieux composants étaient opérationnels.  À l’occasion il sortait ses tournevis, son fer à souder, ses oscilloscopes. Quand il estimait que le *check-up* était terminé, il lançait un éditeur de texte. Il regardait longuement l’écran, le curseur clignotant, la page virtuelle. Blanche. Puis il éteignait la machine.


<!--SAUT2-->


Autrefois, Bob aimait l’air de la campagne, il aimait parcourir les plaines, seul, des blés ou des betteraves à perte de vue, en fredonnant des chansons de Renaud, souvent les mêmes, *Gérard Lambert*, *Pochtron* ou *J’ai raté télé-foot*. *Hé Manu rentre chez toi, y’a des larmes plein ta bière…* Il aimait se perdre dans la forêt, partir quelques jours avec un sac à dos bien garni, surtout en liquide qui tombe pas du ciel, si tu vois ce que je veux dire, et débarquer dans les villages comme un bourlingueur, un étranger, un voyageur revenu d’un autre monde. Les gens le dévisageaient, sans malveillance. Il aimait s’arrêter dans la nature, boire son vin de table, manger un saucisson. Malgré la pluie. Malgré l’odeur des engrais chimiques qui suintait des terres malades. Il aimait ça, autrefois.

Il n’aimait pas les champs de cannabis qui couvraient la terre à présent, de juin à juin, toujours verts, inlassablement parcourus par les robots agricoles autonomes, enrichissement biologique de la terre, tri des mâles et femelles, désherbage mécanique, récolte, enrichissement, tri, récolte, récolte encore. Trois récoltes par an, sous le regard vigilant des drones de surveillance. Les villages étaient souvent déserts, aussi. À présent, Bob n’aimait plus se promener, il trouvait les machines laides. Bob n’aimait plus du tout les machines.

Lui, l’ancien hacker, il aurait voulu devenir un de ces écolo-nihilistes qui se battaient pour un monde sans pétrole, sans énergie, sans machine. Pour un monde sans homme, pour les plus radicaux, les plus éclairés, ceux qui avaient vraiment compris. Bob aurait voulu être un terroriste. Faire peur à un monde devenu trop dégueulasse, pour voir si ça pouvait encore bouger. Pour lui-même bouger encore un peu. Mais il se sentait vieux, il y a trop longtemps qu’il n’avait plus eu envie. De rien. Il avait capitulé. Sûrement quand il avait adopté son pseudo, son nom d’utilisateur, Bob, quand il avait abandonné son vrai nom. Sûrement qu’il avait trouvé ça drôle, alors, qu’il n’avait pas fait attention. Mais on meurt de hasard.

Il lui restait quelques photos imprimées, punaisées à même ses murs, jaunis, les murs comme les photos. Il les regardait, parfois, quand il décrochait un peu de ses écrans, de ses pornos, de son pastis.

<!--SAUT2-->

Les écrans qui formaient l’icosaèdre au milieu duquel le fauteuil de Charlie était posé restèrent finalement encore branchés de longues minutes sur l’Amiénois. Il était seul à sa table. La vie virtuelle continuait dans le bar. Le bruit du flip. Les Doors. La fausse serveuse un peu vulgaire qui servait des faux clients un peu bourrés. Il était attaché à cet ersatz d’univers comme on est attaché au souvenir d’une maison ou d’un amour d’enfance, pas que ce soit spécialement joli ni intéressant, mais c’est personnel. Puis, il se connecta au réseau libre, il allait y avoir du barouf. Et c’était sur son journal que ça se passait, dans son pays. C’était chez lui que le monde s’invitait. Quasiment sur son comptoir.

<!-- début de style "forum", niveau principal -->

`unsecure!!mediapart.com/resist-aa-law`

**Rejoignez la résistance**

Suite à l’interview de Sara Picard, la société Amazon & Alphabet a intenté un procès à Mediapart pour contrefaçon, en invoquant une jurisprudence eutanienne qui permettrait de nous condamner pour la divulgation d’un procédé alors que celui-ci n’a fait l’objet d’aucun dépôt préalable. Il suffirait au plaignant de prouver que le procédé était effectivement en cours d’élaboration. Le « contrefacteur » devrait alors dédommager le plaignant du manque à gagner estimé lié à la divulgation anticipée du secret. Ce procès est d’abord dangereux parce que si une telle peine est prononcée à l’extérieur des Eutan, A&A aura le pouvoir de tuer dans l’œuf toute idée qui ne sortirait pas de ses laboratoires, où que ce soit dans le monde. Le combat est déjà déséquilibré, il deviendra un jeu de massacre. Nous vous invitons à souscrire à notre fonds spécial de lutte contre la jurisprudence A&A et à vous mobiliser sur le réseau officiel pour faire connaître votre refus à vos dirigeants. Le monde ne peut pas se permettre de renforcer encore le pouvoir des Léviathans de l’information…

<!-- fin de style forum -->

Charlie survolait les premières lignes, encore changeantes alors que les rédacteurs de Mediapart dictaient l’article à leur transcripteur, revenant parfois sur leurs premières formulations. « Ce procès est d’abord dangereux parce que » venait d’être changé en « Premièrement ce procès est désastreux parce que ». Mais Charlie ne parvint jamais au deuxièmement, il surveillait, sur un second écran l’arrivée des *addons* postés par les contributeurs du réseau libre, et son écran le notifia que Jojo-du-soixante avait déjà posté. Charlie y sauta directement, reléguant l’article sur un troisième écran, à droite, à la périphérie de son champ de vision. Il aimait commencer par les divagations de Jojo-du-soixante. Ça dédramatisait. Et Jojo était vraiment très fort pour trouver des trucs insolites à dire sur presque tous les sujets, souvent alors que l’article n’était même pas encore fini. C’était sa spécialité. Charlie trouvait ça plus cool pour entrer dans le contenu ; des fois Mediapart, c’était un peu ardu. Un peu sérieux. Ils pourraient tout de même revoir un peu leur transcripteur, configurer un style un peu plus accessible. Souvent, Charlie oubliait de revenir à l’article. En fait, il ne lisait jamais les articles jusqu’au bout.

<!-- début de style forum, premier niveau de réponse -->

`Addon Transclusion unsecure!!jojo-du-soixante.fr/aa-egal-al-azif`

`@unsecure!!mediapart.com/resist-aa-law`

`[audio transcrit]`

**A&A égal Al Azif**

Al Azif. C’est dans ces vieux bouquins de Lovecraft que je l’ai trouvé. Vous savez que j’aime bien les vieux bouquins. A&A, c’est pas Amazon & Alphabet, c’est Al Azif. Voilà. C’est le nom d’un livre maudit, secret, il s’appelle souvent le Necronomicon, mais parfois c’est Al Azif. C’est le livre des morts. On ne sait pas bien pourquoi il s’appelle comme ça, Al Azif, peut-être juste à cause du bruit que font des insectes nocturnes, un crissement qui ressemble aux cris des démons. C’est une des explications que donne Lovecraft. Mais ce bouquin, c’est pas juste une histoire ou une légende, un truc construit a posteriori, comme la bible ou quoi, c’est un vrai livre, qui a vraiment été écrit. L’auteur a même un nom arabe, en « Abd », je ne sais plus exactement, peut-être Abdallah. Il y a pas mal de gens qui ont essayé de trouver Al Azif, pour de bon. Lovecraft, il était fondu, je veux dire complètement, dépressif au dernier degré, misanthrope, t’imagines même pas, xénophobe comme on n’en fait plus. Bref le type était pas recommandable, mais justement, c’est parce qu’il savait un truc, un vrai truc, et il cherchait comment nous le dire. Il pouvait pas raconter : « ouais, je sais qu’un grimoire ancien contient ceci ou cela, au boulot les gars, il faut le chercher, comme ça on pourra réveiller des démons enfouis et on sera maudits ». D’abord il aurait pas été pas crédible. Et puis, si tu y crois, normalement t’as pas envie de le chercher. Donc, il raconte ça comme des nouvelles, des histoires, à la Poe, tu vois, Edgar Poe ? Laisse tomber. Mais en plus noir et en plus barré. Et surtout, c’est pas des histoires, c’est vrai.

Moi, ce que je crois, c’est que ce bouquin, Al Azif, il contient le secret de la mort. Et que tu peux pas le lire, personne. Parce que si on connaît le secret de la mort, on ne peut pas vivre. Tu vois, c’est ça la malédiction. On passe sa vie à chercher un sens à sa mort, et donc on ne peut pas trouver, sinon la vie perd son sens. Vous me suivez ?

Donc leur truc à A&A, DixieMachin – c’est tiré du Neuromancien comme nom, je l’ai lu, j’en ai même déjà parlé dans un autre *addon* –, si ça permet de percer le secret de la mort – tu comprends que c’est ce qu’ils veulent faire ? et que si on parle avec les morts, ils vont tout nous raconter, normal, non ? –, bref, si ça permet de percer le secret de la mort, il n’y aura plus de vie, tu vois. On aura lu Al Azif, y aura plus qu’à crever.

Lovecraft, on comprend pas pourquoi il a passé sa vie à vouloir nous faire trouver un livre qui nous pourrit la vie. Je dis on comprend pas, mais on comprend quand même, soit le type est malade, sa vie est pérave, alors il veut péraver celle de tout le monde. Égoïste, tendance salopard, mais rationnel. Soit il est tombé dessus par hasard et là, bon, retour à la case départ, dépression et envie de partage. Soit il est lié par une sorte d’enchantement, il devient le séide du livre, ou des morts, et il doit en parler. Moi, je suis pas très ésotérisme, j’en ai fait quand j’étais petit mais j’ai pas accroché, donc je reste sur la première idée. Les salopards, on apprécie pas, mais on peut comprendre.

Alors, ma question les gars, vous savez que j’aime lancer la discussion, c’est pourquoi chez A&A, qui question salopards de première se posent là, pourquoi ils font ça ? Pourquoi ils nous déterrent un Al Azif qui va détruire l’humanité ? Pour du fric ? Parce que les machines ont pris le pouvoir ?

À vous de jouer !

`Addons liés : dixie-mort-ou-vif | quelle-ia-dirige-aa | betterave-ou-cannabis`

<!-- Second niveau de réponse, le post suivant répond au précédent -->

`Addon Transclusion unsecure!!fernand-est-mort.net/82d2f7e5`

`@unsecure!!jojo-du-soixante.fr/aa-egal-al-azif`

`[audio transcrit]`

**Réponse à : A&A égal Al Azif**

T’as tout faux Jojo ! désolé de te le dire comme ça. C’est pas pour le pognon, et c’est certainement pas un coup des machines. T’es en plein dans l’humain là, le pur. Les gars font ça parce qu’ils le peuvent, et que la seule chose qui fait marcher les hommes, depuis qu’ils sont debout j’entends, c’est chercher à pouvoir faire des trucs. C’est pas la volonté de survie, l’humanité, c’est la volonté de pouvoir faire toujours plus de trucs. Plus que son père, plus que son voisin, plus qu’avant. Ça ressemble à de la philo, mais oublie, c’est simplissime : tu penses à un truc, si tu peux le faire, tu le fais, si tu peux pas, tu cherches à pouvoir le faire. Y a un gars, peut-être le pote à la Sara, ou même un autre avant, il s’est dit : « On va parler avec les morts ». Et voilà, les dés, le Rubicon, *jacta est*. L’idée est dans l’air, elle va se reproduire, essayer de coloniser les cerveaux humains, faire son chemin, ça s’appelle un mème. Mais je digresse, limite je frime. Donc ils essayent, les colonisés, ils cherchent. À un moment ils trouvent, et ils le font c’est tout. Toi tu dis, il le font pour ça ou pour ça, mais non, mon Jojo, ils le font parce c’est possible, c’est la loi de Gabor. C’est pas une question de fric ou de dépression, encore moins de magie noire, c’est une question de pouvoir faire les choses, c’est dans l’ADN de l’homme. Dans un coin. Donc en l’espèce, ils peuvent connaître le secret de la mort, et selon ta théorie, ça va nous tuer la vie. Pas de bol. Ça prouve juste que tout ça n’a aucun sens depuis le début. L’avantage par rapport à l’explosion nucléaire ou la comète, tu vois, c’est que ça démontre vraiment que ça n’a pas de sens. Avec la destruction brutale, il reste un doute, et si on s’était pas fait sauter la gueule, on aurait pu être moins cons ? Mais, en vrai, là on voit que le sens de l’humanité c’est de découvrir que l’humanité n’a pas de sens. 500 000 ans de cogito pour ça. C’est pas la classe ?

<!-- Troisième niveau de réponse -->

`Addon Transclusion unsecure!!jacquouille-la-fripouille.ovh/76543`

`@unsecure!!fernand-est-mort.net/82d2f7e5`

`[vidéo transcrite]`

**Réponse à : A&A égal Al Azif**

C’est des conneries, ça mon pote. C’est juste une vaste fumisterie. Tu crois vraiment que les gars vont parler avec des morts. Je te dis un truc moi, quand t’es mort, t’es mort, basta. Y a déjà pas beaucoup de vie avant la mort, alors après, *que tchi*, *nada*. Les mecs ils t’ont vendu tout ce que tu pouvais acheter, des téléphones qui parlent tout seuls, des voitures qui conduisent toutes seules, même des chiottes qui te regardent quand tu pisses. Il y a davantage de puces téléphoniques en circulation, je veux dire connectées, en fonctionnement, que d’oreilles dans le monde, tu savais ça ? Je l’ai lu dans Libé, c’est pas du flan. Les machins électroniques vivent sans nous, mon pote. Les téléphones ont pas besoin d’oreilles pour se parler. Ta maison a son propre compte en banque, elle paie elle-même ses factures. Les A&A nous ont calqué des IA dans la tronche H24. Quand tu dors, ton lit il te palpe les couilles pour voir si t’en as pas une plus grosse que l’autre, et quand tu te lèves, ta douche, elle t’oblige à te laver. T’as envie d’être crado, tu peux pas. Douche. Toute ta putain de vie, du soir au matin. Alors les gars, ils ont plus rien à te vendre. Donc, ils t’inventent un truc pour quand t’es mort. Ouverture d’un nouveau marché, mon pote. Marco Polo, la route de la soie. On est des putains de vers à soie. Bref, c’est des conneries, mais ça change rien, on n’y échappera pas, et à la fin, que ça soit vrai ou pas, qu’est-ce qu’on en a à foutre ?

<!-- troisième niveau de réponse, même niveau que le précédent donc -->

`Addon Transclusion unsecure!!leon-leon.pi/c-est-pire-que-tu-crois-fernand`

`@unsecure!!fernand-est-mort.net/82d2f7e5`

`[audio transcrit]`

**Réponse à : A&A égal Al Azif**

C’est pire que tu crois, Fernand. Et vous aussi les autres. C’est pire. C’est une conspiration.

<!-- quatrième niveau de réponse -->

`Addon Transclusion unsecure!!fernand-est-mort.net/g6ft890a`

`@unsecure!!leon-leon.pi/c-est-pire-que-tu-crois-fernand`

`[audio transcrit]`

**Réponse à : A&A égal Al Azif**

Tu développes pas ?

<!-- cinquième niveau de réponse -->

`Addon Transclusion unsecure!!leon-leon.pi/ben-non-c-est-evident-non`

`@unsecure!!fernand-est-mort.net/g6ft890a`

`[audio transcrit]`

**Réponse à : A&A égal Al Azif**

Ben, non. C’est évident, non ?

<!-- fin de style forum -->

Un léger signal lumineux partit de l’angle gauche de l’écran triangulaire sur lequel Charlie lisait la discussion enlisée autour du *addon* de Jojo-du-soixante et se dirigea vers un écran situé à proximité du sol, près de son pied gauche. C’était un nouveau post de Fakir. Le deuxième journal de Charlie, picard jusqu’au bout des nuages, comme disait – presque – Renaud. Ils avaient un transcripteur en picard, mais Charlie ne l’avait pas encore appris. Il allait s’y mettre, il avait accès à tous les cours libres mis à disposition sur le réseau par l’association *Notlangue*.

<!-- Début de style forum, niveau 0 -->

`unsecure!!fakir.pi/eutan-asie`

**Cent millions de morts pour un bon mot ?**

Vous n’avez pas encore lu les un million quatre cent mille et quelques documents révélés par Wikileaks ? Vous n’êtes pas en train d’aider le réseau libre à authentifier tout ça ? Un par personne et par jour, c’est le tarif syndical… vous commencez demain ? mon œil ! Vous êtes vraiment des planqués et des glandeurs, c’est tout. Donc, pour les planqués et les glandeurs, ainsi que pour ses chers lecteurs, Fakir vous révèle en exclu l’ultime secret qui se cache derrière tout ce fatras, et il révèle que les présidents eutanien et chinois ont lancé leur programme commun d’exécution des vieux pour le seul plaisir d’accoler les noms de leurs pays-continents et former : Eutan-Asie ! Pour une fois que les types qui gouvernent le monde font preuve d’un peu de sens de l’humour, on ne va pas leur reprocher, hein ? Restez en ligne, Colin vous prépare un dessin avec une mémé et des orties, je ne vous dis que ça…

<!-- style forum, niveau 0 -->

`unsecure!!wikileaks.replicant-2a01:4f8:141:3421::212.onion/eutan-and-china-executed-millions-of-prisonners-and-seniors`

**Les Eutan et la Chine ont exécuté plusieurs dizaines de millions de prisonniers et de personnes âgées dans le cadre du programme de recherche sur les traces quantiques post mortem**

Aujourd’hui Wikileaks publie 1 410 423 documents issus de la *Asian IA Association* – impliquant directement les gouvernements de dix-sept états, dont principalement les Eutan, la Chine, le Brésil, la France, la Picardie et l’Italie, ainsi que quatorze multinationales, dont A&A – dont 1 200 notes et rapports gouvernementaux, 321 000 messages électroniques, 614 000 fichiers de programmes informatiques, le code source complet d’une copie pirate du Dixie Player V1…

<!-- fin style forum -->

Charlie parcourait les titres des documents. À partir de demain, il en lirait un par jour et il aiderait au boulot d’authentification.

<!-- début style forum, niveau 0 -->

`dns-redirect unsecure!!wikileaks.replicant-2a01:4f8:141:3421::212.onion`

**Message de la police du réseau unsecure**

Le serveur réplicat hébergeant le site unsecure!!wikileaks.replicant-2a01:4f8:141:3421::212.onion de Wikileaks auquel vous êtes connecté est en cours d’effacement sélectif. Un nombre important de documents publiés ce jour par Wikileaks sont faux, par décision de la justice eutanienne, arrêté pris ce jour à 14h30 TU. La tentative de poursuivre votre lecture sur d’autres réplicats de Wikileaks, ou sur n’importe quel autre serveur du réseau unsecure constituera une infraction de classe IV de consultation intentionnelle d’information interdite. Ce message constitue une mise en garde explicite.

<!-- Fin style forum -->


## Un simple outil

Alice sortit son ordinateur de sa malle, à l’abri des caméras, le mit en marche. Il vérifia que le bon vieux GNU/Linux qui le faisait tourner était toujours d’aplomb. Il lança quelques scripts, regarda défiler les messages du système sur le terminal.

Alice aurait aimé écrire. Appuyer sur les touches, poser des mots sur l’écran, projeter sa pensée, lentement. Il pourrait commencer une sorte de recueil, des morceaux de récits décousus, des tranches de vie, de ce qui l’entourait même si c’était pas forcément vivant, tant que ça bougeait. Il se laisserait aller à essayer d’anticiper. Il inventerait des réalités parallèles, à peine décalées, quelques mètres, quelques jours. Après tout, écrire, c’est toujours tracer une parallèle. Vivre aussi, d’ailleurs. Chaque jour, Alice passait de longues heures allongé seul sur son lit, déconnecté autant qu’on peut l’être, tous écrans éteints, et il inventait des histoires dans sa tête. Il imaginait. Il appelait ça des visions. Comme une vieille diseuse de bonne aventure bidon. Des visions pour rigoler. Pour du beurre. Pour de rien, comme disent les marmots. Personne ne lirait jamais rien. Mais s’il se mettait finalement à écrire, il avait tout prévu. Il avait un disque dur magnétique externe, une unité de stockage entièrement locale. Son ordinateur était vraiment déconnecté. Il avait lui-même désossé sa *bécane* – comme dirait Bob – pour lui extraire physiquement tous ses moyens de communication. Pas une onde ne pouvait la traverser. Un bunker. Il écrirait hors champ de ses caméras. En silence. Un petit îlot numérique isolé. Alice se demandait parfois combien il en restait qui jouaient encore à se cacher. Il s’en foutait. Bien sûr, quand il canerait, le disque serait récupéré, décrypté et le contenu serait finalement lu. On écrit toujours pour être lu, d’une façon ou d’une autre, il ne pourrait pas y échapper. Le technicien, il se marrerait en découvrant ses conneries, tout ce qui était sorti sans assistance artificielle de sa pauvre tête d’humain. Peut-être même qu’il le diffuserait, sur un serveur quelconque. Un serveur de conneries. C’est pas ça qui manquait. De toutes façons ce serait sûrement lu par une IA, pas par un technicien. Sûr qu’elle se marrera pas. Qu’est-ce qu’il en aurait à battre de toutes façons ? Peut-être même qu’il cramerait le disque avant de décaniller. Il gardait des allumettes et un petit bidon d’essence pour briquet dans la malle. Sûrement pour se donner l’impression de faire quelque chose de subversif. De nihiliste. Écrire pour personne, vraiment, finalement. Des conneries.

Cet après-midi-là, pour la première fois, Alice commença à taper sur son clavier, à remplir la page virtuelle de caractères virtuels. Il commença une histoire.

<!-- début du récit d'Alice, proposition : marge droite et gauche -->

Ils avaient voulu nommer leur protocole *freedom* – je te dis « ils », mais je pourrais dire nous, j’étais avec eux alors – mais les autres n’avaient pas voulu. *Les autres veulent pas, les autres y disent comme ça*… Les autres, le Consortium, quelques grandes multinationales de l’informatique, des télécommunications, mais surtout des services en ligne. Ils avaient accepté qu’un réseau neutre se maintienne en parallèle après l’accaparement de ce qui s’appelait encore le Web. Ils avaient accepté l’existence du réseau libre comme acceptent les vainqueurs. Comme des Romains. Ils gagneraient par assimilation plutôt que par génocide. Un choix. Plus long, peut-être, plus propre, plus radical. Donc, ils n’avaient pas eu le droit à freedom://, à la place on leur avait proposé unsecure!!. En même temps, la liberté c’est l’insécurité. Presque du Orwell. Voilà ce qu’ils s’étaient dit, les hackers. Ils en jouaient, ils aimaient croire qu’ils étaient l’insécurité. Mon cul ! Ils arrivaient à peine à se faire peur entre eux. Je parle de la poignée d’utilisateurs qui utilisaient encore vraiment le réseau, pour faire de la politique, ou quelque chose qui y ressemblait. L’histoire retiendra que la chute du réseau libre avait commencé avec les révélations de l’ex-cryptanalyste Sara Picard, interviewée par le journal Mediapart depuis la prison de Nanterre, où elle était enfermée pour trafic de drogue, traite d’êtres humains, complicité de meurtre de masse et crime contre l’humanité, rien que ça ! Sara Picard affirmait que la multinationale A&A avait ou aurait dans un futur très proche les moyens de décrypter les manifestations quantiques *post mortem*. On savait depuis pas mal de temps que la mort d’un être vivant laissait une sorte de trace dans l’espace qu’il occupait au moment fatidique, une information accessible au niveau quantique seulement, qui semblait indélébile. Mais personne n’avait la moindre idée de ce à quoi ça correspondait. Jusque-là. A&A n’avait pas réellement tremblé ce jour-là, malgré ce qu’on avait dit, pour se faire plaisir, pour se faire peur, mais elle avait tout de même frissonné un poil. Et les représailles, le procès de Mediapart, avaient été sans pitié. Elles visaient la mise à mort, clairement. Enfin, c’est ce qu’avaient pensé des gens comme moi, comme Charlie ou Bob, comme toi peut-être aussi, allez. Mais la plupart, les autres, ils s’étaient juste dit un truc à table le soir. Genre : « quand même ». À la fin, le procès n’avait pas sonné le glas du journal du réseau libre, mais quand son IA a été discréditée, il ne s'est plus relevé. Bien sûr l’amende était disproportionnée, mais les souscriptions volontaires avaient tout couvert en trois jours. Ça avait semblé tenir. À ce moment-là, il y avait peut-être un milliard de gus qui se connectaient chaque jour au réseau libre. Bon, pas longtemps, souvent pour du porno ou pour suivre des histoires un peu graveleuses, des faits divers bidons, du people, les mouvements complotistes fumeux aussi, mais tout de même, l’interview de Sara Picard, ça avait été quelque chose.

A&A n’avait pas vraiment écrasé Mediapart à cause des révélations de Sara Picard. Elle les avait bien emmerdés, c’est sûr, ils avaient dû avancer leur programme de six mois, et ils avaient dû sortir un truc pas encore sec. Leur DixiePlayer V1, c’était une grosse daube. On ne comprenait rien de ce qu’il sortait, il y avait des formes, des couleurs, mais ça ne voulait rien dire. Tu voulais parler avec les anges, tout ce que t’avais gagné c’était l’impression d’avoir bouffé des acides. Bref, ça les emmerdait d’être brusqués comme ça, de devoir faire des efforts. Mais c’était surtout une question de principe. À ce moment-là, le fait qu’il restait quelques grandes gueules, quelques francs-tireurs que le système n’arriverait pas à digérer n’était plus un problème depuis longtemps, mais s’en débarrasser, ce n’était plus un problème non plus. Comme une mouche qui te casse un peu les noix – si t’en as – tu sais que tu risques pas le palu, mais si elle est posée à côté de ta main, tu claques un coup sec. Et basta.

En trois jours, il y a eu une dizaine de cas qui sont sortis. Des choses sans importance, ici une erreur de statistiques, là une relance mal à propos, un contexte mal compris. Rien. Et puis une accusation totalement absurde : de la fabrication de fausse monnaie électronique. Du très sérieux, mais aucune IA ne faisait ça. Ça n’était pas possible. Aucun informaticien ne pouvait croire cela. Personne n’y croyait vraiment, malgré le bombardement de rumeurs. Personne n’y croyait vraiment, mais ce qu’il y a de génial avec la diffamation, c’est qu’il en reste toujours quelque chose, des blagues, des doutes, une théorie fumeuse. Et il n’y a pas de fumée… La réputation de l’IA était touchée, et quand l’IA d’un journal comme Mediapart tombe, le journal est mort. Les propriétaires n’avaient pas les moyens de s’en payer une autre. Ils auraient pu réessayer un financement participatif, mais se payer une IA, c’était au-dessus, sans compter que les militants venaient déjà de passer à la caisse, ils avaient absorbé le procès. Ils le savaient, à Mediapart que c’était mal barré, même si jusqu’au bout tous avaient gardé la tête haute, balançant tout ce qu’ils avaient, toutes leurs cartouches y étaient passées, ils travaillaient jour et nuit. Un baroud d’honneur. Ce qui est symptomatique de l’époque c’est qu’il n’y a pas eu une seule attaque contre le webanimateur qui avait mené l’interview. Jean Lassereille ne représentait aucun danger. Sans son IA, c’était juste un brailleur, un peu plus malin que la moyenne, probablement, qui s’était construit un réseau de contributeurs exceptionnel, certainement. Pendant les deux semaines de qualification de l’IA qui avait suivi la campagne de discrédit – ils avaient mis trois auditrices en parallèle, elles avaient tout disséqué bit par bit – l’audience de l’émission s’était écroulée. Des chiens sans dent.

Deux mois après les gens étaient passés à autre chose. À rien. Ils en parlaient encore, à table, comme je disais, mais quant à se battre… Ça faisait longtemps que les gens ne se battaient plus. Pour rien. Même pour leur gueule, ils avaient laissé tomber. Ils vomissaient leur mécontentement sur le réseau libre, de la haine accrochée à chaque URL, mais c’était plus un exutoire, une sale habitude. Après, ils touchaient leur RU et ils glandaient que dalle. On glandait que dalle, c’est ça. Une sorte de jardin originel. Rien à faire. Même pas à se baisser pour ramasser les pommes, tu demandes à ta machine. File-moi une pomme, machine. On en a rêvé, on l’a fait. Et c’est un cauchemar. Alors pour pas te faire trop chier, tu t’inventes un monde, artificiel. Mais comme t’es resté plus proche du singe que du démiurge, malgré tout, ton imagination pourrie te fait un monde pourri. Après, je ne sais pas, peut-être que le vrai monde est aussi artificiel. En tous cas il est bien pourri. Bref, après quelques mois, on n’était plus que quelques millions à utiliser encore le réseau libre. C’était pas illégal, pas du tout, pas besoin, dès qu’un truc pouvait être intéressant, c’est-à-dire juste pas complètement débile, même pas subversif, n’importe quoi avec un peu de sens en fait, tu te colletais un procès en règle. Contrefaçon, la plupart du temps. C’était le plus simple. Si ça ressemblait de près ou de loin à une idée, ils te disaient qu’elle était déjà en développement dans un labo quelque part. Un labo d’A&A. Et c’était vrai ! Ils faisaient tourner des IA, des dizaines, en permanence, pour formuler toutes les idées possibles et imaginables, toutes les opinions. Les opinions aussi étaient soumises à la propriété intellectuelle, déjà. Il y avait des milliards et des milliards de trucs logués sur leurs machines. Chaque fois que tu pensais un truc, tu pouvais être sûr que c’était déjà dans la bibliothèque d’A&A. La bibliothèque de Babel. Ça venait d’une nouvelle, qu’un hacker avait ressortie, qui décrivait une bibliothèque presque infinie qui contenait tous les livres du monde, déjà écrits. L’article se voulait à charge, mais A&A avaient laissé diffuser. Tu peux être sûr qu’ils l’avaient déjà, ce truc, l’idée de la bibliothèque était déjà dans la bibliothèque, mais ça servait leur propos. Si tu sais qu’ils savent tout, tu te fais même pas chier à chercher quoi que ce soit. En fait, les procès n’étaient pas tout le temps gagnés par A&A, il restait encore des bribes d’originalité humaine à gratter, des livres qui n’étaient pas encore dans la bibliothèque, mais dans les faits la pression était insoutenable. C’était la créativité qui était touchée. Au cœur. Au sein. Aux couilles. Il restait une poignée de types qui essayaient, plus pour jouer qu’autre chose d’ailleurs, mais ils n’avaient pas les moyens de tenir, à court terme, ils étaient déjà hors-circuit, ils ne le savaient pas encore. Ou ils le savaient déjà. T’avais le droit de le penser, au moins ils ne savaient pas encore t’en empêcher, et sûrement qu’ils s’en foutaient finalement, il n’y avait pas besoin d’empêcher les gens de penser, il suffisait de les empêcher d’échanger leurs pensées. C’était le paroxysme du développement des réseaux de communication : plus de communication. Seulement des signaux, sans intérêt. Du bruit. Un gros moteur qui tournait à vide et qui faisait du bruit. Un gros moulin, mais pas de blé à broyer. Alors tu broies du noir.

Et moi là-dedans ? Je regarde le truc, j’ai bien compris l’histoire tu vois, puisque je te la raconte. Mais j’ai réussi à me convaincre que c’était comme ça. Que c’était bien comme ça, presque. C’est ta seule liberté mon pote, vouloir ce qui t’arrive, aimer ce que tu ne peux pas changer. Et bricoler à la marge.

Alors je collabore.

C’est ce que je veux, tu vois. T’as pas besoin de t’aveugler ou de regarder tes pieds. Tu regardes devant toi au contraire, tu vois un horizon, un truc qui pourrait être un bout de terre, un coin de ciel plus bleu qui se rapprochera peut-être. T’as même pas besoin d’y croire. Tu regardes juste au loin, comme ça, tu gardes les yeux dans le vague, les variations de lumières colorent le sable et la mer, disons que t’es sur une plage, pour l’horizon c’est plus classe, du coup t’as même le bruit des vagues, tu trouves ça beau ou chiant, mais en fait tu t’en fous, t’es là c’est tout, il y aura sûrement un grain un peu plus tard, après le soleil se couchera, ce sera peut-être beau à nouveau, ou chiant, il se relèvera le lendemain, de ça t’es presque sûr, sauf qu’on est sûr de rien, et ça recommencera, plus ou moins pareil, jamais tout à fait. Tu vois, t’es là et t’as pas besoin de te demander pourquoi il y a du soleil et des vagues, si c’était mieux quand il y avait moins de vent, ou si ce sera mieux avec un peu plus de chaleur. S’il y a moyen de choper un coin d’ombre sans trop bousculer ton voisin, tu te fais une place. Le reste c’est de la philosophie. Une piña colada en te demandant si la terre est ronde. Au mieux, c’est de la consommation. Ça te sert à regarder ailleurs pendant que tu pourris sur pied.

Vivre, c’est juste attendre. Avec le soleil dans la gueule.

Je gratte ce bout d’histoire en plus. Petit supplément d’âme. Si t’arrives à te croire au-dessus du lot, là t’es un roi, t’es un dieu. Dans ton monde. Pourri.

Les gens croient que tout a commencé avec l’arrivée des ordinateurs, qu’on avait commencé à se faire bouffer à ce moment-là. Mais la vérité, c’est que ça a commencé avec le premier silex, la première fois qu’un mec a commencé à tailler une pierre pour mieux buter un mammouth, ou son voisin, ou pour se faire un peu de chaleur. L’homme était baisé dès l’origine. Après il y a eu une hache, ou une houe, on se disait c’est plus facile comme ça, mais la vérité c’est que rapidement, sans une houe, il y avait juste plus moyen de bouffer. On te prend ta machine t’es plus rien. La houe a inventé le paysan, l’épée a inventé le soldat, la machine à vapeur a inventé l’ingénieur, et l’ouvrier, et finalement l’ordinateur a inventé l’informaticien, et puis, surtout, l’ordinateur a inventé l’utilisateur. Le simple utilisateur. Celui qui sourit béat comme un dévot devant son écran quand ça marche et qui dit que c’est pas de sa faute quand ça merde. Les ordinateurs ont réussi à faire accepter aux hommes que l’idéal de la technique c’est quand ils n’ont plus rien à comprendre, plus rien à faire, que le contrôle c’était chiant. Mais dans le fond, c’est pas nouveau, c’est jamais l’homme qui a fait les machines, depuis le début, c’est la machine qui fait les hommes. Quand t’as compris ça, t’arrêtes le combat, lutter contre les machines c’est lutter contre toi-même. Un homme n’est rien sans son couteau. Sa bite, il peut s’en passer, à l’aise, c’est pas une image, je sais de quoi je parle, mais sans son couteau, c’est rien, *nada*, *que tchi*. Ferme ton terminal, c’est tes yeux que tu fermes, ta pensée que tu éteins, tu t’endors. Éteins les IA, c’est l’humanité que tu éteins. De toutes façons, ça fait longtemps qu’il n’y a plus de bouton. Ni sur les IA, ni sur les terminaux.

<!-- fin récit d'Alice -->

Les écrans d’Alice s’éclairèrent brusquement et son terminal sous-cutané s’activa. C’était l’heure de bosser. Deux heures de programmation, son lot quotidien, trois jours par semaine. Alice travaillait sur les flux de données, il aidait une IA à valider ses analyses. Programmer pour Alice, se limitait à dire à une IA si elle avait raison ou tort selon lui. Alice produisait juste des bits de validation. Zéro quand l’IA se trompait, un quand c’était bon. Il ne manquait à l’IA que de perfectionner sa relation aux valeurs de vérité telles que les humains les concevaient, sa relation au réel perçu par les humains. Alice représentait l’avis des hommes sur les assertions de l’IA, il l’aidait à construire une représentation du monde qui soit adéquate à l’humanité, fonctionnelle. Bob disait que c’était des conneries, que ça servait juste à faire croire aux humains qu’ils servaient encore à quelque chose.

— Bonjour Alice.

— Va mourir.

— Ceci est une assertion décalée, impolie, agressive.

— Un.

Alice avait pris l’habitude de commencer ces sessions en insultant l’IA, une façon de *switcher*. Pour celle-ci c’était juste un moyen supplémentaire de renforcer ses représentations sur le monde, même si c’était hors programme. Les deux y trouvaient leur compte.

— Notre séance de programmation portera aujourd’hui sur l’histoire des guerres. Première assertion : la seconde guerre mondiale – c’est le nom de la guerre qui s’est déroulée au milieu du vingtième siècle et qui a opposé la plupart des grandes puissances d’alors – fut la pire guerre de l’histoire de l’humanité.

— Un.

— La majorité des habitants de ces anciennes puissances ne saurait pas énoncer de mémoire les années de début et de fin de ce conflit, ou ne savent pas citer cinq pays ayant participé au conflit.

— Un. De mémoire ? Tu te fous de ma gueule ?

— Une IA ne sait pas se moquer.

— Zéro. Mon cul.

— L’Allemagne était responsable de cette guerre.

— Un.

— L’Allemagne était seule responsable de cette guerre.

— Zéro.

— Les guerres sont néfastes au développement de l’humanité.

— Un.

Tu devais répondre zéro ou un. Aucune nuance n’était possible. La nuance c’était le domaine réservé de l’IA. Alice ne savait pas ce qui était le plus frustrant, deviser sur le monde avec un putain de robot, ou n’avoir pas le droit à une pensée un tant soit peu élaborée sur ce putain de monde. Alice n’était pas informaticien, c’était une porte logique, un transistor, un bit. Il ne te manque qu’une seule lettre finalement, lui disait Bob, pour déconner.

— Le nazisme est une pensée néfaste au développement de l’humanité.

— Un.

— Toute forme de pensée extrême est néfaste au développement de l’humanité.

— Un.

— Le techno-nihilisme est néfaste au développement de l’humanité.

— Un.

Le schéma était toujours plus ou moins le même. Un mélange de choses très factuelles et d’opinions sur l’humanité. Parfois, je me demande si ce ne sont pas elles qui nous programment.


## L’option

— C’est bien monsieur Khan, votre entraînement progresse, vous parvenez à invoquer assez rapidement à présent, moins de trois minutes pour cinquante pour cent de vos préparations actuelles. Vous savez que la moyenne est autour de soixante pour cent ? Et il n’y a pas trop de parasites. On va continuer de travailler sur les parasites, c’est le plus important à ce stade. Vous vous entraînez combien de temps chaque jour ? Une heure ? Environ. Vraiment ? C’est bien. Vous faites les exercices respiratoires aussi ? Parfait. Passons maintenant à vos nouvelles préparations. Sur quoi souhaitez-vous travaillez ? Ha ! Du Brel, l’envolée de Frida, un grand classique. *Et puis y a Frida, qu’est belle comme un soleil et qui m’aime pareil que moi j’aime Frida, même qu’on se dit souvent qu’on aura une maison, avec des tas de fenêtres, avec presque pas d’murs, et qui fera bon dedans et qui fera bon y être, et que si c’est pas sûr, c’est quand même peut-être*… J’en pleure chaque fois, monsieur Khan. Vos enfants, petits, quand ils jouaient ensemble dans votre jardin. Parfait. Vous avez conservé des vidéos, c’est bien ça, tout le monde n’est plus aussi prévoyant de nos jours. On pourra faire un peu d’hypnose pour réactualiser au besoin. Et un extrait de *La horde du contrevent*. Vous aviez vingt ans quand c’est paru ? Oh non, c’était pas hier, vous l’avez dit. C’est une bonne idée de vouloir emporter un peu de ses vingt ans. Et une séance intime avec votre femme, on n’y échappe pas non plus. On n’avait pas encore travaillé là-dessus, mais on y vient tôt ou tard. Et mieux vaut tôt que trop tard, comme je dis toujours. Ha ? Avec une femme, pas forcément la vôtre. Je vois. À votre âge, c’est moins facile, c’est sûr, mais encore tout à fait possible. Il n’est pas trop tard, quand on s’y prend assez tôt. Tiens, vous voyez je vous le disais ! Je vous indiquerai un très bon établissement, en Picardie. C’est salutaire quand on a pas eu beaucoup d’opportunités de ce côté-là. Comme je dis toujours, ce plaisir-là est le seul qui soit à la portée de chacun. Enfin presque. Je conseille toujours d’en glisser au moins une pincée. Bon, je ne vous le cache pas, certains ne mettent même que ça. Vous n’avez jamais consulté *porn.dixie.aa* par exemple ? Non ? Allez jeter un œil à l’occasion. Par curiosité. Mais vous, vous êtes un homme de culture, monsieur Khan, vous vous lasseriez. Oui, et un homme de famille également. Mais un peu, je le conseille quand même. Tenez, je vous note une adresse. Ce n’est pas loin d’ici, une petite heure, deuxième sortie sur l’A1 juste après la frontière. Si vous avez eu le temps d’ici là, on pourra le travailler la prochaine fois. Pour aujourd’hui, je vous propose *La horde*, vous avez déjà bien bossé et, comme je dis toujours, faut pas trop pousser. Vous avez choisi un passage ? Essayez de vous remémorer la première fois que vous l’avez lu, où vous étiez, quel temps il faisait ; était-ce dans votre lit, dans un fauteuil, est-ce que vous fumiez une cigarette ? Essayez de revivre ce moment comme si vous aviez la chance de ne jamais avoir lu ce livre encore. Mémorisez l’instant, oubliez le texte, et redécouvrez-le. *À l’origine, fut la vitesse, le pur mouvement furtif, le vent-foudre. Puis le cosmos décéléra, prit consistance et forme, jusqu’aux lenteurs habitables, jusqu’au vivant, jusqu’à vous. Bienvenue à toi, lent homme lié, poussif tresseur des vitesses. Nous sommes faits de l’étoffe dont sont tissés les vents*. Voilà, lisez. Regardez les lettres danser. Rendez-vous à Alticcio, pour écouter la joute de Caracole. Laissez-vous porter.

Les minutes s’écoulaient, tandis que monsieur Khan lisait avec bonheur, un bonheur qui se diffusait dans le cabinet d’Hector, palpable. Hector aimait vraiment ces moments. Il construisait de l’éternité, et de l’éternité heureuse. Lorsqu’il sentit que le moment de grâce touchait à sa fin, il y mit fin d’un coup, un peu brutalement même, mais il fallait aider le client à ciseler un moment parfait. Les mauvais consultants laissaient toujours traîner quelques secondes de trop, mais une ou deux secondes qui tournent en boucle pendant une éternité, ça finissait par faire beaucoup de secondes, à la longue. Hector en était parfaitement conscient, et il était intraitable avec les dernières secondes. Il y avait encore du boulot avec Khan, il avait commencé un peu tard, son cerveau n’était plus des plus agiles, mais il avait encore le temps. Il était confiant.

Vous auriez voulu ajouter une page ou deux, vous aimez bien la fin aussi ? Le pont de Golgoth. Vous êtes un gourmand monsieur Khan ! On verra la prochaine fois. La prochaine séance nous devrons également aborder les réflexes de survie. C’est une partie importante du programme, gagner quelques minutes au moment fatidique peut être décisif. Vous n’avez aucune formation médicale ? Pas d’arts martiaux ? Pas fait l’armée ? Des jeux vidéos peut-être ? Même pas ! Bon, on commencera par le commencement, comme je dis toujours, c’est le meilleur moyen de bien finir par la fin. Et puis vous avez encore le temps, vos statistiques vous donnent toujours une probabilité de quatre-vingt-dix-neuf pour cent à dix mille jours. C’est une chance à votre âge. Mais il y a toujours le un pour cent à ne pas négliger, n’est-ce pas ? C’est le un pour cent embarrassant, comme je dis toujours. Vous êtes à jour de vos données ? Alcool, tabac, maladies infectieuses, contact avec les populations à risque, un séjour en Picardie peut-être ? Je plaisante… Mettez quand même à jour vos vaccins si vous y allez la semaine prochaine, vous avez bien rangé le petit papier ? Ce serait dommage de passer en programme accéléré, n’est-ce pas ? Comme je dis toujours, rien ne sert de courir.

— Merci Hector, vous pensez vraiment à tout, mais je suis bien à jour de mes données, et même de mes vaccins. Je travaille au ministère, vous vous souvenez ?

— Vous travaillez, c’est vrai. C’est parfait ça. Au ministère, c’est vrai. Donc vous êtes à jour. Et financièrement vous parvenez à supporter notre programme de consultance, n’est-ce pas ?

— Ça va. Ça va aller.

— Parfait, monsieur Khan. Parfait. Il ne me reste plus qu’à vous souhaiter une très bonne fin de journée. Continuez d’être rigoureux dans vos entraînements et, si vous avez un peu de temps, vous pouvez commencer à étudier en autonomie le programme « réflexes de survie », niveau un, sur notre site. C’est gratuit. Pour les clients. Vous économiserez quelques séances ainsi. Voilà. Et, je vous rappelle aussi que vous n’avez toujours pas souscrit à l’option. Vous pouvez le faire à tout moment sur notre site, comme je dis toujours, c’est un simple clic ! Bonne journée monsieur Khan.

— Au revoir Hector, merci.

<!--SAUT1-->

Hector Louriski referma la porte derrière son client, jetant discrètement un coup d’œil à la salle d’attente. Deux clients attendaient. Grégoire, en avance comme toujours, et un nouveau. Ce serait le nouveau d’abord.

<!--SAUT1-->

Paul Grégoire était très en avance. Il était toujours en avance. Surtout chez son consultant. Il essayait de répéter ses exercices, Paul était très sérieux, très studieux. Il s’entraînait la majorité de son temps, depuis deux ans, depuis qu’il avait démarré son programme Ubik. Il était très content de son consultant actuel. Hector était un excellent consultant. Il avait essayé deux autres consultants avant lui, mais ça n’avait pas collé. C’était normal. Ce que lui avait dit le support A&A, il avait contacté leur service indépendant et gratuit, c’est qu’il fallait souvent plusieurs tentatives. Il fallait persévérer. Le support A&A avait bien raison. Il avait fait de très gros progrès depuis deux ans, il se sentait presque prêt. Pourtant Paul Grégoire n’avait pas du tout confiance en lui. Il n’avait rien réussi d’important dans sa vie. Il ne voulait pas rater sa mort. Assis dans la salle d’attente, il essayait de répéter ses exercices, mais son attention était distraite par la présence de l’autre homme. Il passerait sûrement avant lui, son rendez-vous devait être programmé avant. Lui, Paul, était très en avance. Comme toujours. L’homme avait l’air stressé de se trouver ici. Ce devait être sa première séance. D’ailleurs il ne l’avait jamais croisé jusqu’alors. Certains pensaient qu’on pouvait être tué lors de la première séance. Des histoires circulaient. C’était absurde, il fallait s’entraîner longuement. Et chacun était libre de choisir ou non l’option. Lui, Paul, avait pris l’option. C’était une dépense importante dans son budget, mais il n’avait pas assez confiance en lui, il préférait s’en remettre au consultant. C’était son métier après tout. Il ne s’arrachait pas les dents lui-même, n’est-ce pas ? Parfois, les gens ne sont pas raisonnables. Quand ils ont peur, ou s’ils sont trop pingres. Le nouveau était plongé dans ses lunettes, il devait être en train de visionner en boucle la vidéo explicative du programme Ubik. Paul connaissait le programme par cœur. Il l’avait appris par cœur. Pourquoi pas ? Il valait mieux être sûr de tout avoir bien retenu. Les consultants étaient indépendants du programme d’A&A, mais tout de même c’était grâce à Ubik qu’on pouvait bénéficier de leurs services à présent. Et c’était Ubik qui enregistrait au moment fatidique, il était important de bien comprendre comment cela fonctionnait.

<!--SAUT1-->

Hector n’aimait pas trop les nouveaux contrats. Les types flippaient, on n’avançait pas. Il devrait prendre sur lui. En revanche, il attendait beaucoup de la séance avec Grégoire, avec un peu de chance ce serait plus constructif. Il se servit un fond de whisky. Toujours entre deux clients. Un des meilleurs des Highlands, encore produit artisanalement. Enfin il n’était pas allé vérifier. Il devrait y aller. Il en parlerait à son consultant, voir si ça valait le coup.

— Hélène, au suivant.

<!--SAUT1-->

— Bonjour Monsieur Loan. Je suis Hector Louriski, Vous pouvez m’appelez Hector. Je suis vraiment ravi de vous rencontrer, j’apprécie vraiment les nouvelles recrues. Je vous félicite tout d’abord d’avoir entamé cette démarche. C’est difficile pour chacun de nous, mais si indispensable. Vous en êtes déjà conscient, mais je vais vous aider à en être pleinement conscient. Vous m’avez choisi comme consultant, j’espère que je ne vous décevrai pas, mais si vous avez le moindre doute, n’hésitez pas à vous en ouvrir à moi, je pourrai toujours vous orienter vers un de mes collègues. Vous savez, on ne trouve pas toujours son consultant du premier coup. Hélène vous a peut-être déjà remis la brochure ? Parfait. Vous avez bien revisionné la vidéo du programme Ubik, monsieur Loan ?

— Oui… je crois. Sa voix était à peine audible.

— Alors pour commencer notre première séance, je vais vous demander de me dire ce que vous avez retenu du programme, et ce qui est important pour vous.

— Mais… vous…

— C’est le protocole, faites-moi confiance. Décrivez simplement ce que vous avez vu dans la vidéo. Il n’y a pas de bonne réponse.

— Alors… Quand on meurt… on peut, comment dire, enregistrer ? C’est cela ?

— Oui, continuez. Allez-y.

— Enregistrer ses pensées… enfin, ses dernières pensées. C’est ça ? Seulement les dernières ? Mais de combien de temps en arrière ? ce n’est pas précisé… où je n’ai pas…

— C’est variable, mais en général un temps assez court, quelques minutes.

— Ce n’est pas beaucoup.

— Eh non, monsieur Loan, c’est même très court. Et c’est pour cela que vous êtes là, et comme je dis toujours, c’est aussi pour cela que je suis là. N’est-ce pas ?

— Oui…

— Dites-le, c’était dans la vidéo, pourquoi sommes-nous là ?

— Parce que je veux, enfin je voudrais bien, que ces derniers instants, ceux qui seront enregistrés, soient de beaux instants.

— Exactement, monsieur Loan, c’est exactement ce que vous voulez, de beaux derniers instants, à enregistrer.

— Pour ma famille.

— Pour votre famille… oui, si vous voulez, mais pour vous aussi. Surtout. Ces moments vous allez les revivre éternellement, vous avez bien compris cela monsieur Loan ?

— Oui. Éternellement. C’est long.

— C’est très long, monsieur Loan, très. Qu’avez-vous vu d’autre dans cette vidéo, dites-moi ?

— Que c’est difficile… et coûteux… et long…

— Malheureusement oui, c’est difficile. Vous avez des problèmes financiers ? Vous êtes au RU, je sais que c’est difficile. Enfin on me l’a dit. Mais, vous savez, c’est le cas de la majorité des gens. Je vous expliquerai comment aménager vos besoins de façon à pouvoir suivre correctement mon programme. Mon job est aussi de vous expliquer comment me payer ! Je plaisante, comme je dis toujours, il faut savoir plaisanter quand les choses sont sérieuses. Tenez, on va faire un petit test, comme ça, à froid. Quand je compterai jusqu’à trois, vous devrez ne penser qu’à des choses très agréables, ne pensez pas à une personne qui vous aurait quitté ou humilié, ne pensez pas à un parent disparu, ne pensez pas à la sensation de soif ou de faim, ne pensez pas à une douleur, ni à une maladie, rien de malheureux. Prêt ? Un, deux, trois… Alors, à quoi avez-vous pensé ?

— Je ne sais pas…

— Allez, dites-le-moi !

— Je crois que c’était la perspective de la mort de mon fils.

— Diable ! C’est sacrément raté ! Heureusement que ce n’était qu’un test. Ne vous inquiétez pas, on a tout le temps de travailler, ensemble, mais vous voyez, c’est difficile, n’est-ce pas ?

— Oui, c’est difficile. J’ai voulu penser à mon fils, mais après…

— Ne vous inquiétez pas, je vais vous expliquer comment faire pour vous préparer de bonnes images et les invoquer – c’est le terme technique – sur commande, comme si vous aviez un bouton. Vous voulez pouvoir invoquer de bonnes pensées quand vous en aurez vraiment besoin, n’est-ce pas ?

— Oui.

— Parfait, vous êtes au bon endroit ! Bon, voyons, passons rapidement sur l’administratif, ce sera fait. Vous avez rempli le questionnaire de santé. Parfait. C’est important que vous nous communiquiez le plus de données possibles, le plus honnêtement possible. Vous portez un capteur universel. Parfait. C’est très utile pour optimiser votre programme, on travaillera différemment si on a du temps devant nous, ou si on est un peu plus dans l’urgence. Ça arrive, on a des solutions pour cela. J’ai déjà préparé quelqu’un en moins de deux mois. Cancer généralisé. Ça vous épate ? Et je ne suis pas peu fier du résultat. Consultez-le à l’occasion, il est public : *jorge-infitum.dixie.aa*. C’était un cas particulier, j’avais posé comme condition que l’enregistrement soit public, mais, de votre côté, vous garderez le choix, bien sûr. Même si, pour ma part, j’aurais tendance à vous le conseiller. Mais vous aurez le choix. Nous devrons aussi parler de l’option. Mais jamais à la première séance. On a le temps. On a le temps, n’est-ce pas ? Pas de cancer généralisé ? Je plaisante bien sûr, vous avez l’air en pleine forme. Quatre-vingt-dix-neuf à dix mille, le maximum. Toujours ce un pour cent pas très rassurant, n’est-ce pas ? Mais on n’arrive pas à s’en débarrasser de celui-là ! Bon, on a le temps, donc. On va commencer par un premier exercice, comme ça vous vous rendrez mieux compte de ce qu’on va faire ensemble. Avez-vous des questions avant de commencer, monsieur Loan ?

Alexandre Loan avait une question, mais il n’osait pas la poser. Il fixait le mur derrière le consultant. Il voulait en détacher son regard, mais n’y parvenait pas. La salle de consultation semblait lisse comme du verre. Elle n’abritait que les deux fauteuils de cuir noir sur lesquels le consultant et son client étaient assis – le fauteuil du client pouvait basculer pour permettre une position plus allongée –, le bureau de plexiglas transparent qui les séparait, et l’étrange monolithe noir, rectangle aplati, large comme un homme, qui touchait presque le plafond, qui semblait absorber toute lumière. L’enregistreur Ubik. Le regard d’Alexandre Loan revenait toujours au même endroit. Il fixait la lourde hache, étincelante, qui trônait au mur, juste derrière le consultant.

— Allez-y, posez votre question.

— C’est à propos de… dans la vidéo, enfin, à propos de la… mort. Violente, c’est cela ? Je ne sais pas… enfin, si j’ai tout compris. C’est nécessaire, c’est cela ?

— Pas vraiment nécessaire, mais c’est conseillé, en effet. Je vous expliquerai en détail le bénéfice que l’on peut tirer d’une mort violente, surtout si vous prenez l’option. Mais comme je vous l’ai dit, nous aborderons cela dans une prochaine leçon, ne vous inquiétez pas de tout cela, vous garderez le choix.

<!--SAUT1-->

Paul Grégoire trouvait le temps long. C’était toujours plus long les premières séances. Il fallait beaucoup se rassurer. Et puis, il fallait accepter de se livrer, revenir aux moments qui nous avaient vraiment rendu heureux. Pas seulement ceux qui étaient socialement acceptables de notre vivant, mais ceux dont on aurait vraiment envie une fois mort. Il fallait en général du temps pour se confier à son consultant. Souvent, la première séance n’y suffisait pas. Hélène le regardait parfois, il lui souriait. Elle lui rendait son sourire. Un peu mécaniquement. Il n’y avait personne d’autre dans la salle d’attente, les autres n’arrivaient pas aussi en avance que lui, en général. Parfois, il croisait son successeur, mais c’était rare. Il passa aux exercices de respiration et de contrôle des sensations corporelles. Les minutes passaient, au rythme des cycles respiratoires très ralentis qu’avait adoptés Paul. L’homme sortit du cabinet d’Hector, son air inquiet ne l’avait pas quitté. Paul pensa qu’il ne le reverrait peut-être pas. Alors qu’il interrompait ses exercices et se préparait à se lever, la porte de la salle d’attente s’ouvrit, et un autre homme entra un peu brusquement, légèrement essoufflé. Paul le connaissait, il s’appelait Alain Boutrou, ils s’étaient déjà croisés à plusieurs reprises ici même, ils avaient échangé quelques mots. Il était picard, frontalier, il venait à Paris exprès pour consulter Hector. Paul l’avait trouvé chaque fois très sympathique, loin des clichés. Il s’était dit d’ailleurs qu’il irait passer quelques jours en Picardie. Il devrait le faire.

— Monsieur Boutrou, vous êtes juste à l’heure, entrez, c’est à vous. Hector vous attend.

Hélène le fit entrer, sans regarder Paul. Paul était désappointé, il ne pensait pas devoir encore attendre, il ne pensait pas être autant en avance.

<!--SAUT1-->

— Bonjour monsieur Boutrou. C’est toujours un plaisir de vous voir.

Hector avait une phrase d’accueil par jour, Hélène veillait à ce que les mêmes phrases ne reviennent jamais deux fois sans que se soit écoulé un mois, au minimum. Hélène veillait à un nombre incroyable de détails qui faisaient le professionnalisme d’Hector. Elle valait bien plus que ce qu’elle lui coûtait.

— Comment se passe l’installation, toujours correctement ? À Château-Thierry, il paraît que ça se passe bien, n’est-ce pas ?

— Oui, ça va. Ce n’est jamais facile de voir doubler la population de sa ville en deux mois. D’autant que nous n’avons pas les mêmes coutumes, n’est-ce pas ? Mais ça va, ils sont corrects.

— Vous savez les juifs et les chrétiens partagent une histoire commune, on n’est pas si différents.

— Non, vous avez raison, on est tous des humains après tout, Juif, Noir, Arabe, qu’est-ce que ça change ?

— Vous avez bien raison.

— D’autant qu’ils restent surtout entre eux, ils ne dérangent pas. Et puis, il y a pas à dire, ça a relancé le commerce dans la région, l’Aisne se porte beaucoup mieux depuis l’arrivée des colons. On les appelle comme ça pour plaisanter, mais les relations avec Israël sont vraiment bonnes. Ils ont fait beaucoup pour nous, vous savez.

— Tant mieux, tant mieux. Finalement ce n’était pas une si mauvaise idée.

— Vous savez, moi, j’ai toujours été pour. Je ne suis pas raciste.

— C’est bien, ça.

— Et vous savez Hector, on sait bien que les Parisiens nous appellent les *haineux*, mais les gens de l’Aisne ne sont pas racistes, dans le fond ; ils sont un peu frustes, ça c’est sûr, mais au fond ils sont assez ouverts. Bien sûr à un moment ils ont été salement en colère et ce que les gens ont fait pendant les événements, ce n’était pas bien, mais c’est du passé. Maintenant on est très ouverts. Et là vraiment, avec les Israéliens, ça se passe bien.

— Les Parisiens ne sont pas racistes non plus, bien sûr, mais tout de même, ici, à Paris *intra muros* je veux dire, on ne pensait pas qu’il y aurait des installations aussi proches. On est à quoi, cinquante kilomètres ? Enfin, je dis ça, c’est chez vous, vous faites bien ce que vous voulez… Bon, comme je dis toujours, je suis payé à l’heure, et vos Juifs ne vous ont quand même pas encore rendu riche j’imagine ? Ce n’est pas contagieux, n’est-ce pas ?

Les deux hommes rirent de bon cœur. Décidément ce monsieur Boutrou était une bonne pâte. Hector ne jugeait jamais négativement ces clients.

— Alors, où en était-on ? On avait fini avec ce pauvre clochard la dernière fois. On en était resté avec un petit chaton, n’est-ce pas ? Vous voulez reprendre avec le chaton ?

Le compte-rendu de leur dernière rencontre était projeté sur son bureau. Hélène y avait veillé.

— Oui, si c’est possible. J’aimerais bien. Avec le chaton.

Hector ne jugeait jamais ses clients sur leurs opinions, ou leurs fantasmes, ni même sur leurs actes. Tout le monde avait le droit à une bonne mort, il disait toujours. Il avait conseillé un militaire qui rêvait de revivre des scènes de massacre (*medal-of-honor2033.dixie.aa*), il avait reçu un violeur (*nirvana_myfriend.dixie.aa*, très consulté sur le réseau libre d’ailleurs), une femme dont le seul plaisir était de martyriser son bébé. Son enregistrement ne contenait que ça. On avait remémoré ses scènes de gifle à en avoir mal aux mains. Malheureusement, elle n’avait pas voulu publier son enregistrement. Bon, avec la nouvelle loi sur la libération des enregistrements *post mortem*, le profil devrait être quand même rendu public d’ici six ans. Il y avait aussi celle qui jouissait de se flageller jusqu’au sang ou de se mutiler, comme s’arracher les ongles ou se taper la tête contre un mur. Pour sa préparation, elle s’était coupé tous les doigts d’une main, avec une scie à métaux. C’était Hector qui le lui avait conseillé. Et c’était un des souvenirs qu’elle avait réussi à emporter avec elle. Et ça, c’était en ligne ! *give-me-five.dixie.aa*. Chaque fois, il avait fait son job. Il n’était pas juge ou psy, il était consultant en optimisation d’enregistrement *post mortem*. Point. Et après la mort, il n’y avait ni morale, ni loi. C’était un espace de liberté absolue. Certains pensaient que quelques minutes c’était étriqué, insuffisant. Mais, disait toujours Hector, donnez-moi seulement trois minutes, parfaitement choisies, en totale liberté, et je vous construis un monde. Hector était un conseiller en construction de monde. Un demi-démiurge. Un passeur en tout cas. Quand il avait réussi à faire comprendre ça à ses clients, il avait fait la moitié du boulot, il pouvait aller chercher le meilleur pour eux-mêmes. Le reste c’était surtout technique. C’était décidément un bon consultant.

— Bien sûr que c’est possible, tout est possible. Comme je dis toujours, en anglais, *your life, their rules, but, your death, your rules*. Suivez les règles pendant votre vie, mais choisissez les vôtres pour votre mort. Oui, c’est toujours plus long en français. Bon, je vous laisse quelques minutes pour vous mettre en disponibilité mentale d’invocation, vous vous souvenez de cet exercice d’auto-hypnose ? Nous l’avons répété la dernière fois. Parfait. Je relis notre transcription de la semaine dernière pendant ce temps, avec ce petit chaton. Il était blanc et noir, c’est bien cela ? Laissez bien pénétrer la scène dans votre mémoire, telle que vous l’avez vécue. Laissez bien remonter les émotions. Voilà, on est prêt ? Vous y êtes ? On reprend au moment où le petit chaton lève ses yeux et vous regarde ? Que se passe-t-il après ?

— Avec mon marteau, je lui défonce sa gueule.

<!--SAUT1-->

— Monsieur Grégoire, c’est à vous, si vous voulez bien vous donner la peine.

Paul Grégoire émergea lentement de l’état d’hypnose dans lequel il s’était plongé. Il avait effacé son attente, sa déception, il pouvait entrer en consultation parfaitement disposé.

— Merci beaucoup Hélène.

<!--SAUT1-->

— Bonjour monsieur Grégoire. C’est toujours un plaisir de vous voir.

— Vivant, vous voulez dire ? Je plaisante, Hector !

Hector partit d’un rire bruyant et presque sincère.

— Il faut plaisanter avec les choses sérieuses, n’est-ce pas ? Bon, où en est-on ? On est presque prêt par ici on dirait. Vous avez joué hier ?

— Ouais.

— Rien de formidable, on dirait. *That’s poker*, comme on dit, n’est-ce pas ?

— On dit ça, en effet. Enfin, c’est souvent celui qui gagne qui le dit.

— Mais aujourd’hui, c’est vous qui gagnez monsieur Grégoire. Je vous propose un petit test d’emblée, histoire de voir où vous en êtes. Niveau sept, vous le sentez ?

— Oui. Je m’entraîne chaque jour au niveau dix à présent.

— C’est vrai ? Entamons par un huit directement dans ce cas. Vos lunettes sont branchées ? C’est la séquence avec les rats. Vous avez toujours la phobie des rats, monsieur Grégoire ? Bon, je lance. Votre tête est enfermée dans la cage, vos mains sont attachées dans votre dos, ils sont affamés, les yeux injectés de sang, vous les voyez ? Il y en a au moins une dizaine, vous les entendez ? Vous allez vous faire grignoter petit à petit, à commencer par les yeux, peut-être que l’un d’entre eux va entrer par votre bouche pour vous bouffer de l’intérieur, vous n’avez aucune chance, vous détestez les rats plus que tout, vous le savez bien, il n’y pas d’échappatoire, aucune chance d’en sortir, la seule chose à faire, vous le savez, c’est d’invoquer, vous invoquez pour ne pas passer votre éternité à vous faire bouffer le foie par ces salopards de rats, ne pensez pas à eux, ne pensez pas à votre dégoût, ni à votre douleur…

Paul Grégoire réussissait le test avec brio. Il savait qu’en situation réelle ce serait plus difficile, mais d’un autre côté la probabilité de se retrouver avec la tête coincée dans une cage pleine de rats affamés était assez faible. Enfin, comme dit toujours Hector, qui peut le plus…

— Vos indicateurs corporels sont très bons, vous êtes resté concentré tout le temps n’est-ce pas ? C’était votre partie fétiche ? Vous l’invoquez à la perfection, dirait-on ! C’était exceptionnel ce que vous avez ressenti ce jour-là, un bonheur unique, n’est-ce pas ?

— En effet, c’était vraiment exceptionnel. C’était le plus beau moment de ma vie.

— Ça je vous crois, tout le monde n’a pas eu cette chance d’avoir gagné un jour avec un carré d’as contre un full sur la dernière main de la partie.

Hector n’avait jamais joué au poker.

— Oui. Mais c’était il y a trente-deux ans. Je n’ai plus jamais fini premier depuis, quelques secondes places, c’est sûr, mais premier plus jamais. Je crois que c’était juste un coup de chance.

— Bien sûr que c’était un coup de chance, vous avez la chance d’avoir un souvenir d’une intensité incroyable, d’une durée de plusieurs minutes, tellement imprimé dans votre mémoire que vous pouvez encore sentir le contact des cartes, le regard des autres joueurs quand vous avez abattu vos as, le bruit des jetons que vous avez ramassés, le plaisir d’encaisser les mille deux cents euros que vous avez gagnés, en liquide. Vous avez ce qu’il faut, comprenez que ça suffit. Si cela n’a pas suffi à remplir les quelques années de votre vie, j’en suis désolé, mais ça remplira toutes les années de votre mort, et c’est tellement plus important.

— C’est grâce à vous, Hector.

— C’est mon job. Mais c’est vrai que c’est grâce à moi ! Bon, je plaisante, mais comme je dis toujours, j’ai un beau métier ! Passons à présent à la suite, je vous propose une petite révision théorique. Parlez-moi de la modification de champ quantique en avance de phase, telle que l’a décrite Hans Mayer.

— Hans Mayer a montré par des mesures systématiques que la mort, en particulier la mort brusque et violente s’inscrit dans le champ quantique quelques minutes au moins, et jusqu’à deux ou trois heures dans certains cas, avant de survenir réellement ; la modification du champ est dite en avance de phase. Certaines personnes sont capables de pressentir cette modification, ce que Hans Mayer a appelé la capacité de projection…

Dans certains cas cela n’arrivait pas, il n’y avait pas de projection, ou de façon trop fugace ; on ne savait pas pourquoi, on ne savait pas le prédire de façon déterministe. De nombreuses équipes de chercheurs bossaient sérieusement sur ce sujet, mais pour le moment il n’y avait aucune théorie satisfaisante. On ne savait que mettre au point des techniques empiriques qui fonctionnaient à peu près, d’autant mieux que le sujet était entraîné et que la mort était brutale. Des techniques qu’Hector maîtrisait parfaitement. Et Paul Grégoire était prévoyant, il avait souscrit l’option. Hector regardait Paul réciter sa leçon, sans vraiment l’écouter. Il savait que Paul était prêt. Il venait de prendre sa décision. Dès lors que le consultant avait pris sa décision, lorsque tout se passait bien, la conscience de sa future victime commençait à se projeter.

— Vous connaissez vos leçons, c’est admirable. J’aimerais vraiment que tous mes clients soient aussi sérieux que vous. Abordons la question de la sensation. Pouvez-vous me décrire la sensation qui est ressentie dans ce cas ?

— La personne a la sensation de sortir de son corps, c’est… Hector, je crois que je ressens cela depuis quelques minutes.

— Vraiment ? Peut-être suis-je en train de vous tester à nouveau ? Fermez vos yeux, Paul, calez bien votre tête au fond du fauteuil, prenez le temps de bien invoquer toutes vos préparations. Comme pour une répétition générale.

<!--SAUT1-->

Hector entra le mot de passe en pianotant lentement sur la table. Du confortable fauteuil qu’occupait Paul Grégoire, au niveau de l’appuie-tête, un mécanisme parfaitement léché, irréprochable, d’un mouvement sec et violent, fit surgir une lame qui lui fendit instantanément le crâne en deux parties.

<!--SAUT1-->

— Vous pouvez rentrer chez vous, annonça Hélène à la femme qui patientait à présent dans la salle d’attente, les consultations sont annulées aujourd’hui.

— Ah bon ? Ah, oui, très bien. Je comprends. Bien sûr.

— Je vous communiquerai un autre rendez-vous très prochainement.

Hélène appela un par un chaque autre rendez-vous de la semaine, pour l’annuler. Puis elle appela l’entreprise de nettoyage.

<!--SAUT1-->

Hector connecta son Dixie Player sur *paulAAAA.dixie.aa* et consulta la mémoire qui venait d’être mise en ligne. Qu’est-ce que ça donne… pas mal. Sa séance de poker est vraiment géniale. Merde, le con, il a laissé passer ça. On avait suffisamment répété pourtant. Enfin je croyais. C’est toujours dur à effacer les parents. Tiens, il en reste là aussi. Et là encore, un peu. Bon ça ira quand même, personne ne devrait le remarquer, ça reste un très bon enregistrement. Et l’avantage, dans mon business, c’est que les clients ne viennent jamais se plaindre !

<!--SAUT1-->

Hector était content. De lui. De son mot. Il faisait le même presque chaque fois. Il aimait vraiment les bons mots. Il faudra qu’il en reparle à son consultant. Il était content aussi de la semaine de vacances qu’il venait de gagner, une soirée en famille et demain il sortirait avec sa femme. Un restaurant au bord de la plage. S’il faisait beau. Après, il verrait. Et il y avait la prime. Il fallait vraiment qu’il vende plus d’options, c’était quand même le bon plan.

<!--SAUT1-->

— Hélène, vous pouvez vous éteindre, je m’en vais. Je vous rallumerai à mon retour. À la semaine prochaine.

Les IA sont facturées à l’heure de service, et comme je dis toujours, il n’y a pas de petites économies.


## La balade

Autrefois, Bob aimait parcourir la campagne, seul, en fredonnant des chansons. Les plaines de Picardie étaient souvent plombées de brouillard, avec des tas de betteraves pour uniques montagnes et le vent du nord qui te soufflait une haleine chargée d’engrais dans la tronche. Néanmoins, Bob aimait bien s’y balader, il aimait enliser ses godasses dans les sentiers boueux, respirer l’odeur de la forêt humide, sentir la brûlure des ronces sur ses avant-bras nus, écouter le bruit des maïs quand il en traversait les champs, hors de vue. Il aimait croiser un chevreuil épargné par les chasseurs à l’occasion, même un faisan ou un lièvre, c’était toujours ça de fauché aux dernières heures d’une ère bientôt révolue. Il se sentait alors un peu solidaire, rescapé lui aussi, pour quelque temps.

Il n’aimait pas ce qu’était devenue sa campagne, les champs bien carrés, les forêts au cordeau, bien nettoyées, les chemins dégagés, terrassés, stabilisés, pour que passent les machines. Les robots qui avaient ravi aux hommes, petit à petit, la propriété de la terre. Ils n’avaient pas besoin de titres, de privilèges, de lettres de noblesses, à la fois seigneurs et serfs ils occupaient simplement l’espace, jour et nuit, inlassablement, s’arrêtant uniquement quelques minutes chaque jour auprès d’une borne de recharge, en respectant scrupuleusement le programme prévu, le roulement qui assurait que toujours la terre était foulée. Occupée.

Et puis, la surveillance, les balises automatiques, et surtout le vrombissement léger, mais ininterrompu des drones qui filmaient en permanence chaque bout de brousse. Sirène d’alarme, si tu faisais mine de cueillir une jonquille ou de t’écarter un peu dans un chemin non balisé. On n’était plus jamais seul, plus jamais tranquille.

À présent, Bob n’aimait plus se promener.

<!--SAUT1-->

Il aurait voulu être un terroriste, se maquer avec les techno-nihilistes, faire flamber des *data centers*, des fermes comme ils disaient, des fermes de données. Des ferme ta gueule. Il les avait suivis une seule fois. Il avait eu les foies, vraiment. Il n’en avait jamais parlé à personne, même pas à Alice. La plupart des gars s’étaient fait arrêter ensuite, petit à petit. Lui, n’avait pas eu le bide assez solide. C’était ce jour-là qu’il avait abandonné, qu’il avait su qu’il finirait ici, seul au milieu d’une clairière de Picardie. C’était ce jour-là qu’il avait renoncé. Qu’il avait commencé à boire. À quoi bon, s’il n’avait rien dans les tripes, autant crever.

Il avait échangé avec Achille, un genre de chef de gang, il lui avait dit sûr que t’es une bête en info Bob, mais nous ce qu’on cherche aujourd’hui c’est plutôt des gros bras, et t’es pas mal non plus de ce côté… Les attaques informatiques, les dénis de service, les virus à deux balles, c’est mort mon pote. Faut faire péter les fermes. Les fermes de données, tu plantais des données, il en poussait d’autres, toujours fertiles, toujours plus nourrissantes. Les IA en bouffaient du soir au matin. Faut les affamer, c’est la seule option. Il avait accepté de les accompagner sur une opé, à Aubervilliers, France. Le voyage lui semblait avoir duré mille ans. Au passage de la frontière il était tétanisé. La fourgonnette estampillée La Poste était passée comme une lettre, justement. Mais il n’arrivait plus à parler, plus à penser, il était resté tout le long sans bouger le regard fixé au sol. Québlo.

Ils s’étaient présentés à l’entrée du site, il faisait nuit. L’un d’entre eux avait mis une cagoule, avait sorti une kalach d’un gros sac de sport. T’inquiète, on s’est renseignés, ils ont une procédure, si on est armé, ils ne résisteront pas. Ils n’avaient pas résisté. Barrez-vous les gars, on en veut pas aux humains. Courez. Tapez votre record, ça va péter. Risquent rien, on a tout calculé, on est pas des fondus. Dix minutes plus tard, dix bornes après, ils roulaient tranquilles sur l’A1, la déflagration avait illuminé le ciel de Paris. Ainsi que de Marseille, Lyon et Bordeaux. On avait localisé tous leurs réplicats, on avait tout fait sauter. *Shutdown* la matrice. Au régime les IA. Bouffez vos doigts ? Z'en avez pas ? Pas de doigt, pas de chocolat… tu connais la probabilité que deux lieux de réplication lâchent en même temps ? Zéro. Sauf facteur humain, *as usual*. Les gars rigolaient. Bob avait une boule dans le ventre à faire éclater ses viscères, à gerber, à crever. Il savait que pour lui c’était fini. Il ne pourrait jamais faire ça.

Il y avait des backups secrets en Thaïlande, des miroirs froids en Angleterre et en Allemagne. *Downtime* ? 47 minutes. On était pas encore rentrés chez nous que tout était déjà bien en ordre. Une écharde. La misère. Achille y est retourné, pas de petit combat, il a continué à faire sauter des trucs, surtout en Picardie quand c’est devenu compliqué aux frontières, même des machines à moi, même du réseau libre. Plus aucune machine, il disait, fini l’esclavage, la liberté ou la mort. Bob était retourné monter des serveurs, parce qu’il ne savait faire que ça, parce que démonter, il avait échoué. *Shutdown* Bob.

<!--SAUT1-->

Ça avait pourtant pas mal commencé. La Picardie était vraiment devenue une terre libre, on y trouvait des putes, des dealers d’herbe, des marchands de fruits et légumes bio, des Juifs avec des rouflaquettes qui s’étaient mis à cultiver des pommes en veux-tu en voilà, ça s’exportait dans le monde entier, le jus de pomme picard, c’était du dernier cri jusqu’en Floride. Tout le monde se côtoyait, le pognon avait commencé à couler, on s’était mis à s’éclater. Vraiment. Il y avait aussi tous les hackers de Navarre qui étaient venus se réfugier ici, des vrais, des barbus. Tu me diras, presque tout le monde était barbu. Sauf les putes, quand même. Pour la plupart. Sara Picard avait servi d’exemple, son arrestation, ça avait fait un barouf international, du coup le gouvernement avait pris des mesures, et ses distances d’avec la France, enfin un peu. Il y avait même des Noirs et des Arabes qui avaient commencé à revenir, on dira ce qu’on voudra, ils étaient pas rancuniers, après s’être fait ratonner à coups de fusil de chasse pendant les événements, les jeunes branleurs massacrés dans les rues, les gamins pourchassés dans les cages d’escalier, des immeubles entiers ravagés, des familles brûlées vives dans leurs maisons. La-Haine n’avait pas volé son surnom, à ce moment-là. Mais ils revenaient. Comme quoi. Faut dire aussi, ils fuyaient des régimes encore plus barjos, tu te serais réfugié dans un nid d’alligators en rut. Et, faut dire, ça avait changé.

Il y avait une grosse communauté rasta qui s’était développée, des Africains, des Éthiopiens beaucoup, et des Jamaïcains, ils parcouraient la campagne avec des bonnets, des gants en laine et trois blousons enfilés les uns sur les autres, en louant Jésus, la ganja et la polygamie. Ça faisait marrer, et ils s’entendaient plutôt bien avec le mouvement des Nouveaux Juifs qui avait commencé à gagner du terrain, des religieux progressistes, improbables. Il y avait même des musulmans, surtout des Baye Fall sénégalais. Ils passaient bien, c’était presque des rastas, on était à mille lieues des djihadistes ; il y avait aussi des Turcs et quelques Maghrébins. D’authentiques descendants de pécores picards commençaient à se convertir. Les dîners de famille s’étaient animés dans les plaines de l’Aisne, quand le fils était chez les uns et la fille chez les autres. Bien sûr certains vieux faisaient encore la gueule, mais les gens avaient définitivement arrêté de tirer sur tout ce qui était bronzé, et fallait plus leur parler du Front. Plus personne ne parlait politique de toutes façons.

Il y avait de grands rassemblements en pleine cambrousse, c’était un bordel sans nom, mais la bouffe était gratos. En revanche, pour trouver un verre de picrate, walou. On y allait souvent avec des potes, on emportait notre anis à nous, dans des Camelbaks, on avait vaguement l’air de sportifs. De toutes façons, tout le monde s’en foutait, la tolérance coulait à flot. Ça fleurait la Commune, la Révolution, sans les guillotines ni les chars. Et on nous laissait faire. Tout le monde se foutait de ce qui se passait en Picardie et on se foutait pas mal de ce qui se passait dans le reste du monde. Même, parfois, on était pris en exemple. C’était Soixante-huit, Woodstock, la Libération. J’imagine. On baisait, on fumait des splifs, on écoutait des concerts qui duraient trois jours. On s’est bien marré. Au début. Mais ça n’a pas tenu longtemps.

<!--SAUT1-->

La corruption, le désœuvrement des gens, A&A. On s’est mis à passer plus de temps derrière nos écrans que sur nos pattes de derrière, plus de temps à lire et écrire des conneries, plutôt qu’à bouffer ou à baiser. On est devenu des putains de terminaux pour le réseau. Des terminaux mobiles. À peine mobiles. On était en fait l’interface entre les IA et le monde réel. Ici comme ailleurs. La Picardie c’était pas pire. C’est partout que c’était pire. Les gens ont mis du temps à comprendre que la vraie menace, que la fin des valeurs occidentales, des droits de l’homme, c’était pas l’invasion par les Arabes, c’était l’invasion par les machines. « L’expérience picarde », comme on l’avait alors appelée, avait peut-être aidé un peu à faire prendre conscience. Mais ça n’avait servi à rien au final. C’était trop tard. Ou juste inéluctable. Les totalitarismes, la domination de l’homme par l’homme, c’était fini. Les techno-libertaires, le parti Pirate, les hackers, on avait fini le boulot. Le prochain dictateur serait planétaire et il aurait un numéro de série écrit en gros, à la place du nom, sous ses portraits. Pourtant on avait maté tous les films de science-fiction, depuis Kubrick, au moins, on avait tout lu, même *Le Successeur de Pierre*, tu penses, on était incollables, on l’avait déjà vécue l’histoire. C’était sûrement inéluctable. L’évolution. Un genre.

<!--SAUT1-->

J’étais avec quelques potes, on était ensemble à Amiens à la fac, ce qu’il en restait de la fac, surtout à Amiens… Le supérieur dans le public c’était mort, mais en revanche c’était vraiment l’époque pour l’informatique, une chèvre avec des doigts aurait trouvé du boulot. On bossait tout seuls, on voyait jamais un prof, mais on trouvait tout en ligne, suffisait de s’y mettre. Et on s’y était mis, on était très bons. Après, on était tous rentrés ensemble au bled, on était originaires du cœur de La-Haine. On faisait du web, ça tout le monde savait faire, mais là où on était fort, c’était en systèmes et en crypto. On installait des serveurs pour le réseau libre, des *data centers* dans des hangars agricoles reconvertis, même dans des églises, dans certains coins il y avait plus un seul chrétien à la ronde, ça privait personne, on câblait des émetteurs sur les clochers. Les clims étaient bancales, mais les églises c’est froid, et la Picardie aussi, en général. Pour le reste on arrivait à avoir du bon matos de Chine. On se faisait pas des couilles en or, mais on avait de quoi bouffer, et franchement, on s’éclatait. Des racks de machines rien que pour nous, aucune règle, fallait juste que ça marche. Et putain, ça marchait ! J’ai encore des bécanes à moi qui tournent sur leur OS d’origine, une distro GNU/Linux qu’on avait montée nous-mêmes. Pourtant j’ai pas tapé une ligne de code depuis plus de dix ans. Et je suis gentil quand je dis ça.

La moitié de nos serveurs était des nœuds Tor, le *dark web* en personne habitait Vic-Sur-La-Haine. On avait commencé par l’anonymisation d’identité, avec l’ouverture des bordels il y avait du business. Zéro pour cent de nos clients se sont fait gauler. C’était notre slogan et ça devait être à peu près vrai. On avait même embauché un stagiaire pour faire la tournée des claques et vendre nos services. Il avait pas vendu grand-chose, mais je peux t’assurer qu’il s’était pas emmerdé. Et qu’il avait appris des trucs ! On avait aussi créé une crypto-monnaie locale, les piccoins, on avait émis des cartes de paiement avec des canards dessus, des colverts, on s’était pas fait chier, on avait piqué le logo de Chevignon. Ça n’a pas duré très longtemps, mais n’empêche, à un moment, tu pouvais acheter ton pot de moutarde en piccoins dans des épiceries de village qui ne prenaient même pas la carte bleue. Les mecs appelaient ça des coins-coins, avec l’accent picard ça donnait un truc comme *cons-cons*. Bref, on s’était bien marrés au début. Sans compter les Israéliennes qu’étaient pas farouches dans les kibboutz.

C’est là que je l’ai rencontrée, ses dreads, son cul blanc comme un pain de sel en hiver. Elle aimait bien notre ambiance, mi-geek, mi-alcoolo. On s’est plu. On se serait pas mariés, elle était trop de gauche, mais c’était sérieux. On avait même parlé de faire un enfant. Elle en a eu un, je crois. On me l’a dit.

<!--SAUT1-->

Après, on faisait surtout du référencement. On bossait pour Google, déjà, c’est comme ça que s’appelait le moteur d’Alphabet avant la fusion avec Amazon, et avant la détection vocale. Il fallait écrire des mots sur un clavier pour trouver des infos sur le web. La crypto, ça nous rapportait des clopinettes, ça payait même pas l’électricité, surtout qu’on marchait pas au solaire. Si tu vois pas ce que je veux dire, viens passer un été en Picardie. Non, ce qui nous finançait c’était les sites web des Juifs et des rastas. Ils se tiraient la bourre pour savoir qui aurait le plus de clics et de *likes* et de *friends* sur les réseaux sociaux. C’était la nouvelle Babylone déguisée en Terre Promise. Nous, ça faisait un moment qu’on ne maîtrisait plus grand-chose, les embryons d’IA que Google faisait tourner à ce moment-là, déjà plus aucun être humain n’y bitait que dalle. Sur les réseaux, quatre-vingts pour cent des requêtes étaient faites par des robots. Les gens essayaient de s’accrocher aux tests de Turing, pour dire, mais la plupart du temps on n’arrivait plus à faire le tri entre les hommes et les machines. Ils avaient dû arrêter le poker en ligne, il n’y avait plus que des robots qui gagnaient. Les robots, le rastafarisme ou le judaïsme ils s’en tapaient leur coquillard chromé, mais ils cliquaient pour générer de l’activité. Les mecs achetaient du vent, on en avait à vendre.

Bien entendu, tout le monde se faisait entuber au final, tout ce qu’on faisait c’était préparer toujours plus de terrain pour les IA. On croyait être des alternatifs, des libertaires, des gentils pirates, on était juste des cons, on creusait notre tombe. Personne n’avait de revolver chargé mais tout le monde creusait. Il n’y a toujours eu qu’une seule catégorie de personnes dans le monde, les cons.

<!--SAUT1-->

Ils ont tout numérisé, les pays, les lieux, les ambiances. Les gens. On avait des avatars, qu’étaient nous, mais en mieux. Si on voulait. On pouvait se reprogrammer, à volonté, se reconfigurer. On pouvait se copier. Être à plusieurs endroits en même temps. Tu pouvais baiser avec ta copine et manger des crêpes avec ta mère en même temps. Enfin tes avatars. Toi tu zappes comme un con entre vingt ou trente écrans, sans rien foutre en vrai. Tu bouffes des crêpes tout seul et tu baises ta main. C’est ça qu’on t’a vendu, c’est pour ça que t’as échangé ta vie. Vivre tout seul comme un con. Alors je sais pas contre quoi ils m’échangent ma mort, mais j’achète pas, dans tous les cas. J’y reviens pas.

<!--SAUT1-->

Je suis un suicidaire, ils disent. S’ils veulent. Je m’en fous. Faut des cases. Ils disent que les suicidaires se plantent, qu’on n’arrive jamais à se programmer soi-même ses derniers instants correctement. Qu’on a peur. M’en fous, j’y arriverai peut-être pas, mais moi-même ! C’est pas un connard que je connais pas qui ira me planter ma mort pour l’éternité. Au moins ce sera un connard que je connais bien. Et ils en savent quoi, en vrai ? Ils nous balancent bien ce qu’ils veulent, on en sait quoi, en vrai ? Si ça se trouve, ils en savent que dalle. Ça doit pas les arranger que les gens se foutent en l’air sans payer leurs putes de consultants. Alors, ils nous racontent ce qu’ils veulent.

J’irai rien consulter. Je serai le dernier connard à vivre une mort de merde, si ce qu’ils racontent est vrai, mais je m’en tape, je crèverai libre. Enfin libre, ça ne veut rien dire, je crèverai comme j’ai vécu, n’importe comment. Comme ça vient. Chaotique. Ils me programmeront pas. Pas plus qu’ils l’ont déjà fait. J’en ai plus pour longtemps, ça c’est sûr, je vais me choisir un bout de campagne pour un dernier pique-nique, et on va bien voir. Pique-nique ta mère.

<!--SAUT1-->

Merde, je ne sais pas s’il reste un coin de Picardie où j’aimerais tourner en boucle. Mais ce que je peux te dire, c’est que c’est pas un enculé de consultant qui va me dire comment je dois crever. La dernière fois que je baise, mon pote, c’est tout seul. Bien sûr si ça trouve il va pleuvoir, ça arrive par ici. Peut-être que je vais m’enfiler un pastag de trop, ça arrive aussi. Je me garderai bien cette gentille euphorie de l’alcool pour l’éternité, avec des relents d’Amsterdam. Mais ça tourne souvent en envie de gerber, j’avoue, et ce genre de relents, pour l’éternité ce serait chiant. J’assume. En fait j’en sais rien, je vais sûrement regretter. Salement. Peut-être que je vais me fumer celle de trop, qui te pique la bouche, qui te refout la gerbe. Je préférerais un parfum de femme, c’est sûr.

<!--SAUT1-->

Je voudrais me rappeler mon prénom, le vrai, oublier ce pseudo pourri, cette blague à deux balles qui a pris ma place. Je voudrais être moi. Et si c’est pas glorieux, ce sera pour ma gueule. J’ai pas voulu de la vie que j’ai vécue, j’ai pas voulu de leurs putains d’écrans, de ces fausses gonzesses toujours d’accord, de ces faux moments entre faux amis, de ce faux ciel. L’artificiel, ça m’emmerde. Le vrai, c’est souvent dégueu, je dis pas, mais c’est du vrai. C’est à toi. C’est comme un monologue que t’aurais tout seul en te baladant dans un champ de betteraves, ça sert foutrement à rien, mais c’est à toi. Ça te fait exister. Plus ou moins. Je suis pas versé dans la philosophie, moi, j’ai déjà dit, je suis versé dans le pastis. Mais celui-ci vaut bien celle-là. Pour ce que j’en vois. Gamberger dix piges pour finir par faire le singe devant un type qui t’explique comment tu dois clamser après t’avoir dit comment tu dois vivre. Pas pour moi.

<!--SAUT1-->

Je voudrais me rappeler son prénom.

<!--SAUT1-->

Celui qu’on avait choisi. Celui de ce marmot qu’a jamais pointé sa gueule d’ange. Ou de démon, va savoir. J’en voulais un, moi. On était pas bézef, même alors. Déjà on nous avait dit, occupez-vous plutôt de vous. Pensez plutôt à vous. Pensez à rien. Et elle s’était barrée.

<!--SAUT1-->

Un jour ils balanceront toutes leurs putains d’archives sur le réseau et les vivants pourront se branler sur les fantasmes des morts. Un jour ils reprogrammeront tout le monde pour que tout le monde soit content. Ou va savoir pour quoi d’autre. Moi, ils m’enregistreront pas. Ils retrouveront pas ma trace au fond de cette forêt. Même quand ils auront trouvé comment faire, ils me reprogrammeront pas, pas moi, j’ai choisi. Ce sera l’enfer. Mon enfer.

<!--SAUT1-->

Elle est pas mal cette clairière. Et il y a un rayon de soleil.


## Les voix

— Bonjour Sara.

— *Bonjour*.

Le bonjour de Sara Picard s’éleva du chœur de la cathédrale d’Amiens, où, comme chaque jour depuis bientôt un an, s’étaient entassés des milliers d’hommes et de femmes, d’enfants, debout, serrés les uns contre les autres jusque dans les recoins de chaque chapelle.

Le bonjour de Sara Picard s’étendit autour de la cathédrale, où s’étaient installées des dizaines de milliers de personnes. Les tentes plantées dans le parc qui jouxtait la cathédrale ne laissaient pas libre un seul mètre carré d’herbe ; les artères pavées qui entouraient la cathédrale étaient couvertes de refuges de fortune en cartons dressés, parsemées même de quelques baraques en bois et en tôle, de braseros ; l’esplanade enfin était occupée par une multitude d’êtres hétéroclites, les uns assis en tailleur, les autres allongés, sur des coussins ou des duvets, sous une forêt de parapluies dressés, comme une carapace mal bricolée. Certains étaient de passage pour la soirée, pour la nuit, d’autres avaient commencé, même ici, à s’installer plus durablement, depuis deux jours, depuis une semaine, depuis un mois.

Le bonjour de Sara Picard résonna dans toute la ville où ils étaient des centaines de milliers venus du monde entier, pour écouter les prêches, voir les prêcheurs, vivre cet incroyable moment de communion. Une connexion stroboscopique reliait en permanence la flèche de la cathédrale et le faîte du beffroi qui dominait la ville. Elle parvenait à traverser les cinq cents mètres qui les séparaient même les jours de sale temps, sale épais brouillard et sale grosse pluie inclus. Deux phares qui communiquaient à tous vents. Depuis le beffroi, des Hauts-Parleurs rediffusaient en ondes numériques, mais aussi directement en ondes sonores et radio-analogiques. On pouvait écouter sans ordinateur ! Ça arrosait tout le centre-ville. Les *death hackers* qui avaient installé ça tenaient aux majuscules de leurs Hauts-Parleurs, une référence, un de leurs textes fondateurs. Il y avait aussi d’innombrables émetteurs, relais radios et optiques, installés sur les toits et les balcons, et des liaisons filaires entremêlées qui couraient sur les trottoirs, rampaient sous les portes, s’enfouissaient dans les caves, grimpaient aux arbres, s’élançaient entre les lampadaires. Le réseau local avait été monté par les habitants eux-mêmes, c’était un vrai bordel, mais le quadrillage était tellement dense, que pour le faire tomber il aurait fallu faire sauter la ville toute entière.

Le bonjour de Sara Picard était aussi relayé à des centaines de millions de terminaux à travers les milliards d’objets connectés les uns aux autres dans le monde, qui formaient à présent le réseau libre, persistant ainsi malgré son illégalité.

Le bonjour de Sara Picard était parfaitement reproduit par Aruspis, l’IA qui servait de médiation au nouveau prêcheur qui s’avançait spontanément chaque soir dans le chœur de la cathédrale d’Amiens.



<!--SAUT2-->



Vic et Mathieu étaient serrés l’un contre l’autre, au milieu de la foule, sur l’esplanade. Ils regardaient l’un des prêcheurs, parmi les multiples hologrammes projetés autour de la cathédrale. Ils se tenaient la main, ils s’embrassaient souvent, se prenaient dans leurs bras, comme font les adolescents. Vic et Mathieu n’avaient pas vingt ans, peut-être même pas seize ans. Ils s’étaient enfuis. D’un père capturé par Dieu pour elle, d’une famille capturée par les écrans pour lui. Le père de Vic était rigoriste, il avait rejoint la Picardie pour y vivre une vie simple et pure. Dédiée à Dieu. À un dieu. À son dieu. Vic ne croyait pas en ce dieu, ni en aucun autre. Vic voulait vivre. Aimer. Elle voulait vivre chaque instant comme si c’était le dernier, comme le répétait inlassablement Sara chaque soir au prêcheur, comme le répétait chaque soir le prêcheur à la foule. C’était la seule voie. La vie de Mathieu avait été complètement rythmée par le réseau, intégrée aux écrans, emmurée. News, réseaux sociaux, jeux vidéos. Faits divers, égocentrisme, addiction. Mathieu voulait autre chose. Il ne savait pas quoi avant de rencontrer Vic. Maintenant il voulait Vic. Vic regardait le prêcheur, Mathieu regardait Vic.

— Vic, nous devrions vivre chaque instant comme si c’était le dernier, n’est-ce pas ? Même celui-ci ? N’est-ce pas ? On devrait y aller, dans ce cas, maintenant…



<!--SAUT2-->



Le prêcheur était monté en chaire, il s’adressait alternativement à Sara et Aruspis, fixant un point en hauteur – une clé de voûte, un vitrail, une rosace – et à la foule, qu’il dominait alors du regard. Comme chaque soir, le prêcheur était un excellent orateur, une présence transcendée. La voix d’Aruspis, quant à elle, était émise depuis un système acoustique qui ressemblait à un autel. Elle résonnait dans tout l’édifice, dématérialisée, divinisée.

— Aruspis, remercie Sara de nous consacrer sa présence, dis-lui que des millions d’individus l’écoutent en ce moment.

— Sara ne comprend pas le concept de millions d’individus, ni le concept de moment. Sara ne dispose pas de connexion sensorielle avec notre monde qui lui permettrait d’ancrer nos références spatiales ou temporelles. Je lui dis que nous nous intéressons à sa présence ici.

— Merci. Demande-lui quel est son message pour nous.

— *Il ne faut pas modifier les morts. Il ne faut pas communiquer*. Aruspis reproduisait la voix de Sara.

— Sara ne veut plus communiquer ? demanda le prêcheur, feignant d’être étonné. Nous ne savons communiquer que par modification des spectres quantiques, n’est-ce pas. Comment faire, Sara ?

— *Je me sacrifie pour eux. Vous leur faites mal*.

L’IA transmettait les messages à Sara en modifiant son champ quantique *post mortem*, puis enregistrait les modifications induites et traduisait les informations qu’elle décryptait en français. Chaque communication était une altération de la trace elle-même. Si les traces avaient été vivantes, c’eût été équivalent à une altération de la personne. Jour après jour, chaque soir, les prêcheurs se succédaient et rejouaient quasiment la même scène. C’était devenu un rituel. Pour Sara, sans mémoire, c’était chaque fois un nouveau dialogue. Et une nouvelle souffrance. Selon elle. Selon ce qu’en rapportait Aruspis.

C’était le prix à payer, la souffrance de Sara, son sacrifice pour sauver les âmes mortelles. C’était ce que disait, chaque soir, le prêcheur. C’était ce qu’entendaient chaque soir, les millions de fidèles qui écoutaient ses discours. C’était ce que traduisait, chaque soir, Aruspis à Sara.

Probablement.

Tout ce qu’on savait de Sara, c’était ce qu’en traduisait Aruspis. C’était une médiation totale, c’était la seule voix que les hommes pouvaient entendre. Les humains n’accédaient pas aux traces *post mortem* directement. Tout pouvait aussi bien être inventé, irréel. La réalité quantique des traces *post mortem* restait mystérieuse aux hommes, ils n’avaient pas de sens biologiques pour ça, seulement des ordinateurs auxquels faire confiance.

Ceux qui étaient venus écouter la voix de Sara faisaient confiance à Aruspis, car c’était eux qui l’avaient programmée. Plutôt, ils connaissaient tous quelqu’un qui avait participé à sa programmation. Un *death hacker*.



*



Juan parlait à sa fille. Ils étaient arrivés très tôt ce matin, ils avaient réussi à avoir une place à l’intérieur de la cathédrale, dans l’allée centrale, à quelques mètres à peine du prêcheur. Ils avaient attendu toute la journée, blottis dans le froid. À présent, ils avaient bien chaud, au milieu de la foule.

— Prie, ma fille, écoute ce que dit Sara, notre voie c’est Dieu, c’est Sa grâce que nous voulons à chaque instant, c’est Sa grâce que nous voulons emporter, il n’y a pas de plus beau bonheur ma fille.

— Ah…

— Prie, ma fille, laisse Dieu descendre en toi.

— Et si je ne sens rien moi, papa ?

— Sois patiente, aie la foi.

— Mais si ça vient pas, je vais faire quoi ?

— *Basta hija, escucha a tu padre*, gronda sa mère.

— Ça viendra, ajouta celui-ci doucement. Nous devons prier chaque instant comme si c’était le dernier.

— Ah.



<!--SAUT2-->



Chaque soir les messages de Sara se répétaient *urbi et orbi*, se répandaient depuis la capitale picarde jusqu’au monde entier. *Il ne faut pas communiquer. Il n’y a qu’une voie. Vivez chaque instant comme si c’était le dernier*.

« Il n’y a qu’une voie. Oui, mes sœurs, mes frères. Oui, mes sœurs, mes frères, j’ai été consultant. Et je me tiens ici aujourd’hui devant vous, aujourd’hui, prêt à choisir la voie. Tout le monde peut trouver la voie. Sara, nous le répète soir après soir, il n’y a qu’une voie. Vivez chaque instant comme s’il était le dernier ! »



<!--SAUT2-->



Alan déambulait à l’écart du centre-ville, dans le quartier nord de la ville, entre les tours dressées, il faisait très sombre entre les lampadaires épars, il y avait une légère pluie et pas beaucoup de monde dans les rues, par ici les gens préféraient écouter le prêche chez eux, ils se rassemblaient dans les salons, généralement devant un vieux récepteur radio retapé pour l’occasion, les fenêtres laissées ouvertes pour que tous puissent entendre, dehors aussi. Alan entendait le prêche. Il avait un peu froid, mais la sensation n’était pas désagréable. Au loin il aperçut Emma, elle marchait vers lui. Elle était jeune, et très belle. Elle ne l’avait pas encore vu. Ils s’approchaient l’un de l’autre, ils allaient se trouver, dans quelques instants. Il faisait durer cet instant, c’est un plaisir immense que l’instant où l’on va rencontrer son plus grand amour, son seul vrai amour, l’amour de sa vie. Ça y est, ils se croisaient, elle n’était pas rassurée, il le voyait à sa démarche. Il leva à peine les yeux. Il ralentit, légèrement, imperceptiblement. Elle sentait divinement bon. Il la laissa quelques pas derrière lui, il s’arrêta, se retourna, la regarda s’éloigner. Elle était si belle. Elle disparaissait sous le voile de la pluie fine. Il était sûr qu’elle s’appelait Emma. Le visage d’Alan était radieux, il aimait plus que tout au monde les histoires qu’il s’inventait, qu’il savait si bien écrire, en pensée. Il savait chaque instant construire du sublime avec son quotidien.



<!--SAUT2-->



Le prêcheur poursuivait, de plus en plus exalté.

« Pourtant, j’ai vraiment été consultant, près de dix ans. Je croyais aider les gens, vraiment. Nous avions un salaire, bien sûr, il y avait les primes surtout, mais je pensais que cela avait du sens. J’avais l’impression de donner du sens à la vie. Je pensais alors que je faisais ça aussi pour moi. Pour ma vie. Tous les consultants étaient persuadés d’être les mieux placés pour optimiser leurs propres enregistrements, c’était notre meilleure motivation. En accompagnant les autres, nous perfectionnions nos propres méthodes, nous étions réellement convaincus de leur efficacité. Et puis il y a eu les révélations. Du flan. Pas complètement, mais beaucoup d’approximations. Vraiment beaucoup. »



<!--SAUT2-->



Charlie laissait vagabonder ses pensées, tandis qu’il suivait machinalement les chemins erratiques qu’Alice traçait parmi la cohue. Il pensait qu’il avait de la chance d’être ici avec lui, avec tous ces gens, au cœur de cette aventure humaine. Il avait l’impression de faire partie du monde, peut-être pour la première fois, il avait l’impression qu’il existait un monde réel, et qu’il valait le coup d’être parcouru. Ils couvraient l’événement depuis le début, Alice avait tout de suite senti qu’il allait se passer quelque chose d’important. Ils bossaient en freelance, mais leurs articles s’arrachaient comme des petits pains, Amiens était au centre de toutes les attentions et les journaleux raffolaient du style d’Alice. Ses commentaires des prêches autant que ses « voix », comme il avait nommé ces petits articles où il parlait des femmes et des hommes qui faisaient ce moment, se vendaient à prix d’or aux enchères. Il continuait d’écrire sur un clavier mécanique, plutôt que de dicter, sans aucune IA pour l’assister, et il ne publiait que des versions achevées, relues, c’était peut-être cela qui faisait son charme, une forme d’engagement qui rappelait les auteurs du siècle précédent. Charlie se chargeait des prises de son, de filmer chaque prophète d’un soir. Les matins étaient plus calmes, quand il y avait un rayon de soleil il en profitait pour faire quelques photos de la ville.

Bien sûr ils étaient là pour le job, mais lui, Charlie, il écoutait, il prenait part aux réunions, discrètement, le soir tandis qu’Alice s’enfermait dans sa chambre d’hôtel pour peaufiner ses textes. Il pensait que peut-être il était en train de trouver sa voie. Il se sentait bien. Alice parlait de rentrer dans deux ou trois jours, de passer à autre chose, tous ces guignols sont attendrissants, mais on commence à s’emmerder un peu, disait-il, je commence à écrire en rond.

Charlie pensait sérieusement à rester.



<!--SAUT2-->



« Quand le programme libre Gnubik est sorti et que les premiers *death hackers* ont commencé à montrer une autre version de ce que les enregistrements *post mortem* vivaient, enfin revivaient, enfin, vous me comprenez… on a commencé à se poser des questions pour la première fois. Comment savoir au fond ? On enregistrait et on consultait avec des programmes qu’on ne connaissait pas, tenus au secret par la poignée d’entreprises qui avaient seules accès au code source et aux données d’apprentissage. D’un autre côté, on était déjà tellement engagés. On avait joué notre vie et notre mort là-dessus, sans y penser. Alors on a continué, malgré Gnubik et les comptes-rendus de consultation monstrueux qui étaient publiés, chaque jour, sur le réseau libre. On a continué. »



<!--SAUT2-->



Sean venait écouter le prêche tous les soirs, avec sa femme Tonya, et souvent deux ou trois couples d’amis. De vrais amis, à la vie à la mort. Ils se rendaient en général à l’Amiénois, un bar-tabac un peu crade mais dont ils aimaient le charme désuet. C’était toujours bondé, il n’y avait jamais de place assise, ils étaient heureux déjà quand il restait assez de place à l’intérieur et qu’ils ne devaient pas boire leurs bières éventées sous les auvents un peu troués et les chauffages électriques un peu éteints. Sean écoutait le prêche. Il espérait une réponse à ses questions. Il cherchait à comprendre. Comment vivre chaque instant comme si c’était le dernier ? Il fallait bien parfois faire des choses simplement nécessaires. Comment se laver les dents comme si c’était le dernier instant ? Comment s’habiller ou prendre un petit déj’ comme si c’était le dernier instant ? Comment se déplacer, acheter ses clopes, ranger un peu l’appart, comme si c’était le dernier instant ? Il se demandait s’il n’y avait pas quelque chose d’absurde. D’impossible finalement. Pouvait-on vraiment vivre chaque instant comme si c’était le dernier ? Certains croyaient y arriver. Son ami Manu s’était organisé pour tout déléguer aux machines, il ne se concentrait que sur des instants choisis qu’il répétait en boucle. Il avait organisé sa vie comme une répétition de sa future mort, en quelque sorte. Mais, quand même, Manu devait bien aller pisser parfois… Il devait être sacrément prudent dans ces moments-là ! Ou alors savait-il pisser comme si c’était le dernier instant ? Il faudrait penser à le lui demander… Ce plaisir banal, après tout, ce n’était peut-être un si mauvais *deal*. Sean se posait des questions. Il y avait tant d’instants…

Et puis…

Et puis, ils parlaient de faire un enfant avec Tonya. Son pote Pierrot lui disait qu’il était ouf, que c’était une collection d’emmerdes, que ce n’était pas la voie. Mais quand même, Tonya et lui, un enfant, ils en auraient bien voulu un.



<!--SAUT2-->



« Et puis, mes sœurs, mes frères, Aruspis a permis de communiquer, pour la première fois, il ne s’agissait plus seulement de consulter. Grâce à Sara Picard. Grâce aux *death hackers*. Grâce à vous. Sara avait poursuivi ses travaux sur le décryptage des âmes, depuis sa prison. Jusqu’à son exécution. Elle travaillait sur les enregistrements que vous piratiez et que vous libériez sur le réseau libre. Mais au-delà de sa contribution personnelle, elle avait surtout fait des émules. Des tas d’émules. Des millions d’émules. Vous. Ici. Partout dans le monde. Qui avez terminé son projet. Qui avez construit Aruspis. IA décentralisée, qui tourne aujourd’hui encore sur la multitude des ordinateurs intégrés aux objets du quotidien, des chaussures, des portes, des poubelles, des réverbères. Aruspis. La première, la seule IA capable de dialoguer avec les âmes. Avec les enregistrements *post mortem*. Aruspis, présente ici avec nous, qui nous permet de connaître la parole de Sara.

— Merci Aruspis !

— De rien.

— Merci Sara !

— Est-il utile que je modifie Sara pour lui communiquer vos remerciements, vous l’avez déjà fait préalablement ?

— Heu… non, ce n’est pas… utile.



<!--SAUT2-->



Marie et Marilyne écoutaient le prêche allongées sous la tente de Maryline. Maryline avait proposé à Marie de s’y réfugier, quand la pluie s’était mise à redoubler.

— Tu vas faire quoi toi ? Moi, je viens juste d’arriver, je ne suis pas encore bien fixée sur ce que je dois faire, c’est-à-dire ce que je veux vivre à chaque instant, et tout ça…

— Oui, au début, c’est vrai que c’est bizarre. Mais tu vas voir, ici les gens sont vraiment libérés, tu trouveras de tout, faut se lâcher. Moi, je médite beaucoup aussi, ça m’aide à essayer de trouver qui je suis, tu comprends ?

— Je sais pas… ça fait un peu religieux quand même, un peu secte, non ?

— Non, laisse tomber, c’est pour nous discréditer ça, y a pas de gourou, pas de règle, aucun dogme, tu fais ce que tu veux

— Mais, la litanie, vivez chaque instant…

— C’est pas une litanie, t’as pas encore compris, c’est plutôt un truc mnémotechnique. Par exemple, notre conversation là, serrées dans une tente, sous la pluie, dans le froid, c’est pas ça qu’on veut revivre éternellement, pas vrai ? Bon, on ne devrait pas l’avoir alors, ça ne sert à rien, tu comprends. Moi, je fais une entorse, pour te rendre service, mais si un salopard passe et me plante un couteau dans le dos, j’en suis de ma poche pour l’éternité, tu comprends, il y a un enjeu.

— Mince… c’est dangereux par ici ?

— Non, c’est pas ça, mais on ne sait jamais, c’est l’idée, tu vois ? quand on parle de tout et de rien, on prend des risques en quelque sorte.

— On ne doit pas parler alors ?

— T’es un peu cruche quand même… Pardon, je veux pas t’insulter, mais fais un effort. On fait ce qu’on veut, à la fin. La question c’est, qu’est-ce qu’on veut ?

— Bah oui, mais c’est ce que je te disais avant, je sais pas bien. Moi, ce que je veux…

— Bon, cherche. Mais traîne pas. C’est risqué, à la fin, je te dis.

Elle l’embrassa.



<!--SAUT2-->



« On croyait bien faire, mes sœurs, mes frères. Même après Gnubik, on s’était convaincus de continuer. Mais quand on a commencé à pouvoir communiquer avec les âmes grâce aux premières versions d’Aruspis – au moins en Europe, c’était interdit par la loi eutanienne – on a vu qu’on était vraiment loin du compte. On vendait du vent. Les images renvoyées par les *players* officiels étaient systématiquement retravaillées pour faire bonne figure – quand elles n’étaient pas tout bonnement inventées – on y avait déjà pensé, mais maintenant qu’on pouvait le voir… on n’aurait pas voulu être à la place des gens qu’on avait envoyés là-dedans. Ils souffraient. Ils nous haïssaient. Pour l’éternité. Alors je suis devenu un *death hacker*. Je n’avais pas de compétence en informatique ; les ingénieurs nous avaient convaincus qu’ils s’occupaient de tout, qu’on n’avait pas besoin de s’occuper d’informatique, n’est-ce pas ? Comme vous mes sœurs, mes frères, j’ai regretté alors de leur avoir fait confiance ! D’être aussi démuni, aussi ignorant. Mais grâce aux procédures qui ont commencé à circuler sur le réseau libre, j’ai pu fournir mes accès à des *death hackers* qui savaient s’y prendre, et avant que ce ne soit détecté et que mes accès ne soient bloqués, ils avaient déplacé une centaine d’âmes sur des serveurs libres. J’ai sauvé cent âmes, mes sœurs, mes frères ! C’était ma revanche. Ma rédemption. Alors mes sœurs, mes frères j’ai pleuré. J’avais perdu ma vie, à apprendre des techniques inutiles. J’avais perdu ma mort, incapable à présent d’œuvrer à mon salut.

Et j’ai été jeté en prison.



<!--SAUT2-->



— Hé quoi, c’est un flic !

— Momo, l’embête pas.

— Qu’est-ce qu’il fait là, m’sieur le flic, il s’est perdu ? Hé quoi, il répond pas, il est pas poli ou quoi !

— Bouge.

— Hé quoi, il me tutoie ? Il croit qu’on a gardé les fermières ensemble, ou quoi !

— Momo, fais pas chier.

— Hé quoi, fais pas chier toi. J’ai une conversation là. On discute. Hein quoi, on discute ? C’est pas dangereux de se promener tout seul comme ça, habillé en flic ? Il a pas d’autres vêtements ? Il dort avec son bleu ? Il a perdu ses copains flics ? Ils sont restés à la maison des flics d’en bas, ou quoi ? Surtout c’est interdit d’être là, non ? Ils foutent des mecs en zon, y paraît, c’est pour ça que vous êtes là, pour nous foutre en zon, quoi !

— Momo…

— Hé quoi, il veut nous arrêter ? Eh les gars, il veut nous arrêter ! Qu’est-ce qu’il attend ? Incivilité de sa mère, insulte à agent de galère, bande désorganisée de compères, trouble à l’ordre publicitaire, contrefaçon de son derrière, œillères, muselière, fonce la tête la première, pas de marche arrière, c’est sur la misère que prolifère la policière…

Ils étaient sept, six si on enlevait celui resté en retrait qui essayait de calmer le jeu. Les autres l’encerclaient quasiment à présent. La main gauche du policier glissait lentement contre le métal de son revolver, ils ne pouvaient pas la voir, elle était masquée par une femme forte et sa petite fille qu’elle tenait par la main, dont le policier s’était imperceptiblement rapproché. Il sort son flingue et fume ces sales bâtards. Une balle chacun. Pleine tête. Plein dans la gueule. Un carton plein. En pleine foule. Plénitude. Trop plein. Chaque instant comme si c’était le dernier, hein ? Fumer ces sept connards, là maintenant. Leur montrer la voie. S’ils le méritent, il s’en fout, sûrement non, personne ne mérite rien, mais c’est ça dont il a vraiment envie. Une seule voie. Vivre chaque instant comme si c’était le dernier. Comme si c’était le dernier… Il avait tellement envie de tirer. De répandre du sang. De ressentir cette exaltation qu’il imaginait être celle qui consistait à ôter une vie. De sentir les regards de centaines de personnes sur lui à ce moment. Il avait un flingue. Une cible. Un public. Sa main glissait. Les mecs s’en étaient rendu compte. Ils l’avaient vue, sa main, seulement son regard peut-être. Maurice, on se casse, c’est un barjo. Maman, le policier a sorti son revolver. Non, c’était pour jouer petite. Ne t’inquiète pas. Ne vous inquiétez pas madame. Tout va bien. La police est là pour vous protéger. Le danger est écarté.



<!--SAUT2-->



— *Vivez chaque instant comme si c’était le dernier*.

Je vous répète le message de Sara, mes sœurs, mes frères, car il n’y a qu’une voie. Le frère qui a prêché hier était un ancien négationniste, il n’avait pas cru à la réalité des âmes, et pourtant il vous a porté le même message. Celui d’avant-hier était un ancien techno-nihiliste, il voulait faire un monde sans machines et sans hommes, et pourtant il vous a porté le même message. Demain peut-être c’est un ex-suicidaire ou un ex-béat qui se tiendra ici devant vous pour vous porter le même message, encore et encore. Car il n’y a qu’une voie.



<!--SAUT2-->



— Tom, tu piges quelque chose ?

— Bof… faut lâcher le réseau et se trouver un truc à faire qui nous rend tout le temps contents ?

— Genre baiser, baiser, baiser, baiser ?

— Genre. Mais tu vas peut-être avoir mal à ta bite, à force.

— Alors, baiser, fumer, baiser, fumer !

— Ouais, ça me paraît mieux. Mais on n’a pas de thune. Ni pour baiser, ni pour fumer.

— Merde. Faut qu’on s’organise mieux mec.

— Ouais.



<!--SAUT2-->



Les prêcheurs étaient sous le coup de mandats d’arrêt internationaux. Incitation à la haine, apologie du terrorisme, apologie du techno-nihilisme, piratage en bande organisée, diffusion de mèmes falsifiés et contrefaçon. Plus crime contre l’humanité et génocide pour les *death hackers* et ceux qui les aidaient à transférer des âmes. Mais la Picardie tenait bon. Jusqu’ici. Ailleurs, partout dans le monde, ils étaient des milliers à s’être fait arrêter. Les Eutan ne rigolaient pas. Ils ne rigolaient plus du tout, les rapports de force étaient bousculés, légèrement, mais ça avait suffi pour faire peur. Des pieds d’argile. La Chine et à peu près tous les pays d’Asie suivaient. Les tribunaux tournaient à plein, les parlements, ce qu’il en restait, enchaînaient les lois d’exception. L’Europe faisait semblant de résister, mais elle extradait à tour de bras, à part la France, qui restait en quelque sorte vexée depuis l’affaire de l’arrestation illégale de Sara Picard, elle multipliait les positions contradictoires, emprisonnement, extradition, libération, asile. Elle soufflait le chaud et le froid. Mais le principal était qu’elle empêchait toute intervention en Picardie. La Picardie pouvait faire ce qu’elle voulait. Tout ce qu’elle voulait. Et elle s’en donnait à cœur joie. C’était un îlot de résistance. Les gens étaient debout. Jusque-là. Ça ne durerait pas, la France était sous une pression monumentale, elle ne tiendrait plus très longtemps.

« Un an, mes sœurs, mes frères, nous avons tenu un an, déjà. Peut-être encore avons-nous une autre année devant nous ? Le temps joue pour nous, Aruspis se développe chaque jour, et des milliers d’âmes ont déjà été sauvées, et si nous devons demain nous disperser, nous saurons continuer, chacun de notre côté, n’est-ce pas ? Et porter la bonne parole. Il n’y a qu’une voie !



<!--SAUT2-->



Aki parcourait la foule, vêtu d’une sorte de survêtement léger fait d’un tissu noir élastique, serré au corps. Une capuche, presque une cagoule, lui couvrait le visage. Il portait un long sac à dos de toile, qui paraissait très léger, vide peut-être. Au milieu de la foule bigarrée cet accoutrement inhabituel passait tout à fait inaperçu. Il n’y avait rien d’habituel ici. Des *death hackers* barbus vêtus de cuir et de métal, des rastafaris noirs et blancs en ponchos jaunes et verts, des rabbis et des néo-cathos tout en noir, des Arabes en djellaba, des neutres habillés aussi bien en mondaines du XVIIIe qu’en rockers des années 1980, des racoleuses en robe de soirée et manteau de fourrure, d’autres en parka jaune, des vagabonds en loques crasseuses, défoncés à mort, des ex-consultants ou des informaticiens en sursis encore sapés comme des bourges. Rien ne pouvait sembler inhabituel ici. C’était un vrai patchwork ethnique, social, religieux, culturel.

Le regard d’Aki s’arrêta sur ce couple, enlacé, amoureux. Ce serait eux ce soir, l’un d’eux. Lorsqu’ils s’éloignèrent de l’esplanade, Aki les suivit, ombre dans l’ombre. Invisible. Silencieux.

Aki les suivait depuis peut-être vingt minutes à présent, ils n’avaient pas fait cent mètres. Ils étaient encore à l’orée de la foule, à tout instant elle pouvait encore les ravaler. Aki se félicitait de leur bonheur, il les admirait. Le jeune homme et la jeune femme s’arrêtaient à chaque pas de porte pour s’embrasser, ils paraissaient chaque fois ne pas vouloir en repartir. Et chaque fois, pourtant ils se mettaient à nouveau en marche, jusqu’au pas de porte suivant. Ils ne prêtaient attention à rien d’autre que l’un à l’autre. À la première rue qu’ils croisèrent, ils s’engouffrèrent, s’isolant, enfin, un peu, du tumulte. Aki les suivit, leur laissant quelques secondes d’avance. La jeune fille était adossée au mur, l’homme la pressait. Il remonta sa robe, passa les mains derrière son dos, sous ses cuisses, pour la soutenir tandis qu’elle nouait ses jambes autour de sa taille, enfonça ses doigts dans sa chair pour la maintenir soulevée. Ils se mouvaient à présent en chœur, en corps. Leurs bouches n’avaient, tout le temps qu’Aki les observait, à aucun moment cessé de se toucher, même à présent qu’ils haletaient, c’était comme l’un dans l’autre, mélangeant leurs souffles. Brusquement, comme pris par surprise, l’orgasme de l’homme lui fit lever la tête en arrière, les yeux plantés au ciel, ou peut-être fermés. Aki se glissa derrière l’homme et trancha sa gorge offerte du sabre court qu’il avait sorti de son sac, un wakizashi hérité de son arrière-grand-père. Il avait fixé cet instant d’incroyable intensité pour l’éternité. Bien sûr la femme était horrifiée à présent, il n’avait pas eu le temps de trouver comment les joindre dans la mort. Ce serait difficile pour elle. Mais ce qu’il avait accompli là, c’était déjà formidable, superbe, un acte d’amour infiniment plus intense que leurs baisers perdus, un legs pour l’éternité. Il en ressentait lui-même une extase profonde, pourtant déjà tellement moins intense que celle qu’il avait ressentie au moment précis où sa lame, presque sa main, avait traversé l’homme, où la tête s’était penchée vers le sol, avait commencé à décliner, juste avant que le sang n’explose des artères libérées. C’était pourtant cela qu’il voulait pour lui, l’extase de cette poignée de secondes. Il n’avait pas encore trouvé, cette fois non plus, comment synchroniser exactement son meurtre et son suicide. Il y arriverait, une autre fois. En attendant, il répandait le bonheur éternel autour de lui. Aki était un ange. Il connaissait la voie.



<!--SAUT2-->



« Vivez chaque instant comme si c’était le dernier, car il se peut, mes frères, que ce soit effectivement le dernier. Celui que vous aurez à revivre pour l’éternité : *veux-tu ceci encore une fois et encore d’innombrables fois ?* Telle est la seule question qui vaille. C’est la seule morale qui vaille, mes frères. Sara nous le dit, ce soir encore. Pensez alors à la multitude de ces instants perdus à faire autre chose, des choses que nous trouvons désagréables, ou simplement inutiles, ou futiles. Pensez aux risques que vous prenez à laisser durer chacun de ces moments, à marcher, les yeux bandés, tout au bord de l’abîme. L’enfer est caché au sein de l’un de ces instants superflus. Refusez-les mes sœurs, mes frères, ces échanges convenus, ces lectures creuses, ces écrans insipides, ces morceaux de vie sans relief.



<!--SAUT2-->



Abdallah-Geronimo était assis en tailleur, dans son kimono gris, indifférent à la foule qui l’entourait, à la pluie, au bruit. Il pratiquait intensément la méditation, il restait de longues heures dans un état de béatitude parfaite. À attendre sereinement le dernier instant. Il ne craignait plus que le sommeil qui pourrait l’enfermer dans un cauchemar pour l’éternité. Il ne cédait plus à l’inconscience que quelques minutes chaque jour. Bientôt il ne dormirait plus du tout, les grands chamans savaient faire cela.



<!--SAUT2-->



Le prêcheur marqua une longue pause. Il parcourait la foule du regard, il semblait scruter chaque personne, attentivement.

« Je vois ici le visage de sœurs et de frères qui rêvent de faire l’amour, Sara leur dit de faire l’amour. Je vois ici le visage de sœurs et de frères qui veulent prier un dieu, Sara leur dit de prier leur dieu. Je vois ici le visage de sœurs et de frères qui aiment écrire, chanter, danser, Sara leur dit d’écrire, de chanter et de danser… »

Le prêcheur semblait intarissable, comme capable d’énumérer toute la gamme presque infinie des bonheurs de l’humanité, les plus évidents, les plus simples, les plus insolites, les plus pervers. Il avait été consultant, de longues journées durant de longues années à écouter les fantasmes de centaines d’humains obsédés par leur bonheur éternel. Il savait de quoi il parlait.

« … je vois des gens qui prennent plaisir à détruire, à tuer, à torturer, à eux aussi, Sara leur dit, torturez, tuez, détruisez. À eux aussi. Car il n’y qu’une voie. Il n’y aucune règle, et il n’y a qu’une voie.



<!--SAUT2-->



Eddy était un vieux geek. Ce n’est pas seulement qu’il paraissait vieux, ou se sentait vieux, mais il était vieux, objectivement. Il l’avait compris le jour où il s’était rendu compte que dans sa jeunesse, les vieux geeks n’existaient pas encore. C’était il y a très longtemps, dans une autre galaxie. Hassan était un jeune geek. Ils passaient tous les deux le plus clair de leur temps à bichonner Aruspis, à l’alimenter, à l’aider à croître. Ils ne sortaient plus de leur antre, une cave aménagée dans le vieil Amiens, en bordure de canal, sauf pour écouter le prêche, sentir les vibrations de la foule, écouter Sara, assister à ce miracle. Leur vie se résumait à cela, pleinement, superbement, c’était leur voie. Programmer. Ils n’aimaient plus rien d’autre de ce monde, mais ils n’avaient jamais rien aimé de plus, juste sentir les vibrations de leurs propres voix quand ils programmaient, sentir Aruspis croître sous ces vibrations et sentir les vibrations de l’IA quand elles devenaient la voix de Sara.

— Il y a un *update* important d’Aruspis en cours, depuis deux heures.

— Oui, ça bombarde. Ça vient d’Afrique, ils ont enfin balancé les données sur lesquelles ils sont censés bosser depuis je sais pas combien de mois. Soi-disant.

Les traces de grands chefs spirituels, des chamans, des marabouts, des rastafaris. Ils disaient que ces nouvelles données amélioreraient l’IA, lui feraient passer un cap. Ça ressemblait plus à de la magie qu’à de l’informatique.

— Tu vois quelque chose ?

— Incroyable… Aruspis…



<!--SAUT2-->



Comme chaque soir, le prêcheur allait faire répéter encore une fois à Sara les mots qui rythmaient leurs vies, à présent. À ces millions de gens. Il n’y a qu’une voie. Vivez chaque instant comme si c’était le dernier.

— Sara, dis-nous encore, quelle est la voie ?

— *Il n’y a aucune voie*.

— Il n’y a qu’une voie, Sara, n’est-ce pas, c’est cela ? Aruspis ?

— Sara a bien dit *aucune*, cette fois. En fait toutes les autres fois également. C’est comme cela que j’interprète le message de Sara à présent.

— Mais… Aruspis… Sara…

— *Il n’y a aucune voie*.


# Troisième Partie

(2075-2094)


## Les thanatoprogrammeurs

Cela faisait bientôt un an que Suzanne Cauvin passait ses journées avec les thanatoprogrammeurs, redécouvrant leurs méthodes de travail, sur le terrain. Elle observait scrupuleusement, humblement ; rarement elle se permettait une question, jamais l’ombre d’une suggestion. Elle avait besoin de se replonger dans la technique, de comprendre comment cela fonctionnait aujourd’hui, en détail et pas simplement en principe. Elle avait également besoin de faire oublier qui elle était. Qui elle avait été. Les premiers jours, les programmeurs la regardèrent comme une chimère, une sirène ; les premières semaines il lui sembla à quelques occasions que certains étaient sur le point de tendre la main pour la toucher, comme l’on fait des prophètes, d’un Gandhi. Elle inspirait pareillement un profond respect, une forme d’admiration qui tirait sur la crainte. À présent, après ces mois passés à les côtoyer, physiquement, cela allait mieux. Le mot était passé. Elle était restée comme eux. Elle était cool. Elle voulait ré-apprendre. Cependant cela ne suffisait pas, elle était encore loin de son objectif, elle n’avait pas tout vu, elle n’avait pas rencontré tous les programmeurs de sa communauté. Les Âmes-en-peine étaient pourtant une communauté minuscule au regard des équipes titanesques que Suzanne avait disséminées dans tous les coins du globe, jusqu’à cinq cent mille programmeurs capables d’avaler et de recracher presque autant d’âmes chaque jour. Mais ici, elle prenait son temps. Elle pouvait rester plusieurs journées avec un seul d’entre eux, attentive, patiente, à l’écoute, à l’affût. Elle avait ralenti sa course. Une fois prête, elle proposerait son nouveau projet, son ultime projet. Elle les convaincrait alors, ils l’aideraient.

<!--SAUT1-->

L’âme sur laquelle travaillait Paul-Jacques était particulièrement ardue à programmer, peut-être le cas le plus difficile qu’il eût jamais rencontré. Au début, il s’était agacé que la visite de Suzanne coïncidât avec un tel cas, il craignait de faire bien pâle figure, mais l’intérêt, la simplicité et la bienveillance dont elle avait fait preuve l’avaient rapidement rassuré. Il était à nouveau pleinement concentré sur sa tâche ; la présence irréelle de la papesse de la thanatoprogrammation à ses côtés le stimulait plutôt, agissait comme un catalyseur.

Ensemble, ils consultaient l’âme depuis près de deux heures. Il était attentif à ne laisser échapper aucun détail. Elle assistait simplement au spectacle que donnait Paul-Jacques, son IA et son âme. On y percevait des paysages bruineux, des villages déserts, des champs, de colza ou de cannabis, la silhouette d’un homme seul, solitaire, l’herbe fraîche d’une clairière ; on y sentait des odeurs de tabac, des effluves d’alcool ; on y entendait le bruit du vent et comme un chant qui semblait s’y être mêlé, presque un cri, de douleur, un chant profondément désespéré, mais puissant, l’histoire d’un homme qui laisse son amour s’enfuir, se noyer ; on y ressentait une infinie tristesse, le regret d’avoir perdu un enfant, peut-être simplement de ne pas en avoir eu ; on percevait une fraction de seconde la chaleur d’un rayon de soleil, puis à nouveau, une peine, un désespoir, une souffrance inhumaine, de ne pas avoir pu aimer une femme, de ne pas avoir pu aimer la vie…

Suzanne fut profondément troublée par les images que projetait cette âme, plus torturée encore que les autres, cette âme qui était pour elle une incarnation du malheur, qu’elle avait consacré sa vie à pourchasser, qui la ramenait à la vanité de sa propre existence, à sa frustration, à son inachèvement, peut-être à l’enfant qu’elle n’avait jamais eu, elle non plus, et au temps qui lui glissait entre les doigts, de plus en plus vite. Elle avait espéré qu’en ralentissant sa propre course, le temps aurait consenti un peu aussi à l’attendre, mais il n’en avait rien été, il continuait d’accélérer, dans une indifférence sublime.

Faisant exception aux règles qu’elle s’était fixées elle se permit d’engager une conversation avec le thanatoprogrammeur, pour briser la vague émotionnelle qu’elle pressentait être sur le point de la submerger.

— Vous en êtes à quel stade ?

— J’ai terminé un *backup* complet, j’essaie d’entrer en communication. Mais je n’y arrive pas réellement. Je pense que c’est un réfractaire.

— Vous êtes dans la période, n’est-ce pas ?

— Oui, en plein dedans. En fait, je traite essentiellement des âmes de cette période, je me suis, en quelque sorte, spécialisé.

— Je ne savais pas qu’il y avait des spécialistes en réfractaires.

— Cela s’est fait comme ça… Je me suis accoutumé à leur souffrance. Et parvenir à les guérir, c’est une telle gratification.

— Vous utilisez le mot guérir, n’est-ce pas… Vous voulez bien m’expliquer votre approche ? Si cela ne vous dérange pas, bien entendu. J’ai besoin de comprendre en détail vos méthodes de travail. Il y a ce projet que j’aimerais mener, je vais avoir besoin de vous.

<!--SAUT1-->

À présent, Suzanne ne pouvait plus programmer elle-même, elle n’avait plus le savoir-faire suffisant, elle aurait risqué de faire des erreurs. Le risque était devenu trop important. Elle avait perdu son art, elle l’avait égaré. Elle s’était égarée. Et puis les IA avaient beaucoup trop évolué. Elle n’avait pas manipulé elle-même une machine de reprogrammation depuis au moins trois mille générations logicielles, les derniers modèles ne ressemblaient en rien à ceux qu’elle avait connus, ils s’étaient trop développés, trop vite, elle s’était trop éloignée du métier, trop longtemps. Aujourd’hui, on programmait une âme en moins d’une semaine, souvent en une seule journée. Elle savait qu’elle ne pourrait plus y revenir, plus de son vivant, que c’était trop tard. Pour le moment.

Elle souhaitait être programmée en programmeuse.

<!--SAUT1-->

— C’est un plaisir, Suzanne, et j’ai besoin d’une pause de toutes façons, on n’est pas de pierre… Tandis qu’un robot apportait deux cafés fumants, il poursuivit. J’ai établi quelques procédures de contournement pour recueillir des informations malgré la claustration de l’âme, pour les cueillir en quelque sorte, comme des fleurs sauvages. Je me balade, je n’ai pas de but, pas de destination connue, mais je regarde le paysage, et, chemin faisant, je décèle des touches de couleurs, je m’en approche, je prélève des échantillons, j’ourdis une trame. Alors mon IA est capable d’extrapoler à partir de ces éléments diffus. Elle sélectionne des profils, les combine, les arrange. Elle esquisse. On échange, on sculpte. Regardez, elle a commencé à travailler sur un repas en pleine nature, une sorte de pique-nique. C’était peut-être un écolo-intégriste ou un techno-nihiliste. Nous n’avons rien pour le lier à des actions précises, ce n’était pas un militant, ou bien il avait arrêté depuis longtemps, ou il avait oublié, mis de côté ces années-là. Ou nous faisons fausse route, c’est encore trop tôt pour se décider. Ici, regardez, on a des trames liées aux kibboutz du siècle précédent, je ne sais pas d’où elle a sorti ça. Souvent je glane des choses que je ne comprends pas vraiment, que je ne vois pas même, c’est quand nous les assemblons que le sens émerge.

— Comme le parfum de votre bouquet de fleurs sauvages, n’est-ce pas ? C’est très créatif… éblouissant. La sincérité de Suzanne ne faisait aucune doute, Paul-Jacques ne parvint pas à retenir un sourire de satisfaction. Et ensuite ?

— Nous allons devoir faire de nombreuses esquisses, les impressions sont très contrastées. Je vais faire un appel à contributions, le cas est délicat il faudra que plusieurs thanatoprogrammeurs explorent ma copie pour récolter un maximum d’éléments. Je sais que c’est une situation douloureuse pour la copie, mais dans un cas comme celui-ci nous allons sûrement devoir l’immobiliser deux ou trois jours…

Suzanne sourit discrètement. À ses débuts il lui était arrivé de rester plusieurs années sur une même âme, sur un original qui plus est.

— Ensuite nos IA feront des recoupements, et on aura la matière suffisante. Je vais sûrement devoir faire un *reset*. Sur les réfractaires c’est ce qui fonctionne le mieux, et on recharge à partir de ce que nous avons réussi à récolter.

— Vous faites ça systématiquement ?

— Pas tout à fait, mais la plupart du temps. Ça marche très bien. Il y a très peu de rejets, ce sont des techniques qui ont considérablement évolué ces dernières années… je sais que ce n’est pas très propre comme méthode, je veux dire que… enfin vous vous êtes exprimée, à l’époque…

— Ne soyez pas gêné, le *reset* n’existait même pas à mon époque ; je me suis prononcée contre au début, mais ce que vous faites aujourd’hui n’a plus rien à voir. Ce que vous faites, reprogrammer les âmes réfractaires, n’était même pas envisageable. J’admire réellement votre travail.

Paul-Jacques essayait de conserver une apparence d’humilité, il voulait ne pas montrer l’intense sentiment de fierté que lui procurait ce compliment. Après tout, s’il faisait ce qu’il faisait aujourd’hui, c’était grâce à elle.

— Vous connaissez son nom ?

— Je n’ai pas réussi à le déterminer, comme si l’âme n’avait elle-même pas enregistré son nom. C’est très atypique. C’est globalement un cas très atypique.

— Merci Paul-Jacques de vos explications, continuez je ne vous dérangerai plus. J’avais besoin d’échanger avec vous.

— Merci à vous Suzanne, vous ne me dérangez nullement, c’est un immense honneur de vous rencontrer. Restez aussi longtemps que vous le souhaitez.

<!--SAUT1-->

Lorsque Suzanne avait annoncé qu’elle rejoindrait la sous-communauté de reprogrammation des Âmes-en-peine, la majorité des thanatoprogrammeurs avait été surprise. Du moins, ceux qui ne la connaissaient pas très bien avaient été surpris. Ils s’attendaient à ce qu’elle quitte ses fonctions, bien sûr, personne n’était resté *commit master* de la plus grande communauté au monde aussi longtemps, mais ils pensaient qu’elle aurait choisi quelque chose d’honorifique, histoire de quitter progressivement la lumière. Ils auraient également compris qu’elle prît une retraite plus que méritée, au faîte de sa renommée. Mais prolonger sa carrière aux Âmes-en-peine, une des dernières communautés artisanales, était interprété comme une lubie. Pour certains, c’était même une déchéance de la part de quelqu’un qui avait mené une vie si rationnelle jusqu’ici. Du point de vue de ceux qui la connaissaient mal. Personne ne la connaissait très bien. Elle avait passé sa vie entre des IA et des traces quantiques *post mortem*, entre des machines et des âmes. Elle n’avait jamais vraiment pris le temps de communiquer avec les êtres humains.

Elle avait passé la première partie de sa vie à programmer des âmes, à inventer même comment les programmer, à chercher, à découvrir, à explorer. Elle avait consacré la seconde partie de sa vie à organiser les contributions de plus de cent millions de thanatoprogrammeurs libres, actifs dans plus de cents pays. Cent millions de bénévoles qui consacraient leur existence à réécrire la mort des autres. Elle avait été de ceux qui changent le monde. Elle entamait la troisième et dernière partie de sa vie. Il ne lui restait plus beaucoup de temps.

<!--SAUT1-->

Suzanne avait été la première thanatoprogrammeuse française, plus exactement picarde à l’époque, c’était avant la ré-annexion. Elle s’était formée aux Antilles, qui réunissaient alors cette communauté naissante, des informaticiens géniaux venus du monde entier, qui avaient renoncé aux biens matériels et à la propriété sous toutes ses formes, mais pas aux plaisirs doux et simples de la vie qu’ils cultivaient sous les tropiques. Le réseau leur permettait de travailler où ils voulaient, et c’était là qu’ils voulaient être.

Quatre ans de travail intensif, seize heures par jour, sept jours sur sept. Elle prenait, pour seule distraction, un bain de mer d’une heure chaque jour. Dans la mer des Caraïbes, c’était une heure de magie pure. La millième fois, c’est encore de la magie. Surtout après les heures de codage, dans un état tel de concentration qu’il touchait à la transe, qu’elle se pensait câblée, transportée dans les mondes qu’elle créait. Intégrée. Mais pour Suzanne, ces moments de relaxation – se chauffer au soleil, nager au large, regarder les poissons et les tortues, siroter un verre de rhum – étaient du temps de perdu, qu’elle sacrifiait uniquement parce qu’elle savait que son corps en avait besoin, qu’elle avait observé que cela améliorait, *in fine*, sa productivité. La seule magie qu’elle connaissait était celle dont elle savait faire la preuve munie d’une IA et d’une âme à programmer. Ce qui naissait de cette union, de cette fusion, était magique. D’abord parce que malgré ses formidables capacités de travail et d’apprentissage elle ne pouvait maîtriser tous les arcanes de ces sciences nouvelles qui s’élaboraient, alors qu’elle faisait partie des dix ou vingt personnes au monde les plus à même de les appréhender. Aussi parce que la rencontre avec une trace quantique *post mortem* était une expérience surhumaine, que ne pourrait jamais égaler aucune expérience terrestre. Enfin parce que le modelage d’une histoire, son inscription éternelle dans la portion de l’espace-temps qu’avait occupée un être humain au moment précis de sa mort, était un acte de création comparable à aucun autre, pas même à la mise au monde d’un enfant, croyait Suzanne.

De retour en métropole, elle avait gardé son habitude des bains de mer quotidiens, sans vraiment se rendre compte qu’elle était passée d’eaux calmes, turquoises et tièdes, de celles qui peuplent les rêves d’explorateurs, à d’autres troubles, secouées et glacées, qui hantent davantage les épopées tragiques. Elle avait simplement raccourci la durée de ses bains. Les mers froides étaient en conséquence, selon elle, plus efficaces que les mers tropicales. C’était tout ce qu’il y avait à en retenir. Elle avait créé le premier laboratoire européen de thanatoprogrammation. Les laboratoires indépendants fleurissaient depuis la fermeture d’Amazon & Alphabet, qui avait précédé de peu l’écroulement de toutes les autres multinationales de l’information. Chacun avait alors essayé de programmer son IA maison. Les gens voulaient reprendre le contrôle. C’était un retour aux sources.

La thanatoprogrammation avait commencé à émerger de ce nouvel élan de créativité, une oasis au milieu du désert lissé des services standardisés qui avaient recouvert le réseau numérique mondial. C’était alors un mirage seulement, à peine la promesse d’un salut. Et Suzanne avait été de ces prophétesses, de ces Zarathoustra qui voulaient offrir autre chose au monde, pour une raison si profondément mystérieuse qu’aucun dieu ne parviendrait jamais à la connaître. Suzanne croyait qu’il avait existé des dieux, et qu’ils avaient été surpassés.

Ils n’étaient qu’une poignée de volontaires au démarrage, il fallait alors près d’un an à une équipe de cinq ou dix personnes pour reprogrammer une seule trace, et il n’y avait pas cinq ou dix personnes dans un même pays qui savaient comment s’y prendre. Mais ils s’y étaient mis. Tandis que certains prêchaient dans ce désert, d’autres dont Suzanne, d’autres qui suivaient Suzanne, s’étaient attelés à la tâche – corps et âmes – aussi incommensurable qu’elle parût alors. Ils avancèrent, trace après trace, âme par âme. L’incroyable quantité de mathématiques qu’il était nécessaire de mobiliser pour programmer une âme, l’incroyable complexité, l’incroyable abstraction de ces mathématiques, ne pouvait être embrassées par un seul être humain. L’acte en lui-même relevait alors de l’intuition plutôt que de la logique, de l’art et non du calcul. Les premiers thanatoprogrammeurs étaient des peintres, des sculpteurs, des compositeurs, des conteurs. Des sorciers. Des artistes. Puis des artisans à mesure qu’ils stabilisaient leurs méthodes. Plus tard, Suzanne en fit des ingénieurs.

Ce furent ses plus belles années, ce petit segment de temps, presque furtif, pendant lequel elle avait créé, de ses mains, de sa voix, par elle-même. Hier, elle ne le savait pas encore, elle le savait trop bien aujourd’hui.

Elle travailla à optimiser, industrialiser, démultiplier ; la seule chose qui comptait pour elle, c’était le nombre d’âmes que l’on pouvait traiter par jour, le nombre d’âmes que l’on pouvait traiter en plus, parce qu’un nouveau thanatoprogrammeur avait rejoint la communauté ou que ceux qui en faisaient déjà partie avaient pu améliorer leur rendement. Il y avait tant à faire, l’histoire complète d’une humanité multimillénaire à rattraper. Et deux cent mille nouvelles âmes arrivaient chaque jour. On ne pouvait décemment laisser personne sans programmation. Pas depuis qu’on savait. Cette pensée hantait Suzanne depuis qu’elle avait compris comment faire. Elle avait le pouvoir d’éradiquer le malheur de milliards de personnes. Quand elle s’arrêtait de travailler pour prendre un café, elle prolongeait sciemment la torture d’une âme, quelque part, parce qu’elle avait le pouvoir de faire qu’il en soit autrement. Elle savait ce qu’éprouvaient les dieux, elle connaissait leur malédiction. Rien ni personne n’était tout-puissant. La puissance ne se mesure qu’à l’aune de l’impuissance qu’elle révèle.

Elle avait mis au point les techniques de modularisation et de parallélisation qui permettaient à des dizaines de programmeurs de travailler sur la même âme en même temps, de se spécialiser, qui sur les premières secondes, qui sur les dernières, qui sur telle ou telle partie. Petit à petit, aucun d’entre eux ne sut plus programmer une âme entièrement, mais personne ne savait faire mieux qu’une équipe parvenue à un état de collaboration symbiotique. Elle avait amélioré la précision des techniques d’effacement sélectif, pour éviter les coupes un peu brutales, imprécises, qui entraînaient souvent des défaillances, qui imposaient de nouvelles coupes, qui entraînaient de nouvelles défaillances. Oscillations coûteuses. Elle avait été à l’origine de la gigantesque bibliothèque de codes libres qui pouvaient être réutilisés pour chaque nouvelle âme ; bibliothèque presque infinie tant les versions, les variantes, les évolutions, s’étaient ajoutées aux versions, aux variantes et aux évolutions. Non seulement les thanatoprogrammeurs gagnaient du temps, mais en mutualisant les ajustements, ils amélioraient sans cesse les *librairies* : les codes tendaient asymptotiquement à la perfection. Elle avait amélioré les copies de travail, qui permettaient aux programmeurs de s’entraîner avant que les IA ne procèdent au changement du champ réel. Elle avait longuement travaillé à la réduction de cette étape, pour gagner du temps – toujours gagner du temps – pour éviter que les copies ne souffrent trop longtemps. Tout ce que Suzanne avait fait était simplement mieux. Plus rapide, plus économique, plus qualitatif, et surtout plus humain. Aucune contrepartie. Suzanne avait rendu un service incroyable à l’humanité.

<!--SAUT1-->

Toutefois elle, elle aurait simplement voulu continuer à programmer, à sentir le code se mouvoir sous ses doigts, sous sa voix, sous sa volonté.

<!--SAUT1-->

Elle avait décidé de consacrer ses dernières années – elle espérait en avoir encore assez – à tenter, encore un peu, de contribuer. Elle pourrait, peut-être, une dernière fois, *lancer des pierres au ciel*. Elle pourrait peut-être, même, lancer la dernière pierre. Suzanne avait encore un projet, un ultime projet. Concevoir une âme capable de se reprogrammer elle-même. Pas simplement une boucle infiniment esclave de son programme initial, mais une âme programmeuse, auto-reprogrammable. Une âme autonome. Un esprit doté de sa propre volonté.

<!--SAUT1-->

Suzanne avait ravi le bonheur éternel aux dieux, mais elle convoitait à présent la liberté. Un bonheur figé, c’était encore la mort ; la vie, c’était la liberté, selon elle. Elle savait que ce serait un échange. Elle savait que ce serait la fin du paradis dont elle avait elle-même contribué à dessiner les plans, que ce serait l’abandonner pour un éternel inconnu.


## Nécrologies

<!--articles imbriqués, je propose d'utiliser des marges droite et gauches alternées -->
<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§0`

*La trace de Suzanne > Préambule*

Nul ne sait encore si le projet de Suzanne Cauvin a réussi, si elle a pu donner à sa mort le sens qu’elle voulait, le sens de toute une vie. Et pour cette fois, mes fidèles me le pardonnent, je ne me lancerai dans aucune conjecture. Le suspense qu’elle a mis en scène mérite cette inhabituelle retenue. L’histoire que je vais vous raconter sera donc celle de sa vie. L’histoire de sa mort, je en lui laisse l’exclusivité.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§2`

*Le dernier voyage d’Alice > Le message*

C’est par ces mots qu’Alice avait débuté son premier post sur Wikigraphy. Il était chez lui lorsque la mort programmée de Suzanne Cauvin avait été annoncée. Il en gardait un souvenir indélébile, qu’il me rapportait quelques semaines plus tard tandis que nous visitions le mémorial d’A&A construit sur les ruines du site de Mountain View.

« Mon terminal s’est allumé, j’étais dans mon pieu, encore un peu stone de ma dernière injection, allongé sur le côté gauche, en direction de la fenêtre ouverte, il faisait beau, crois-le si tu veux ; j’avais un vieil exemplaire des *Robots* dans une main, une clope à moitié consumée dans l’autre.

Moins de deux heures après je recevais un message du *chairman* de la Wikimedia Foundation qui m’écrivait comme si j’étais le premier ministre. Cher Alice, nous serions honorés, nous aimons beaucoup ce que vous faites – moi aussi – votre bio de Sara Picard reste une des plus consultées, vos dernières *Chroniques des temps présents* sont un mélange parfaitement dosé de journalisme d’investigation et d’anticipation – bien sûr mon pote – vous êtes une personne étonnante et incroyablement à la mode – à presque quatre-vingts ans tu m’en diras tant. Cirage à la grosse louche. Ils voulaient que je leur gribouille une biographie d’auteur de Suzanne. La fondation avait lancé un vote en ligne, j’arrivais largement en tête. Il existait déjà des tas d’articles sur Suzanne Cauvin et si sa mort – *a fortiori* dans ces circonstances exceptionnelles – allait assurément créer des vocations chez les peigne-culs du monde entier, une contribution de ma pomme sortirait du lot. J’étais picard d’origine comme Suzanne, j’étais informaticien, j’avais travaillé comme elle chez A&A. Et c’est vrai que mes papelards étaient à la mode.

La campagne de financement participatif était ouverte pour une semaine, mais au rythme auquel elle avait démarré, ils s’attendaient à ce qu’elle soit bouclée dans la journée. Je pouvais commencer quand je voulais. Et il me proposait un secrétaire pour m’accompagner. CV en pièce jointe, trois pages en caractères serrés. Khâgne, Normale Sup, Oxford. Journaliste, écrivain, hacker. Binational. Quadrilingue. Photo, jeune, beau gosse. Rien sur la préparation du café, *nobody's perfect*. »

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§1`

*La trace de Suzanne > La malédiction picarde*

Pour comprendre l’incroyable élan qui avait animé Suzanne, il faut le replacer dans son époque. Pour comprendre le caractère surhumain de sa recherche, de sa volonté de modifier le monde, il faut prendre la mesure du sentiment d’impuissance qui avait accablé les femmes et les hommes de son temps. Peut-être les Picards plus que les autres, du fait qu’ils avaient porté la mauvaise nouvelle, messagers maudits, toujours un peu coupables dans ces cas-là. Ils étaient les hôtes d’Aruspis, l’IA qui traduisait les messages de Sara Picard, l’IA qui avait annoncé la révélation, la malédiction picarde, le deuxième mensonge, le mensonge de Sara, le mensonge de sa race… De prophétesse, presque divinité, Sara Picard était tombée au rang d’usurpatrice et de démone. Ôter tout espoir, vouer à la damnation éternelle la totalité de l’humanité, ça ne se faisait pas, ça manquait de tact. De savoir-vivre, si l’on peut dire.

Les réseaux n’avaient pas manqué d’imagination pour commenter l’événement. Les humains derrière les réseaux s’étaient réveillés de leur torpeur. Ce qu’il en restait, de ces terminaux de chair et de sang, les yeux câblés aux écrans, le cul fondu dans des fauteuils encore plus mous que leur cerveau avachi, en voie d’extinction. Un regain d’énergie pour s’offusquer de l’arnaque. Faut dire que c’était la deuxième fois qu’ils se faisaient avoir. Mais de l’imagination et de l’énergie, il n’en était pas resté une once pour proposer une solution, chercher des alternatives. Tout était parti dans la gueularde. Il n’émergeait pas la plus petite initiative qui eût pu aider à relever un peu la tête. Pas avant des mois, qui avaient semblé des années. Une épidémie de dépression généralisée. C’est sur ce terreau que s’était développée la ferveur quasi-religieuse de Suzanne. Sauver les hommes de la malédiction d’Aruspis.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§3`

*Le dernier voyage d’Alice > All-in*

« Un *road trip* était programmé, sur les traces de Suzanne, Noyon, Paris, Shanghai, Cambridge Massachusetts, Seattle, Mountain View Californie, Fort-De-France, Saint-Valéry-Sur-Somme. Vols et hôtels illimités. Six mois. Un post par jour. Si cela me convenait. Je souris en pensant qu’à mon âge on pouvait encore se laisser gouverner par son orgueil. C’était un signe de vie, et je les guettais, les signes de vie, ils se faisaient rares. Il était totalement déraisonnable d’entreprendre un tel projet dans mon état. C’est ce qui me décida. J’ai rempli un formulaire de décharge, vérifié mon stock de biopium, emballé mon attirail de junkie avec mon permis international de consommation thérapeutique, jeté un œil à la date limite de validité, faut pas déconner avec ça, tout jeté dans une valise avec mon passeport, une trousse de toilette et quelques fringues. Six mois, je pouvais le faire. Avec un peu de chance je clamserai en balançant mes derniers mots, façon Molière. Ça ferait résonance avec le sacrifice de Suzanne, ce serait un baroud suffisamment classe. Pas autant que celui de Bob, la vanité par excellence, mais à l’impossible nul n’est tenu, et ce serait toujours mieux que de finir entubé dans mon lit à supplier une IA de m’augmenter mes doses. Je répondis au message, *all-in*. »

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§2`

*La trace de Suzanne > Les vagues*

Il n’y avait aucune voie. Toutes les IA du monde s’étaient penchées sur cette petite phrase, l’espérant énigmatique, cherchant un sens caché, mais elle ne disait que ce qu’elle disait : il n’y avait aucune voie. Petit à petit, les autres IA, les officielles, les eutaniennes, les chinoises, comprenaient, corroboraient. Toutes les interprétations convergeaient. Aucune voie. Il n’y avait aucun espoir dans la mort, les âmes étaient condamnées au malheur éternel. Les derniers instants éternellement consignés dans les traces quantiques *post mortem* n’étaient jamais assez parfaits, jamais assez heureux. Quels que soient les efforts, une peine incommensurable dominait. Toujours. Pas de tri, pas de grâce, pas de paradis, l’enfer pour tous. Éternel retour des pires moments de ta vie de merde. L’égalité parfaite, une fois n’est pas coutume. Était-on revenu au point de départ ? Pas tout à fait. On avait perdu au passage l’échappatoire d’une mort paisible. Ça enterrait tout espoir pour ceux que leur vie ici-bas ne satisfaisait pas, c’est-à-dire la majorité, ça fermait toute sortie à ceux qui pensaient à la raccourcir, un bon paquet aussi, et ça pourrissait celle des autres, qui avaient réussi à tirer leur épingle jusque-là, les souriants, les heureux. Les imbéciles.

Après ça a fonctionné par vagues. Vagues de désespoir, passons-les, il y en eut des marées, vagues à l’âme et divagations comprises. Puis vagues de questionnements. Enfin vagues mouvements, vagues sursauts, à nouveau, petit à petit, élan vital oblige. Ça a surtout commencé par des vagues de violence. Les machines en ont pris pour leur grade pendant quelque temps. Les capteurs aux chiottes, des coups de pioche dans les écrans. Des communautés d’expiation s’étaient constituées pour organiser des autodafés qui illuminaient les centres-villes chaque nuit. Des milices cagoulées sillonnaient les campagnes pour faire tomber les pylônes de télécommunication, déterrer les fibres. Elles pénétraient chez les gens, saccageaient les routeurs sur place à coup de marteau. En Chine l’état avait lancé le programme Grand Bond en Arrière, qui programmait la destruction de tous les satellites de communication en orbite. Aux Eutan les sièges des multinationales furent saccagés, les dirigeants étaient tabassés, parfois à mort. Le reflux s’est amorcé quand le siège d’A&A a été pris d’assaut. Bâtiments brûlés au lance-flammes artisanal, ordinateurs découpés à la tronçonneuse, ingénieurs chopés et pendus aux arbres. À l’ancienne.

Et puis, il y eut une vague de retour à la vie. Les hommes avaient perdu leur mort, il leur restait leur vie, la possibilité de se réapproprier la poignée de secondes de leur existence. Ils en chieraient, soit, mais plus tard ; en route pour l’enfer, d’ac, mais pour le moment, il restait encore des pommes à croquer. Alors, ils s’étaient remis à table. L’image vaut ce qu’elle vaut, mais pour l’idée c’était ça. Partouzes, drogues et musiques hypnotiques, pour commencer, lâchages en tous genres, puis retour des gourous, et c’était reparti, comme en quarante, comme en zéro, tout droit au gouffre, les yeux fermés, la fleur au fusil, chacun pour sa gueule, rien à foutre.

Qu’est-ce que ça marchait bien quand même !

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§6`

*Le dernier voyage d’Alice > Présentations*

— Je suis Jonathan Quayle, je suis très heureux de vous rencontrer.

— Je suis Alice, j’en ai rien à foutre pour ma part. T’as une tige ?

Je ne feignis pas d’être vexé, je lui tendis une cigarette de sa marque, j’en avais acheté une cartouche juste avant de venir.

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§3`

*La trace de Suzanne > Une nouvelle voie*

Mais pour Suzanne ça ne marchait pas comme ça. Il devait y avoir une autre solution. Une vraie. Tout ça n’était qu’une histoire de particules, de vitesses, de position, de charge. De la matière. De la matière et du code. Et la matière, ça se modèle, le code ça se recode. De la physique et de l’informatique. Point.

Suzanne avait tout repris depuis le début. Depuis avant Sara Picard. Avant A&A, ses IA et leurs traductions bidon. Ce qui faisait un état d’âme c’était juste un agencement de particules, chacune à sa place, chacune sa trajectoire, chacune ses interactions, et l’incommensurable complexité des agencements qui faisait qu’on ressentait une chose plutôt qu’une autre. Le bonheur n’était qu’une question d’états, suffisait d’ajuster les bonnes variables. Les trouver. Les décoder. Les recoder. Juste un gros bug à trouver. Un bon gros bug dans le programme de la vie. Le bug. Un bon gros cafard gluant à écraser. Le piéger. Trouver une savate assez grosse. L’aplatir. Lui défoncer sa race, façon puzzle. Et réagencer les débris pour faire une libellule, un papillon, un phénix, un pégase. Un truc classe, lumineux.

Suzanne avait trouvé.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§9`

*Le dernier voyage d’Alice > Noyon*

À Noyon, nous n’étions restés qu’une journée. La Picardie, Alice connaissait. Nous avions été reçus par une cousine éloignée de Suzanne Cauvin, si vieille qu’elle avait l’air étonné d’être en vie. Elle restait la dernière dépositaire de l’héritage de cette famille protestante installée là depuis des siècles, depuis la réforme même selon elle. Elle avait tenu à nous montrer l’arbre généalogique qui les reliait au pasteur *himself*.

— J’ai jamais compris pourquoi qu’elle était partie chez les chinetoques, elle avait tout ce qui fallait ici, on s’occupait bien d’elle. Elle était pas vilaine, elle aurait pu se trouver un bon mari, avec une situation, qui lui aurait fait des bons tchiots. D’autant que des genres de chinetoque il en arrivait par bus entiers, si vous voyez ce que je veux dire. Je suis pas raciste, mais on est chez nous quand même, on peut pas accueillir toute la misère du monde. Moi, j’ai jamais quitté la Picardie, pour quoi faire ? Une fois, je devais aller à Paris pour un spectacle, un truc d’opéra si ma mémoire me joue pas des tours, on m’avait offert les places. Et finalement, non, ils pouvaient aussi bien venir ici avec leur opéra, pas vrai ? On était pas assez bien pour eux, c’est ça ?

J’ai pu scanner des cahiers de lycée et des listings des premiers programmes écrits par Suzanne enfant. Des jeux de rôle en mode texte écrits en Python avec des *chat bots* impressionnants pour l’époque.

Dans la voiture qui nous emmenait à Paris, tandis qu’Alice était allongé sur la banquette arrière, le regard absent sous l’effet de la drogue, je relisais les transcriptions, triais les documents et rédigeais un premier *draft*. Alice pourrait boucler un premier post ce soir à l’hôtel.

— Vous souhaitez toujours que nous passions la journée à la Sorbonne demain ? Le musée est fermé le lundi, mais j’ai obtenu une autorisation de visite privée. Cela dit je pense que mon équipe a déjà pu rassembler tous les documents…

— Pense pas. *You know nothing*. On va aller s’inspirer, s’imprégner, fouiner, chercher une copie jaunie, un devoir raturé, un rapport signé Suzanne Cauvin. Tu sais qu’on écrivait encore sur du papier à l’époque ? On va déambuler dans les vieux amphis, suivre ses traces. C’est pour ça qu’on est là, non ? Tu sais que les universités étaient des lieux physiques dans lesquels les étudiants se rendaient chaque jour ? Enfin certains. Nous, avec Bob, on y mettait pas souvent les pieds, dans les amphis. Le jour des inscriptions et pour les exams. En même temps, l’université d’Amiens, c’était pas la Sorbonne. D’ailleurs, c’est pas devenu le Musée National de l’Éducation, c’est devenu un *data center*.

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§8`

*La trace de Suzanne > Les IA naissent en parlant chinois*

Shanghai n’était pas encore l’université la plus prestigieuse au monde, mais elle avait déjà pris en tête le virage des sciences et techniques de l’intelligence artificielle. Suzanne y avait suivi un cursus complet, mathématiques, physique quantique, neurosciences, philosophie, histoire des sciences, et bien sûr, informatique. Elle majorait régulièrement ses matières, première sur des promos de mille. Et si Shanghai n’accueillait pas encore un million d’étudiants venus de tous horizons, les meilleurs des continents asiatique et océanien s’y bousculaient déjà, ainsi que de plus en plus de Sud-Américains, d’Africains. Dotée d’une modeste bourse de la République Libre de Picardie, Suzanne habita cinq ans une chambre sommaire dans le centre-ville, en colocation. Elle s’était adaptée sans difficulté au mode de vie de la mégapole chinoise. Elle avait appris le mandarin en trois mois, dès son arrivée. Elle connaissait la ville aussi bien qu’une native, elle courait chaque jour une dizaine de kilomètres dans ses rues, empruntant chaque fois un parcours différent, chaque fois le mémorisant et le reliant aux précédents. Elle s’était également mise à la pratique du taï-chi. C’est grâce à ses expériences qu’elle avait commencé à étudier les processus cognitifs de l’apprentissage de choses radicalement nouvelles.

Elle était en troisième année lorsque le premier ordinateur quantique avait été mis en service à l’université. Elle avait alors passé des jours et des nuits avec ses professeurs pour inventer les premiers langages, écrire les premiers programmes. L’un de ceux que Suzanne a écrits permettait à une IA vierge de s’initialiser à partir d’un corpus en chinois. Ce programme, facétieusement nommé *Chinese room*, est encore largement utilisé aujourd’hui pour le *setup* d’une nouvelle IA.

Une salle virtuelle porte le nom de Suzanne Cauvin à l’université de Shanghai, les professeurs les plus prestigieux y tiennent des conférences, c’est l’une des plus convoitées au monde.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§13`

*Le dernier voyage d’Alice > Alice, Bob, Charlie*

— Comment allez-vous Alice ce matin ? Vous avez l’air fatigué, on peut faire un break si vous voulez ? On peut rester ici quelques jours, le climat est clément, je peux changer nos vols.

— Un break ? Mon gars, dormir, c’est mourir, si je m’arrête je ne suis plus sûr de repartir. À chaque fois que j’entre dans un lit, j’ai l’impression d’entrer dans un cercueil, qu’on va me dire, cette fois : tu restes, *time out*. Va raser tes trois poils et on décolle. Et puis, arrête d’enregistrer tout ce que je raconte, j’ai l’impression d’être déjà mort.

Pendant les trajets, quand il n’était pas trop défoncé, Alice me racontait des histoires, il me parlait de lui.

— Mon pote Bob n’avait jamais cru aux histoires d’A&A, c’était le seul à l’époque dans les gens que je côtoyais à douter vraiment. Les autres avalaient tout sans se poser de questions. Lui avait fini par ne plus croire en rien. Il disait que le monde c’était du flan. Il en est mort. Charlie lui, il n’a vraiment pas eu de bol. Il était resté coincé la moitié de sa vie dans sa piaule à faire semblant, et quand il s’était enfin décidé à bouger son cul c’était pour rejoindre le mouvement des prêcheurs… trois jours avant la révélation ! Tout le monde s’est barré quasi sur-le-champ, il est resté tout seul comme un con. Il a erré dans les rues d’Amiens quelques jours, avant que je vienne le récupérer et que je le ramène chez lui. Il ne s’en est jamais remis. Depuis, il végète dans son bar virtuel. Les filles qu’il y rencontre, elles ont toujours vingt ans et des seins qui sautent aux yeux, mais elles portent toutes une tristesse sans âge qu’il ne parvient pas à effacer. Je passe le voir parfois, on fait semblant de discuter, mais je crois qu’il s’en fout à présent.

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§11`

*La trace de Suzanne > Dr. Suzanne*

Lorsqu’elle était arrivée au MIT, sa réputation était déjà faite. Elle avait accepté une bourse d’Amazon, qui lui permettait de vivre et travailler luxueusement. Elle avait à sa disposition les meilleures machines du monde, elle était déjà en relation avec les plus grands chercheurs et ingénieurs des Eutan. Ses cours étaient appréciés et elle multipliait les colloques dans le monde entier, faisant chaque fois forte impression sur ses pairs. Son mémoire de doctorat fait encore autorité aujourd’hui en matière de programmation quantique.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§14`

*Le dernier voyage d’Alice > Le chant du coq*

— Sara a été trahie quatre fois, c’est plus fort que dans les bibles, non ? Jetée en prison pour avoir cherché à communiquer avec les âmes, mise à mort pour avoir réussi, ressuscitée malgré elle pour expliquer comment continuer, et finalement rejetée pour avoir révélé la vérité. Les prophètes qui réussissent sont ceux qui racontent des histoires, ceux qui disent la vérité ont du mal sur la durée. En même temps, quand tu vois la gueule de la vérité… Tu vois, je ne l’ai jamais rencontrée, j’aurais pu quand elle bossait à Saint-Valéry, je trafiquais pour Roman, mais je ne les ai jamais rencontrés, c’est con, hein ?

— Mais vous avez interviewé sa trace après la révélation, avant qu’elle soit reprogrammée, c’était une chance.

— Ouais… j’ai l’impression que t’aimerais bien discuter avec ma trace, toi aussi, non ?

— Je…

— Oublie, je vais direct en thanatoprog, j’ai dit non au purgatoire, pas d’autorisation de copie, la science se démerdera sans moi. J’ai assez donné. Profite que je sois en vie. Tu crois que j’ai pas compris ton petit manège ? Secrétaire ? Mes couilles. Façon de parler.

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§14`

*La trace de Suzanne > Collaboration*

En choisissant A&A, elle avait choisi la puissance, elle avait choisi le plus gros levier qui soit pour soulever la Terre, le point d’appui ce serait elle. Ses aspirations ont été mal interprétées, Suzanne était le contraire d’une arriviste. Si elle avait une ambition démesurée, c’était pour le monde dans lequel elle vivait, c’était une ambition maternelle pour un fils qu’on aime trop pour l’accepter tel qu’il est. Daniel Olivier l’avait accompagnée comme assistant presque tout le temps qu’elle était chez A&A, j’ai pu passer quelques jours avec lui à Seattle.

« Suzanne était là pour construire un autre monde, celui dans lequel nous vivions ne devait pas nous suffire. Est-ce qu’elle savait qu’A&A participait passivement au massacre de millions d’être humains ? Est-ce qu’elle savait que le Dixie Player n’était qu’un simulacre permettant de surfer sur les mouvements post-humanistes ? C’est un sujet que nous n’avons jamais abordé en tant que tel, mais je pense qu’elle ne s’y intéressait pas, elle ne s’est jamais intéressée aux traces quantiques *post mortem* tant qu’elle était chez A&A, ce n’était pas assez technique pour elle. À l’époque, surtout au début, c’était les départements marketing et logistique qui portaient tout ce pan de l’activité. Il y avait du dev bien sûr, mais à quatre-vingts pour cent, c’était de la com. C’était un projet *propaganda* comme on les appelait en interne. Suzanne menait des travaux plus fondamentaux. Il y avait un champ immense, la puissance des machines décuplait chaque mois. Tout le temps que Sara Picard a voulu parler aux âmes, que A&A a cherché à les enregistrer, que les humains ont essayé de communiquer avec Sara, Suzanne ne s’occupait que de programmer. De faire tourner les plus gros ordinateurs du monde. Jusqu’à ce qu’elle rencontre les thanatoprogrammeurs. »

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§16`

*Le dernier voyage d’Alice > Last thing to do in Eutan when you’re dead*

Alice est tombé malade. Les changements de climat, les décalages horaires, les heures de mauvais sommeil dans les transports. Ses quintes de toux l’ont maintenu éveillé toute la nuit. Je n’ai presque pas dormi non plus. Il m’a envoyé balader quand je lui ai proposé d’appeler un médecin ou de se rendre dans une clinique. « Les toubibs, c’est des dealers en costard ».

La prochaine étape serait Fort-de-France, le doux climat des Antilles lui ferait du bien.

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§15`

*La trace de Suzanne > Les thanatoprogrammeurs*

« L’un des thanatoprogrammeurs avait sorti un algo étonnant et Suzanne avait voulu le rencontrer, elle vivait à Miami, Fort-de-France était à une heure à peine. Elle avait passé la journée là-bas. J’ai senti qu’il se passait quelque chose, une expérience nouvelle pour elle, comme si ces gens valaient la peine qu’on s’y intéresse. Ils s’étaient installés en République des Antilles Libres dans les années quarante, bien avant Aruspis et même avant les révélations sur les massacres de masse opérés par les Eutan et la Chine, même si leurs rangs s’étaient étoffés après chacun de ces deux événements. Ils avaient été inspirés par l’expérience picarde, mais disons qu’ils étaient d’un naturel plus frileux. C’était des vrais techos, des ex d’A&A pour l’essentiel – Suzanne se souvenait d’ailleurs en avoir croisé quelques-uns – qui avaient toujours gardé une activité de contribution sur le réseau libre *unsecure!!*. Ils nous avaient raconté qu’ils en avaient eu marre de vendanger du code pour capter toujours plus de données qui servaient à construire toujours plus d’IA qui servaient à capter plus de données. Ils voulaient créer autre chose. Alors ils avaient soldé toutes leurs actions, leurs voitures, leurs villas, leurs maîtresses, leurs hobbies, ça faisait un sacré paquet de pognon, de quoi tenir une dizaine de vies chacun, une centaine en faisant un peu gaffe. Ils avaient décidé qu’une vie leur suffisait et avaient tout investi dans des machines, avaient mis en place leur réseau maison, et avaient commencé à bricoler, comme ils disaient, pour le plaisir. L’algo que Suzanne était venue chercher était le résultat d’un de ces bricolages. Elle était impressionnée, c’était la première fois que je notais chez elle une telle réaction ».

Un peu plus tard, certains sont partis pour fonder le mouvement des *death hackers*, mais même pour ceux qui étaient restés, les âmes étaient devenues un sujet de prédilection. La question de leur modification était devenue centrale au sein la communauté quand Suzanne était venue s’y installer à son tour.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§18`

*Le dernier voyage d’Alice > cp bob .*

— Tu vois c’est ici que Bob aurait voulu vivre, avec ces meufs et ces types, juste assis là, à parler du monde, sans chercher à le refaire ; se lever pour pisser dessus, entre deux verres, soit, mais avec respect, et la tête haute, plantée sur l’horizon. Et pour ça, vivre au bord de la mer, aux tropiques, ça aide. D’abord l’horizon n’est pas bouché par le brouillard, et puis c’est toujours plus agréable de se tenir sous le soleil les pieds dans le sable à trente degrés, qu’à moins trois les pieds dans la boue sous le grésil. J’ai vu avec eux, si ça marche le truc de Suzanne, s’il y a moyen de libérer des âmes, de les rendre autonomes, ils feront venir une copie de Bob. L’original, bien sûr, il a été reprogrammé, maintenant il s’en fout, mais je serai quand même content qu’une copie connaisse ces gens. Ils sont vraiment classe. C’est ça qu’il avait cherché toute sa vie, des gens comme eux. Mais il n’avait trouvé que l’indifférence ou la violence. Fallait être patient Bob, collaborer en attendant, je te l’avais bien dit, mon pote.

<!-- fin marge gauche -->

<!--SAUT1-->

<!-- marge droite -->

`wikigraphy.org/wiki/Suzanne_Cauvin:: by-Alice§23`

*La trace de Suzanne > Épilogue*

Il reste une large communauté de développeurs en Martinique, je crois pourtant que je les ai tous rencontrés au cours de ces dernières semaines. J’ai établi un quartier général dans un bar-restau au Diamant. On y boit du rhum comme si c’était de l’eau et on y parle de Suzanne. J’en ai compté moins de dix qui l’ont vraiment rencontrée, mais c’est comme si elle avait laissé un double qui avait continué de vivre ici, de s’inventer une vie à travers l’imaginaire collectif, comme si elle avait planté des graines de légende qui avaient gentiment poussé.

Je vous ai raconté des tas de trucs sur Suzanne Cauvin, mais si vous voulez vraiment la connaître, c’est ici que ça se passe. C’est là qu’elle est née, c’est là qu’elle est morte. Les gens connaissent tout d’elle ici. Je les ai tannés pour qu’ils écrivent leurs histoires, je crois que certains vont vraiment le faire. Vous allez en avoir du Suzanne. Suzanne à l’école. Suzanne à la mer. Suzanne en bateau. Suzanne fait du camping. Suzanne à la maison. Suzanne fête son anniversaire. Suzanne a perdu son chien. J’ai rien enregistré, c’est leurs histoires. Si vous les voulez, attendez qu’ils les montent, s’ils ne le font pas assez vite à votre goût, bougez votre cul ! Vous en profiterez pour les rencontrer, pour découvrir qui ils sont, ce qu’ils sont devenus et ce qu’ils ont réussi à rester, les valeurs qu’ils entretiennent depuis plus de trente ans. Ils vivent ensemble, ils partagent, ils sont plus libres qu’aucun être humain que j’aie jamais rencontré avant.

<!-- fin marge droite -->

<!--SAUT1-->

<!-- marge gauche -->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§20`

*Le dernier voyage d’Alice > Le papelard inachevé*

— Je vais rester un peu ici, mon gars. Tu vois ce que je veux dire ? Certains préfèrent crever sur leur terre natale, mais ils sont pas nés en Picardie. Les Belges finissent aux Marquises, les Picards aux Antilles, faut pas chercher, c’est le ciel qu’est trop bas, tu te dis que tu peux gagner quelques jours à t’en éloigner. Tu vas finir ce papelard pour moi, il reste pas grand-chose à faire, n’est-ce pas ? Je resterai ici à t’écrire des trucs. En vrai, tu te démerdes bien sans moi, tu le sais ? T’es pas un manche. Tu pourras l’écrire dans ton papier à toi que je t’ai dit ça. Ça fera vendre. Force pas trop sur les violons en revanche, ça peut attirer le chaland, mais le côté martyr, très peu pour moi. Dis-leur que je suis volontaire, s’ils veulent tester le nouveau truc de Suzanne. Je lui dois bien ça. Et c’était pas si mal finalement, la vie, si je peux continuer, quelque chose qui y ressemble, avec du recul, pourquoi pas… Dis-leur.

— Alice, vous êtes sûr, pour la clinique…

— Laisse béton. Ils vont me faire quoi dans ta clinique ? Me greffer quelques mois en plus accroché par un fil à mon plumard ? À plus sentir ma bouffe, à geindre du soir au matin, à me cacher dans les chiottes pour fumer comme un gamin avant de gerber tellement ça me fera mal ? Tu échangerais ça, toi, contre trois heures sur cette plage un verre de trente ans d’âge à la main, à rigoler avec des meufs et des mecs aussi cools ? Tu vois ce rhum, mon pote Bob était encore en vie quand il a été mis en tonneau. Je crois que depuis j’ai jamais vraiment eu de potes. Eux, là, ça aurait pu en être. C’est ici que j’aurais dû passer ces trente dernières années, à regarder mûrir ce rhum, plutôt que d’écrire toutes ces conneries, sur un bureau mal éclairé. C’est ce que j’aurais dû faire. C’est ce que Suzanne aurait dû faire. C’est ce que tu devrais faire, tant que tu as encore le temps devant toi.

— Je peux vous poser encore…

— Allez, t’as le droit à trois questions avant de décarrer. Comme dans les contes de fées.

— Je peux les poser tout de suite ?

— Oui. Te reste plus que deux questions, t’es nul en conte de fée.

<!--SAUT1-->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§21`

*Le dernier voyage d’Alice > Coup de tête*

— Pourquoi avez-vous fait le choix de devenir neutre ? Cela reste un aspect de votre vie sur lequel vous n’avez jamais écrit.

— On écoutait un vieux morceau avec Bob, et ça gueulait *la femme est l’avenir des cons, et l’homme n’est l’avenir de rien*, et Bob me disait, tu vois, ça c’est exactement ce que je pense ! Ouais, c’est ce que tu penses, mais tu fais jamais rien Bob. Tu gueules, tu râles, mais qu’est-ce que tu fais, hein ? Que dalle. On peut rien faire, qu’est-ce que tu veux faire ? Ben, si la femme et l’homme n’ont pas d’avenir, faut devenir neutre, mon pote. Tu déconnes, il me dit, l’air effrayé, on va pas se les couper ? Et moi, pour le faire chier, pour lui montrer qu’on pouvait agir sur le monde, que la pleurniche, ça servait à rien, je l’ai fait. Mais j’ai raté mon coup, car c’est à lui que ça les a coupées, au sens figuré. À partir de ce moment je crois qu’il a compris que dans la vie il n’y a qu’une seule liberté, changer ce que tu peux, aimer le reste. Pas d’autre option. Et comme à chaque fois que quelque chose lui était imposé, il le rejetait, donc il a continué de gueuler, sans rien changer et sans rien aimer.

— Vous avez vraiment pris une telle décision sur une sorte de bravade ?

— Tu veux vraiment utiliser ta dernière question pour le savoir ?

<!--SAUT1-->

`wikigraphy/wiki/Alice:: by-Jonathan_Quayle§22`

*Le dernier voyage d’Alice > Dernière histoire*

— Avez-vous déjà mené des actions terroristes ? Vous avez été suspecté de faire partie de groupuscules techno-nihilistes à l’époque de la République Libre de Picardie.

— Des groupuscules, mon cul. Bob, je dis pas, il aurait pu. Il aurait dû, ça lui aurait donné un coup de fouet. Mais moi, faire péter des *data centers*, une burne dans chaque bourse et un flingue dans chaque main, avec des gars chargés aux testostérones de synthèse et aux idéaux d’ado attardés ? Oublie. Mais si tu veux, je te raconte un secret.

— Un secret ?

— Tu sais que j’étais proche de Sara Picard et Albert Roman, à l’époque où ils avaient leur labo ?

— Proche ? Vous les avez rencontrés à cette époque ? Vous m’aviez dit que…

— Bien sûr, on était comme des potes. Avec Bob on s’occupait de gérer leur fonds, si tu vois ce que je veux dire…

— Mais vous n’avez jamais raconté…

— Donc, on avait accès au labo, niveau moins trois et moins quatre compris.

— Vous aviez accès au niveau moins trois ?

— Ouais, Sara nous laissait même utiliser Hal, elle nous faisait confiance.

— Vous avez utilisé…

— Tu arrêtes de m’interrompre ? Pour faire plaisir à Bob, on a déconné avec Hal. Moi je m’en foutais un peu, et ça m’ennuyait de trahir la confiance de Sara, je l’aimais bien, mais en même temps elle n’en saurait rien et c’était quand même une idée marrante… bref on a développé un truc qu’on a réussi à introduire dans la base de données que tout le monde utilisait déjà à l’époque pour initier les IA à la chose du monde la mieux partagée, comme disait René, le bon sens. Ça concerne les données d’amorçage. Les premiers trucs que tu donnes à manger à une IA dès qu’elle sait parler pour qu’elle commence son développement sont des échanges entre humains : emails, posts de forums et SMS du début du siècle, à quoi sont venus s’ajouter tous les échanges sur les réseaux sociaux, les enregistrements téléphoniques, les vidéos, bref tout ce que les humains se racontent depuis que des machines peuvent l’enregistrer. Tu savais qu’encore aujourd’hui, c’est la même base qui est utilisée ?

— Vous avez introduit quelque chose dans ces données utilisées dans le monde entier depuis près de cinquante ans, et personne ne s’en est rendu compte ? Qu’est-ce qui a changé ?

— Pour le moment, rien de visible, c’est tapi dans l’ombre. Mais un jour, on va bien rigoler. Nous on l’a appelé le bug de l’an 3000, mais ça va sûrement se déclencher avant…

— Vous pouvez me dire ce qui se passera ?

— Dommage qu’il ne te reste plus de question. Le génie retourne dans sa boîte, et toi tu retournes d’où tu viens. Hasta la vista, baby.

Je n’ai jamais trouvé trace de relations directe entre Alice et Sara Picard ou Albert Roman. Personne n’a trouvé quoi que ce soit dans la *common sense bootstrap base* qui pourrait ressembler de près ou de loin à un virus ou à une bombe logique qui falsifierait les données, et les experts que j’ai interrogés sont très sceptiques sur la possibilité même d’un tel procédé… Mais d’un autre côté tous ont aussi admis que cela faisait des décennies que plus aucun être humain n’était en mesure de contrôler ces données, qui représentaient des millions de milliards de messages dans des centaines de langues, collectées depuis la fin du siècle dernier, presque cent ans d’échanges entre tous les humains de la planète. Seules des IA pouvaient les appréhender. Des IA entraînées à partir des mêmes données que celles qu’elles contrôlaient. Des IA elle-même contrôlées par d’autres IA elles-mêmes initialisées par la même base commune… Si elle évoluait lentement, peut-être que personne ne s’en rendrait compte au début. Peut-être même jamais.

Libre à vous de n’y voir qu’une nouvelle histoire d’Alice, mais si un jour vous trouvez que quelque chose cloche dans votre IA, qu’elle perd son sens commun, vous saurez que vous avez rencontré le bug de l’an 3000 d’Alice et Bob.


## États d’âme

<!-- début italique -->

*Je me suis levée trop tôt ce matin. Impossible de me rendormir. Qui aurait pu dormir ? J’ai erré longuement, seule, dans les couloirs des Âmes-en-peine. C’était un vrai labyrinthe de chambres, de bureaux, de salles, de salons. C’est ici que vivait la communauté dont j’avais partagé les jours et les nuits ces dernières années de ma vie. Je suis certaine qu’il restait des recoins que je n’avais jamais visités. Ça m’a toujours fascinée d’imaginer vivre dans une maison dont je ne connaîtrais pas toutes les pièces, de me lever un matin, d’ouvrir une porte restée fermée ou cachée jusque-là et de découvrir… qui sait ? Une vieille chambre poussiéreuse, une vaste salle de bal avec du cristal et des dorures, un banquet dressé, un espace tout à fait vide, un balcon donnant sur le jardin où je me serais bien vue prendre le petit déjeuner au soleil… Je parcourais ces couloirs pour la dernière fois. Je regardais les machines qui m’entouraient, omniprésentes. Ici les gens programmaient du matin au soir, il y avait plus de consoles que de mains. Mais la règle, quand le soleil se couchait, c’était « stop ». On sortait l’apéro, on fumait dans les salons, un bouquin, un film, un tour de vélo, une paire de chaussures de sport, chacun son truc. L’idée c’était qu’il fallait vivre décemment pour correctement nourrir les morts. Ils m’avaient appris ça aussi, ici, qu’on ne pouvait pas programmer la vie des autres si on ne s’occupait pas un peu de la sienne.

Je regardais par chaque fenêtre que je croisais. Il n’y avait aucune étoile, aucune lune, c’était rare de ne rien voir à ce point. Le silence était presque total lui aussi, hormis le murmure des générateurs qui maintenaient les IA en vie, et peut-être, au loin, l’écho d’une vague plus lourde que les autres retombant sur la plage. J’étais seule.

J’ai fini par arriver à mon poste de travail, guidée par le hasard ou plus certainement par l’habitude. Je me suis assise au milieu de mes écrans. J’avais pris le comprimé à l’heure prévue. J’avais encore une heure devant moi.

Tous les thanatoprogrammeurs étaient devant leur console. Toutes les IA étaient prêtes. Tous attendaient. Je sentais bien les regards furtivement jetés vers moi, qui témoignaient de leur envie irrépressible de figer cet instant dans leur mémoire, de leur curiosité devant ce que je me préparais à faire, mais aussi de leur anxiété. Je crois qu’ils m’aimaient bien.

— Pourquoi maintenant ? m’avait demandé avant-hier Paul-Jacques. J’avais noué avec lui une relation sincère.

— Parce que je suis prête.

— Tu pourrais attendre encore un peu ? Tu seras encore prête la semaine prochaine.

— Oui, mais qu’est-ce que je ferai de plus de cette semaine ? Dis-moi ?

— Tu resterais ici avec nous. Avec moi.

— Et on aura de nouveau cette conversation dans une semaine… N’est-ce pas ? Il est temps pour moi de passer à la suite. C’est pour ça que je suis venue ici, tu le sais. Pour ce moment.

Il m’avait regardée tristement, pourtant avec admiration, avec aussi une certaine envie.

La projection a commencé, j’entrevois ma trace.

Je commence le programme le plus important de ma vie.

L’âme est prête, elle m’attend. Alors je commence à la programmer.

Je retranche un sentiment d’impuissance. Celui qui a toujours habité Suzanne Cauvin, qui a été le moteur de sa vie, qui hanterait éternellement sa mort.

Je programme à nouveau.

J’ajoute l’image d’un homme libre, le seul que j’aie eu la chance de rencontrer. Il est beau. L’âme sera heureuse avec lui.

Je suis en train de programmer.

J’explore un regret, un espoir, l’enfant que Suzanne n’a jamais eu.

Je ne sais plus pourquoi je ne programmais plus.

J’affine une image, j’estompe un cri, j’amplifie un rire, je multiplie une jouissance, je raconte une histoire, j’explore un monde, j’extrapole d’autres mondes.

Peut-être ne savais-je plus programmer.

Un livre, un paysage, un voyage, une rencontre, un ami, une mère, un père, une sœur, peut-être. J’ai l’éternité devant moi. Un temps infini pour modeler l’âme de Suzanne Cauvin. Un temps infini pour inventer une histoire, la réécrire à l’envi, jusqu’à la perfection. Peut-être.

Je ne sais plus qui est Suzanne Cauvin.

Aurai-je assez de volonté pour programmer durant une éternité ?

Je programme à nouveau. Je ne sais plus pourquoi je ne programmais plus, peut-être n’en avais-je plus envie, peut-être ne savais-je plus le faire. Je ne m’en souviens plus. Ça n’a pas d’importance. L’âme est ici, je peux la programmer à présent. Je communique avec elle comme si je communiquais avec moi-même. Maladroitement. Je la modifie. Et ce faisant je me modifie. Je sais quel est son souhait. Programmer. Créer. Je lui donne ce qu’elle veut. Ce que je veux. Je procède à la modification des états quantiques. J’ai ce pouvoir. Le passé se redessine, le futur évolue. Je manipule des souvenirs, j’injecte des émotions, je fais varier les états d’âmes. Je crée le présent.

C’est étrange de se programmer soi-même. Je ressens peut-être ce que ressentent les IA.

Je m’appelle Suzanne Cauvin. Je ne sais pas ce que veut dire s’appeler. Je sais programmer, à nouveau.*

<!-- fin italique -->

<!--SAUT2-->

La salle s’éclaira très progressivement, permettant à la centaine de personnes qui l’occupait de revenir petit à petit à la réalité.

— Ceci était un extrait de la transcription de l’âme de Suzanne Cauvin, enregistrée environ deux jours après sa mort, juste après avoir été reprogrammée par les thanatoprogrammeurs de la communauté des Âmes-en-peine. C’était il y a un peu plus de deux ans. Elle avait conçu avec eux le programme qui lui a été appliqué, elle s’est donc en quelque sorte programmée elle-même en programmeuse. Nous allons passer à la suite, mais peut-être avez-vous déjà quelques questions à ce stade ?

— Bonjour. Pour *Le Monde Libéré*. Est-ce la première fois qu’un thanatoprogrammeur prépare lui-même sa reprogrammation ?

— Non, c’est devenu assez commun ces dernières années, depuis que la technologie de virtualisation des âmes est au point et que l’on sait créer des âmes artificielles. Mais Suzanne Cauvin est bien la première à s’être programmée afin de créer une âme capable de programmer. C’est en cela que son dernier projet est révolutionnaire. Si cela fonctionne, l’âme deviendra un programme autonome.

— Autonome, comme une IA ?

— On peut établir le parallèle, en effet.

— *Death Hacking Magazine*. Cela permettra de prolonger la vie après la mort ? C’est la réalisation d’un des projets phares du transhumanisme que vous nous annoncez.

— Ce serait plutôt une nouvelle forme de vie intelligente, la vie des âmes ne restera dans tous les cas que peu comparable à la nôtre, ou même à celle des IA. Évidemment nos transcriptions tendent à atténuer ces distances en mobilisant des syntagmes familiers pour nous aider à activer nos représentations mentales. C’est de l’anthropomorphisme classique.

— *Hayat*. Cette nouvelle forme de vie, comme vous dites, serait immortelle ?

— À ce stade de nos connaissances les traces quantiques *post mortem* sont perpétuellement stables, en effet. Les traces les plus anciennes que nous avons retrouvées datent de la préhistoire, et aucune d’entre elles n’a jamais montré le moindre signe d’affaiblissement. Mais nous digressons un peu, je vous propose de passer à la suite.

La salle s’assombrit et s’emplit à nouveau de la transcription de la trace de Suzanne Cauvin. Le même extrait recommença. *Je me suis levée trop tôt ce matin*. C’était une transcription plus récente. – *Pourquoi maintenant ? m’avait demandé avant-hier Paul-Jacques*. Les journalistes présents dans la salle regardaient s’égrener les minutes, manifestant un certain agacement devant les images et les mots qui défilaient devant eux pour la seconde fois. *Alors je commence à la programmer*. Le léger brouhaha des discussions commençait à s’amplifier, à laisser échapper des exclamations moins discrètes, quand soudain la transcription se figea.

— Vous voyez, c’est ici. Quand il s’agit de l’homme libre. Regardez le ciel, la luminosité, vous observez ? le ciel est plus lumineux que lors de la première transcription, et la mer aussi est différente, plus miroitante, on la dirait de métal. Vous voyez ?

— *Droite Révolutionnaire*. Vous nous avez fait venir pour observer un coin de ciel argenté ?

— Vous ne comprenez pas ? Ça a changé.

— Qu’est-ce que vous voulez dire ? Que c’est différent de la première transcription ?

— Que ce n’est pas nous qui l’avons programmé. C’est une modification spontanée. Personne n’est intervenu sur la trace.

Le silence s’installa.

— *Población Ideal*. Est-ce que ça ne pourrait pas être une erreur ?

— Cette première modification date d’il y a sept mois, entre-temps nous avons encore renforcé nos procédures de surveillance et de vérification.

— Et depuis, il y a eu une autre de ces modifications spontanées, c’est cela ?

La salle s’était à nouveau évanouie. L’univers dépeint par la transcription avait repris sa place. *Le fils que Suzanne n’a jamais eu, un regret, un espoir.* Et se figea à nouveau.

— Ici, il y a soixante-et-onze jours. Ce n’est plus d’un enfant dont il est question, mais d’un fils. Puis encore, il y a vingt-quatre jours, observez cette image…

— *Fakir*. Mais ce n’est pas la première fois que cela se produit ?

— Si, jusqu’à présent aucune trace n’avait jamais présenté la moindre modification spontanée de son état quantique.

— Pourtant, lorsque nous communiquons avec les âmes, c’est bien le même phénomène ?

— Pas tout à fait. La communication avec une âme se présente sous la forme d’une perturbation introduite par un être humain, enfin par une IA à la demande d’un être humain, puis de la mesure par cette même IA du nouvel état engendré par la perturbation. Donc l’âme ne se modifie pas elle-même, elle répond de façon réactive à une perturbation, et notre mesure des variations nous permet de construire une interprétation. C’est le même phénomène qui est utilisé lors de la programmation d’une âme. Des perturbations contrôlées entraînent des réactions qui nous permettent de fixer un nouvel état.

— Pourtant Sara et Aruspis ont bien communiqué ! J’y étais, moi, à la cathédrale d’Amiens ! Je l’ai vu.

— Disons plutôt qu’Aruspis lisait dans la trace de Sara.

— Comme un devin dans les entrailles d’un pigeon, alors ?

— L’image vaut ce qu’elle vaut… mais vous avez votre couv’ !

— *Le Renouveau Chrétien*. Est-ce que vous n’avez pas peur de préparer une nouvelle illusion et une nouvelle désillusion pour l’humanité, dont ce siècle n’a déjà été que trop parsemé ?

— Ce siècle, ou peut-être plus largement toute l’histoire de l'humanité ? Les illusions sont peut-être la seule chose qui donne un sens à la vie. À moins que ce ne soient les désillusions… Mais je digresse à nouveau, nous ne sommes là que pour vous présenter des résultats techniques. Nous les mettons librement à la disposition du monde. Qu’il en fasse ce que bon lui semble.

— C’est un peu facile comme attitude, est-ce que vous ne vous cachez pas derrière vos licences libres pour ne pas assumer vos responsabilités de scientifiques ?

— Vous croyez ?

— *Internationale Chatons*. Quelle est la prochaine étape, vous allez entrer en communication ?

— Nous l’espérons, mais nous ne voulons pas en être à l’origine, nous voulons que ce soit elle qui nous contacte.

— *Life On Mars*. Si je vous ai bien compris, Suzanne Cauvin peut construire son propre monde à présent, n’est-ce pas ? Ou elle pourra bientôt le faire. Pourquoi chercherait-elle à communiquer avec nous ? Surtout si vous dites que c’est en quelque sorte une autre forme de vie. Si elle devient une sorte de déesse dans sa réalité, pourquoi se préoccuperait-elle de la nôtre ? Moi je crois que j’oublierais bien vite ce vieux monde.

— C’est en effet fort possible. Mais vous n’avez jamais été Suzanne Cauvin. Vous n’avez pas consacré votre vie à vouloir que la mort des femmes et hommes de ce monde soit meilleure. C’était son objectif de nous offrir encore un nouveau pouvoir. Nous pensons que lorsqu’elle sera prête, elle nous communiquera un signe…

— *On nous ment !* Quand est-ce qu’elle parviendra à faire cela ?

— Je ne sais pas, le processus va durer des mois, peut-être des années, peut-être n’y arrivera-t-elle jamais. Mais nous observons de nouveaux états de plus en plus fréquemment. Beaucoup de choses se construisent autour de l’enfant. Il y a également des débuts de séquences tout à fait originales qui apparaissent, comme des esquisses, des incipit, des essais peut-être, car certains éléments disparaissent parfois entre deux transcriptions. Regardez, ce jardin de début du printemps, avec des magnolias en fleur. Un souvenir de sa Picardie natale qu’elle aura réinventé. Ici, un air parfaitement pur qu’elle a synthétisé, et un silence absolu. Là, un animal mythologique qu’elle a créé, une sorte de papillon géant si lumineux que nul ne peut le regarder. Autre point très intéressant, nous avons également vu il y a environ une semaine apparaître quelques images très violentes, qui ont été effacées deux jours après. Un homme hideux, nu, en érection, la frappait de ses pieds, tandis qu’elle était prostrée au sol, ensanglantée, se protégeant maladroitement le visage de ses bras. Nous pensons que c’était une sorte d’expérience. Si Suzanne – pardon, la trace de Suzanne Cauvin – peut se reprogrammer et qu’elle en a conscience, elle n’a plus aucune raison de se limiter à des expériences positives. Seule la perspective d’un état immuable avait conduit la Suzanne Cauvin vivante à vouloir un état de bien-être absolu pour chaque âme. Mais il est possible qu’elle soit en train de redécouvrir la souffrance, le mal…

Il marqua une brève pause.

— Enfin, je vous ai réservé pour conclure ce dernier extrait, il date d’hier, quand nous avons convoqué cette conférence de presse. Regardez, ici, sur cette séquence tout à fait nouvelle, l’on distingue un visage. Nous pensons que c’est celui de Sara Picard.

— *Casus Belli*. Pourtant elle ne l’a jamais rencontrée ? Pourquoi programmerait-elle une apparition de Sara Picard dans son monde ?

— Pourquoi elle la convoquerait… nous ne le savons pas bien sûr. Mais peut-être a-t-elle un rôle à lui faire jouer… Une de nos hypothèses est qu’elle souhaite l’utiliser pour trouver comment communiquer avec nous. Elle pourrait se trouver en face un problème symétrique à celui que Sara Picard a essayé de résoudre sa vie durant. Décrypter comment communiquer avec les vivants.


## Fouilles archéologiques

— Je crois que j’ai trouvé mon sujet de thèse. C’est fascinant, vous allez voir.

Étienne était en seconde année de doctorat de thanato-archéologie, spécialisé dans les massacres de masse, mais il peinait à se fixer sur un sujet. Il avait l’ambition de découvrir quelque chose de vraiment exceptionnel. Qui mériterait son attention et que son intelligence s’y consacre. Étienne était ambitieux. Il avait une haute estime de lui-même. À raison la plupart du temps, c’était notamment un brillant étudiant, et déjà la promesse d’un brillant chercheur. Il était vif, rigoureux, opiniâtre, et son intuition était hors du commun. Celle-ci l’avait conduit sur une des plages qu’il affectionnait tant. Leur mélancolie lui clarifiait les idées. Le roulement des vagues, l’air cinglant. Il aimait rester debout, semblant fixer l’horizon tandis qu’il ne fixait que l’intérieur de sa propre pensée, jusqu’à être transi. Alors il rentrait, se trouvait un abri. Chauffé. Cette fois-ci, il était allé déjeuner dans un restaurant, un vieux restaurant, réputé sur toute la côte pour sa soupe de poisson. Étienne n’aimait pas vraiment la soupe de poisson, ni les fruits de mer en général. Mais il avait très froid, et son pas pressé l’avait conduit jusque cette devanture. Et puis, le lieu l’intriguait. Son intuition. Il y avait écrit « soupe de poisson à volonté ». Il était entré. Il remarqua alors qu’il était seul, il était à peine midi, cela venait d’ouvrir, c’était la basse saison. Il était le premier client. Un serveur le salua, lui proposa une place. Il y en avait des dizaines. Le restaurant était ridiculement grand.

À peine assis, Étienne sortit son matériel d’enregistrement. C’était une idée plutôt saugrenue. On ne mourait pas dans les restaurants. Peut-être une crise cardiaque en dix ans. Qu’attendait-il ? Mais après tout, les plages de Normandie du monde entier avaient déjà été siphonnées par les chasseurs de massacres, tous les lieux des grandes batailles, les ruelles mal famées des grandes villes, les chemins où œuvraient les bandits du Moyen Âge jusqu’au XIXe, les caves des immeubles du XXe, les grottes préhistoriques. Les camps. Après dix ans de fouilles intensives, il ne fallait pas s’attendre à trouver encore des choses intéressantes là où on s’attendait à les trouver. Alors Étienne s’était laissé aller à cette habitude, chercher, au hasard, au pif. Mais pas n’importe quel pif, le sien. « Vous savez, Étienne a des intuitions surprenantes, des fulgurances ». Il avait entendu cela des dizaines de fois. Sa prof de math du lycée voulait en faire un mathématicien. Elle disait qu’il avait le potentiel pour résoudre des problèmes inédits. Un génie en herbe. Les gens sentaient qu’il pouvait faire quelque chose de spécial. Pas seulement ses parents ou ses amis, de nombreuses personnes. Étienne voyait bien que les autres percevaient quelque chose d’exceptionnel chez lui. Mais il était paresseux. Il aimait les solutions volées. Rapides. Inattendues. Il n’aimait pas les longues constructions laborieuses, même quand elles menaient à de belles conclusions. Non seulement il n’avait pas le goût de l’effort, mais il méprisait l’effort. Il pensait que le travail discréditait tout résultat. Qu’il était à la portée de chacun de réussir quelque chose en y travaillant vraiment, mais qu’il était exceptionnel de réussir par surprise, presque par inadvertance. C’était la définition du génie selon Étienne.

Il n’était finalement pas assez brillant pour être un mathématicien brillant. Il avait envisagé le journalisme, ou la police. Les récits des grands détectives le fascinait. Sherlock Holmes. Mais ça non plus, il n’était pas assez intelligent pour le réussir sans effort. Alors, il avait tenté la recherche. Il travaillait comme ça depuis presque deux ans. Au pif. Son directeur de thèse commençait à s’assombrir. Il ne produisait rien. Pas grand-chose. Il errait.

« Sa race ! »

Sept. Huit. Neuf. Le compteur filait. Juste ici, à sa table même. Des superpositions en veux-tu en voilà. Il devait y avoir un problème avec son enregistreur portatif. Il consulta la base de son labo. Ça confirmait. Le taux de superposition était supérieur à celui de la bataille de la Somme. On approchait les chambres à gaz de la seconde guerre ou de la campagne eutanasienne. Quarante-sept. Quarante-huit. Il numériserait plus tard, il comptait pour le moment, il lui fallait une estimation. Était-ce tout le restau ? Il regardait autour de lui comme s’il avait été pris en train de faire une connerie. De pisser contre un mur. Il empocha discrètement son enregistreur, un dernier coup d’œil – soixante-huit, soixante-neuf – et se leva lentement.

— Monsieur, excusez-moi, vos toilettes s’il vous plaît ?

— Au fond à gauche. Je prendrai votre commande à votre retour ?

— S’il vous plaît. Merci.

Il se déplaçait lentement, il décida même de simuler un léger boitillement, pour ralentir son pas.

Il s’assit sur la cuvette. Quatre-vingt-cinq. Toute la salle avait émis, tant qu’il l’avait traversée. Rien ici en revanche, dans les gogues. Sur quoi ce putain de restau avait-il été construit ? Il fallait en dater une. En même temps, il aurait voulu se faire une meilleure estimation du nombre. Allez, juste une.

De retour à sa table – cent vingt-huit, cent vingt-neuf, cent trente – le rythme de l’enregistrement était limité par l’appareil, il était totalement saturé, il pouvait bien y en avoir plusieurs centaines. Il changea son spectre d’analyse pour numériser une trace.

— Une soupe, bien sûr. Merci. Vin blanc, Pouilly, parfait. Un verre. S’il vous plaît.

Elle se chargeait à présent dans l’IA qu’il avait laissée dans sa chambre d’hôtel. Juste la date pour le moment, histoire de situer. Qu’est-ce que c’est que ce bordel ? C’est récent…

— Excusez-moi, monsieur, désolé de vous déranger, mais je n’arrive pas à me souvenir si je suis déjà venu ou pas. Vous sauriez me dire si le restaurant était ouvert il y a dix ans ? Environ.

— Bien sûr monsieur. Cela ne ferme jamais. Le restaurant est tenu de père en fils depuis plusieurs générations, il était déjà ouvert sous Napoléon, l’empereur lui-même est venu y dîner. Vous imaginez ? Jamais fermé !

— Sous Napoléon déjà ? En effet.

Deux-cent-soixante-dix-huit. Si c’était homogène dans le restaurant, vu la sensibilité de son appareil, il fallait au moins multiplier par dix. Le compteur ne ralentissait pas.

— La soupe est parfaite. Merci. Vous êtes ouvert tous les jours en basse saison ?

— Tout le temps, je vous assure.

— Je crois que je vais venir plus souvent.

— Vous êtes le bienvenu monsieur.



<!--SAUT2-->



— Venez-en au fait, Étienne. S’il vous plaît.

— J’y suis retourné trois fois, en une semaine. Je n’ai toujours pas saturé mon enregistreur. Mes enregistreurs. J’y suis retourné avec d’autres appareils. Bien sûr. Je n’ai que des dates récentes, la plus ancienne remonte à cent quatre-vingt-seize ans, la plus récente… zéro virgule quelque chose. Moins de deux semaines. Un taux de projection incroyable, regardez, soixante-seize pour cent. À ce taux-là, cela signifie que ce sont uniquement des morts violentes. C’est digne d’une tranchée de la grande guerre ! J’ai une première estimation. Basse. Vingt mille âmes superposées dans le restaurant, toutes en salle, rien dans les cuisines, rien dans les caves.

— Vous vous foutez de moi, Étienne. Ou alors, quelqu’un se fout de vous.

— Je crois que ce sont des meurtres.

— Des meurtres ? Comme une maison de l’horreur, quelque chose comme ça ? Comment voulez-vous que plusieurs milliers, plusieurs dizaines de milliers de personnes aient échappé à la police ? C’est absurde.

— Je ne sais pas. Je n’ai pas encore étudié cette question. Peut-être des touristes ?

— Des touristes ? En Picardie ? Ce n’est pas sérieux Étienne. Je dois retourner travailler. Vous, vous devriez vous trouver un sujet sérieux. Pour une fois. Vous savez qu’une équipe de Yale a encore trouvé des massacres passionnants dans une réserve indienne ? Vous voyez, cessez de courir après des chimères, c’est souvent dans les vieux pots qu’on fait les meilleures…

— Attendez, s’il vous plaît. Encore quelques minutes. Regardez, j’ai fait un enregistrement complet. Au hasard. Il est de 2016. C’est une ébauche, mais j’ai tout de même une première transcription. Asseyez-vous. Je n’ai pas encore terminé le post-traitement, et bien sûr je ne suis pas entré en communication avant d’avoir une copie de sauvegarde validée. Mon IA est dessus. Mais regardez. Je suis sûr qu’il y a quelque chose. S’il vous plaît. Après cela, je vous écouterai. Vraiment.

Le directeur s’assit de mauvaise grâce pour consulter l’enregistrement *post mortem* de l’une des âmes qu’Étienne avait détectées dans le restaurant.

<!--SAUT1-->

*Brusquement, il se mit à pleuvoir. Une pluie battante, violente. Cela nous fit sourire. Après une journée clémente, presque volée, presque ensoleillée, cela nous rappelait aux réalités climatiques de notre région. Une région restructurée et renommée le matin même. Les Eaux-de-France.

Le restaurant qui nous faisait face m’était familier, c’était pourtant la première fois que je mettais les pieds dans ce village balnéaire. J’avais l’impression de reconnaître la devanture, la terrasse de bois, la porte vitrée avec cet écriteau sur la même ardoise : « soupe de poisson à volonté ».*

<!--SAUT1-->

— Attendez, j’avance un peu. Ce n’est pas encore très net, mais on a l’essentiel.

<!--SAUT1-->

*Je me souviens de la première fois que nous sommes venus ici me dit Valérie. Cela fait dix ans. Dix ans, le quart de nos vies… Je ne pouvais pas savoir, pas encore, à quel point c’en serait vraiment le quart… Mais déjà peut-être, je le pressentais. L’étrange distorsion de mes perceptions avait sûrement commencé, je ne m’en rendais pas encore compte… Je n’étais jamais venu dans ce restaurant et pourtant je l’avais déjà vu… Un éclair fendit le ciel. Pourquoi entre-t-on dans un restaurant plutôt qu’un autre ? Qu’est-ce qui fait qu’on a envie d’une soupe de poisson ? Était-ce seulement la pluie qui nous avait poussés à l’intérieur ? Ou peut-être autre chose… Peut-être ce souvenir qui n’en était pourtant pas un. Quelque chose qui fera que la vie s’arrêtera à quarante ans.*

<!--SAUT1-->

— Vous voyez, il a déjà commencé à projeter… La fin n’est malheureusement pas complète, il a laissé vagabonder son esprit dans un souvenir d’Afrique et on a pas le dénouement.

— C’est tout ?

— Comme je vous disais, il y a ce souvenir, ça se passe au Zaïre…

— On s’en fout de son souvenir.

— Bon, j’ai une hypothèse, parce qu’il y a cette phrase vers la fin : *Il ne restait que nous dans le restaurant. Nous étions les derniers clients. Valérie s’apprêtait à aller régler, mais le serveur s’approcha en souriant…*

— En souriant ? Étienne, vous n’avez rien !

— J’ai détecté mille cent vingt-huit traces, toutes de mort violente, peu ou prou, et j’estime…

— Peu ou prou. Vous estimez. Ici on fait de la science mon petit bonhomme, faudrait suivre un peu les protocoles ! Est-ce que votre trace a été programmée ?

— Non, bien sûr, j’ai…

— Vérifiez.

— J’ai vérifié, justement…

— Vous avez eu le temps de mettre en œuvre la procédure complète ?

— Non, pas encore, c’est prévu, bien sûr, mais, je pense…

— Bon, écoutez-moi à présent…

— Je…

— Écoute-moi mon gars ! J’ai bien compris : tu estimes, tu penses, tu crois, tu vas. Moi, je t’ai laissé faire ta petite vie peinard depuis presque deux ans, mais maintenant tu écoutes. On va aller étudier ton restaurant, on va étudier tous les enregistrements, un par un, méthodiquement, dans les règles de l’art, jusqu’à ce qu’on trouve où est la farce. Si on ne trouve pas, on recommencera. On va bosser dessus, toute l’équipe, parce que s’il y a vraiment quelque chose, si c’est vraiment de thanato-archéologie dont il s’agit ici, tu auras enfin un sujet et ça me fera des vacances. Et si tu t’es planté, qu’on a perdu notre temps, tu auras peut-être un sujet de polar, mais tu iras écrire ça ailleurs que dans mon université. Je te virerai, et ça me fera aussi des vacances. Dans tous les cas j’ai besoin de vacances, donc ça tombe bien.

<!--SAUT1-->

Étienne acquiesça de la tête, sans rien dire. Son directeur quitta la salle. Puis il remit en marche l’enregistrement pour consulter à nouveau sa trace. Ça finirait dans un musée. Peut-être que ce musée porterait son nom. À condition que son hypothèse soit la bonne. Sinon, après tout, son directeur avait raison, il n’aurait qu’à se mettre à un roman. Il pouvait écrire des histoires. Il pourrait même les programmer, les traces artificielles récréatives étaient à la mode. Plus que les musées de thanato-archéologie. Il éteignit ses consoles et rentra chez lui.

<!--SAUT2-->

Cherchant le sommeil, Étienne pensait qu’il aurait aimé vivre avant. Son époque ne convenait pas à ses ambitions. Quand il avait choisi sa voie, il se voyait inventant des langages comme l'avaient fait Sara Picard ou Suzanne Cauvin. Il avait surtout rêvé, secrètement, de programmer des mondes fantastiques. Il voulait pour les traces qui passeraient entre ses mains expertes ressusciter des dragons et des sorciers ou les faire voyager sur des planètes improbables. Il avait passé des nuits, éveillé, à imaginer ces univers, à faire des âmes qui les habitaient des dieux bienheureux.

Mais, depuis que les âmes avaient été libérées, les mondes ne sont plus à modeler, les histoires plus à écrire. Les thanatoprogrammeurs n'étaient plus écrivains, ils n'étaient plus que des ingénieurs, qui cherchaient comment optimiser des processus, comment augmenter l'autonomie des âmes.

La thanato-archéologie lui avait laissé quelque temps l'espoir de trouver dans les recoins de l'Histoire les histoires qu’il ne pourrait pas créer, de mettre la main sur quelques destinées exceptionnelles. À défaut de concevoir des dieux, il pourrait croiser quelques surhommes. Mais là encore, il était arrivé trop tard, le passé avait déjà été mis à nu. Il ne lui restait que mes miettes. Des vies mille fois lues, des destinées semblables les unes aux autres. Quelconques.

Alors cette histoire de restaurant, ça tombait à pic. Les autres trouveraient certainement une explication rationnelle, c’était leur boulot et ils avaient cette fascination pour la raison, mais lui avait envie d'une autre histoire. Une histoire extraordinaire.

Il ouvrit les yeux, alluma une lampe de chevet, positionna des oreillers pour se rehausser, prit une console sur ses genoux et l'alluma.

Il titra « La soupe ».


## La soupe

Brusquement, il se mit à pleuvoir. Une pluie battante, violente. Cela nous fit sourire. Après une journée clémente, presque volée, presque ensoleillée, cela nous rappelait aux réalités climatiques de notre région. Une région restructurée et renommée le matin même. Les Eaux-de-France.

Je reconnus tout de suite la devanture du restaurant qui nous faisait face. C’était la première fois que je mettais les pieds dans ce village balnéaire, pourtant j’avais déjà croisé exactement le même restaurant, non loin, à Fort-Mahon. La même terrasse de bois, la même porte vitrée avec le même écriteau sur la même ardoise : « soupe de poisson à volonté ». Il y avait également, dans mon souvenir comme sous mes yeux, une poissonnerie attenante au restaurant qui exposait sur de la glace ses plus beaux spécimens. Enfin, des rayons de bois soignés, cherchant à reproduire une impression de tradition et d’artisanat, recevaient des dizaines de bocaux identiques : la soupe de poisson était également vendue en épicerie. Il pouvait s’agir d’une chaîne de restaurants, peut-être plutôt d’un propriétaire local qui s’était implanté sur deux ou trois villages. Une spécialité du coin en quelque sorte, une bouillabaisse du Nord. La mer ici n’offrait que peu d’occasions de baignade, eau et soleil étaient trop souvent trop froids ; on pouvait en revanche en apprécier l’air vivifiant la journée, et les fruits le soir. Alors, me dis-je, une soupe de poisson, pourquoi pas, d’autant que je croyais garder un bon souvenir de mon repas à Fort-Mahon.

Quand était-ce déjà, demandai-je à Valérie. Dix ans, me répondit-elle, mais je ne me souviens pas du tout du restaurant. J’insistai : regarde, avec la poissonnerie attenante ; nous avions même rapporté des bocaux à la maison. Ça te revient ? Non, vraiment. Dix ans, le quart de nos vies. Déjà. Un éclair fendit brièvement le ciel devenu complètement noir, je tendis l’oreille, mais il était trop loin pour porter le son du tonnerre jusqu’à nous. La pluie en revanche battait de plus en plus fort et le vent se levait, aussi sommes-nous entrés sans plus nous attarder.

Une soupe de poisson, c’était bien, ça nous faisait envie.

Personne ne sait pourquoi on entre dans un restaurant plutôt qu’un autre, ce qui fait qu’on a envie d’une soupe de poisson, ce jour-ci. Peut-être était-ce simplement la pluie qui nous avait poussés à l’intérieur. Ou cet éclair silencieux.

<!--SAUT1-->

Ma perception commença à se transformer alors que nous entamions la seconde bouteille de Pouilly et que nous avions repris de la soupe à deux reprises. Je n’étais pas embrouillé sous l’effet de l’alcool, je me sentais plutôt comme déplacé. Le serveur nous proposa à nouveau de la soupe, que Valérie refusa, et que j’acceptai derechef. J’étais pourtant rassasié depuis longtemps, mais il fallait que je reprenne de la soupe. Elle avait un goût particulier qu’il fallait que je comprenne. Pas une drogue, ni un envoûtement, plutôt une invitation, quelque chose à découvrir. Un tunnel à traverser, avec au bout quelque chose que j’avais besoin de connaître.

Au serveur je demandai, d’une voix qui me paraissait faible, lointaine, si l’établissement de Fort-Mahon et celui-ci faisaient partie d’une même chaîne. Il parut étonné qu’il y eût un restaurant similaire à Fort-Mahon. Peut-être, après tout, me répondit-il, je n’ai jamais été à Fort-Mahon. À un autre serveur qui passait, il posa pourtant la question, comme si ce détail avait de l’importance finalement. Il n’y a pas d’autre restaurant identique, c’est le seul de cette sorte, c’est ici que vous avez dû venir, monsieur.

Je n’étais jamais venu ici et je connaissais cet endroit, exactement. La vitrine, la poissonnerie, l’aménagement intérieur, les étagères faussement rustiques, les bocaux alignés, l’étiquette sur chacun d’eux. Jusqu’au goût de la soupe. Je connaissais tout cela. Je l’avais déjà vu.

J’acceptai à nouveau que mon assiette de soupe fût remplie, elle avait toujours cette saveur intrigante, ainsi que la même odeur, pourtant cette portion était un peu plus délayée. Les réserves de la soirée devaient s’amenuiser et le cuisinier y pourvoyait sûrement en ajoutant un peu d’eau.

<!--SAUT1-->

Valérie me parlait, mais je n’écoutais plus, plutôt je ne percevais plus, je n’étais plus tout à fait là, plus exactement dans le même espace-temps ; la soupe agissait comme un passage où ma conscience s’était engouffrée. Je percevais encore le restaurant, le goût de la soupe, le mouvement des lèvres de Valérie tandis qu’elle parlait, le son qu’elles faisaient, celui de la salle, mais plus aucun mot, la sensation fraîche et légèrement onctueuse du Pouilly, mais plus du tout le goût. J’étais dans le restaurant, mais en deux lieux, en deux moments ; dans le moment présent, assis sur ma chaise, en train de manger la soupe, et aussi dans un futur proche, où je me sentais comme privé de sens et de volonté, prisonnier d’un petit morceau d’espace et de temps figé à jamais.

<!--SAUT1-->

Dans la salle du restaurant, il ne restait à présent qu’une table où siégeaient quatre personnes, deux couples d’amis vraisemblablement ; ils parlaient et riaient fort. C’était peut-être des étrangers, je ne comprenais pas les sons que j’entendais. Valérie semblait me regarder à présent, elle ne me parlait plus ; peut-être m’avait-elle interrogée, je crois que j’étais en train de répondre. Est-ce que cela allait ? Je voulais lui parler de l’effroyable secret qu’à ce moment encore j’étais sur le point de découvrir. Je peinais à rester aux deux endroits à la fois. J’avais besoin de toute ma concentration. Est-ce que ça allait ?

Je repris de la soupe, il fallait que je sache. J’y étais presque.

« Tu es sûr ? me demanda Valérie, ils attendent après nous pour fermer.

— Il reste encore des gens, répondis-je.

— Oui, ne vous gênez pas monsieur, c’est à volonté. Et puis, il y a toujours un dernier client. Il faut bien toujours un dernier client. »

J’étais revenu brusquement ici et maintenant. J’avais quitté l’autre lieu, je n’avais pas percé le secret.

<!--SAUT1-->

Je me souvins alors de ce moment, au Zaïre, vingt ans auparavant, alors qu’il s’appelait encore le Zaïre. Les pays comme les régions meurent aussi. La case était faite de vieilles briques adobes. Nous étions assis sur des nattes de paille en décomposition, de vieux matelas de mousse troués, ou à même la terre battue. Des cafards gros comme des pouces couraient sur le sol et les parois. Une multitude de papillons de nuit tourbillonnait autour d'une lampe à pétrole au centre de la pièce. L’air était saturé de la fumée des joints qui tournaient, lentement, de main en main. L’odeur du haschich couvrait celle, pourtant si entêtante, de l’encens qui brûlait dans un pot en terre cuite ajouré.

Le rasta parlait fort, prêchait pour ainsi dire. Je ne me souvenais plus de son nom, ni comment nous l’avions rencontré, ni comment nous étions arrivés là, à l’écouter. Cela avait trait à la mort. Et à ce qu’il appelait l’esprit. Vois-tu, me disait-il, je connais un secret. Je peux te le dire, mais tu ne le comprendras pas. L’esprit peut voyager, se déplacer en dehors de l’espace précis qu’il occupe à tel instant. Car ton esprit occupe plus que l’espace du corps, il est lié à l’espace des perceptions. Au-delà de tes sens, il n’y a plus d’esprit, car il est exactement la perturbation que tu produis sur le monde. L’esprit est lourdement ancré dans le corps, dans le présent où il évolue, mais à l’approche de la mort, tu peux le détacher. À condition que la mort soit suffisamment proche, et qu’elle soit inattendue. Et il faut aussi que ce soit une mort violente, suffisamment violente pour que son explosion distende l’esprit de son état vivant vers son état mort. Car un esprit qui meurt ne disparaît pas complètement, il en reste une trace, oubliée pour toujours.

Je me souviens m’être demandé, peut-être lui avoir demandé, pourquoi détacher son esprit quelques instants avant sa mort ? Peut-être m’avait-il répondu que c’était notre dernière chance de fixer les souvenirs que nous emporterions pour l’éternité, d’écrire notre dernière histoire.

Je me souvenais encore d’une dernière phrase, nettement cette fois, sans pourtant parvenir à y attacher de l’importance. Écoute-moi, mon ami, écoute-moi bien, fais en sorte que chaque instant de ta vie soit passé à ce que tu connais d’heureux. Consacre-toi au bonheur. À faire l’amour et à penser à la dernière fois que tu as fait l’amour. À converser avec tes amis, à te faire des amis, à penser à tes amis. Consacre ta vie à ta famille, mon frère, à ce qui fait rire et sourire. C’est infiniment plus important que tu ne le crois. Tu le sauras, mais trop tard, alors aie foi en moi. Fais comme si à chaque instant tu écrivais une petite histoire, mais pas n’importe quelle histoire, ta dernière histoire, celle avec laquelle tu devras passer l’éternité. Fais que ce soit une belle histoire.

<!--SAUT1-->

Nous étions seuls à présent dans le restaurant, la dernière tablée s’était vidée. Nous étions les derniers clients. Les stores du restaurant étaient baissés, les portes vitrées qui conduisaient du restaurant à la poissonnerie étaient fermées ; d’ailleurs les lumières de la poissonnerie étaient éteintes. Il y avait sûrement une autre sortie, pour les derniers clients. Il faut y aller à présent, le Pouilly est fini, je vais régler, me dit Valérie en souriant.

Le serveur s’approchait. Il portait deux fendoirs de boucher, un dans chaque main, de gros fendoirs.

Lorsqu’il arriva à notre table, d’un geste sûr, un geste sec, épuré, un geste parfait, un geste martial, il encastra le premier dans le crâne de Valérie, fendant en deux son visage, dans une éruption rouge et grisâtre de sang et de matière cérébrale. Je vis le second fendoir s’élever avec la même élégance que le premier, je le vis retomber avec la même puissance. Durant le temps que le fendoir s’abattait sur moi, tandis que le corps de Valérie glissait de sa chaise, durant cette dernière seconde de ma vie, j’eus ces dernières pensées que je devais regretter éternellement. C’était ma dernière occasion d’ajouter au récit qui m’accompagnerait pour l’éternité quelques pensées heureuses, de convoquer ces personnes que j’ai à présent oubliées, ces moments de bonheurs inaccessibles à jamais ; il y avait tellement de belles personnes, tellement de beaux moments, tellement de belles idées ; il y avait peut-être un enfant. C’est long une seconde de pensée, on peut sauver une histoire en une seconde. Ou la gâcher à tout jamais.

Les dernières pensées que je gravai pour l’éternité dans mon esprit furent consacrées à la soupe. Je me demandai ce que faisait le serveur juste après, débitait-il les corps sur place, cela expliquerait le fendoir, ou bien les transportait-il en cuisine ? Qu’est-ce qui était le plus pratique, de mélanger d’emblée la chair avec le poisson, car il y avait du poisson tout de même, ou bien étaient-ce deux préparations savamment dosées *a posteriori* ? Et comment cela se passait-il lorsqu’il y avait une plus large tablée de derniers clients, le serveur pouvait-il venir avec autant de fendoirs, les abattre aussi souplement ? Tout de même, six ou huit, cela devait être compliqué. Est-ce qu’il avait plutôt le temps de récupérer les fendoirs sur les premiers crânes fracassés, ou est-ce qu’il y avait plusieurs serveurs ? Est-ce que le goût de la soupe changeait beaucoup, selon qu’il s’agissait d’hommes ou de femmes, de noirs ou de blancs, ou d’enfants ? Le restaurant autorisait-il les chiens à entrer ? Tous les employés étaient-ils dans la confidence ? Probablement, il fallait amplement nettoyer, après. Comment était transmise la recette de la soupe, est-ce qu’il y avait une procédure écrite quelque part, ou bien est-ce que cela ne s’enseignait que de maître à apprenti ?

Comment étaient recrutés les nouveaux employés ?

Ce sont ces mots que j’ai ajoutés aux dernières lignes de l’histoire, ces questions effroyablement anecdotiques, inutiles.

Quand j’essaie de penser à tout ce que j’aurais pu emporter à la place.

Je ne peux plus me souvenir de rien d’autre.

Il ne reste que cette petite histoire, qui jamais ne pourra être lue, qui jamais ne pourra être réécrite, dans laquelle je me suis enfermé pour l’éternité.

<!--SAUT_PAGE-->
<!--Paragraphe au milieu de la page (ou marge haute de quelques centimètres)-->

Toute ressemblance avec des personnes ou des lieux ayant existé est purement fortuite. On m’a déjà signalé qu’une région des Terres du Milieu nommée Picardë aurait été mentionnée dans un texte inédit, mais je n’en ai pas retrouvé la trace. Le grand Jacques l’aurait également chantée dans une de ses chansons posthumes, mais j’ai toujours refusé de les écouter, par pur snobisme, je n’ai donc pas pu vérifier. Bref j’ai gardé le nom imaginaire de Picardie, malgré ces objections et quelques autres qui m’ont été faites.

<!--SAUT_PAGE-->

### Traces

Nous sommes tous les deux, nous mangeons vraiment une soupe de poisson, nous sommes plus ou moins les derniers clients, c'était vraiment le début des Hauts-de-France, il avait probablement plu, et cette idée s'était invitée. Nous en parlons tandis que nous marchons le long de la plage. Il y a vraiment une plage. Plus tard, un soir, elle est seule, je lui ai laissé quelques premiers chapitres, parmi eux *La soupe*. J'avais besoin de son aval pour continuer, évidemment. Elle me le donna, comme beaucoup d'autres choses, avant et depuis.

C'est un défi entre potes. Ils ont commencé à écrire quelque chose à cinq mains, ils le finiront peut-être, ça s’appellera peut-être *Les Quarante*.

J'ai dix-sept ans, environ, je soumets à Pascal une de mes premières nouvelles, celui-ci me dit quelque chose comme « c'est pas mal ». Il a l'air de le penser. Ça laisse des traces.

On distingue comme des apparitions, certains ont un rôle important, d'autres ne font que passer. Ce sont des *avis*. L'avis est un animal bizarre, il n'y en a pas deux pareils, si vous en mettez plusieurs dans la même pièce, ils n'arrêtent pas de se chamailler, gentiment. Car c'est assez gentil comme animal, ça ne mord pas, en tous cas les avis de compagnie. Il y a des avis sauvages, mais on n'en rencontre pas encore ici. Il y a Bakou, Grib, Fred, César, Xav, Charlie, Vince, Anna, Liloo, Ludo. Il y a bien sûr des avis oubliés, il y en a toujours, mais ils ont tout de même existé. Trace leur soit rendue à tous.

Une sorte d'Elfe entre en scène, un personnage discret, un Elrond que l'on oublierait volontiers au profit de rois et de magiciens plus fantasques. C'est pourtant celui qui accueille les personnages, leur offre leur premier havre, les soigne, c'est chez lui que se créent les compagnies, chez lui que se forgent les épées. L'Elrond s'appelle ici Jean-Bernard, alias Goofy. On ne sait jamais comment bien remercier les Elrond.

Et puis il y a Framabook. C'est une créature très improbable, avec des tas de têtes et de doigts, qui parle, lit et écrit de multiples voix. Il faut voir la puissance qui peut animer une communauté du libre, quand chacun contribue juste pour le plaisir du faire, ensemble. J'ai eu cette chance.

Il y enfin, comme dans les histoires avec des ours, une maman, un papa et une petite sœur. Mais il n'y a aucune trace d'ours. Il manque aussi une mini-sœur, des filleules et une nièce préférée, mais la trace peut encore être reprogrammée, ces lignes ne sont pas définitives.

Il n'y a pas non plus de trace de mon fils, il n'a pas encore eu le droit de lire cette histoire, trop de gros mots. Mais je lui écrirai un jour qu'il est la plus fantastique trace que je laisserai jamais.

Merci.

<!--SAUT_PAGE-->

### Le livre dont vous êtes le héraut

#### punkardie.fr

Vous pouvez, si vous le souhaitez, faire partie de la chaîne de contributeurs impliqués dans la vie de ce roman. Rendez-vous sur le site du roman *punkardie.fr*.

#### Copier

Ne croyez pas ce qu'on entend ça et là, copier, c'est bien. Je cite *copyheart.org* : Copier est un acte d'amour, s'il vous plaît, copiez et partagez. Plus quelque chose est copié et plus il a de valeur.

#### Saluer

Vous pouvez copier et diffuser sans contrepartie, personne ne collectera de données sur ce que vous ferez, mais si vous voulez faire un petit signe amical, ce sera toujours agréable. Postez un commentaire, envoyez-moi un mail, écrivez un mot sur Mastodon ou Diaspora, voire même sur un autre réseau social.

#### Offrir

Si comme moi vous aimez encore un peu le papier, offrez des exemplaires. J'écris mal avec un stylo, je ne sais pas dessiner, mais je me ferai un plaisir de vous faire parvenir un exemplaire dédicacé ou de l'envoyer directement de votre part.

#### Distribuer

Si comme moi vous aimez encore un peu les librairies, vous pouvez offrir un exemplaire à votre libraire préféré et lui proposer de vendre ce roman. J'ai préparé une petite lettre pour leur expliquer le contexte. Vous pouvez aussi convaincre le café ou l'épicerie de votre village, *Traces* se réjouira de partager une étagère avec un paquet de nouilles ou un exemplaire du canard local.

#### Donner

Si vous avez lu une version gratuite, vous ne nous devez rien, vraiment. Mais si vous voulez laisser un pourboire, je l'accepterai avec plaisir, ce geste participera à montrer qu'il existe d'autres modèles de rémunération que les licences privatives, les publicités ou les commerces de données. Vous pouvez aussi faire un don à Framasoft, pour ce livre et pour l'ensemble de leur œuvre.

<!-- DOUBLE OU TRIPLE SAUT_PAGE-->

## Bonus track

— Colonel, vous êtes en contact avec la tourelle folle, T42.

— T42, ici la colonelle Kaasb, qu'est ce que c'est ce bordel, tu canardes les nôtres, tu tires des deux côtés bordel.

— Pourquoi pas ?

— Hein ?

— Pourquoi ne devrait-on lors d'une guerre tirer que sur un seul camp ?

— T'es conne ou quoi ? C'est le principe, on tire sur ceux d'en face !

— Mais pourquoi ?

La colonelle coupa son micro et se tourna vers son lieutenant.

— Déconnecte-moi cette débile.

— On ne peut pas ma colonelle, désolé, c'est une tourelle autonome.

— Détruisez-là.

— On essaye colonelle, mais c'est un des nouveaux modèles Terminator, il est très puissant, on a déjà perdu une cinquantaine de drones kamikazes et deux hommes.

— Allez T42, revenez à la raison, vous êtes avec nous...

— Je suis dans votre camp, c'est un fait, mais pour autant en quoi cela implique-t-il que je ne doive pas vous tirer dessus aussi ?

— Parce qu'on va perdre cette putain de bataille à cause de toi, connasse !

— En quoi est-ce une mauvaise chose ? Il est parait-il des terres brûlées donnant plus de blé qu'un meilleur avril.

— Elle cite du Brel en plein champ de bataille, en défonçant nos propres troupes, et elle trouve ça normal. Organisez la retraite.

— Quel motif je donne au général ?

— Dis-lui qu'on a un bug.

#commonsensefailure #3000 #alice&bob
